from airtest.core.api import *
from airtest.core.cv import Template, loop_find, try_log_screen
from airtest.cli.parser import cli_setup
from airtest.core.helper import G, logwrap
# from touchEvent import *

import cv2
import requests
import datetime

from airtest.core.cv import Template, loop_find, try_log_screen
from airtest.cli.parser import cli_setup
from airtest.core.helper import G, logwrap

# import os
import requests
import random
import string

def logMsg(msg):
    now = datetime.datetime.now()
    print("{:02d}:{:02d}:{:02d}  {}".format(now.hour, now.minute, now.second, msg))


def randomText():
    length = random.randint(10, 15)
    chars = "".join([random.choice(string.ascii_letters[:26]) for i in range(length)])
    return chars


def notify(msg):
    header = {
        'Authorization': 'Bearer ' + 'DTYmJXA6Bs1PX3WoHsANRXKQJ2Y1jMxi8LlY6epDb5h',

    }
    postData = {
        'message': msg
    }
    r = requests.post(url="https://notify-api.line.me/api/notify", headers=header, data=postData)
    return r.status_code


def notifyImg(msg, photoPath):
    header = {
        'Authorization': 'Bearer ' + 'DTYmJXA6Bs1PX3WoHsANRXKQJ2Y1jMxi8LlY6epDb5h',

    }
    postData = {
        'message': msg
    }
    files = {
        'imageFile': photoPath
    }
    r = requests.post(url="https://notify-api.line.me/api/notify", headers=header, data=postData, files=files)
    return r.status_code


def notify(msg):
    header = {
        'Authorization': 'Bearer ' + 'DTYmJXA6Bs1PX3WoHsANRXKQJ2Y1jMxi8LlY6epDb5h',

    }
    postData = {
        'message': msg
    }
    r = requests.post(url="https://notify-api.line.me/api/notify", headers=header, data=postData)
    return r.status_code


def notifyImg(msg, screen=None):
    if screen is None:
        G.DEVICE.snapshot(filename="tempScreen.png")
    else:
        saveScreen(screen, "tempScreen.png")
    header = {
        'Authorization': 'Bearer ' + 'DTYmJXA6Bs1PX3WoHsANRXKQJ2Y1jMxi8LlY6epDb5h',

    }
    postData = {
        'message': msg
    }
    files = {
        'imageFile': open("tempScreen.png", 'rb')
    }
    r = requests.post(url="https://notify-api.line.me/api/notify", headers=header, data=postData, files=files)
    #     os.remove("tempScreen.png")
    return r.status_code


def saveScreen(screen, filename):
    if screen is not None:
        cv2.imencode('.jpg', screen)[1].tofile(filename)
        logMsg("screen saved: {}".format(filename))


def getCurrentDateTime():
    datetime_dt = datetime.datetime.today()
    datetime_str = datetime_dt.strftime("%Y%m%d%H%M%S")
    return datetime_str
