import numpy as np
import cv2

class BoxItem:
    def __init__(self, label, box):
        self.labtl = label
        self.box = np.asarray(box)

    def __str__(self):
        return str(self.labtl) + " " + str(self.getCenterPoint())

    def getCenterPoint(self):
        return [self.box[0][0] + (self.box[0][2] - self.box[0][0]) / 2, self.box[0][1] + (self.box[0][3] - self.box[0][1]) / 2]

    def getLabel(self):
        return self.labtl

    def getBox(self):
        return self.box

class ScreenFindItemForHSV:

    def __init__(self):
        self.im = None

    def find(self, screen, infoList):
        boxItems = []
        labels = []
        imgBlur = self.contrast_img(screen, 1.3, 3)
        self.im = cv2.fastNlMeansDenoisingColored(screen, None, 10, 10, 7, 21)
        hsv_img = cv2.cvtColor(self.im, cv2.COLOR_BGR2HSV)  # HSV image

        # All
        for info in infoList:
            print(info)
            imgGrey = cv2.inRange(hsv_img, np.array(info["hsvMin"], np.uint8), np.array(info["hsvMax"], np.uint8))
            ret, thresh = cv2.threshold(imgGrey, 127, 255, 0)
            info["img"], contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            for cnt in contours:
                x, y, w, h = cv2.boundingRect(cnt)
                if w > info["width"] and h > info["height"]:
                    print("{} x: {}, y: {}, w: {}, h: {}".format(info["name"], x, y, w, h))
                    cv2.rectangle(self.im, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    cv2.putText(self.im, info["name"], (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255), 5,
                                cv2.LINE_AA)
                    cv2.putText(self.im, info["name"], (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2, cv2.LINE_AA)
                    pos = [[x, y, x + w, y + h]]
                    boxItem = BoxItem(info["name"], pos)
                    boxItems.append(boxItem)
                    labels.append(info["name"])
            # cv2.imshow("info", info["img"])
            # cv2.waitKey(0)

        return boxItems, labels

    def contrast_img(self, img1, c, b):  # 亮度就是每个像素所有通道都加上b
        rows, cols, channels = img1.shape

        # 新建全零(黑色)图片数组:np.zeros(img1.shape, dtype=uint8)
        blank = np.zeros([rows, cols, channels], img1.dtype)
        dst = cv2.addWeighted(img1, c, blank, 1-c, b)
        return dst

    def orbInfoFormat(self, name, hsvMin, hsvMax, width, height):
        return {"name": name,
                "img": None,
                "hsvMin": hsvMin,
                "hsvMax": hsvMax,
                "width": width,
                "height": height
                }