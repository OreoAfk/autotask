import numpy as np
import cv2
import win32gui
import win32con
import win32ui
import re
from time import sleep
import time
from pynput import keyboard
import math
import platform
import requests
from win32api import GetSystemMetrics
from PIL import Image
import datetime
import pyautogui
from datetime import date, timedelta, datetime, tzinfo
import win32com.client
import os
from ahk import AHK
import random
import netifaces
import urllib.request

def retryMethod(method, num_retries=3):
    for attempt_no in range(num_retries):
        try:
            # print("is check retry img")
            result = method()
            printSleep(1)
            return result
        except Exception as error:
            if attempt_no < (num_retries - 1):
                # notify("Error: Invalid number")
                pass
            else:
                raise error


def windowChangeIp():

    response = 0

    while response != 1:

        tempIp = ".".join(map(str, (random.randint(0, 255)
                                for _ in range(4))))

        response = os.system("ping -n 1 " + tempIp)
        # and then check the response...
        if response != 0:
            gateways = netifaces.gateways()
            default_gateway = gateways['default'][netifaces.AF_INET][0]
            os.system('netsh int ip set address name="區域連線" source=static addr={} mask=255.255.255.0 gateway={} gwmetric=1'.format(tempIp, default_gateway))
            os.system('netsh int ip set dns "區域連線" static {} primary'.format(default_gateway))

            print('netsh int ip set address name="區域連線" source=static addr={} mask=255.255.255.0 gateway={} gwmetric=1'.format(tempIp, default_gateway))
            print('netsh int ip set dns "區域連線" static {} primary'.format(default_gateway))
            printSleep(7, "ip change {}".format(tempIp))

            if checkNetworkIsConnected() is False:

                os.system('netsh interface ip set address "區域連線" source=dhcp')
                printSleep(7, "dhcp net work")
            # pingstatus = "Network Error"
            # 'netsh interface ip set dns "區域連線" static 192.168.0.200'
            # 'netsh interface ip set dns "區域連線" dhcp'
            # 'netsh interface ip set address "區域連線" dhcp'
            # 'netsh interface ip set address "區域連線" dhcp'
            # os.system('netsh interface ip set address "區域連線" static 192.168.1.2 255.255.255.0 192.168.1.254 1')
            # os.system('netsh interface ip set dnsservers "區域連線" static 8.8.8.8')
            # os.system('netsh interface ip add dnsservers "區域連線" 168.95.1.1')
            # os.system('netsh interface ip set winsservers "區域連線" static 192.168.1.1')
            #
            # 'netsh int ip set address "區域連線" static 192.168.0.101 255.255.255.0 192.168.0.254 1'
            # 'netsh int ip set dns "區域連線" static 192.168.0.254 primary'
            #
            # 'netsh int ip set address name="區域連線" source=static addr=192.168.0.101 mask=255.255.255.0 gateway=none'
        # os.system("ipconfig /release")
        # os.system("net stop dhcp")
        # os.system("net start dhcp")
        # os.system("ipconfig /renew")
        # os.system('netsh interface ipv4 set address "區域連線" dhcp')
        # os.system('netsh interface ipv4 set dnsservers "區域連線" dhcp')
        # os.system('netsh interface ipv4 set winsservers "區域連線" dhcp')


def getWindow_W_H(hwnd):
    print("move top {}".format(hwnd))
    # 取得目標視窗的大小
    if hwnd == 0:
        w, h = pyautogui.size()
        return (0, 0, w, h)
    left, top, right, bot = win32gui.GetWindowRect(hwnd)
    width = right - left - 15
    height = bot - top - 11
    return (left, top, width, height)

def checkNetworkIsConnected():
    try:
        urllib.request.urlopen('http://google.com')
        return True
    except:
        return False

def getWindow_Img(hwnd):
    # 將 hwnd 換成 WindowLong
    hw = 0
    s = win32gui.GetWindowLong(hw, win32con.GWL_EXSTYLE)
    win32gui.SetWindowLong(hw, win32con.GWL_EXSTYLE, s | win32con.WS_EX_LAYERED)
    # 判斷視窗是否最小化
    show = win32gui.IsIconic(hw)
    # 將視窗圖層屬性改變成透明
    # 還原視窗並拉到最前方
    # 取消最大小化動畫
    # 取得視窗寬高
    if show == 1:
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 0)
        win32gui.SetLayeredWindowAttributes(hw, 0, 0, win32con.LWA_ALPHA)
        win32gui.ShowWindow(hw, win32con.SW_RESTORE)
        # x, y, width, height = getWindow_W_H(hw)
        # 創造輸出圖層
    hwindc = win32gui.GetWindowDC(hw)
    srcdc = win32ui.CreateDCFromHandle(hwindc)
    memdc = srcdc.CreateCompatibleDC()
    bmp = win32ui.CreateBitmap()
    # 取得視窗寬高
    x, y, width, height = getWindow_W_H(hwnd)
    # 如果視窗最小化，則移到Z軸最下方
    if show == 1: win32gui.SetWindowPos(hw, win32con.HWND_BOTTOM, x, y, width, height, win32con.SWP_NOACTIVATE)
    # 複製目標圖層，貼上到 bmp
    bmp.CreateCompatibleBitmap(srcdc, width, height)
    memdc.SelectObject(bmp)
    memdc.BitBlt((0, 0), (width, height), srcdc, (8, 3), win32con.SRCCOPY)
    # 將 bitmap 轉換成 np
    signedIntsArray = bmp.GetBitmapBits(True)
    img = np.fromstring(signedIntsArray, dtype='uint8')
    img.shape = (height, width, 4)  # png，具有透明度的
    # 釋放device content
    srcdc.DeleteDC()
    memdc.DeleteDC()
    win32gui.ReleaseDC(hw, hwindc)
    win32gui.DeleteObject(bmp.GetHandle())
    # 還原目標屬性
    if show == 1:
        win32gui.SetLayeredWindowAttributes(hw, 0, 255, win32con.LWA_ALPHA)
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 1)
    # 回傳圖片
    return img

def findImgOnPhoto(image, file_name, description=None, samePercentage=0.01, showPreScreen=False):
    if image is None:
        return None
    frame = image
    frame2 = frame[:, :, :3]
    template = cv2.imread(file_name)
    result = cv2.matchTemplate(frame2, template, cv2.TM_SQDIFF_NORMED)
    c, w, h = template.shape[::-1]
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    top_left = min_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)  # 右下角的位置
    center_point = (top_left[0] + int(w / 2), top_left[1] + int(h / 2))
    print(" sm:{}".format(min_val))
    print("-{}  x:{}, y:{}, h:{}, w:{}".format(description, center_point[0], center_point[1], h, w))
    if min_val > samePercentage:
        if description is not None:
            print("-{} not find, {}".format(description, min_val))
        return None
    if showPreScreen:
        cv2.rectangle(frame, top_left, bottom_right, (0, 255, 255), 2)
        cv2.imshow("img_template", frame)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    return center_point


def findImg(file_name, description=None, hw=None, samePercentage=0.01, showPreScreen=False):
    if hw is None:
        hw = 0
    frame = retryMethod(lambda: getWindow_Img(hw))
    frame2 = frame[:, :, :3]
    template = cv2.imread(file_name)
    result = cv2.matchTemplate(frame2, template, cv2.TM_SQDIFF_NORMED)
    c, w, h = template.shape[::-1]
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)

    top_left = min_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)  # 右下角的位置
    center_point = (top_left[0] + int(w / 2), top_left[1] + int(h / 2))
    print(f"-find img {description}  x:{center_point[0]}, y:{center_point[1]}, h:{h}, w:{w}, sm:{min_val}")
    if min_val > samePercentage:
        if description is not None:
            print("-{} not find, {}".format(description, min_val))
        return None
    if showPreScreen:
        cv2.rectangle(frame, top_left, bottom_right, (0, 255, 255), 2)
        cv2.imshow("img_template", frame)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    return center_point


# def findImgUntil():
#     pass

def findUntil(img, description="", delay=0, maxCount=0, failMethod=None):
    if delay != 0:
        printSleep(delay)
    loopCnt = 0
    if maxCount == 0:
        while True:
            loopCnt += 1
            print("find until: {}.{}".format(loopCnt, description))
            pos = findImg(img, description)
            if findImg(img):
                return pos
            else:
                # yield False
                if failMethod is not None:
                    failMethod()
                printSleep(delay)
    else:
        for i in range(0, maxCount):
            loopCnt += 1
            print("find until: {}.{}".format(loopCnt, description))
            pos = findImg(img, description)
            if pos is not None:
                return pos
            else:
                printSleep(delay)
        return None


def delayTap(ahk, x, y, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    sleep(0.5)
    ahk.click()
    if description is not None:
        print("tap x:{}, y:{}, {}".format(x, y, description))
    else:
        print("tap x:{}, y:{}".format(x, y))


def get_hwnd_and_title_list():
    """return a list of window handlers based on it process id"""

    def callback(h, m):
        if win32gui.IsWindow(h) and win32gui.IsWindowEnabled(h) and win32gui.IsWindowVisible(h):
            hwnd_title.update({h: win32gui.GetWindowText(h)})

    hwnd_title = dict()
    win32gui.EnumWindows(callback, hwnd_title)
    return hwnd_title


def find_hwnd_by_name(name, is_full_same=True):
    hwnd_map = get_hwnd_and_title_list()
    hwnds = []
    if is_full_same:
        for h, t in hwnd_map.items():
            if name == t or isinstance(t, list) and name in t:
                hwnds.append(h)
    else:
        for h, t in hwnd_map.items():
            if t is not None and t.count(name) > 0:
                hwnds.append(h)
    return hwnds


def set_window_to_top(hwnd, x=None, y=None, width=None, height=None, if_foreground=False):
    # https: // stackoverflow.com / questions / 30200381 / python - win32gui - setasforegroundwindow - function - not -working - properly
    try:
        if if_foreground:
            shell = win32com.client.Dispatch("WScript.Shell")
            shell.SendKeys('%')
            win32gui.SetForegroundWindow(hwnd)
        win32gui.ShowWindow(hwnd, win32con.SW_SHOWNORMAL)
        if x is not None and y is not None and width is not None and height is not None:
            win32gui.MoveWindow(hwnd, x, y, width, height, True)
            print("move: {}".format(hwnd))
    except Exception as error:
        print("move screen error".format(error))


def notify(msg):
    hostName = platform.node()
    header = {
        'Authorization': 'Bearer ' + 'DTYmJXA6Bs1PX3WoHsANRXKQJ2Y1jMxi8LlY6epDb5h',
    }
    postData = {
        'message': hostName + " " + msg
    }
    r = requests.post(url="https://notify-api.line.me/api/notify", headers=header, data=postData)
    return r.status_code


def notifyImg(msg, screen=None):
    hostName = platform.node()

    if screen is not None:
        im = Image.fromarray(screen, 'RGBA')
        im.save("tempScreen.png")

    header = {
        'Authorization': 'Bearer ' + 'DTYmJXA6Bs1PX3WoHsANRXKQJ2Y1jMxi8LlY6epDb5h',

    }
    postData = {
        'message': hostName + " " + msg
    }
    files = {
        'imageFile': open("tempScreen.png", 'rb')
    }
    r = requests.post(url="https://notify-api.line.me/api/notify", headers=header, data=postData, files=files)
    return r.status_code

def printSleep(delay, description=""):
    current_time = math.modf(delay)
    loopCount = current_time[1]
    decimalDelay = current_time[0]

    for i in range(0, int(loopCount)):
        print("sleep: {} {}".format((delay - i), description))
        sleep(1)

    if decimalDelay > 0:
        print("sleep: {} {}".format(decimalDelay, description))
        sleep(decimalDelay)

def delayKeyDown(ahk, str, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.key_press(str)
    if description is not None:
        print("key down: {}, {}".format(str, description))
    else:
        print("key down: {}".format(str))


def getCurrentDateTime(dateFormat="%Y%m%d%H%M%S"):
    datetime_dt = datetime.today()
    datetime_str = datetime_dt.strftime(dateFormat)
    return datetime_str


def is_time_between(begin_time, end_time):
    now = datetime.now()
    if end_time > begin_time:
        print("not same date")
        new_date_begin_time = datetime.strptime(begin_time, "%H:%M")
        # todo
        # begin_time = new_date_begin_time.hour
    current_time = now.strftime("%H:%M")
    return current_time >= begin_time and current_time <= end_time

def screenSave(screen=None, path=".", fileName="screenCap.png"):
    tempScreen = screen
    if tempScreen is None:
        tempScreen = getWindow_Img(0)
    tempIm = Image.fromarray(tempScreen)

    tempIm.save("{}/{}".format(path, fileName), "png")


def deleteFiles(path=".", rFilter='\d.*\.mp4$', oldDate=1):
    files = []
    for _file in os.listdir(path):
        if re.search(rFilter, _file):
            files.append(_file)
    for filename in files:
        if (datetime.now() - datetime.fromtimestamp(os.stat(filename).st_mtime)).days > oldDate:
            os.remove(filename)
            print("remove {}".format(_file))
        else:
            print("{} not delete".format(filename))
