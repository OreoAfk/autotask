import cv2
import numpy as np
import time
from tkinter import filedialog
import tkinter as tk
from PIL import ImageTk, Image

# imgRedGrey = cv2.inRange(hsv_img, np.array([0, 90, 150], np.uint8), np.array([22, 255, 255], np.uint8))
#     ret, thresh = cv2.threshold(imgRedGrey, 127, 255, 0)
#     tempImg, redContours, redHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
noxImgPath = "C:/Users/hugho/Nox_share/Screenshots"

class App(tk.Frame):
    def __init__(self, master):
        self.imgHeight = 500
        self.imgWidth = 500
        # https://www.itread01.com/content/1545830346.html
        tk.Frame.__init__(self, master)
        self.changeCanvas = tk.Canvas(master, width=self.imgWidth, height=self.imgHeight)
        self.orgCanvas = tk.Canvas(master, width=self.imgWidth, height=self.imgHeight)
        btnCanvas = tk.Canvas(master, width=200)
        btnCanvas.grid(row=0, column=0)
        self.changeCanvas.grid(row=0, column=1)
        self.orgCanvas.grid(row=0, column=2)
        img = Image.open("D:/py/小圓/screencap.png")
        self.orgImg = self.resizeImg(img)
        self.orgPhotoImg = ImageTk.PhotoImage(self.orgImg)
        self.currentOrgImg = ImageTk.PhotoImage(self.orgImg)
        self.changeImgCanvas = self.changeCanvas.create_image(20, 20, anchor=tk.NW, image=self.currentOrgImg)
        self.orgImgCanvas = self.orgCanvas.create_image(20, 20, anchor=tk.NW, image=self.currentOrgImg)
        self.initBtn(btnCanvas)
        self.lowerRed = tk.Scale(master, length=300, from_=0, to=255,orient='horizontal', command=self.updateImgState)
        self.lowerRed.grid(row=1, column=1)
        self.lowerGreen = tk.Scale(master, length=300, from_=0, to=255, orient='horizontal', command=self.updateImgState)
        self.lowerGreen.grid(row=2, column=1)
        self.lowerBlue = tk.Scale(master, length=300, from_=0, to=255, orient='horizontal', command=self.updateImgState)
        self.lowerBlue.grid(row=3, column=1)

        self.upperRed = tk.Scale(master, length=300, from_=0, to=255, orient='horizontal', command=self.updateImgState)
        self.upperRed.set(100)
        self.upperRed.grid(row=1, column=2)
        self.upperGreen = tk.Scale(master, length=300, from_=0, to=255, orient='horizontal', command=self.updateImgState)
        self.upperGreen.set(100)
        self.upperGreen.grid(row=2, column=2)
        self.upperBlue = tk.Scale(master, length=300, from_=0, to=255, orient='horizontal', command=self.updateImgState)
        self.upperBlue.set(100)
        self.upperBlue.grid(row=3, column=2)
        self.updateImgState(0)

    def selectImg(self):
        filename = filedialog.askopenfilename(initialdir=noxImgPath, title="Select file", filetypes=(("img files", "*.jpg *.png"), ("all files", "*.*")))
        print(filename)
        img = Image.open(filename)
        self.orgImg = self.resizeImg(img)
        self.currentOrgImg = ImageTk.PhotoImage(self.orgImg)
        self.orgPhotoImg = ImageTk.PhotoImage(self.orgImg)
        self.changeCanvas.itemconfig(self.changeImgCanvas, image=self.currentOrgImg)
        self.orgCanvas.itemconfig(self.orgImgCanvas, image=self.currentOrgImg)
        self.updateImgState(0)

    def resizeImg(self, img):
        width, height = img.size
        if width > height:
            img = img.resize((self.imgWidth, int(self.imgWidth * height / width)), Image.ANTIALIAS)
            return img
        else:
            img = img.resize((int(self.imgHeight * width / height), self.imgHeight), Image.ANTIALIAS)
            return img

    def initBtn(self, window):
        selectBtn = tk.Button(window, text='Select Img', width=15, height=2, command=self.selectImg)
        selectBtn.grid(row=0, column=0, sticky="w")
        filterBtn = tk.Button(window, text='Filter', width=15, height=2, command=self.filterItem)
        filterBtn.grid(row=1, column=0, sticky="w")

    def updateImgState(self, val):
        # print(self.lowerRed.get())
        hsv_img = cv2.cvtColor(np.asarray(self.orgImg), cv2.COLOR_RGB2HSV)
        imgGrey = cv2.inRange(hsv_img, np.array([self.lowerRed.get(), self.lowerGreen.get(), self.lowerBlue.get()], np.uint8), np.array([self.upperRed.get(), self.upperGreen.get(), self.upperBlue.get()], np.uint8))
        # print(type(imgGrey))
        self.greyImg = imgGrey
        self.currentOrgImg = ImageTk.PhotoImage(Image.fromarray(imgGrey))
        self.changeCanvas.itemconfig(self.changeImgCanvas, image=self.currentOrgImg)
        self.orgCanvas.itemconfig(self.orgImgCanvas, image=self.orgPhotoImg)

    def filterItem(self):
        ret, thresh = cv2.threshold(self.greyImg, 127, 255, 0)
        tempImg, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # Image.fromarray(tempImg).show()
        # self.orgImg.show()
        im = np.asarray(self.orgImg)
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
        Image.fromarray(im).show()
        #red - 61, 21, 167, 119, 174, 255
        #blue - 220, 1, 0, 255, 255, 214
        # green - 0, 154, 0, 174, 255, 160
        #lignt-28, 221, 241, 194, 255, 255
        #black - 150, 0, 101, 255, 144, 210


if __name__ == '__main__':
    root = tk.Tk()
    app = App(root)
    app.mainloop()