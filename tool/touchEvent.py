class MotionEvent(object):
    """
    Motion Event to be performed by Minitouch
    """
    def getcmd(self, transform=None):
        raise NotImplementedError

class DownEvent(MotionEvent):
    def __init__(self, coordinates, contact=0, pressure=50):
        """
        Finger Down Event
        :param coordinates: finger down coordinates in (x, y)
        :param contact: multi-touch action, starts from 0
        :param pressure: touch pressure
        """
        super(DownEvent, self).__init__()
        self.coordinates = coordinates
        self.contact = contact
        self.pressure = pressure

    def getcmd(self, transform=None):
        if transform:
            x, y = transform(*self.coordinates)
        else:
            x, y = self.coordinates
        cmd = "d {:.0f} {:.0f} {:.0f} {:.0f}\nc\n".format(self.contact, x, y, self.pressure)
        return cmd


class UpEvent(MotionEvent):
    def __init__(self, contact=0):
        """
        Finger Up Event
        :param contact: multi-touch action, starts from 0
        """
        super(UpEvent, self).__init__()
        self.contact = contact

    def getcmd(self, transform=None):
        cmd = "u {:.0f}\nc\n".format(self.contact)
        return cmd


class MoveEvent(MotionEvent):
    def __init__(self, coordinates, contact=0, pressure=50):
        """
        Finger Move Event
        :param coordinates: finger move to coordinates in (x, y)
        :param contact: multi-touch action, starts from 0
        :param pressure: touch pressure
        """
        super(MoveEvent, self).__init__()
        self.coordinates = coordinates
        self.contact = contact
        self.pressure = pressure

    def getcmd(self, transform=None):
        if transform:
            x, y = transform(*self.coordinates)
        else:
            x, y = self.coordinates
        cmd = "m {:.0f} {:.0f} {:.0f} {:.0f}\nc\n".format(self.contact, x, y, self.pressure)
        return cmd


class SleepEvent(MotionEvent):
    def __init__(self, seconds):
        self.seconds = seconds

    def getcmd(self, transform=None):
        return None