import numpy as np
import cv2
import win32gui
import win32con
import win32ui

import re
from time import sleep
import threading
from PIL import Image, ImageDraw, ImageFont
import textwrap
from datetime import datetime

def FindWindow_bySearch(pattern):
    window_list = []
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
    for each in window_list:
        if re.search(pattern, win32gui.GetWindowText(each)) is not None:
            return each


def getWindow_W_H(hwnd):
    # 取得目標視窗的大小
    left, top, right, bot = win32gui.GetWindowRect(hwnd)
    width = right - left - 15
    height = bot - top - 11
    return (left, top, width, height)


def getWindow_Img(hwnd):
    # 將 hwnd 換成 WindowLong
    s = win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE)
    win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, s | win32con.WS_EX_LAYERED)
    # 判斷視窗是否最小化
    show = win32gui.IsIconic(hwnd)
    # 將視窗圖層屬性改變成透明
    # 還原視窗並拉到最前方
    # 取消最大小化動畫
    # 取得視窗寬高
    if show == 1:
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 0)
        win32gui.SetLayeredWindowAttributes(hwnd, 0, 0, win32con.LWA_ALPHA)
        win32gui.ShowWindow(hwnd, win32con.SW_RESTORE)
        x, y, width, height = getWindow_W_H(hwnd)
        # 創造輸出圖層
    hwindc = win32gui.GetWindowDC(hwnd)
    srcdc = win32ui.CreateDCFromHandle(hwindc)
    memdc = srcdc.CreateCompatibleDC()
    bmp = win32ui.CreateBitmap()
    # 取得視窗寬高
    x, y, width, height = getWindow_W_H(hwnd)
    # 如果視窗最小化，則移到Z軸最下方
    if show == 1: win32gui.SetWindowPos(hwnd, win32con.HWND_BOTTOM, x, y, width, height, win32con.SWP_NOACTIVATE)
    # 複製目標圖層，貼上到 bmp
    bmp.CreateCompatibleBitmap(srcdc, width, height)
    memdc.SelectObject(bmp)
    memdc.BitBlt((0, 0), (width, height), srcdc, (8, 3), win32con.SRCCOPY)
    # 將 bitmap 轉換成 np
    signedIntsArray = bmp.GetBitmapBits(True)
    img = np.fromstring(signedIntsArray, dtype='uint8')
    img.shape = (height, width, 4)  # png，具有透明度的
    # 釋放device content
    srcdc.DeleteDC()
    memdc.DeleteDC()
    win32gui.ReleaseDC(hwnd, hwindc)
    win32gui.DeleteObject(bmp.GetHandle())
    # 還原目標屬性
    if show == 1:
        win32gui.SetLayeredWindowAttributes(hwnd, 0, 255, win32con.LWA_ALPHA)
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 1)
    # 回傳圖片
    return img


def screenCapToVideo(screenName = None):
    hwnd = 0
    if screenName is not None:
        hwnd = FindWindow_bySearch(screenName)
    t = threading.currentThread()
    while getattr(t, "do_run", True):
        sleep(0.03)
        frame = getWindow_Img(hwnd)
        cv2.imshow("screen box", frame)
        k = cv2.waitKey(30) & 0xFF  # 64bits! need a mask
        if k == 27:
            cv2.destroyAllWindows()
            break


if __name__ == '__main__':
    hwnd = 0
    # hwnd = FindWindow_bySearch("弓箭,崩3,pad,艦B")
    # hwnd = 330828
    cnt = 1
    maxCount = 500
    x, y, width, height = getWindow_W_H(hwnd)
    isVertical = True
    if width > height:
        isVertical = False

    width = int(width / 2)
    height = int(height / 2)

    logWidth = width
    logHeight = 100

    videoWidth = width
    videoHeight = height + logHeight

    if isVertical:
        logWidth = 100
        logHeight = height
        videoWidth = width + logWidth
        videoHeight = height

    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
    vout = cv2.VideoWriter('output.mp4', 0x00000021, 20, (videoWidth, videoHeight))
    logList = []

    videoLogMode = 0
    if isVertical:
        videoLogMode = 1

    position = (5, 10)
    textSize, _ = cv2.getTextSize("logText", cv2.FONT_HERSHEY_SIMPLEX, 0.2, 1)
    lineHeight = textSize[1] + 5

    # print("{}, {}, {}".format())
    print("start time: {}")


    while True:
        logImg = np.zeros((logHeight, logWidth, 4), np.uint8)
        logImg.fill(255)
        # logImg = Image.new('RGB', (logWidth, logHeight), (0,0,0,0))

        if cnt % 100 == 0:
            logList.append("xxx {}".format(cnt))
            print(datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3])
            print(logList)
        logCnt = len(logList)
        logText = ""
        # if logCnt >= 3:
        #     logText = "{}\n{}\n{}".format( logList[logCnt - 3],  logList[logCnt - 2],  logList[logCnt - 1])
        # elif logCnt >= 2:
        #     logText = "{}\n{}".format(logList[logCnt - 2], logList[logCnt - 1])
        # elif logCnt >= 1:
        #     logText = logList[logCnt - 1]
        logText = "\n".join(logList[-10:])

        x, y0 = position
        for i, line in enumerate(logText.split('\n')):
            y = y0 + i * lineHeight
            cv2.putText(logImg, line, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.2, (0, 0, 0), 1, cv2.LINE_AA)

        frame = getWindow_Img(hwnd)
        cnt += 1
        pic = cv2.resize(frame, (width, height))
        pic = np.concatenate([pic, logImg], videoLogMode)
        # pic = cv2.GaussianBlur(frame, (15, 15), 0)
        vout.write(pic)

        cv2.imshow("screen box", pic)
        k = cv2.waitKey(30) & 0xFF  # 64bits! need a mask
        if k == ord("q"):
            cv2.destroyAllWindows()
            break

    vout.release()
    print("saved")
    # https://blog.csdn.net/u010420283/article/details/89706794