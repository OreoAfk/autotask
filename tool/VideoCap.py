import numpy as np
import cv2
import win32gui
import win32con
import win32ui
import pyautogui

import re
from time import sleep
import threading
from PIL import Image, ImageDraw, ImageFont
import textwrap
from datetime import datetime
import signal
import time

class VideoCap:
    def __init__(self, screenName=None):
        signal.signal(signal.SIGINT, self.signal_handler)
        self.hwnd = 0
        self.width = None
        self.height = None
        self.left = None
        self.top = None
        self.scale = 1
        self.recording = False
        self.fps = 15
        self.autoDelete = True
        if screenName is not None:
            self.hwnd = self.FindWindow_bySearch(screenName)


    def signal_handler(self, sig, frame):
        pass

    def getWindow_Img(self, hwnd):
        # 將 hwnd 換成 WindowLong
        s = win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE)
        win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, s | win32con.WS_EX_LAYERED)
        # 判斷視窗是否最小化
        show = win32gui.IsIconic(hwnd)
        # 將視窗圖層屬性改變成透明
        # 還原視窗並拉到最前方
        # 取消最大小化動畫
        # 取得視窗寬高
        if show == 1:
            win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 0)
            win32gui.SetLayeredWindowAttributes(hwnd, 0, 0, win32con.LWA_ALPHA)
            win32gui.ShowWindow(hwnd, win32con.SW_RESTORE)
            # x, y, width, height = self.getWindow_W_H(hwnd)
            # 創造輸出圖層
        hwindc = win32gui.GetWindowDC(hwnd)
        srcdc = win32ui.CreateDCFromHandle(hwindc)
        memdc = srcdc.CreateCompatibleDC()
        bmp = win32ui.CreateBitmap()
        # 取得視窗寬高
        x, y, width, height = self.getWindow_W_H(hwnd)
        # 如果視窗最小化，則移到Z軸最下方
        if show == 1: win32gui.SetWindowPos(hwnd, win32con.HWND_BOTTOM, x, y, width, height, win32con.SWP_NOACTIVATE)
        # 複製目標圖層，貼上到 bmp
        bmp.CreateCompatibleBitmap(srcdc, width, height)
        memdc.SelectObject(bmp)
        memdc.BitBlt((0, 0), (width, height), srcdc, (8, 3), win32con.SRCCOPY)
        # 將 bitmap 轉換成 np
        signedIntsArray = bmp.GetBitmapBits(True)
        img = np.fromstring(signedIntsArray, dtype='uint8')
        img.shape = (height, width, 4)  # png，具有透明度的
        # 釋放device content
        srcdc.DeleteDC()
        memdc.DeleteDC()
        win32gui.ReleaseDC(hwnd, hwindc)
        win32gui.DeleteObject(bmp.GetHandle())
        # 還原目標屬性
        if show == 1:
            win32gui.SetLayeredWindowAttributes(hwnd, 0, 255, win32con.LWA_ALPHA)
            win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 1)
        # 回傳圖片
        return img

    def getWindow_W_H(self, hwnd):
        # 取得目標視窗的大小

        if self.left is not None and self.top is not None and self.width is not None and self.height is not None:
            return (self.left, self.top, self.width, self.height)
        else:
            left, top, right, bot = win32gui.GetWindowRect(hwnd)
            width = right - left - 15
            height = bot - top - 11
            return (left, top, width, height)

    def FindWindow_bySearch(self, pattern):
        window_list = []
        win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
        for each in window_list:
            if re.search(pattern, win32gui.GetWindowText(each)) is not None:
                return each

    def start(self):
        sleep(1)
        self.recording = True
        left, top, width, height = self.getWindow_W_H(self.hwnd)
        print("w: {}, h:{}".format(width, height))

        today = datetime.today()
        now = today.now()
        vout = cv2.VideoWriter('output{}{}{}{:02d}{:02d}{:02d}.mp4'.format(
            today.year, today.month, today.day, now.hour, now.minute, now.second
        ), 0x00000021, self.fps, (int(width * self.scale), int(height * self.scale)))

        textSize, _ = cv2.getTextSize("logText", cv2.FONT_HERSHEY_SIMPLEX, 0.2, 1)
        # print("start time: {}")

        while self.recording:
            st = time.time()
            frame = self.getWindow_Img(self.hwnd)
            pic = cv2.resize(frame, (int(width * self.scale), int(height * self.scale)))
            point = pyautogui.position()
            cv2.circle(pic, point, 6, (0, 0, 0), 2)
            cv2.circle(pic, point, 5, (0, 255, 255), -1)
            vout.write(pic)
            # cv2.imshow("screen box", pic)
            en = time.time()
            delay = max(0, int((1 / self.fps - (en - st)) * 1000))
            k = cv2.waitKey(delay) & 0xFF  # 64bits! need a mask
            if k == ord("q"):
                cv2.destroyAllWindows()
                break

        vout.release()

        print("saved")



    def stop(self):
        self.recording = False
        sleep(1)

if __name__ == '__main__':
    videoCap = VideoCap()
    videoCap.hwnd = 0
    videoCap.left = 0
    videoCap.top = 0
    videoCap.width = 1920
    videoCap.height = 1080
    videoCap.scale = 1
    t1 = threading.Thread(target=videoCap.start)
    t1.setDaemon(True)
    t1.start()

    sleep(20)
    videoCap.stop()