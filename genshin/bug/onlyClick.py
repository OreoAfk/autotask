from airtest.core.api import *
from airtest.cli.parser import cli_setup
import sys
import datetime
sys.path.append("../..")
from tool.tools import *
from base.dailyQuest import DailyQuest

if not cli_setup():
    auto_setup(__file__, logdir=True, devices=[
        "Android://127.0.0.1:5037/c572159a?cap_method=JAVACAP&&ori_method=ADBORI&&touch_method=ADBTOUCH",
    ])

class QuestClick(DailyQuest):
    def __init__(self):
        pass

    def start(self):
        while True:
            # self.delayLongTap(1800, 900, 0.3, longTapTime=6)
            self.delayLongTap(1800, 900, 1, 3)
            self.printSleep(6)
if __name__ == '__main__':
    q = QuestClick()
    q.start()