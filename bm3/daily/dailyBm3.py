# -*- encoding=utf8 -*-
__author__ = "hugho"

import time
import sys
sys.path.append("../..")
from airtest.core.api import *
from airtest.core.cv import Template, loop_find, try_log_screen
from airtest.cli.parser import cli_setup
from airtest.core.helper import G, logwrap

from tool.tools import *
from base.dailyQuest import DailyQuest
# if not cli_setup():
#     auto_setup(__file__, logdir=True, devices=[
#             "Android://127.0.0.1:5037/127.0.0.1:62001?cap_method=JAVACAP&&ori_method=ADBORI",
#     ])
#
# def openApp():
#     stop_app("com.miHoYo.bh3tw")
#     sleep(1)
#     start_app("com.miHoYo.bh3tw")
#     sleep(1)
#
# def dailyStone():
#     sleep(5)
#     if tapScreenIsMatch(exists_ex(Template(r"tpl1565059754934.png", record_pos=(0.084, 0.233), resolution=(1280, 720))), 2, 742, 677):
#         self.delayTap(641, 532, 3)  # 確定
#
#     if tapScreenIsMatch(exists_ex(Template(r"tpl1565060556284.png", record_pos=(0.084, 0.234), resolution=(1280, 720))), 2, 735, 660):
#         self.delayTap(641, 532, 3)  # 確定
#
# #         self.delayTap(1211, 529, 5)  # 500石
# # def buyBox():
# #     if tapScreenIsMatch(exists_ex(Template(r"tpl1575339018354.png", record_pos=(0.451, 0.237), resolution=(1280, 720))), 2, 1080, 479):
# #         self.delayTap(252, 363, 3)  # 確定
# #         swipeTo(97, 535, 120, 100)
# def dailyStorySweep():
#     if exists_ex(Template(r"tpl1575339018354.png", record_pos=(0.451, 0.237), resolution=(1280, 720))):
#         self.delayTap(780, 37, 2)
#         self.delayTap(791, 525, 2) # 買體力
#         self.delayTap(628, 507, 5) # 確定
#
#         self.delayTap(1043, 667, 2)
#         self.delayTap(870, 513, 8)
#         self.delayTap(824, 481, 2) # 領取金幣
#     self.delayTap(818, 682, 5)
#     self.delayTap(920, 239, 5) # 第一個可能未拿
#     self.delayTap(637, 548, 3) # 確定
#     self.delayTap(920, 239, 2) # 第一個
# #     self.delayTap(78, 531, 2) # 前一個任
#     self.delayTap(525, 531, 1) # 後一個任
#     self.delayTap(780, 644, 1) # 一鍵派遣
#     self.delayTap(1113, 639, 1) # 確定探險
#
#     self.delayTap(920, 370, 2) # 第二個
#     self.delayTap(525, 531, 1) # 後一個任
#     self.delayTap(780, 644, 1) # 一鍵派遣
#     self.delayTap(1113, 639, 1) # 確定探險
#
#     self.delayTap(920, 493, 2) # 第三個
#     self.delayTap(780, 644, 1) # 一鍵派遣
#     self.delayTap(1113, 639, 1) # 確定探險
#
#     self.delayTap(920, 618, 2) # 第四個
#     self.delayTap(780, 644, 1) # 一鍵派遣
#     self.delayTap(1113, 639, 1) # 確定探險
#
#     self.printSleep(2)
#     swipeTo(578, 545, 529, 291, 1)
#
#     self.delayTap(920, 516, 2) # 第五個
#     self.delayTap(780, 644, 1) # 一鍵派遣
#     self.delayTap(1113, 639, 1) # 確定探險
#
#     self.delayTap(920, 633, 2) # 第六個
#     self.delayTap(780, 644, 1) # 一鍵派遣
#     self.delayTap(1113, 639, 1) # 確定探險
#
#     self.printSleep(3)
#     swipeTo(578, 545, 529, 291, 1)
#     self.delayTap(920, 534, 2) # 第七個
#     self.delayTap(780, 644, 1) # 一鍵派遣
#     self.delayTap(1113, 639, 1) # 確定探險
#     self.delayTap(43, 37, 2) # 返回
#
# def dailyWork():
#     self.delayTap(1005, 644, 3) #打工
#     self.delayTap(1240, 355, 5)
#     for i in range(8):
#         self.delayTap(1008, 81, 2)
#         self.delayTap(634, 534, 1)
#
#     for i in range(8):
#         self.delayTap(1008, 81, 2)
#         self.delayTap(850, 662, 1) # 一鍵派遣
#         self.delayTap(1142, 679, 2) # 確定打工
#         self.delayTap(1242, 358, 3)
#
# def dailyQuest():
# #     openApp()
# #     sleep(300)
# #     if exists_ex(Template(r"tpl1564821109297.png", record_pos=(0.404, 0.234), resolution=(1280, 720)), 100, 2):
# #         self.delayTap(650, 350, 2)
# #     canLogin = True
# #     while(canLogin):
# #         canLogin = not exists_ex(Template(r"tpl1564821109297.png", record_pos=(0.404, 0.234), resolution=(1280, 720)), 100, 2)
# #         if canLogin == False:
# #             self.delayTap(650, 350, 2)
# #         sleep(10)
#
# #     sleep(30)
# #     tapScreenIsMatch(exists_ex(Template(r"tpl1575338226864.png", record_pos=(-0.116, -0.213), resolution=(1280, 720))), 2, 1145, 70)
#
# #     dailyStone()
# #     dailyStorySweep()
#     dailyWork()
# #     stop_app("com.miHoYo.bh3tw")

class Bm3Daily(DailyQuest):
    
    def __init__(self):
        pass
    
    def dailyQuest(self):
        self.delayTap(1005, 666, 2, "打工")
        self.delayTap(1242, 351, 3, "<")
        for i in range(8):
            self.delayTap(1038, 90, 2, "完成")
            self.delayTap(637, 530, 2, "確定")
        for i in range(8):
            self.delayTap(1038, 90, 2, "第一個")
            self.delayTap(870, 668, 2, "一鍵派遣")
            self.delayTap(870, 668, 2, "一鍵派遣2")
            self.delayTap(1114, 664, 2, "開始打工")
            self.delayTap(1189, 330, 1, ">")
            self.delayTap(1189, 330, 2, ">")
    
    def dailyExpedition(self):
        pass
        self.delayTap(1028, 678, 2, "家園")
        self.delayTap(365, 236, 10, "拿錢")
        self.delayTap(365, 236, 1, "按空白")
        
        self.delayTap(826, 687, 2, "遠征")
        # self.delayTap(1147, 183, 1, "材料")
        self.printSleep(10)
        self.delayTap(940, 243, 1, "開始遠征 1")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")
        self.delayTap(788, 35, 3, "吃體力")
        self.delayTap(254, 400, 2, "體力兌換")
        self.delayTap(920, 544, 2, "確定")
        self.delayTap(644, 509, 2, "確定")
        
        self.delayTap(940, 362, 2, "開始遠征 2")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")
        
        self.delayTap(940, 496, 2, "開始遠征 3")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")
        
        self.delayTap(940, 624, 2, "開始遠征 4")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")
        self.printSleep(3)
        self.delayTap(1147, 183, 1, "材料")
        # swipeTo(760, 620, 760, 222, 2)
        # self.delayTap(940, 366, 2, "開始遠征 5")
        # self.delayTap(805, 645, 1, "一鍵派遣")
        # self.delayTap(1095, 645, 2, "確定探險")
        #
        # self.delayTap(940, 492, 2, "開始遠征 6")
        # self.delayTap(805, 645, 1, "一鍵派遣")
        # self.delayTap(1095, 645, 2, "確定探險")
        #
        # self.delayTap(940, 613, 2, "開始遠征 7")
        # self.delayTap(805, 645, 1, "一鍵派遣")
        # self.delayTap(1095, 645, 2, "確定探險")

        self.delayTap(940, 243, 1, "開始遠征 1")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")

        self.delayTap(940, 362, 2, "開始遠征 2")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")

        self.delayTap(940, 496, 2, "開始遠征 3")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")

        self.delayTap(940, 624, 2, "開始遠征 4")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")

        self.printSleep(3)
        self.swipeTo(760, 620, 760, 222, 2)
        self.delayTap(940, 366, 2, "開始遠征 5")
        self.delayTap(805, 645, 1, "一鍵派遣")
        self.delayTap(1095, 645, 2, "確定探險")

        # self.delayTap(940, 358, 2, "開始遠征 8")
        # self.delayTap(805, 645, 1, "一鍵派遣")
        # self.delayTap(1095, 645, 2, "確定探險")
        
        self.delayTap(41, 54, 2, "返回")
        

    def buyItem(self):
        self.delayTap(1069, 466, 2, "補給")
        self.delayTap(227, 544, 2, "商店")
        self.printSleep(5)
        self.swipeTo(105, 517, 105, 200)
        self.delayTap(110, 383, 2, "秘銀商店")
        self.delayTap(384, 415, 2, "時空封印箱")
        for i in range(4):
            self.delayTap(1076, 676, 1, "買買買!")
            self.delayTap(1076, 676, 0.5)
        self.delayTap(90, 539, 2, "連線軍備")
        self.delayTap(384, 415, 2, "時空封印箱")
        for i in range(3):
            self.delayTap(1076, 676, 1, "買買買!")
            self.delayTap(1076, 676, 0.5)

    def connectDevice(self):
        if not cli_setup():
            auto_setup(__file__, logdir=True, devices=[
                "Android://127.0.0.1:5037/127.0.0.1:62001?cap_method=JAVACAP&&ori_method=ADBORI",
            ])
    
    def startQuest(self):
        self.connectDevice()
        self.dailyExpedition()
        self.dailyQuest()
        self.delayTap(41, 54, 12, "返回")
        self.delayTap(41, 54, 3, "返回")
        self.delayTap(1089, 459, 5, "補給")
        self.delayTap(269, 549, 5, "前往商店")
        self.printSleep(10)
        # self.buyItem()

if __name__ == '__main__':
   dailyQuest = Bm3Daily()
   dailyQuest.startQuest()

# Template(r"tpl1565237811945.png", record_pos=(-0.005, -0.041), resolution=(1280, 720)) 772 417
# Template(r"tpl1565237843024.png", record_pos=(0.24, 0.128), resolution=(1280, 720))
# Template(r"tpl1565237856211.png", record_pos=(-0.43, -0.127), resolution=(1280, 720))
# Template(r"tpl1565237866087.png", record_pos=(0.001, -0.141), resolution=(1280, 720))    # generate html report
# Template(r"tpl1565237876124.png", record_pos=(-0.432, -0.068), resolution=(1280, 720))    # from airtest.report.report import simple_report
#     # simple_report(__file__, logpath=True)

