import requests
import json
import time
import hmac
import hashlib

apiKey=''
apiSecretKey=''
apiKey_test=''
apiSecretKey_test=''

baseUrl='https://api.binance.com'
baseUrl_test='https://testnet.binance.vision'

coin_list=['BTC','LTC','BNB']

def createTimeStamp():
    return int(time.time()*1000)


def param2string(param):
    """本函数用于根据param（一个参数字典）生成后缀字符串"""
    s = ''
    for k in param.keys():
        s += k
        s += '='
        s += str(param[k])
        s += '&'
    return s[:-1]


def hashing(query_string):
    """本函数用于根据后缀生成签名"""
    # 代码参考来源：https://github.com/binance-exchange/binance-signature-examples/blob/master/python/signature.py
    return hmac.new(apiSecretKey_test.encode('utf-8'), query_string.encode('utf-8'), hashlib.sha256).hexdigest()


if __name__ == '__main__':
    print(createTimeStamp())