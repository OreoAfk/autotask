from airtest.core.helper import G
from airtest.core.api import *
from airtest.core.android.minitouch import *

import math
# import time
from tool.tools import *

class DailyQuest(object):

    def __init__(self):
        pass

    def startQuest(self):
        pass

    def defauleScreenSize(self):
        return 0, 0

    def getCapScreen(self, name=None):
        screen = G.DEVICE.snapshot(filename=name)
        return screen

    def loop_find_ex(self, query, timeout=ST.FIND_TIMEOUT, threshold=None, interval=0.5, intervalfunc=None):
        start_time = time.time()
        while True:
            screen = G.DEVICE.snapshot(filename=None)

            if screen is not None:
                if threshold:
                    query.threshold = threshold
                match_pos = query.match_in(screen)
                if match_pos:
                    return match_pos

            if intervalfunc is not None:
                intervalfunc()

            # 超时则raise，未超时则进行下次循环:
            if (time.time() - start_time) > timeout:
                raise TargetNotFoundError('Picture %s not found in screen' % query)
            else:
                time.sleep(interval)

    def exists_ex(self, v, timeout=ST.FIND_TIMEOUT_TMP, delay=0.5):
        try:
            pos = self.loop_find_ex(v, timeout, None, delay)
        except TargetNotFoundError:
            return False
        else:
            return pos

    # def delayTap(x, y, second=0, longTapTime=1):
    #     sleep(second)
    #     for _ in range(longTapTime):
    #         G.DEVICE.touch((x, y))
    #         sleep(0.05)
    #     delay_after_operation()

    def delayTap(self, x, y, second=0, actionName=None, isTap=True, longTapTime=1):
        if isTap:
            self.printSleep(second)
        if actionName is not None:
            logMsg("x: {}, y: {}, t: {}".format(x, y, actionName))
        else:
            logMsg("x: {}, y: {}".format(x, y))
        for _ in range(longTapTime):
            if isTap:
                G.DEVICE.touch((x, y))
                sleep(0.05)
        if isTap:
            delay_after_operation()

    def delayLongTap(self, x, y, second=0, actionName=None, longTapTime=1):
        self.printSleep(second)
        G.DEVICE.shell("input touchscreen swipe {} {} {} {}".format(x, y, x, y, longTapTime * 1000))

        logMsg("tap: {} {} {}".format(x, y, actionName))

    def sendMsg(self, msg):
        logMsg("msg: {}".format(msg))

    def screenMatch(self, screen, query):
        if screen is None:
            logMsg("screen none")
            return False
        else:
            match_pos = query.match_in(screen)
            #         print(screen)
            if match_pos:
                print(query)
                logMsg("screen match : " + str(match_pos))
                return match_pos
            return False

    def tapScreenIsMatch(self, position, delay=0, x=-1, y=-1):
        if position:
            if x >= 0 or y >= 0:
                self.delayTap(x, y, delay)
            else:
                self.delayTap(position[0], position[1], delay)
            return True
        return False

    def loop_find_ex(self, query, timeout=ST.FIND_TIMEOUT, threshold=None, interval=0.5, intervalfunc=None):
        start_time = time.time()
        while True:
            screen = G.DEVICE.snapshot(filename=None)

            if screen is not None:
                if threshold:
                    query.threshold = threshold
                match_pos = query.match_in(screen)
                if match_pos:
                    return match_pos

            if intervalfunc is not None:
                intervalfunc()

            # 超时则raise，未超时则进行下次循环:
            if (time.time() - start_time) >= timeout:
                raise TargetNotFoundError('Picture %s not found in screen' % query)
            else:
                time.sleep(interval)

    def exists_ex(self, v, timeout=ST.FIND_TIMEOUT_TMP, description="", delay=0.5):
        try:
            pos = self.loop_find_ex(v, timeout, None, delay)
        except TargetNotFoundError:
            return False
        else:
            if description is not "":
                logMsg(description)
            return pos



    def screenMatch(self, screen, query, description=""):
        if screen is None:
            logMsg("screen none")
            return False
        else:
            match_pos = query.match_in(screen)
            #         print(screen)
            if match_pos:
                print(query)
                if description is not "":
                    logMsg(description)
                logMsg("screen match : " + str(match_pos))
                return match_pos
            return False

    def tapScreenIsMatch(self, position, delay=0, x=-1, y=-1):
        if position:
            if x >= 0 or y >= 0:
                self.delayTap(x, y, delay)
            else:
                self.delayTap(position[0], position[1], delay)
            return True
        return False

    def printSleep(self, delay):
        currentTime = math.modf(delay)
        loopCount = currentTime[1]
        decimalDelay = currentTime[0]

        for i in range(0, int(loopCount)):
            logMsg("sleep: " + str(delay - i))
            sleep(1)

        if decimalDelay > 0:
            logMsg("sleep: " + str(decimalDelay))
            sleep(decimalDelay)

    def findUntil(self, img, delay, maxCount=0, failMethod=None):
        self.printSleep(delay)
        if maxCount == 0:
            while (True):
                if self.exists_ex(img):
                    return True
                else:
                    # yield False
                    if failMethod is not None:
                        failMethod()
                    self.printSleep(delay)
        else:
            for i in range(0, maxCount):
                if self.exists_ex(img):
                    return True
                else:
                    self.printSleep(delay)
            return False

    # http://airtest.netease.com/docs/cn/9_faq/3_api_faq.html
    def swipeTo(self, x1, y1, x2, y2, time=3):
        print("from ({},{}) to ({}, {}), time: {}".format(x1, x2, y1, y2, time))
        swipe_event = [DownEvent((x1, y1)), SleepEvent(0.3)]
        pTime = time * 10
        pX = (x2 - x1) / pTime
        pY = (y2 - y1) / pTime
        for i in range(pTime):
            swipe_event.append(MoveEvent((x1 + pX * i, y1 + pY * i)))
            swipe_event.append(SleepEvent(0.1))

        swipe_event.append(UpEvent())
        device().minitouch.perform(swipe_event)

