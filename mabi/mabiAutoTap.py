from ahk import AHK
import time
# from tool.tools import *

def press(key, delay):
    ahk.key_down(key)  # Press down (but do not release) Control key
    time.sleep(delay)
    ahk.key_up(key)  # Release the key

def collectionAnimals():
    loopCnt = 0
    while True:
        pos = ahk.mouse_position
        print(loopCnt)
        if pos[0] < 100 and pos[1] < 100:
            print("end")
            break

        if loopCnt % 20 == 0:
            ahk.key_press("7")
            time.sleep(1)
            ahk.key_press("5")
            time.sleep(6)
            ahk.key_press("Esc")
            time.sleep(1)
            ahk.key_press("9")
            time.sleep(1)
            ahk.key_press("Tab")
            time.sleep(1)

        print("tap : {}".format(pos))
        ahk.click()  # Click the primary mouse button
        time.sleep(2)
        loopCnt += 1

def collectionSong():
    loopCnt = 0
    while True:
        pos = ahk.mouse_position
        print(loopCnt)
        if pos[0] < 100 and pos[1] < 100:
            print("end")
            break

        if loopCnt % 40 == 0:
            ahk.key_press("7")
            time.sleep(1)
            ahk.key_press("5")
            time.sleep(6)
            ahk.key_press("Esc")
            time.sleep(1)
            ahk.key_press("9")
            time.sleep(1)
            # ahk.key_press("Tab")
            # time.sleep(1)

        print("tap : {}".format(pos))
        ahk.click()  # Click the primary mouse button
        time.sleep(2)
        loopCnt += 1


def collectionWood():
    press("Left", 0.5)
    loopCnt = 0
    while True:
        ahk.click()
        time.sleep(3)
        loopCnt += 1

def collectionNail():
    loopCnt = 0
    while True:
        pos = ahk.mouse_position
        print(loopCnt)
        if pos[0] < 100 and pos[1] < 100:
            print("end")
            break
        if loopCnt % 5 == 0:
            ahk.key_press("2")
            time.sleep(2)
        print("tap : {}".format(pos))
        ahk.click()  # Click the primary mouse button
        time.sleep(0.7)
        loopCnt += 1

def findAttackMonster():
    loopCnt = 0
    while True:
        pos = ahk.mouse_position
        print(loopCnt)
        if pos[0] < 100 and pos[1] < 100:
            print("end")
            break
        ahk.key_press("2")
        # ahk.key_down('Control')
        # print("tap : {}".format(pos))
        # ahk.click()  # Click the primary mouse button
        # ahk.key_up('Control')
        time.sleep(0.5)
        loopCnt += 1

if __name__ == '__main__':
    ahk = AHK()
    print("1: {}, 2: {} 3: {} 4: {} 5:{}".format("寵物採集", "木頭採集", "單純點擊", "豐年採集", "升段"))
    mode = input()
    print("key in")
    time.sleep(6)
    if mode == "1":
        collectionAnimals()
    elif mode == "2":
        collectionWood()
    elif mode == "3":
        collectionNail()
    elif mode == "4":
        collectionSong()
    elif mode == "5" or mode == "":
        findAttackMonster()


    # time.sleep(3)
    # ahk.key_down('Up')  # Press down (but do not release) Control key
    # time.sleep(3)
    # ahk.key_up('Up')  # Release the key
