import sys
sys.path.append("../..")

import win32api
from tool.wintools import *
from tool import wintools
from mabi.promotion.sheetEdit import ExtManager
import numpy as np
import math
import os

acc = []
pwd = []
bf_hwnd = None
mabi_hwnd = []
screenWidth = 1920
screenHeight = 1080

gameName = "新瑪奇 mabinogi"


def getWindow_W_H(hwnd):
    return (0, 0, screenWidth, screenHeight)

def init_account():
    with open("./account.txt", 'r') as fp:
        line = fp.readline()
        while line:
            arr = line.rstrip('\n').split(",")
            if len(arr) == 2:
                acc.append(arr[0])
                pwd.append(arr[1])
                print(arr[0], arr[1])
            line = fp.readline()

def FindWindow_bySearch(pattern):
    window_list = []
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
    for each in window_list:
        if re.search(pattern, win32gui.GetWindowText(each)) is not None:
            return each


def delayTap(x, y, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_move(x=x, y=y, blocking=True)
    ahk.click()
    if description is not None:
        print("tap x:{}, y:{}, {}".format(x, y, description))
    else:
        print("tap x:{}, y:{}".format(x, y))

def login(ac, pw):
    print("login: {}".format(ac))
    tryLogin = True
    stepCnt = 0
    loopCnt = 0

    while tryLogin:
        if loopCnt > 5:
            raise Exception("loop on login: step {}".format(stepCnt))
        print("step: {}, loop: {}".format(stepCnt, loopCnt))

        openIE()
        loopCnt += 1
        printSleep(5)
        if stepCnt is 0:
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                delayTap(pos[0], pos[1], 1, " 右下登入")
                printSleep(5)
                stepCnt = 1
                loopCnt = 0
            else:
                pos = findImg("../img/26.PNG", "登出", samePercentage=0.1)
                if pos is not None:
                    delayTap(pos[0], pos[1], 1, "登出")
                    printSleep(5)
                    pos = findImg("../img/27.PNG", "確定", samePercentage=0.12)
                    if pos is not None:
                        delayTap(pos[0], pos[1], 1, "確定")
                        printSleep(5)
                continue

        if stepCnt is 1:
            pos = findImg("../img/23.PNG", "登入(綠)", samePercentage=0.05)
            if pos is not None:
                delayTap(pos[0] - 150, pos[1] - 20, 1, " 帳號或電郵")
                ahk.send_input(ac)
                delayTap(pos[0] - 150, pos[1] + 10, 1, "密碼")
                ahk.send_input(pw)
                delayTap(628, 228, 1, "Login")
                printSleep(15)
                stepCnt = 2
                loopCnt = 0
            else:
                stepCnt = 0
                continue

        if stepCnt is 2:
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                stepCnt = 0
                continue
            printSleep(2)

            pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.11)
            if pos is not None:
                delayTap(pos[0], pos[1], 1, "快速啟動")
                delayTap(pos[0], pos[1] - 100, 5, "帳1")
                printSleep(10)
                closeIE()
                focusStartMabiWindow()
                printSleep(3)
                stepCnt = 3
                loopCnt = 0
            else:
                continue

        if stepCnt is 3:
            if loopCnt > 2:
                pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.08)
                if pos is not None:
                    delayTap(pos[0], pos[1], 1, "快速啟動")
                    delayTap(pos[0], pos[1] - 100, 5, "帳1")
                    printSleep(10)
                    closeIE()
                    focusStartMabiWindow()
                    printSleep(3)
                    stepCnt = 3
                    loopCnt = 0
                else:
                    continue

            closeIE()
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                stepCnt = 0
                continue
            focusStartMabiWindow()
            closeIE()
            printSleep(2)
            pos = findImg("../img/28.PNG", "開始遊戲", samePercentage=0.15)
            if pos is not None:
                delayTap(pos[0], pos[1], 1, "開始遊戲")
                printSleep(3)
                moveCMDWindow()
                stepCnt = 4
                loopCnt = 0
            else:
                continue

        if stepCnt is 4:
            pos = findImg("../img/29.PNG", "同意")
            if pos is not None:
                delayTap(pos[0], pos[1], 1, "同意")
                stepCnt = 5
                loopCnt = 0
            else:
                continue

        # if stepCnt is 5:
        #     pos = findImg("../img/30.PNG", "開始")
        #     if pos is not None:
        #         delayTap(pos[0], pos[1], 2, "進入愛爾琳")
        #         printSleep(20)
        #         stepCnt = 6
        #     else:
        #         continue

        tryLogin = False
        stepCnt = 0

def logout():
    global bf_hwnd
    openIE()
    if bf_hwnd is None:
        openIE()
    else:
        # win32gui.SetForegroundWindow(bf_hwnd)
        win32gui.ShowWindow(bf_hwnd, win32con.SW_SHOWNORMAL)
    printSleep(5)
    pos = findImg("../img/26.PNG", "登出", samePercentage=0.1)
    if pos is not None:
        delayTap(pos[0], pos[1], 1, "登出")
        printSleep(5)
        pos = findImg("../img/27.PNG", "確定", samePercentage=0.12)
        if pos is not None:
            delayTap(pos[0], pos[1], 1, "確定")
            printSleep(5)
    bf_hwnd = None


def closeIE():
    print("closeIE")
    global bf_hwnd
    window_list = []
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
    for each in window_list:
        window_name = win32gui.GetWindowText(each)
        if window_name is not None and window_name.count("Internet Explorer") > 0:
            win32gui.SendMessage(each, win32con.WM_CLOSE, 0, 0)
    bf_hwnd = None

def openIE():
    print("openIE")
    global bf_hwnd
    closeIE()
    os.startfile("C:\Program Files (x86)\Internet Explorer\iexplore.exe")
    printSleep(20)
    bf_hwnd = FindWindow_bySearch("beanfun! - Internet Explorer")
    win32gui.ShowWindow(bf_hwnd, win32con.SW_SHOWNORMAL)
    print("bf: {}".format(bf_hwnd))

def resizeWindow(hwnd):
    print("{} resize {}, {}".format(hwnd, 1000, 800))
    win32gui.ShowWindow(hwnd, win32con.SW_SHOWNORMAL)
    printSleep(5)
    win32gui.MoveWindow(hwnd, 0, 0, 1000, 800, True)

def closeGame(hwnd):
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    printSleep(2)
    pos = findImg("../img/31.PNG", "關瑪奇")
    if pos is not None:
        delayTap(pos[0], pos[1], 2, "確定")

def signal_handler(sig):
    notify("error")
    sys.exit(0)

def delayKeyDown(str, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.key_press(str)
    if description is not None:
        print("key down: {}".format(description))

def select_charter():
    pos = findImg("../img/1.PNG", "選角")
    if pos is not None:
        delayTap(700, 300, 1, "角色1")
        delayTap(50, 750, 3, "開始")
        printSleep(30)

    delayTap(120, 770, 1, "menu")
    delayKeyDown("i", 1, "背包")
    delayKeyDown("i", 1, "背包")
    printSleep(2)
    pos = findImg("../img/15.PNG")
    if pos is not None:
        delayKeyDown("i", 1, "背包")
        printSleep(2)
    pos = findImg("../img/3.PNG", "1000等")
    if pos is not None:
        delayTap(500, 400, 1)
        printSleep(3)
        pos = findImg("../img/4.PNG", "智能系統")
        if pos is not None:
            delayTap(870, 190, 1, "關閉")
    printSleep(3)
    pos = findImg("../img/4.PNG", "智能系統")
    if pos is not None:
        delayTap(870, 190, 1, "關閉")
        printSleep(3)
        pos = findImg("../img/3.PNG", "1000等")
        if pos is not None:
            delayTap(500, 400, 1)
    printSleep(2)

def resizeMabiWindow(hwnd):
    win32gui.ShowWindow(hwnd, win32con.SW_SHOWNORMAL)
    printSleep(2)
    win32gui.MoveWindow(hwnd, 0, 0, 1000, 800, True)
    delayTap(500, 10, 0, "點擊瑪奇")
    printSleep(2)

def initMabiHwnd():
    # win32gui.EnumWindows(get_all_hwnd, 0)
    print("name: {}".format(gameName))
    for h, t in get_hwnd_and_title_list().items():
        if t == gameName:
            mabi_hwnd.append(h)

def focusStartMabiWindow():

    mabi_h = find_hwnd_by_name("mabinogi")
    if mabi_h is not None:
        set_window_to_top(mabi_h[0])
    else:
        print("not find ")

def moveCMDWindow():
    print("moveCMDWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and "cmd" in t:
            # win32gui.SetForegroundWindow(h)
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            win32gui.MoveWindow(h, 1000, 0, 500, 800, True)
            print("move the cmd")
            break

if __name__ == '__main__':

    focusStartMabiWindow()
    input("a")

    print("is use control: ")
    isHolding = True
    if input("is holding?") is not None:
        isHolding = False
    try:
        ahk = AHK()
        win32api.SetConsoleCtrlHandler(signal_handler, True)

        screenWidth = GetSystemMetrics(0)
        screenHeight = GetSystemMetrics(1)
        init_account()
        moveCMDWindow()

        preAcc = np.array_split(acc, math.ceil(len(acc) / 5))
        wintools.getWindow_W_H = getWindow_W_H
        for acs in preAcc:

            for i in range(len(acs)):

                login(acc[i], pwd[i])
                moveCMDWindow()
                printSleep(10)
                for h in mabi_hwnd:
                    print("resize mabi")
                    win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
                    win32gui.MoveWindow(h, 0, 0, 1000, 800, True)
                logout()

            initMabiHwnd()
            for h in mabi_hwnd:
                win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
                printSleep(2)
                win32gui.MoveWindow(h, 0, 0, 1000, 800, True)
                delayTap(500, 10, 1, "點擊瑪奇")
                printSleep(2)
                select_charter()

            notify("{} is open".format(acs))

            if isHolding:
                printSleep(60 * 40) #40 min
            else:
                input("waiting until key in any")
            for h in mabi_hwnd:
                # check is get item
                closeGame(h)

            mabi_hwnd = []

        notify("end login")
    except Exception as error:
        # exc_type, exc_obj, exc_tb = sys.exc_info()
        # fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        # print(exc_type, fname, exc_tb.tb_lineno)
        print(error)
        # notify("error: {}".format(error))


