import sys
sys.path.append("../..")

import win32api
from tool.wintools import *
from tool import wintools
from mabi.promotion.sheetEdit import ExtManager
import numpy as np
import math
import os
from base import *

acc = []
pwd = []
bf_hwnd = None
mabi_hwnd = []
screenWidth = 1920
screenHeight = 1080

gameName = "新瑪奇 mabinogi"

def delayTap(x, y, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    sleep(0.5)
    ahk.click()
    if description is not None:
        print("tap x:{}, y:{}, {}".format(x, y, description))
    else:
        print("tap x:{}, y:{}".format(x, y))

def delayKeyDown(str, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.key_press(str)
    if description is not None:
        print("key down: {}".format(description))

def select_charter():
    global charterCount
    pos = findImg("../img/1.PNG", "選角", samePercentage=0.15)
    if pos is not None:
        if charterCount > 9:
            delayTap(970, 700, 1)
            delayTap(700, 700, 2, "角色{}".format(charterCount))
        else:
            delayTap(700, 250 + charterCount * 50, 1, "角色{}".format(charterCount))
        delayTap(50, 750, 3, "開始")
        printSleep(10)

    delayTap(120, 770, 1, "menu")
    delayKeyDown("i", 1, "背包")
    delayKeyDown("i", 1, "背包")
    printSleep(2)
    pos = findImg("../img/15.PNG")
    if pos is not None:
        delayKeyDown("i", 1, "背包")
        printSleep(2)
    pos = findImg("../img/3.PNG", "1000等")
    if pos is not None:
        delayTap(500, 400, 1)
        printSleep(3)
        pos = findImg("../img/4.PNG", "智能系統")
        if pos is not None:
            delayTap(870, 190, 1, "關閉")
    printSleep(3)
    pos = findImg("../img/4.PNG", "智能系統")
    if pos is not None:
        delayTap(870, 190, 1, "關閉")
        printSleep(3)
        pos = findImg("../img/3.PNG", "1000等")
        if pos is not None:
            delayTap(500, 400, 1)
    printSleep(2)


if __name__ == '__main__':
    ahk = AHK()
    a = [133248, 133526, 133388, 7473058]
    b = find_hwnd_by_name("新瑪奇 mabinogi")
    newMabi = [elem for elem in b if elem not in a]
    set_window_to_top(newMabi[0], 0, 0, 1000, 800, if_foreground=True)

    for i in range(1, 10, 1):
        charterCount = i
        supplementMoneyPos = None
        patWalletPos = None
        select_charter()
        delayTap(650, 500, 2, "close")
        delayTap(550, 500, 0.5,"close")
        printSleep(1)
        delayTap(120, 770, 1, "menu")
        delayTap(120, 680, 1, "登出")
        delayTap(550, 480, 1, "確定")
        printSleep(5)
    # [elem for elem in b if elem not in a]
    # print(find_hwnd_by_name("新瑪奇 mabinogi"))

