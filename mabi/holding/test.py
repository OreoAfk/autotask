import sys
sys.path.append("../..")
import numpy as np
import win32api
from tool.wintools import *
from tool import wintools
import platform

def func_a(func_a_arg_a, func, **kwargs):
    print(func_a_arg_a)
    func(**kwargs)

def func_b(arg_a):
    print(arg_a)

def func_c():
    print('Hello World')

if __name__ == '__main__':
    func_a(func_a_arg_a='temp', arg_a='Hello Python', func=func_b)
    func_a(func_a_arg_a='temp', func=func_c)

    arr = np.array(["1", "2", "3", "4", "5", "6"])

    newarr = np.array_split(arr, math.ceil(len(arr) / 5))

    for prearr in newarr:
        print(prearr)
        for s in prearr:
            print(s)
    print(newarr)

