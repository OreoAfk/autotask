import sys
sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from mabi.promotion.orignal_promotion import MabiPromotion
import socket

acc = []
pwd = []
mabi_hwnd = []
screenWidth = 1920
screenHeight = 1080
pcName = platform.node()

def getWindow_W_H():
    return (0, 0, screenWidth, screenHeight)

def init_account():
    with open("./account.txt", 'r') as fp:
        line = fp.readline()
        while line:
            arr = line.rstrip('\n').split(",")
            if len(arr) == 2:
                acc.append(arr[0])
                pwd.append(arr[1])
                print(arr[0], arr[1])
            line = fp.readline()

# def FindWindow_bySearch(pattern):
#     window_list = []
#     win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
#     for each in window_list:
#         if re.search(pattern, win32gui.GetWindowText(each)) is not None:
#             return each

def focusStartMabiWindow():
    print("focusStartMabiWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t == "mabinogi":
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            print("find the mabi window")
            break

def moveCMDWindow():
    print("moveCMDWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("cmd" in t or "taskeng" in t): #
            # win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            # win32gui.MoveWindow(h, 1000, 0, 500, 600, True)
            set_window_to_top(h, 1000, 0, 500, 600, if_foreground=True)
            print("move the cmd")
            break

def moveIEWindow():
    print("moveIEWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("Explorer" in t): #
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            win32gui.SetForegroundWindow(h)
            win32gui.MoveWindow(h, 0, 0, 1000, 600, True)
            print("move the ie")
            break
        print(" {} {}".format(h, t))

def moveChromeWindow():
    print("moveChromeWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("Chrome" in t):
            set_window_to_top(h, 0, 0, 1000, 600)
            print("move the Chrome")
            ie_hwnd = h
            break
        print(" {} {}".format(h, t))


def openChrome(daily=5):
    os.startfile("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe")
    printSleep(daily)
    moveChromeWindow()


def closeChrome():
    for i in find_hwnd_by_name("訊息", False):
        win32gui.SendMessage(i, win32con.WM_CLOSE, 0, 0)
    for i in find_hwnd_by_name("錯誤", False):
        win32gui.SendMessage(i, win32con.WM_CLOSE, 0, 0)

    ie_hwnd_list = find_hwnd_by_name("Internet Explorer", False)
    for h in ie_hwnd_list:
        win32gui.SendMessage(h, win32con.WM_CLOSE, 0, 0)
    chrome_hwnd_list = find_hwnd_by_name("Chrome", False)
    for h in chrome_hwnd_list:
        win32gui.SendMessage(h, win32con.WM_CLOSE, 0, 0)

def openMabi():
    delayTap(ahk, 112, 562, 2, "快速啟動")
    delayTap(ahk, 109, 464, 5, "瑪奇")
    printSleep(20)
    pos = findImg("../img/28.PNG", "開始遊戲", samePercentage=0.15)
    if pos is not None:
        delayTap(ahk, pos[0], pos[1], 1, "開始遊戲")
    printSleep(8)
    pos = findImg("../img/29.PNG", "同意")
    if pos is not None:
        delayTap(ahk, pos[0], pos[1], 1, "同意")

def login(ac, pw):
    closeChrome()
    printSleep(3)
    openChrome()
    printSleep(5)
    while True:
        pos = findImg("../img/92.PNG", description="Login")
        if pos is not None:
            delayTap(ahk, 927, 560, 1, " 右下登入")
            printSleep(10)
            pos = findImg("../img/93.PNG", description="Login")
            if pos is not None:
                delayTap(ahk, pos[0] - 40, pos[1] - 100, 8, " 帳號或電郵")
                ahk.send_input(ac)
                delayTap(ahk, pos[0] - 40, pos[1] - 60, 2, "密碼")
                ahk.send_input(pw)
                delayTap(ahk, pos[0], pos[1], 2, "Login")
            # delayTap(ahk, 746, 446, 2, "Login")
            openMabi()
            printSleep(60)
            break
        else:
            closeChrome()
            printSleep(3)
            openChrome()
            printSleep(5)


def logout():
    pass



def closeIE():
    window_list = []
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
    for each in window_list:
        window_name = win32gui.GetWindowText(each)
        if window_name is not None and window_name.count("Internet Explorer") > 0:
            win32gui.SendMessage(each, win32con.WM_CLOSE, 0, 0)

def openIE(isFirstInit=False):
    pass

    print("openIE")
    # bf_hwnd = FindWindow_bySearch("beanfun! - Internet Explorer")
    # bf_hwnd = FindWindow_bySearch("beanfun! - Windows Internet Explorer")

    # resizeWindow(bf_hwnd)

# def closeGame(hwnd):
#     win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
#     printSleep(2)
#     pos = findImg("../img/31.PNG", "關瑪奇")
#     if pos is not None:
#         delayTap(ahk, pos[0], pos[1], 2, "確定")

def getCurrentTime():
    return getCurrentDateTime("%H:%M:%S")

def signal_handler(sig):
    # notify("error")
    extManager.addHestory([getCurrentTime(), pcName, "error"])
    sys.exit(0)

extManager = ExtManager()


def saveStatus(accPos, charterPos):
    tempstatus = getCurrentStatus()
    tempstatus["accPos"] = accPos
    tempstatus["charterPos"] = charterPos


def getCurrentStatus():
    return {
        "accPos": 1,
        "charterPos": 2
    }

if __name__ == '__main__':

    accIndex = 1
    charterIndex = 2
    if len(sys.argv) > 2:
        accIndex = int(sys.argv[1])
        charterIndex = int(sys.argv[2])

    print("a: {}, c{}".format(accIndex, charterIndex))
    try:
        ahk = AHK()
        win32api.SetConsoleCtrlHandler(signal_handler, True)

        screenWidth = GetSystemMetrics(0)
        screenHeight = GetSystemMetrics(1)
        init_account()
        moveCMDWindow()
        for i in range(accIndex - 1, len(acc)):
            errorRestartCharter = 2
            # wintools.getWindow_W_H = getWindow_W_H
            login(acc[i], pwd[i])
            if i == 0:
                printSleep(40)
            closeIE()
            mabiPromotion = MabiPromotion(charterIndex)
            mabiPromotion.account = acc[i]
            # printSleep(60)
            pos = findImg("../img/43.PNG", "斷線", samePercentage=0.1)
            if pos is not None:
                extManager.addHestory(
                    [getCurrentTime(), pcName, "error: 斷線(關機)"])
                mabiPromotion.closeGame()
                printSleep(10)
                os.system('shutdown -s')
                input()


            # check is open mabi charter screen else relogin
            # notify("{} start: {}".format(i+1, acc[i]))
            extManager.addHestory([getCurrentTime(), pcName, "{} start: {}".format(i+1, acc[i])])

            while mabiPromotion.taskStart() is False:
                printSleep(5)
                errorRestartCharter = mabiPromotion.charterCount
                if errorRestartCharter < 10 and errorRestartCharter is not 1:
                    errorRestartCharter = errorRestartCharter + 1

                pos = findImg("../img/43.PNG", "斷線", samePercentage=0.06)
                if pos is not None:
                    extManager.addHestory(
                        [getCurrentTime(), pcName, "error: 斷線(關機2)"])
                    mabiPromotion.closeGame()
                    printSleep(30)
                    os.system('shutdown -s')
                    input()

                login(acc[i], pwd[i])
                # printSleep(60)
                # notify("{} restart: {}".format(i+1, acc[i]))
                extManager.addHestory([getCurrentTime(), pcName, "restart: {}, startCharter{}".format(acc[i], errorRestartCharter)])
                mabiPromotion.__init__(errorRestartCharter)
                mabiPromotion.taskStart()
            charterIndex = 2
            # exec(open("./promotion_vm.py","r",encoding="utf-8").read())
            printSleep(30)

            # closeGame()
            logout()
            closeIE()

            if i % 3 == 0:
                windowChangeIp()
            localHostIp = socket.gethostbyname(socket.gethostname())
            extManager.addHestory([getCurrentTime(), pcName, "{} IP: {}".format(i+1, localHostIp)])

            if is_time_between("05:00", "07:00"):
                break
            # if is_time_between("")
        # notify("finish loop holding")
        extManager.addHestory([getCurrentTime(), pcName, "finish loop holding"])
        printSleep(2)
        os.system('shutdown -s')
    except Exception as error:
        errLine = 'Error on line {}'.format(sys.exc_info()[-1].tb_lineno)
        errMsg = "error: line {} - {}(關機)".format(errLine, error)
        # notify(errMsg)
        extManager.addHestory([getCurrentTime(), pcName, errMsg])
        os.system('shutdown -s')
