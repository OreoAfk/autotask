import sys
sys.path.append("../..")
from pynput import keyboard
from threading import Thread
from time import sleep
from tool.wintools import *


def on_press(key, abortKey='esc'):
    global isUseMed
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    if k == "f2":
        isUseMed = True

    print('pressed %s' % (k))
    if k == abortKey:
        print('end loop ...')
        return False  # stop listener


def delayKeyDownClick(x, y, key, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    sleep(0.1)
    ahk.key_down(key)
    sleep(0.5)
    ahk.click()
    sleep(0.2)
    ahk.key_up(key)
    if description is not None:
        print("key tap x:{}, y:{}, key: {} {}".format(x, y, key, description))
    else:
        print("key tap x:{}, y:{}, key: {}".format(x, y, key))


def main_task():
    global isUseMed
    j = 0
    while True:
        if j % 30 == 0 and isUseMed:
            delayKeyDown(ahk, "F2", 0.5)

        delayKeyDown(ahk, "2")
        if j % 4 == 0:
            delayKeyDownClick(1465, 667, "Control", 0, "攻擊 {}".format(j))
        if j % 10 == 0:
            pos = findImg("../img/57.PNG",  samePercentage=0.1)
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 0.5)
                delayTap(ahk, 1391, 840, 0.5)
                delayTap(ahk, 1391, 730, 0.5)
                delayTap(ahk, 1391, 810, 0.5)
                delayTap(ahk, 1433, 814, 0.5)
                delayTap(ahk, 1433, 854, 0.5)
                delayTap(ahk, 1433, 654, 0.5)
                delayTap(ahk, 1433, 654, 0.5)

        printSleep(0.5)
        j += 1


if __name__ == '__main__':
    ahk = AHK()
    isUseMed = True

    abortKey = 't'
    listener = keyboard.Listener(on_press=on_press, abortKey=abortKey)
    listener.start()  # start to listen on a separate thread
    Thread(target=main_task, args=(), name='loop_fun', daemon=True).start()
    listener.join()  # wait for abortKey