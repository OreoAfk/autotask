import gspread
from oauth2client.service_account import ServiceAccountCredentials
from gspread.models import Cell

#
#
#
# sheet =
# sheet.update_cell(12, 1, "a")

class ExtManager:

    def __init__(self, formKey="1bWYp4mlsJmJ1Usba_QXHgmNI26DJ8nIYjqpKodaCK5A", worksheet="history",
                 updateForm = None, auth_json_path = "../json/mabisheet-6395d4ab6a18.json"):
        gss_scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(auth_json_path, gss_scope)
        gss_client = gspread.authorize(credentials)
        spreadsheet_key = formKey
        self.wSheet = worksheet
        self.spread_sheet = gss_client.open_by_key(spreadsheet_key)
        self.history_sheet = self.spread_sheet.worksheet(worksheet)

        self.updateSheet = None
        if updateForm is not None:
            self.updateSheet = self.spread_sheet.worksheet(updateForm)
        else:
            self.updateSheet = self.spread_sheet.worksheet(worksheet)

    def addHestory(self, arr):
        try:
            self.history_sheet.insert_row(arr, 2)
        except Exception as error:
            # self.history_sheet.insert_row(arr, 2)
            print("google error: {}".format(error))

    def update(self, text, row, cal):
        if self.updateSheet is not None:
            print("r:{}, c:{}, t:{}".format(row, cal, text))
            self.updateSheet.update_cell(row, cal, text)

    def updateFormList(self, textList, r, startCol):
        try:
            if self.updateSheet is not None:
                cells = []
                pos = 0
                for c in range(startCol, startCol + len(textList), 1):
                    cells.append(Cell(row=r, col=c, value=textList[pos]))
                    pos += 1
                self.updateSheet.update_cells(cells)
        except:
            print("log error")


    def getRowList(self, row):
        tempRow = []
        tempSheet = self.spread_sheet.worksheet(self.wSheet)
        if tempSheet is not None:
            currentSheet = tempSheet.get(0)
            for i in range(0, (len(currentSheet) - 1)):
                if len(currentSheet[i]) == 0:
                    cell = ""
                else:
                    cell = currentSheet[i][row]
                tempRow.append(cell)
            return tempRow

    def getRowDice(self, rowNum=[0]):
        tempDict = dict()
        if rowNum is not None and len(rowNum) > 0:
            for r in rowNum:
                tempDict[r] = []
            tempDict
            tempSheet = self.spread_sheet.worksheet(self.wSheet)
            if tempSheet is not None:
                currentSheet = tempSheet.get(0)
                for i in range(0, (len(currentSheet) - 1)):
                    for r in rowNum:
                        if len(currentSheet[i]) <= r:
                            cell = ""
                        else:
                            cell = currentSheet[i][r]
                        tempDict[r].append(cell)

        return tempDict

if __name__ == '__main__':
    extManager = ExtManager(updateForm="history")
    # extManager.addHestory(["1", "2"])
    # extManager.updateFormState("oreo", 1, 1) # left top
    list = extManager.getRowList(1)
    print(list)