import sys
sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from mabi.promotion.orignal_promotion import MabiPromotion
import socket

def closeIE():
    window_list = []
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
    for each in window_list:
        window_name = win32gui.GetWindowText(each)
        if window_name is not None and window_name.count("Internet Explorer") > 0:
            win32gui.SendMessage(each, win32con.WM_CLOSE, 0, 0)

def openIE(isFirstInit=False):
    closeIE()
    os.startfile("C:\Program Files (x86)\Internet Explorer\iexplore.exe")
    printSleep(7)
    moveIEWindow()
    isNotOpen = True
    while (isNotOpen):
        if findImg("../img/91.PNG", "chrome", samePercentage=0.12):
            # delayTap(ahk, 760, 118, 1, "關window")
            delayTap(ahk, 760, 84, 1, "關window")
            printSleep(3)
        pos = findImg("../img/42.PNG", "BF 已載入", samePercentage=0.1)
        if pos is not None:
            isNotOpen = False
        else:
            closeIE()
            os.startfile("C:\Program Files (x86)\Internet Explorer\iexplore.exe")
            printSleep(10, "登入waiting")
            moveIEWindow()
            if findImg("../img/91.PNG", "chrome", samePercentage=0.12):
                #delayTap(ahk, 760, 118, 1, "關window")
                delayTap(ahk, 760, 84, 1, "關window")
                printSleep(3)

    print("openIE")
def moveIEWindow():
    print("moveIEWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("Explorer" in t): #
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            win32gui.SetForegroundWindow(h)
            win32gui.MoveWindow(h, 0, 0, 1000, 600, True)
            print("move the ie")
            break
        print(" {} {}".format(h, t))

if __name__ == '__main__':
    ahk = AHK()
    openIE()
    moveIEWindow()