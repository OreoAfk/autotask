import sys
sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from mabi.promotion.orignal_promotion import MabiPromotion
import socket

acc = []
pwd = []
mabi_hwnd = []
screenWidth = 1920
screenHeight = 1080
pcName = platform.node()

def getWindow_W_H():
    return (0, 0, screenWidth, screenHeight)

def init_account():
    with open("./account.txt", 'r') as fp:
        line = fp.readline()
        while line:
            arr = line.rstrip('\n').split(",")
            if len(arr) == 2:
                acc.append(arr[0])
                pwd.append(arr[1])
                print(arr[0], arr[1])
            line = fp.readline()

# def FindWindow_bySearch(pattern):
#     window_list = []
#     win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
#     for each in window_list:
#         if re.search(pattern, win32gui.GetWindowText(each)) is not None:
#             return each

def focusStartMabiWindow():
    print("focusStartMabiWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t == "mabinogi":
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            print("find the mabi window")
            break

def moveCMDWindow():
    print("moveCMDWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("cmd" in t or "taskeng" in t): #
            # win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            # win32gui.MoveWindow(h, 1000, 0, 500, 600, True)
            set_window_to_top(h, 1000, 0, 500, 600, if_foreground=True)
            print("move the cmd")
            break

def moveIEWindow():
    print("moveIEWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("Explorer" in t): #
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            win32gui.SetForegroundWindow(h)
            win32gui.MoveWindow(h, 0, 0, 1000, 600, True)
            print("move the ie")
            break
        print(" {} {}".format(h, t))

def login(ac, pw):
    tryLogin = True
    stepCnt = 0
    loopCnt = 0

    while tryLogin:
        if loopCnt > 5:
            raise Exception("loop on login: step {}".format(stepCnt))
        print("step: {}".format(stepCnt))

        openIE()
        loopCnt += 1
        printSleep(5)
        if stepCnt is 0:
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 1, " 右下登入")
                printSleep(5)
                stepCnt = 1
                loopCnt = 0
            else:
                pos = findImg("../img/26.PNG", "登出", samePercentage=0.1)
                if pos is not None:
                    delayTap(ahk, pos[0], pos[1], 1, "登出")
                    printSleep(5)
                    pos = findImg("../img/27.PNG", "確定", samePercentage=0.12)
                    if pos is not None:
                        delayTap(ahk, pos[0], pos[1], 1, "確定")
                        printSleep(5)
                continue

        if stepCnt is 1:
            pos = findImg("../img/23.PNG", "登入(綠)", samePercentage=0.02)
            if pos is not None:
                delayTap(ahk, pos[0] - 150, pos[1] - 20, 1, " 帳號或電郵")
                ahk.send_input(ac)
                delayTap(ahk, pos[0] - 150, pos[1] + 10, 1, "密碼")
                ahk.send_input(pw)
                delayTap(ahk, 628, 228, 1, "Login")
                delayTap(ahk, 477, 340, 3, "確定")
                delayTap(ahk, 713, 562, 1, "cancel")
                printSleep(15)
                stepCnt = 2
                loopCnt = 0
            else:
                stepCnt = 0
                continue

        if stepCnt is 2:
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                stepCnt = 0
                continue
            printSleep(2)

            pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.08)
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 1, "快速啟動")
                delayTap(ahk, pos[0], pos[1] - 100, 5, "帳1")
                printSleep(10)
                closeIE()
                focusStartMabiWindow()
                printSleep(3)
                stepCnt = 3
                loopCnt = 0
            else:
                continue

        if stepCnt is 3:
            if loopCnt > 2:
                pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.08)
                if pos is not None:
                    delayTap(ahk, pos[0], pos[1], 1, "快速啟動")
                    delayTap(ahk, pos[0], pos[1] - 100, 5, "帳1")
                    printSleep(10)
                    closeIE()
                    focusStartMabiWindow()
                    printSleep(3)
                    stepCnt = 3
                    loopCnt = 0
                else:
                    continue

            closeIE()
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                stepCnt = 0
                continue
            focusStartMabiWindow()
            closeIE()
            printSleep(2)
            pos = findImg("../img/28.PNG", "開始遊戲", samePercentage=0.15)
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 1, "開始遊戲")
                printSleep(6)
                moveCMDWindow()
                stepCnt = 4
                loopCnt = 0
            else:
                continue

        if stepCnt is 4:
            pos = findImg("../img/29.PNG", "同意")
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 1, "同意")
                stepCnt = 5
                loopCnt = 0
            else:
                continue

        # if stepCnt is 5:
        #     pos = findImg("../img/30.PNG", "開始")
        #     if pos is not None:
        #         delayTap(ahk, pos[0], pos[1], 2, "進入愛爾琳")
        #         printSleep(20)
        #         stepCnt = 6
        #     else:
        #         continue

        tryLogin = False
        stepCnt = 0


def logout():
    openIE()
    printSleep(5)
    pos = findImg("../img/26.PNG", "登出", samePercentage=0.1)
    if pos is not None:
        delayTap(ahk, pos[0], pos[1], 1, "登出")
        printSleep(5)
        pos = findImg("../img/27.PNG", "確定", samePercentage=0.12)
        if pos is not None:
            delayTap(ahk, pos[0], pos[1], 1, "確定")
            printSleep(5)


def closeIE():
    window_list = []
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
    for each in window_list:
        window_name = win32gui.GetWindowText(each)
        if window_name is not None and window_name.count("Internet Explorer") > 0:
            win32gui.SendMessage(each, win32con.WM_CLOSE, 0, 0)

def openIE(isFirstInit=False):
    closeIE()
    os.startfile("C:\Program Files (x86)\Internet Explorer\iexplore.exe")
    printSleep(7)
    moveIEWindow()
    isNotOpen = True
    while (isNotOpen):
        pos = findImg("../img/42.PNG", "BF 已載入", samePercentage=0.1)
        if pos is not None:
            isNotOpen = False
        else:
            closeIE()
            os.startfile("C:\Program Files (x86)\Internet Explorer\iexplore.exe")
            printSleep(10, "登入waiting")

    print("openIE")
    # bf_hwnd = FindWindow_bySearch("beanfun! - Internet Explorer")
    # bf_hwnd = FindWindow_bySearch("beanfun! - Windows Internet Explorer")

    # resizeWindow(bf_hwnd)

# def closeGame(hwnd):
#     win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
#     printSleep(2)
#     pos = findImg("../img/31.PNG", "關瑪奇")
#     if pos is not None:
#         delayTap(ahk, pos[0], pos[1], 2, "確定")

def getCurrentTime():
    return getCurrentDateTime("%H:%M:%S")

def signal_handler(sig):
    # notify("error")
    extManager.addHestory([getCurrentTime(), pcName, "error"])
    sys.exit(0)

extManager = ExtManager()


def saveStatus(accPos, charterPos):
    tempstatus = getCurrentStatus()
    tempstatus["accPos"] = accPos
    tempstatus["charterPos"] = charterPos


def getCurrentStatus():
    return {
        "accPos": 1,
        "charterPos": 2
    }

if __name__ == '__main__':

    accIndex = 1
    charterIndex = 2
    if len(sys.argv) > 2:
        accIndex = int(sys.argv[1])
        charterIndex = int(sys.argv[2])

    print("a: {}, c{}".format(accIndex, charterIndex))
    try:
        ahk = AHK()
        win32api.SetConsoleCtrlHandler(signal_handler, True)

        screenWidth = GetSystemMetrics(0)
        screenHeight = GetSystemMetrics(1)
        init_account()
        moveCMDWindow()
        for i in range(accIndex - 1, len(acc)):
            errorRestartCharter = 2
            # wintools.getWindow_W_H = getWindow_W_H
            login(acc[i], pwd[i])
            if i == 0:
                printSleep(40)
            closeIE()
            mabiPromotion = MabiPromotion(charterIndex)
            mabiPromotion.account = acc[i]
            printSleep(60)
            pos = findImg("../img/43.PNG", "斷線", samePercentage=0.1)
            if pos is not None:
                extManager.addHestory(
                    [getCurrentTime(), pcName, "error: 斷線"])
                mabiPromotion.closeGame()
                printSleep(10)
                input()

            # check is open mabi charter screen else relogin
            # notify("{} start: {}".format(i+1, acc[i]))
            extManager.addHestory([getCurrentTime(), pcName, "{} start: {}".format(i+1, acc[i])])

            while mabiPromotion.taskStart() is False:
                printSleep(5)
                errorRestartCharter = mabiPromotion.charterCount
                if errorRestartCharter < 10 and errorRestartCharter is not 1:
                    errorRestartCharter = errorRestartCharter + 1

                pos = findImg("../img/43.PNG", "斷線", samePercentage=0.06)
                if pos is not None:
                    extManager.addHestory(
                        [getCurrentTime(), pcName, "error: 斷線"])
                    mabiPromotion.closeGame()
                    printSleep(10)

                login(acc[i], pwd[i])
                printSleep(60)
                # notify("{} restart: {}".format(i+1, acc[i]))
                extManager.addHestory([getCurrentTime(), pcName, "restart: {}, startCharter{}".format(acc[i], errorRestartCharter)])
                mabiPromotion.__init__(errorRestartCharter)
                mabiPromotion.taskStart()
            charterIndex = 2
            # exec(open("./promotion_vm.py","r",encoding="utf-8").read())
            printSleep(30)

            # closeGame()
            logout()
            closeIE()

            #if i % 3 == 0:
            #    windowChangeIp()
            localHostIp = socket.gethostbyname(socket.gethostname())
            extManager.addHestory([getCurrentTime(), pcName, "{} IP: {}".format(i+1, localHostIp)])

            if is_time_between("05:00", "07:00"):
                break
            # if is_time_between("")
        # notify("finish loop holding")
        extManager.addHestory([getCurrentTime(), pcName, "finish loop holding"])
        printSleep(2)
        os.system('shutdown -s')
    except Exception as error:
        errLine = 'Error on line {}'.format(sys.exc_info()[-1].tb_lineno)
        errMsg = "error: line {} - {}".format(errLine, error)
        # notify(errMsg)
        extManager.addHestory([getCurrentTime(), pcName, errMsg])
