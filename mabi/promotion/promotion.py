import win32api, win32con, win32gui, win32ui, win32service, os, time

from ahk import AHK
import time
from pynput import keyboard
import requests
import datetime
# 升段腳本

from pynput.mouse import Button, Controller
import numpy as np
import cv2
from PIL import Image
import math

gameName = "新瑪奇 mabinogi"
break_program = False

charterCount = 2

hwnd = None
hwnd_main = None
hwnd_sub = None
hwnd_bf = None
hwnd_title = dict()


def on_press(key):
    global break_program
    print(key)
    if key == keyboard.Key.esc:
        print('end pressed')
        break_program = True
        return False

def findAttackMonster():
    loopCnt = 0
    while True:
        pos = ahk.mouse_position
        print(loopCnt)
        if pos[0] < 100 and pos[1] < 100:
            print("end")
            break
        ahk.key_press("2")
        time.sleep(0.5)
        loopCnt += 1

def getWindow_Img(hwnd):
    # 將 hwnd 換成 WindowLong
    s = win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE)
    win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, s | win32con.WS_EX_LAYERED)
    # 判斷視窗是否最小化
    show = win32gui.IsIconic(hwnd)
    # 將視窗圖層屬性改變成透明
    # 還原視窗並拉到最前方
    # 取消最大小化動畫
    # 取得視窗寬高
    if show == 1:
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 0)
        win32gui.SetLayeredWindowAttributes(hwnd, 0, 0, win32con.LWA_ALPHA)
        win32gui.ShowWindow(hwnd, win32con.SW_RESTORE)
        x, y, width, height = getWindow_W_H(hwnd)
        # 創造輸出圖層
    hwindc = win32gui.GetWindowDC(hwnd)
    srcdc = win32ui.CreateDCFromHandle(hwindc)
    memdc = srcdc.CreateCompatibleDC()
    bmp = win32ui.CreateBitmap()
    # 取得視窗寬高
    x, y, width, height = getWindow_W_H(hwnd)
    # 如果視窗最小化，則移到Z軸最下方
    if show == 1: win32gui.SetWindowPos(hwnd, win32con.HWND_BOTTOM, x, y, width, height, win32con.SWP_NOACTIVATE)
    # 複製目標圖層，貼上到 bmp
    bmp.CreateCompatibleBitmap(srcdc, width, height)
    memdc.SelectObject(bmp)
    memdc.BitBlt((0, 0), (width, height), srcdc, (8, 3), win32con.SRCCOPY)
    # 將 bitmap 轉換成 np
    signedIntsArray = bmp.GetBitmapBits(True)
    img = np.fromstring(signedIntsArray, dtype='uint8')
    img.shape = (height, width, 4)  # png，具有透明度的
    # 釋放device content
    srcdc.DeleteDC()
    memdc.DeleteDC()
    win32gui.ReleaseDC(hwnd, hwindc)
    win32gui.DeleteObject(bmp.GetHandle())
    # 還原目標屬性
    if show == 1:
        win32gui.SetLayeredWindowAttributes(hwnd, 0, 255, win32con.LWA_ALPHA)
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 1)
    # 回傳圖片
    return img


def get_all_hwnd(h, mouse):
    if win32gui.IsWindow(h) and win32gui.IsWindowEnabled(h) and win32gui.IsWindowVisible(h):
        hwnd_title.update({h: win32gui.GetWindowText(h)})

def initBFHwnd():
    win32gui.EnumWindows(get_all_hwnd, 0)
    global hwnd_bf
    for h, t in hwnd_title.items():
        if t == "beanfun! - Internet Explorer":
            hwnd_bf = h


def initMainSubHwnd():
    win32gui.EnumWindows(get_all_hwnd, 0)
    global hwnd_main, hwnd_sub, hwnd
    for h, t in hwnd_title.items():
        if t == gameName:
            if hwnd_main is None and findImg("./img/10.PNG", "主帳", h):
                hwnd_main = h
            elif hwnd_sub is None and findImg("./img/9.PNG", "副帳", h):
                hwnd_sub = h
            else:
                hwnd = h

def getWindow_W_H(hwnd):
    # 取得目標視窗的大小
    left, top, right, bot = win32gui.GetWindowRect(hwnd)
    width = right - left - 15
    height = bot - top - 11
    return (left, top, width, height)


def findImg(file_name, description=None, hw=None):
    if hw is None:
        hw = hwnd
    frame = getWindow_Img(hw)
    frame2 = frame[:, :, :3]
    template = cv2.imread(file_name)
    result = cv2.matchTemplate(frame2, template, cv2.TM_SQDIFF_NORMED)
    c, w, h = template.shape[::-1]
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)

    top_left = min_loc
    # bottom_right = (top_left[0] + w, top_left[1] + h)  # 右下角的位置
    # cv2.rectangle(frame, top_left, bottom_right, (0, 255, 255), 2)
    # cv2.imshow("img_template", frame)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    center_point = (top_left[0] + int(w / 2), top_left[1] + int(h / 2))
    print("-{} x:{}, y:{}, h:{}, w:{}".format(description, center_point[0], center_point[1], h, w))
    if min_val > 0.01:
        if description is not None:
            print("-{} not find, {}".format(description, min_val))
        return None
    return center_point


def printSleep(delay):
    current_time = math.modf(delay)
    loopCount = current_time[1]
    decimalDelay = current_time[0]

    for i in range(0, int(loopCount)):
        print("sleep: {}".format(delay - i))
        time.sleep(1)

    if decimalDelay > 0:
        print("sleep: {}".format(decimalDelay))
        time.sleep(decimalDelay)


def delayTap(x, y, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_move(x=x, y=y, blocking=True)
    ahk.click()
    if description is not None:
        print("tap x:{}, y:{}, {}".format(x, y, description))
    else:
        print("tap x:{}, y:{}".format(x, y))


def delayKeyDown(str, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.key_press(str)
    if description is not None:
        print("key down: {}".format(description))


def getPatWalletPos(num):
    pos = findImg("./img/13.PNG")
    if pos is not None:
        # 寵物錢包
        if num == 2:
            return pos[0] + 10, pos[1] + 50
        elif num == 3:
            return pos[0] + 10, pos[1] + 95
        elif num == 4:
            return pos[0] + 10, pos[1] + 145
        elif num == 5:
            return pos[0] - 40, pos[1]
        elif num == 6:
            return pos[0] - 40, pos[1] + 50
        elif num == 7:
            return pos[0] - 40, pos[1] + 95
        elif num == 8:
            return pos[0] - 40, pos[1] + 145
        elif num == 9:
            return pos[0] - 40, pos[1]

    return 0, 0


def notify(msg):
    header = {
        'Authorization': 'Bearer ' + 'DTYmJXA6Bs1PX3WoHsANRXKQJ2Y1jMxi8LlY6epDb5h',
    }
    postData = {
        'message': msg
    }
    r = requests.post(url="https://notify-api.line.me/api/notify", headers=header, data=postData)
    return r.status_code


def notifyImg(msg, screen=None):
    if screen is not None:
        im = Image.fromarray(screen, 'RGBA')
        im.save("tempScreen.png")

    header = {
        'Authorization': 'Bearer ' + 'DTYmJXA6Bs1PX3WoHsANRXKQJ2Y1jMxi8LlY6epDb5h',

    }
    postData = {
        'message': msg
    }
    files = {
        'imageFile': open("tempScreen.png", 'rb')
    }
    r = requests.post(url="https://notify-api.line.me/api/notify", headers=header, data=postData, files=files)
    #     os.remove("tempScreen.png")
    return r.status_code


def getCurrentDateTime():
    datetime_dt = datetime.datetime.today()
    datetime_str = datetime_dt.strftime("%Y%m%d%H%M%S")
    return datetime_str


def promotion_action():
    pos = findImg("./img/15.PNG", "開背包")
    if pos is None:
        delayKeyDown("i", 1, "開背包")

    delayKeyDown("2", 2, "召寵")
    delayKeyDown(".", 2, "打開寵背包")
    printSleep(2)
    x, y = getPatWalletPos(charterCount)
    delayTap(x, y, 2, "寵物背包錢包 {}".format(charterCount))
    pos = findImg("./img/15.PNG")
    delayTap(pos[0] + 170, pos[1] + 235, 2, "自己錢包")
    delayTap(x, y, 2, "寵物背包")
    printSleep(2)
    delayKeyDown("i", 2, "關背包")
    delayKeyDown(".", 1, "關寵物背包")
    ahk.key_down('Control')
    delayTap(100, 190, 0, "升段嚮導")
    ahk.key_up('Control')
    delayKeyDown("Space", 2)
    delayTap(220, 700, 2, "升段考試")
    delayTap(380, 290, 1, "戰鬥")
    delayTap(380, 350, 1, "重擊3段")
    delayTap(590, 530, 1, "申請")
    delayKeyDown("Tab", 5, "換武器")
    delayTap(220, 700, 0, "開始")
    for j in range(500):
        delayKeyDown("2")
        if j % 4 == 0:
            ahk.key_down('Control')
            delayTap(500, 500, 0, "攻擊")
            ahk.key_up('Control')
        printSleep(0.5)
    printSleep(15)
    delayTap(950, 100, 0, "EXIT")
    delayTap(550, 480, 1, "確定")
    printSleep(5)
    pos = findImg("./img/15.PNG", "物品欄")
    if pos is None:
        delayKeyDown("i", 1, "物品欄")
        printSleep(2)

    delayKeyDown("2", 1, "寵物")
    delayKeyDown(".", 2, "寵物欄")
    printSleep(2)
    pos = findImg("./img/13.PNG", "寵物包升段章")
    if pos is not None:
        delayTap(pos[0] - 75, pos[1] - 10, 2, "在寵物欄拿章")
        delayTap(900, 475, 3, "放下章")
        delayTap(938, 410, 3, "堆疊")
        delayTap(900, 475, 3, "拿起章")
        delayTap(pos[0] - 75, pos[1] - 10, 2, "放章")
    notifyImg("screen", getWindow_Img(hwnd))
    delayTap(120, 770, 1, "menu")
    delayTap(120, 680, 1, "登出")
    delayTap(550, 480, 1, "確定")
    printSleep(5)

def select_charter():
    global charterCount
    pos = findImg("./img/1.PNG", "選角")
    if pos is not None:
        if charterCount > 9:
            delayTap(970, 700, 1)
            delayTap(700, 700, 2, "角色{}".format(charterCount))
        else:
            delayTap(700, 250 + charterCount * 50, 1, "角色{}".format(charterCount))
        delayTap(50, 750, 3, "開始")
        printSleep(20)

    pos = findImg("./img/3.PNG", "1000等")
    if pos is not None:
        delayTap(500, 400, 1)
        printSleep(3)
        pos = findImg("./img/4.PNG", "智能系統")
        if pos is not None:
            delayTap(870, 190, 1, "關閉")
    printSleep(3)
    pos = findImg("./img/4.PNG", "智能系統")
    if pos is not None:
        delayTap(870, 190, 1, "關閉")
        printSleep(3)
        pos = findImg("./img/3.PNG", "1000等")
        if pos is not None:
            delayTap(500, 400, 1)

def getSupplementMoneyPos(num):
    pos = findImg("./img/14.PNG")
    if pos is not None:
        if num == 2:
            return pos[0] - 140, pos[1]
        if num == 3:
            return pos[0] - 90, pos[1]
        if num == 4:
            return pos[0] - 40, pos[1]
        if num == 5:
            return pos[0] - 140, pos[1] + 50
        if num == 6:
            return pos[0] - 90, pos[1] + 50
        if num == 7:
            return pos[0] - 40, pos[1] + 50
        if num == 8:
            return pos[0] + 5, pos[1] + 50
        if num == 9:
            return pos[0] - 140, pos[1] + 100
        if num == 10:
            return pos[0] - 90, pos[1] + 100

    return 0, 0

def supplementMoney():
    printSleep(5)
    pos = findImg("./img/15.PNG", "有背包")
    if pos is not None:
        delayKeyDown("i", 1, "關背包")
    ahk.mouse_move(x=900, y=400, blocking=True)
    ahk.key_down('Control')
    delayTap(900, 400, 0, "銀行")
    ahk.key_up('Control')
    delayTap(220, 700, 5, "打開銀行")

    for i in range(5):
        delayTap(420, 570, 5, "支出")
        delayKeyDown("Enter", 2, "取錢(10W)")
    delayTap(590, 490, 1, "關閉")
    delayTap(480, 600, 2, "交易結束")
    # delayKeyDown("k", 2, "自動視角(北)")

    pos = findImg("./img/15.PNG", "開背包")
    if pos is None:
        delayKeyDown("i", 1, "開背包")

    delayKeyDown("2", 2, "召寵")
    delayKeyDown(".", 2, "打開寵背包")
    printSleep(2)

    for c in range(2, 7, 1):
        x2, y2 = getSupplementMoneyPos(c)
        delayTap(x2, y2, 2, "自己錢包")

        x, y = getPatWalletPos(c)
        delayTap(x, y, 2, "寵物背包錢包 {}".format(c))

        delayTap(x2, y2, 2, "自己錢包")
    printSleep(2)
    pos = findImg("./img/13.PNG", "寵物包升段章")
    if pos is not None:
        delayTap(pos[0] - 75, pos[1] - 10, 2, "在寵物欄拿章")
        delayTap(900, 475, 3, "放下章")
        printSleep(1)
        ahk.right_click()
        printSleep(2)
        ahk.key_down('Shift')
        delayTap(900, 475, 0, "分割章")
        ahk.key_up('Shift')
        delayTap(850, 550, 2, "分割1個章")

        # delayTap(938, 410, 3, "堆疊")
        # delayTap(900, 475, 3, "拿起章")
        delayTap(pos[0] - 75, pos[1] - 10, 2, "放章")

    printSleep(2)
    delayKeyDown("i", 2, "關背包")
    delayKeyDown(".", 1, "關寵物背包")
    ahk.mouse_move(200, 600)
    printSleep(1)
    ahk.key_down('Control')
    delayTap(200, 600, 0, "信箱")
    ahk.key_up('Control')
    delayTap(260, 620, 5, "發送信件")
    printSleep(1)
    ahk.send_input('oreo2')
    delayTap(600, 280, 1, "角色確認")
    delayKeyDown("i", 2, "開背包")
    delayTap(900, 475, 2, "拿起章")
    delayTap(400, 550, 1, "放到信箱")
    delayTap(470, 570, 1, "價錢要求")
    delayTap(550, 570, 1)
    ahk.send_input('500000')
    delayTap(400, 640, 1, "發送")
    delayTap(550, 480, 2, "確認")
    delayTap(140, 700, 1, "結束")
    delayTap(140, 700, 1, "關閉對話視窗")
def closeGame():
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    delayTap(550, 480, 2, "確認")


if __name__ == '__main__':
    ahk = AHK()
    initBFHwnd()

    # for j in
    initMainSubHwnd()
    if hwnd is None:
        notify("找不到畫面")
        input("找不到畫面")
    win32gui.SetForegroundWindow(hwnd)
    win32gui.MoveWindow(hwnd, 0, 0, 1000, 800, True)

    for i in range(2, 7, 1):
        charterCount = i
        select_charter()
        promotion_action()
        printSleep(10)

    charterCount = 1
    win32gui.SetForegroundWindow(hwnd)
    select_charter()

    supplementMoney()

    # closeGame()
