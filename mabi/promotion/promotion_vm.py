import sys
sys.path.append("../..")
sys.path.append("..")
# https://repo.anaconda.com/archive/
import win32api, win32con, win32gui, win32ui, win32service, os, time

from ahk import AHK
import time
from pynput import keyboard
import requests
import threading

# 升段腳本

from pynput.mouse import Button, Controller
import numpy as np
import cv2

import math
import signal
import platform

from tool.VideoCap import VideoCap
from tool.wintools import *
from tool import wintools
from mabi.promotion.sheetEdit import ExtManager

gameName = "新瑪奇 mabinogi"
break_program = False

charterCount = 2

hwnd = None
hwnd_main = None
hwnd_sub = None
hwnd_bf = None
hwnd_title = dict()
# pos
supplementMoneyPos = None
patWalletPos = None
attackCount = 440
isNeedClearExpiredItem = False
isNeedRecording = False
videoCap = VideoCap()
extManager = ExtManager()
pcName = platform.node()

class MabiPromotion:

    def __init__(self):
        pass


def getWindow_W_H(hwnd):
    return (0, 0, 1920, 1080)

wintools.getWindow_W_H = getWindow_W_H

def on_press(key):
    global break_program
    print(key)
    if key == keyboard.Key.esc:
        print('end pressed')
        break_program = True
        return False


def findAttackMonster():
    loopCnt = 0
    while True:
        pos = ahk.mouse_position
        print(loopCnt)
        if pos[0] < 100 and pos[1] < 100:
            print("end")
            break
        ahk.key_press("2")
        sleep(0.5)
        loopCnt += 1


def get_all_hwnd(h, mouse):
    if win32gui.IsWindow(h) and win32gui.IsWindowEnabled(h) and win32gui.IsWindowVisible(h):
        hwnd_title.update({h: win32gui.GetWindowText(h)})

def initMainSubHwnd():
    # win32gui.EnumWindows(get_all_hwnd, 0)
    global hwnd_main, hwnd_sub, hwnd
    print("name: {}".format(gameName))
    for h, t in get_hwnd_and_title_list().items():
        if t == gameName:
            if hwnd_main is None and findImg("../img/10.PNG", "主帳", h):
                hwnd_main = h
            elif hwnd_sub is None and findImg("../img/9.PNG", "副帳", h):
                hwnd_sub = h
            else:
                hwnd = h

def getCurrentTime():
    return getCurrentDateTime("%H:%M:%S")

def delayTap(x, y, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    sleep(0.5)
    ahk.click()
    if description is not None:
        print("tap x:{}, y:{}, {}".format(x, y, description))
    else:
        print("tap x:{}, y:{}".format(x, y))

def delayKeyDownClick(x, y, key, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    sleep(0.1)
    ahk.key_down(key)
    sleep(0.5)
    ahk.click()
    sleep(0.2)
    ahk.key_up(key)
    if description is not None:
        print("key tap x:{}, y:{}, key: {} {}".format(x, y, key, description))
    else:
        print("key tap x:{}, y:{}, key: {}".format(x, y, key))


def delayKeyDown(str, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.key_press(str)
    if description is not None:
        print("key down: {}".format(description))


def getPatWalletPos(num):
    global patWalletPos
    if patWalletPos is None:
        patWalletPos = findImg("../img/13.PNG")
    if patWalletPos is not None:
        # 寵物錢包
        if num == 2:
            return patWalletPos[0] + 10, patWalletPos[1] + 50
        elif num == 3:
            return patWalletPos[0] + 10, patWalletPos[1] + 95
        elif num == 4:
            return patWalletPos[0] + 10, patWalletPos[1] + 145
        elif num == 5:
            return patWalletPos[0] - 40, patWalletPos[1]
        elif num == 6:
            return patWalletPos[0] - 40, patWalletPos[1] + 50
        elif num == 7:
            return patWalletPos[0] - 40, patWalletPos[1] + 95
        elif num == 8:
            return patWalletPos[0] - 40, patWalletPos[1] + 145
        elif num == 9:
            return patWalletPos[0] - 90, patWalletPos[1] + 50
        elif num == 10:
            return patWalletPos[0] - 90,  patWalletPos[1] + 95

    return 0, 0


def promotion_action():
    pos = findImg("../img/15.PNG", "開背包")
    if pos is None:
        delayKeyDown("i", 1, "開背包")

    if isNeedClearExpiredItem:
        delayTap(920, 410, 3, "移除過期物品")
        printSleep(2)
        pos = findImg("../img/17.PNG", "銷毀")
        if pos is not None:
            delayTap(pos[0], pos[1], 3, "銷毀")

    delayKeyDown("2", 2, "召寵")
    delayKeyDown(".", 5, "打開寵物背包")
    printSleep(2)
    x, y = getPatWalletPos(charterCount)
    delayTap(x, y, 2, "寵物背包錢包 {}".format(charterCount))
    delayKeyDown(".", 2, "關閉寵物背包")
    printSleep(1)
    pos = findUntil("../img/15.PNG", "開背包", maxCount=20)

    # pos = findImg("../img/15.PNG", "開背包")
    if pos is None:
        delayKeyDown("i", 2, "開背包")
        printSleep(4)
        # pos = findImg("../img/15.PNG", "開背包")
        # if pos is None:
        #     delayKeyDown("i", 2, "開背包")
        #     printSleep(4)
        # pos = findImg("../img/15.PNG", "開背包")
        if pos is None:
            notifyImg("not open back", getWindow_Img(hwnd))
            raise Exception("not open back")

    delayTap(pos[0] + 170, pos[1] + 235, 2, "自己錢包")
    delayKeyDown(".", 2, "打開寵物背包")
    delayTap(x, y, 2, "寵物背包")
    printSleep(2)
    delayKeyDown("i", 2, "關背包")
    delayKeyDown(".", 1, "關寵物背包")
    delayKeyDownClick(500, 500, "Control", 3, "升段嚮導")
    delayKeyDownClick(500, 500, "Control", 3, "升段嚮導")
    # delayKeyDown("Space", 2)
    printSleep(3)
    delayTap(220, 700, 2, "升段考試")
    delayTap(380, 290, 1, "戰鬥")
    delayTap(380, 350, 1, "重擊3段")
    delayTap(590, 530, 1, "申請")
    delayKeyDown("Tab", 10, "換武器")
    delayTap(220, 700, 2, "開始")
    for j in range(attackCount):
        delayKeyDown("2")
        if j % 4 == 0:
            delayKeyDownClick(500, 500, "Control", 0, "攻擊 {}".format(j))
        printSleep(0.5)
    printSleep(10)
    delayTap(950, 100, 0, "EXIT")
    delayTap(550, 480, 1, "確定")
    printSleep(5)
    pos = findImg("../img/15.PNG", "物品欄")
    if pos is None:
        delayKeyDown("i", 1, "物品欄")
        printSleep(2)

    delayKeyDown("2", 1, "寵物")
    delayKeyDown(".", 2, "寵物欄")
    printSleep(2)
    pos = findImg("../img/13.PNG", "寵物包升段章")
    if pos is not None:
        delayTap(pos[0] - 75, pos[1] - 10, 2, "在寵物欄拿章")
        delayTap(900, 475, 3, "放下章")
        delayTap(938, 410, 3, "堆疊")
        delayTap(900, 475, 3, "拿起章")
        delayTap(pos[0] - 75, pos[1] - 10, 2, "放章")
    # notify("第{}只角色完成".format(charterCount))
    extManager.addHestory([getCurrentTime(), pcName, "第{}只角色完成".format(charterCount)])
    delayTap(120, 770, 1, "menu")
    delayTap(120, 680, 1, "登出")
    delayTap(550, 480, 1, "確定")
    printSleep(5)

def checkIsDisConnectGame():
    pos = findImg("../img/33.PNG", "斷線", samePercentage=0.06)
    if pos is not None:
        closeGame()
        printSleep(15)

def select_charter():
    global charterCount
    pos = findImg("../img/1.PNG", "選角")
    if pos is not None:
        if charterCount > 9:
            delayTap(970, 700, 1)
            delayTap(700, 700, 2, "角色{}".format(charterCount))
        else:
            delayTap(700, 250 + charterCount * 50, 1, "角色{}".format(charterCount))
        delayTap(50, 750, 3, "開始")
        printSleep(30)

    delayTap(120, 770, 1, "menu")
    delayKeyDown("i", 1, "背包")
    delayKeyDown("i", 1, "背包")
    printSleep(2)
    pos = findImg("../img/15.PNG")
    if pos is not None:
        delayKeyDown("i", 1, "背包")
        printSleep(2)
    pos = findImg("../img/3.PNG", "1000等")
    if pos is not None:
        delayTap(500, 400, 1)
        printSleep(3)
        pos = findImg("../img/4.PNG", "智能系統")
        if pos is not None:
            delayTap(870, 190, 1, "關閉")
    printSleep(3)
    pos = findImg("../img/4.PNG", "智能系統")
    if pos is not None:
        delayTap(870, 190, 1, "關閉")
        printSleep(3)
        pos = findImg("../img/3.PNG", "1000等")
        if pos is not None:
            delayTap(500, 400, 1)
    printSleep(2)

def getSupplementMoneyPos(num):
    global supplementMoneyPos
    if supplementMoneyPos is None:
      supplementMoneyPos = findImg("../img/14.PNG")
    if supplementMoneyPos is not None:
        if num == 2:
            return supplementMoneyPos[0] - 140, supplementMoneyPos[1]
        if num == 3:
            return supplementMoneyPos[0] - 90, supplementMoneyPos[1]
        if num == 4:
            return supplementMoneyPos[0] - 40, supplementMoneyPos[1]
        if num == 5:
            return supplementMoneyPos[0] - 140, supplementMoneyPos[1] + 50
        if num == 6:
            return supplementMoneyPos[0] - 90, supplementMoneyPos[1] + 50
        if num == 7:
            return supplementMoneyPos[0] - 40, supplementMoneyPos[1] + 50
        if num == 8:
            return supplementMoneyPos[0] + 5, supplementMoneyPos[1] + 50
        if num == 9:
            return supplementMoneyPos[0] - 140, supplementMoneyPos[1] + 100
        if num == 10:
            return supplementMoneyPos[0] - 90, supplementMoneyPos[1] + 100

    return 0, 0

def supplementMoney():
    isError = False

    pos = findImg("../img/15.PNG", "有背包")
    if pos is not None:
        delayKeyDown("i", 1, "關背包")

    delayKeyDownClick(900, 400, "Control", 1, "銀行NPC")
    printSleep(5)
    delayKeyDown("Space", 1, "Space")
    printSleep(3)
    delayTap(220, 700, 3, "打開銀行")
    printSleep(3)
    delayTap(370, 570, 1, "存錢")
    delayKeyDown("Enter", 1, "存錢")
    for i in range(5):
        delayTap(420, 570, 1, "支出")
        delayKeyDown("Enter", 1, "取錢(10W)")
    delayTap(590, 490, 1, "關閉")
    delayTap(480, 600, 2, "交易結束")
    # delayKeyDown("k", 2, "自動視角(北)")

    printSleep(2)
    pos = findImg("../img/15.PNG", "開背包")
    if pos is None:
        delayKeyDown("i", 1, "開背包")
        delayTap(938, 410, 2, "堆疊")

    if isNeedClearExpiredItem:
        delayTap(920, 410, 3, "移除過期物品")
        printSleep(2)
        pos = findImg("../img/17.PNG", "銷毀")
        if pos is not None:
            delayTap(pos[0], pos[1], 3, "銷毀")

    delayKeyDown("2", 2, "召寵")

    printSleep(2)

    for c in range(2, 10, 1):
        x2, y2 = getSupplementMoneyPos(c)

        delayTap(x2, y2, 1, "自己錢包")
        delayKeyDown(".", 1, "打開寵背包")
        printSleep(1)
        x, y = getPatWalletPos(c)
        delayTap(x, y, 1, "寵物背包錢包 {}".format(c))
        delayKeyDown(".", 1, "關閉寵背包")
        delayTap(x2, y2, 1, "自己錢包")
    delayKeyDown(".", 1, "打開寵背包")
    printSleep(2)
    pos = findImg("../img/13.PNG", "寵物包升段章")
    if pos is not None:
        delayTap(pos[0] - 75, pos[1] - 10, 2, "在寵物欄拿章")
        delayTap(900, 475, 3, "放下章")
        printSleep(1)
        delayTap(pos[0] - 75, pos[1] - 10, 2, "暫放寵物背包")
        printSleep(2)
        delayKeyDownClick(900, 475, "Shift", 1, "分割章")
        delayTap(850, 550, 2, "分割1個章")
        delayTap(pos[0] - 75, pos[1] - 10, 2, "放章(1)")
        delayTap(900, 475, 3, "放下物品")
        delayTap(pos[0] - 100, pos[1] - 10, 2, "放章(多)")
    # notifyImg("章", getWindow_Img(hwnd))
    # notify("章")
    extManager.addHestory([getCurrentTime(), pcName, "章"])

    printSleep(2)
    delayKeyDown("i", 2, "關背包")
    delayKeyDown(".", 1, "關寵物背包")
    delayKeyDownClick(200, 600, "Control", 1, "信箱")

    tempCheckImg = getWindow_Img(hwnd)
    # notifyImg("checking", getWindow_Img(hwnd))

    delayTap(500, 330, 5)
    delayTap(500, 330, 0.1, "收錢")
    ahk.click()
    delayTap(400, 640, 1, "接收")
    delayTap(550, 480, 2, "確認")

    delayTap(260, 620, 2, "發送信件")
    printSleep(1)
    ahk.send_input('oreo2')
    delayTap(600, 280, 1, "角色確認")
    delayKeyDown(".", 2, "開寵物背包")
    printSleep(2)
    pos = findImg("../img/13.PNG", "寵物包升段章")
    if pos is not None:
        delayTap(pos[0] - 100, pos[1] - 10, 2, "拿章(多)")
        delayKeyDown(".", 2, "關寵物背包")
    delayTap(400, 550, 1, "放到信箱")
    delayTap(470, 570, 1, "價錢要求")
    delayTap(550, 570, 1)
    ahk.send_input('300000')
    pos = findImg("../img/35.PNG", "check is send screen")
    if pos is None:
        isError = True
        notifyImg("checking", tempCheckImg)
        notify("send screen error")
        extManager.addHestory([getCurrentTime(), pcName, "send screen error"])
        notifyImg("送出", getWindow_Img(hwnd))
    extManager.addHestory([getCurrentTime(), pcName, "送出"])

    delayTap(400, 640, 1, "發送")
    delayTap(550, 480, 2, "確認")
    delayTap(140, 700, 1, "結束")
    delayTap(140, 700, 1, "關閉對話視窗")
    delayKeyDown("`", 2, "收起寵物")
    tempCheckImg = None
    isError = False

def closeGame():
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    delayTap(550, 480, 2, "確認")


def signal_handler(sig):
    print("pressed ctrl + c")
    if isNeedRecording:
        videoCap.stop()
    notifyImg("error-stop", getWindow_Img(hwnd))
    extManager.addHestory([getCurrentTime(), pcName, "error-stop"])

    sys.exit(0)

def getHwnd():
    return hwnd

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)

    try:
        win32api.SetConsoleCtrlHandler(signal_handler, True)

        if isNeedRecording:
            videoCap.hwnd = 0
            videoCap.left = 0
            videoCap.top = 0
            videoCap.width = 1920
            videoCap.height = 1080
            videoCap.scale = 0.5


            t1 = threading.Thread(target=videoCap.start)
            t1.setDaemon(True)
            t1.start()
        ahk = AHK()

        if datetime.now().day % 3 == 0:
            isNeedClearExpiredItem = True
        initMainSubHwnd()
        if hwnd is None:
            notify("找不到畫面")
            input("找不到畫面")
        win32gui.SetForegroundWindow(hwnd)
        printSleep(2)
        win32gui.MoveWindow(hwnd, 0, 0, 1000, 800, True)
        delayTap(500, 10, 0, "點擊瑪奇")
        printSleep(2)

        for i in range(2, 10, 1):
            charterCount = i
            supplementMoneyPos = None
            patWalletPos = None
            select_charter()
            promotion_action()
            printSleep(10)

        charterCount = 1
        win32gui.SetForegroundWindow(hwnd)
        printSleep(2)
        select_charter()

        supplementMoney()

        closeGame()
        if isNeedRecording:
            videoCap.stop()

    except Exception as error:
        print("error: {}".format(error))
        notifyImg("error: {}".format(error), getWindow_Img(hwnd))
        extManager.addHestory([getCurrentTime(), pcName, "error: {}".format(error)])
        printSleep(3)
        input()


# https://tw-mabi.weebly.com/bes.html