import sys
sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from mabi.promotion.orignal_promotion import MabiPromotion

acc = []
pwd = []
bf_hwnd = None
mabi_hwnd = []
screenWidth = 1920
screenHeight = 1080
pcName = platform.node()

def getWindow_W_H(hwnd):
    return (0, 0, screenWidth, screenHeight)

def init_account():
    with open("./account.txt", 'r') as fp:
        line = fp.readline()
        while line:
            arr = line.rstrip('\n').split(",")
            if len(arr) == 2:
                acc.append(arr[0])
                pwd.append(arr[1])
                print(arr[0], arr[1])
            line = fp.readline()

def FindWindow_bySearch(pattern):
    window_list = []
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
    for each in window_list:
        if re.search(pattern, win32gui.GetWindowText(each)) is not None:
            return each


def delayTap(x, y, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_move(x=x, y=y, blocking=True)
    ahk.click()
    if description is not None:
        print("tap x:{}, y:{}, {}".format(x, y, description))
    else:
        print("tap x:{}, y:{}".format(x, y))

def focusStartMabiWindow():
    print("focusStartMabiWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t == "mabinogi":
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            print("find the mabi window")
            break

def moveCMDWindow():
    print("moveCMDWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and "cmd" in t:
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            win32gui.MoveWindow(h, 1000, 0, 500, 600, True)
            print("move the cmd")
            break

def login(ac, pw):
    tryLogin = True
    stepCnt = 0
    loopCnt = 0

    while tryLogin:
        if loopCnt > 5:
            raise Exception("loop on login: step {}".format(stepCnt))
        print("step: {}".format(stepCnt))

        openIE()
        loopCnt += 1
        printSleep(5)
        if stepCnt is 0:
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                delayTap(pos[0], pos[1], 1, " 右下登入")
                printSleep(5)
                stepCnt = 1
                loopCnt = 0
            else:
                pos = findImg("../img/26.PNG", "登出", samePercentage=0.1)
                if pos is not None:
                    delayTap(pos[0], pos[1], 1, "登出")
                    printSleep(5)
                    pos = findImg("../img/27.PNG", "確定", samePercentage=0.12)
                    if pos is not None:
                        delayTap(pos[0], pos[1], 1, "確定")
                        printSleep(5)
                continue

        if stepCnt is 1:
            pos = findImg("../img/23.PNG", "登入(綠)", samePercentage=0.02)
            if pos is not None:
                delayTap(pos[0] - 150, pos[1] - 20, 1, " 帳號或電郵")
                ahk.send_input(ac)
                delayTap(pos[0] - 150, pos[1] + 10, 1, "密碼")
                ahk.send_input(pw)
                delayTap(628, 228, 1, "Login")
                printSleep(15)
                stepCnt = 2
                loopCnt = 0
            else:
                stepCnt = 0
                continue

        if stepCnt is 2:
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                stepCnt = 0
                continue
            printSleep(2)

            pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.08)
            if pos is not None:
                delayTap(pos[0], pos[1], 1, "快速啟動")
                delayTap(pos[0], pos[1] - 100, 5, "帳1")
                printSleep(10)
                closeIE()
                focusStartMabiWindow()
                printSleep(3)
                stepCnt = 3
                loopCnt = 0
            else:
                continue

        if stepCnt is 3:
            if loopCnt > 2:
                pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.08)
                if pos is not None:
                    delayTap(pos[0], pos[1], 1, "快速啟動")
                    delayTap(pos[0], pos[1] - 100, 5, "帳1")
                    printSleep(10)
                    closeIE()
                    focusStartMabiWindow()
                    printSleep(3)
                    stepCnt = 3
                    loopCnt = 0
                else:
                    continue

            closeIE()
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                stepCnt = 0
                continue
            focusStartMabiWindow()
            closeIE()
            printSleep(2)
            pos = findImg("../img/28.PNG", "開始遊戲", samePercentage=0.15)
            if pos is not None:
                delayTap(pos[0], pos[1], 1, "開始遊戲")
                printSleep(6)
                moveCMDWindow()
                stepCnt = 4
                loopCnt = 0
            else:
                continue

        if stepCnt is 4:
            pos = findImg("../img/29.PNG", "同意")
            if pos is not None:
                delayTap(pos[0], pos[1], 1, "同意")
                stepCnt = 5
                loopCnt = 0
            else:
                continue

        # if stepCnt is 5:
        #     pos = findImg("../img/30.PNG", "開始")
        #     if pos is not None:
        #         delayTap(pos[0], pos[1], 2, "進入愛爾琳")
        #         printSleep(20)
        #         stepCnt = 6
        #     else:
        #         continue

        tryLogin = False
        stepCnt = 0


def logout():
    global bf_hwnd
    openIE()
    if bf_hwnd is None:
        openIE()
    else:
        printSleep(5)
        win32gui.ShowWindow(bf_hwnd, win32con.SW_SHOWNORMAL)
    printSleep(5)
    pos = findImg("../img/26.PNG", "登出", samePercentage=0.1)
    if pos is not None:
        delayTap(pos[0], pos[1], 1, "登出")
        printSleep(5)
        pos = findImg("../img/27.PNG", "確定", samePercentage=0.12)
        if pos is not None:
            delayTap(pos[0], pos[1], 1, "確定")
            printSleep(5)
    bf_hwnd = None


def closeIE():
    global bf_hwnd
    window_list = []
    win32gui.EnumWindows(lambda hWnd, param: param.append(hWnd), window_list)
    for each in window_list:
        window_name = win32gui.GetWindowText(each)
        if window_name is not None and window_name.count("Internet Explorer") > 0:
            win32gui.SendMessage(each, win32con.WM_CLOSE, 0, 0)
    bf_hwnd = None

def openIE(isFirstInit=False):
    global bf_hwnd
    closeIE()
    os.startfile("C:\Program Files (x86)\Internet Explorer\iexplore.exe")

    printSleep(10)
    isNotOpen = True
    while (isNotOpen):
        pos = findImg("../img/42.PNG", "BF 已載入", samePercentage=0.1)
        if pos is not None:
            isNotOpen = False
        else:
            printSleep(10, "登入waiting")

    print("openIE")
    for h, t in get_hwnd_and_title_list().items():
        if t == "mabinogi":
            bf_hwnd = h
            print("bf: {}".format(bf_hwnd))
            win32gui.ShowWindow(bf_hwnd, win32con.SW_SHOWNORMAL)
            printSleep(5)
            win32gui.MoveWindow(h, 0, 0, 1000, 800, True)
            break
    # bf_hwnd = FindWindow_bySearch("beanfun! - Internet Explorer")
    # bf_hwnd = FindWindow_bySearch("beanfun! - Windows Internet Explorer")

    # resizeWindow(bf_hwnd)

def resizeWindow(hwnd):
    print("{} resize {}, {}".format(hwnd, 1000, 800))
    win32gui.ShowWindow(bf_hwnd, win32con.SW_SHOWNORMAL)
    printSleep(5)
    win32gui.MoveWindow(hwnd, 0, 0, 1000, 800, True)


def closeGame(hwnd):
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    printSleep(2)
    pos = findImg("../img/31.PNG", "關瑪奇")
    if pos is not None:
        delayTap(pos[0], pos[1], 2, "確定")

def getCurrentTime():
    return getCurrentDateTime("%H:%M:%S")

def signal_handler(sig):
    notify("error")
    extManager.addHestory([getCurrentTime(), pcName, "error"])
    sys.exit(0)


extManager = ExtManager()

if __name__ == '__main__':
    try:
        ahk = AHK()
        win32api.SetConsoleCtrlHandler(signal_handler, True)

        screenWidth = GetSystemMetrics(0)
        screenHeight = GetSystemMetrics(1)
        init_account()
        moveCMDWindow()
        for i in range(len(acc)):
            errorRestartCharter = 2
            wintools.getWindow_W_H = getWindow_W_H
            login(acc[i], pwd[i])
            printSleep(60)
            # check is open mabi charter screen else relogin
            notify("start: {}".format(acc[i]))
            extManager.addHestory([getCurrentTime(), pcName, "start: {}".format(acc[i])])
            mabiPromotion = MabiPromotion()
            while mabiPromotion.taskStart() is False:
                printSleep(5)
                errorRestartCharter = mabiPromotion.charterCount
                if errorRestartCharter < 10 and errorRestartCharter is not 1:
                    errorRestartCharter = errorRestartCharter + 1

                pos = findImg("../img/43.PNG", "斷線", samePercentage=0.1)
                if pos is not None:
                    extManager.addHestory(
                        [getCurrentTime(), pcName, "斷線"])
                    mabiPromotion.closeGame()
                    printSleep(10)

                login(acc[i], pwd[i])
                printSleep(60)
                notify("restart: {}".format(acc[i]))
                extManager.addHestory([getCurrentTime(), pcName, "restart: {}, startCharter{}".format(acc[i], errorRestartCharter)])
                mabiPromotion.__init__(errorRestartCharter)
                mabiPromotion.taskStart()

            # exec(open("./promotion_vm.py","r",encoding="utf-8").read())
            printSleep(30)

            # closeGame()
            logout()

            if is_time_between("05:00", "07:00"):
                break
            # if is_time_between("")
        notify("finish loop holding")
        extManager.addHestory([getCurrentTime(), pcName, "finish loop holding"])
        printSleep(2)
        os.system('shutdown -s')
    except Exception as error:
        errLine = 'Error on line {}'.format(sys.exc_info()[-1].tb_lineno)
        errMsg = "error: line {} - {}".format(errLine, error)
        notify(errMsg)
        extManager.addHestory([getCurrentTime(), pcName, errMsg])
