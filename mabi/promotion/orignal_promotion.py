import sys

sys.path.append("../..")
sys.path.append("..")
# https://repo.anaconda.com/archive/
import win32api, win32con, win32gui, win32ui, win32service, os, time

from ahk import AHK
import time
from pynput import keyboard
import requests
import threading

# 升段腳本

from pynput.mouse import Button, Controller
import numpy as np
import cv2

import math
import signal
import platform

from tool.VideoCap import VideoCap
from tool.wintools import *
from tool import wintools
from mabi.promotion.sheetEdit import ExtManager


class MabiPromotion:

    def __init__(self, startCharter=2):
        self.gameName = "新瑪奇 mabinogi"
        self.break_program = False

        self.account = None
        self.errorCnt = 0
        self.charterCount = startCharter

        self.hwnd = None
        self.hwnd_main = None
        self.hwnd_sub = None
        self.hwnd_bf = None
        self.hwnd_title = dict()

        self.supplementMoneyPos = None
        self.patWalletPos = None
        self.attackCount = 400
        self.isNeedClearExpiredItem = False
        self.isNeedRecording = False
        self.videoCap = VideoCap()
        self.extManager = ExtManager()
        self.pcName = platform.node()
        self.ahk = AHK()
        self.errImgList = []

    def getWindow_W_H(self, hwnd):
        return (0, 0, 1920, 1080)

    def on_press(self, key):
        print(key)
        if key == keyboard.Key.esc:
            print('end pressed')
            self.break_program = True
            return False

    # def findAttackMonster(self):
    #     loopCnt = 0
    #     while True:
    #         pos = self.ahk.mouse_position
    #         print(loopCnt)
    #         if pos[0] < 100 and pos[1] < 100:
    #             print("end")
    #             break
    #         self.ahk.key_press("2")
    #         sleep(0.5)
    #         loopCnt += 1

    # def get_all_hwnd(self, h, mouse):
    #     if win32gui.IsWindow(h) and win32gui.IsWindowEnabled(h) and win32gui.IsWindowVisible(h):
    #         self.hwnd_title.update({h: win32gui.GetWindowText(h)})

    def initMainSubHwnd(self):
        # win32gui.EnumWindows(get_all_hwnd, 0)
        print("name: {}".format(self.gameName))
        for h, t in get_hwnd_and_title_list().items():
            if t == self.gameName:
                if self.hwnd_main is None and findImg("../img/10.PNG", "主帳", h):
                    self.hwnd_main = h
                elif self.hwnd_sub is None and findImg("../img/9.PNG", "副帳", h):
                    self.hwnd_sub = h
                else:
                    self.hwnd = h

    def getCurrentTime(self):
        return getCurrentDateTime("%H:%M:%S")

    # def delayTap(self, x, y, delay=0, description=None):
    #     if delay is not None and delay != 0:
    #         printSleep(delay)
    #     self.ahk.mouse_position = (x, y)
    #     sleep(0.5)
    #     self.ahk.click()
    #     if description is not None:
    #         print("tap x:{}, y:{}, {}".format(x, y, description))
    #     else:
    #         print("tap x:{}, y:{}".format(x, y))

    def delayKeyDownClick(self, x, y, key, delay=0, description=None):
        if delay is not None and delay != 0:
            printSleep(delay)
        self.ahk.mouse_position = (x, y)
        sleep(0.1)
        self.ahk.key_down(key)
        sleep(0.5)
        self.ahk.click()
        sleep(0.2)
        self.ahk.key_up(key)
        if description is not None:
            print("key tap x:{}, y:{}, key: {} {}".format(x, y, key, description))
        else:
            print("key tap x:{}, y:{}, key: {}".format(x, y, key))

    def delayKeyDown(self, str, delay=0, description=None):
        if delay is not None and delay != 0:
            printSleep(delay)
        self.ahk.key_press(str)
        if description is not None:
            print("key down: {}".format(description))

    def getPatWalletPos(self, num):
        if self.patWalletPos is None:
            self.patWalletPos = findImg("../img/13.PNG")
        if self.patWalletPos is not None:
            # 寵物錢包
            if num == 2:
                return self.patWalletPos[0] + 10, self.patWalletPos[1] + 50
            elif num == 3:
                return self.patWalletPos[0] + 10, self.patWalletPos[1] + 95
            elif num == 4:
                return self.patWalletPos[0] + 10, self.patWalletPos[1] + 145
            elif num == 5:
                return self.patWalletPos[0] - 40, self.patWalletPos[1]
            elif num == 6:
                return self.patWalletPos[0] - 40, self.patWalletPos[1] + 50
            elif num == 7:
                return self.patWalletPos[0] - 40, self.patWalletPos[1] + 95
            elif num == 8:
                return self.patWalletPos[0] - 40, self.patWalletPos[1] + 145
            elif num == 9:
                return self.patWalletPos[0] - 90, self.patWalletPos[1] + 50
            elif num == 10:
                return self.patWalletPos[0] - 90, self.patWalletPos[1] + 95

        return 0, 0

    def promotion_action(self):
        pos = findImg("../img/15.PNG", "開背包")
        if pos is None:
            self.delayKeyDown("i", 1, "開背包")
            printSleep(2)

        if self.isNeedClearExpiredItem:
            delayTap(self.ahk, 891, 410, 3, "移除過期物品")
            printSleep(2)
            pos = findImg("../img/17.PNG", "銷毀")
            if pos is not None:
                delayTap(self.ahk, pos[0], pos[1], 3, "銷毀")

        printSleep(2)

        tempScreen = retryMethod(lambda: getWindow_Img(self.hwnd))

        pos = findImgOnPhoto(tempScreen[800 - 400:800 - 300, 1000 - 160:1000], "../img/54.PNG", description="有祝水",
                             samePercentage=0.25)

        #pos2 = findImgOnPhoto(tempScreen[800 - 150:800 - 100, 1000 - 180:1000 - 100], "../img/55.PNG", "沒有錢包",
        #                     samePercentage=0.2)

        if pos is not None:
            self.delayKeyDown("2", 2, "召寵")
            self.delayKeyDown(".", 5, "打開寵物背包")
            printSleep(2)
            #x, y = self.getPatWalletPos(self.charterCount)
            #delayTap(self.ahk, x, y, 2, "寵物背包錢包 {}".format(self.charterCount))


            self.paperLoc = findImg("../img/13.PNG") #1357 573
            if self.paperLoc is not None:
                delayTap(self.ahk, self.paperLoc[0] - 40, self.paperLoc[1] + 220, 1, "寵物金幣接收") #1316, 793
                printSleep(2)
                self.ahk.key_press('backspace')
                self.ahk.send_input('30000')
                delayTap(self.ahk, self.paperLoc[0] + 30, self.paperLoc[1] + 289, 1, "確定") #1387, 862
            else:
                print("not pat")
            self.delayKeyDown(".", 2, "關閉寵物背包")
            printSleep(1)
            pos = findUntil("../img/15.PNG", "開背包", maxCount=20)

            # pos = findImg("../img/15.PNG", "開背包")
            if pos is None:
                self.delayKeyDown("i", 2, "開背包")
                printSleep(4)
                if pos is None:
                    notifyImg("not open back", getWindow_Img(self.hwnd))
                    return False

            #delayTap(self.ahk, pos[0] + 170, pos[1] + 235, 2, "自己錢包")


            self.delayKeyDown(".", 2, "打開寵物背包")
            # delayTap(self.ahk, x, y, 2, "寵物背包")
            printSleep(2)
            self.delayKeyDown("i", 2, "關背包")
            self.delayKeyDown(".", 1, "關寵物背包")
            self.delayKeyDownClick(500, 500, "Control", 3, "升段嚮導")
            self.delayKeyDownClick(500, 500, "Control", 3, "升段嚮導")
            # self.delayKeyDown("Space", 2)
            pos = findImg("../img/69.PNG", "參加升段式")
            if pos is not None:
                delayTap(self.ahk, 220, 700, 2, "取消對話")
                self.extManager.addHestory([self.getCurrentTime(), self.pcName, "error: 第{}只角色 升段".format(self.charterCount)])
            elif findImg("../img/71.PNG", "參加升段式"):
                delayTap(self.ahk, 220, 700, 2, "取消對話")
                self.extManager.addHestory(
                    [self.getCurrentTime(), self.pcName, "error: 第{}只角色 升段".format(self.charterCount)])
            else:
                printSleep(3)
                delayTap(self.ahk, 220, 700, 2, "升段考試")
                delayTap(self.ahk, 380, 290, 1, "戰鬥")
                delayTap(self.ahk, 380, 350, 1, "重擊3段")
                delayTap(self.ahk, 590, 530, 1, "申請")

                self.delayKeyDown("Tab", 10, "換武器")
                tempScreen = retryMethod(lambda: getWindow_Img(self.hwnd))
                pos = findImgOnPhoto(tempScreen[800 - 600:800 - 300, 1000 - 600:1000 - 200], "../img/59.PNG", "沒選入關", samePercentage=0.1)
                pos2 = findImgOnPhoto(tempScreen[800 - 600:800 - 300, 1000 - 600:1000 - 200], "../img/60.PNG", "沒選入關", samePercentage=0.2)
                pos3 = findImgOnPhoto(tempScreen[800 - 600:800 - 300, 1000 - 600:1000 - 200], "../img/61.PNG", "沒選入關", samePercentage=0.2)
                pos4 = findImgOnPhoto(tempScreen[800 - 600:800 - 300, 1000 - 600:1000 - 200], "../img/62.PNG", "沒選入關", samePercentage=0.2)

                printSleep(3)
                if all(v is None for v in [pos, pos2, pos3, pos4]):
                    delayTap(self.ahk, 220, 700, 2, "開始")
                    delayTap(self.ahk, 220, 700, 1, "開始")
                    for j in range(self.attackCount):
                        self.delayKeyDown("2")
                        if j % 4 == 0:
                            self.delayKeyDownClick(597, 370, "Control", 0, "攻擊 {}".format(j))
                        printSleep(0.5)
                        # if j > 380:
                        #     printSleep()
                    printSleep(10)
                    delayTap(self.ahk, 950, 100, 0, "EXIT")
                    delayTap(self.ahk, 550, 480, 1, "確定")
                    printSleep(5)
                    tempScreen = retryMethod(lambda: getWindow_Img(self.hwnd))
                    pos = findImgOnPhoto(tempScreen[800 - 600:800 - 300, 1000 - 600:1000 - 200], "../img/59.PNG", "沒選入關", samePercentage=0.1)
                    pos2 = findImgOnPhoto(tempScreen[800 - 600:800 - 300, 1000 - 600:1000 - 200], "../img/60.PNG", "沒選入關", samePercentage=0.2)
                    pos3 = findImgOnPhoto(tempScreen[800 - 600:800 - 300, 1000 - 600:1000 - 200], "../img/61.PNG", "沒選入關", samePercentage=0.2)
                    pos4 = findImgOnPhoto(tempScreen[800 - 600:800 - 300, 1000 - 600:1000 - 200], "../img/62.PNG", "沒選入關", samePercentage=0.2)


                    if all(v is None for v in [pos, pos2, pos3, pos4]):
                        pos = findImg("../img/15.PNG", "物品欄")
                        if pos is None:
                            self.delayKeyDown("i", 1, "物品欄")
                            printSleep(2)

                        self.delayKeyDown("2", 1, "寵物")

                        printSleep(2)
                        tempScreen = retryMethod(lambda: getWindow_Img(self.hwnd))

                        self.delayKeyDown(".", 2, "寵物欄")
                        printSleep(2)


                        pos = findImgOnPhoto(tempScreen[800 - 400:800 - 300, 1000 - 160:1000], "../img/54.PNG", description="有祝水",
                                             samePercentage=0.25)
                        if pos is None:
                            notifyImg("error: 第{}只角色沒祝水 two", screen=tempScreen)
                            self.errorCnt += 1
                            self.extManager.addHestory(
                                [self.getCurrentTime(), self.pcName, "error: 第{}只角色沒祝水 two".format(self.charterCount)])

                        # pos = findImgOnPhoto(tempScreen[800 - 150:800 - 100, 1000 - 180:1000 - 100], "../img/55.PNG",
                        #                       "沒有錢包",
                        #                       samePercentage=0.2)
                        # if pos is not None:
                        #     self.errorCnt += 1
                        #     notifyImg("error: 第{}只角色沒有錢包 two".format(self.charterCount), getWindow_Img(self.hwnd))
                        #     self.extManager.addHestory(
                        #         [self.getCurrentTime(), self.pcName, "error: 第{}只角色沒有錢包 two".format(self.charterCount)])
                        printSleep(2)

                        pos = findImg("../img/13.PNG", "寵物包升段章")
                        if pos is not None:
                            delayTap(self.ahk, pos[0] - 75, pos[1] - 10, 2, "在寵物欄拿章")
                            delayTap(self.ahk, 900, 475, 3, "放下章")
                            delayTap(self.ahk, 912, 410, 3, "堆疊")
                            delayTap(self.ahk, 900, 475, 3, "拿起章")
                            delayTap(self.ahk, pos[0] - 75, pos[1] - 10, 3, "放章")
                            printSleep(3)
                        # notify("第{}只角色完成".format(charterCount))
                        self.extManager.addHestory([self.getCurrentTime(), self.pcName, "第{}只角色完成".format(self.charterCount)])
                    else:
                        notifyImg("error: 第{}只角色失敗".format(self.charterCount), getWindow_Img(self.hwnd))
                        self.errorCnt += 1
                        self.extManager.addHestory(
                            [self.getCurrentTime(), self.pcName, "error: 第{}只角色失敗".format(self.charterCount)])
                        delayTap(self.ahk, 130, 700, 2, "對話結束")
                else:
                    self.extManager.addHestory(
                        [self.getCurrentTime(), self.pcName, "error: 第{}只角色沒進入".format(self.charterCount)])
                    self.errorCnt += 1
                    delayTap(self.ahk, 130, 700, 2, "對話結束")

        elif pos is None:
            notifyImg("error: 第{}只角色沒祝水".format(self.charterCount), screen=tempScreen)
            self.errorCnt += 1
            self.extManager.addHestory(
                [self.getCurrentTime(), self.pcName, "error: 第{}只角色沒祝水".format(self.charterCount)])
        # elif pos2 is not None:
        #     self.errorCnt += 1
        #     notifyImg("error: 第{}只角色沒有錢包".format(self.charterCount), getWindow_Img(self.hwnd))
        #     self.extManager.addHestory(
        #         [self.getCurrentTime(), self.pcName, "error: 第{}只角色沒有錢包".format(self.charterCount)])

        # self.delayKeyDown("`", 2, "寵物解除")

        delayTap(self.ahk, 120, 770, 1, "menu")
        delayTap(self.ahk, 120, 680, 1, "登出")
        delayTap(self.ahk, 550, 480, 1, "確定")
        printSleep(5)

    def checkIsDisConnectGame(self):
        pos = findImg("../img/56.PNG", "斷線", samePercentage=0.15)
        if pos is not None:
            self.extManager.addHestory(
                [self.getCurrentTime(), self.pcName, "error: 第 {} 斷線".format(self.charterCount)])
            self.closeGame()
            printSleep(15)
            return True
        return False

    def select_charter(self, needCheck=True):
        pos = findImg("../img/1.PNG", "選角")
        if pos is not None:
            if self.charterCount > 9:
                delayTap(self.ahk, 970, 700, 1)
                delayTap(self.ahk, 700, 700, 2, "角色{}".format(self.charterCount))
            else:
                delayTap(self.ahk, 700, 250 + self.charterCount * 50, 1, "角色{}".format(self.charterCount))
            delayTap(self.ahk, 50, 750, 3, "開始")
            if needCheck:
                printSleep(30)

        if needCheck:
            delayTap(self.ahk, 630, 480, 1, "生日 取消")
            delayTap(self.ahk, 120, 770, 1, "menu")
            self.delayKeyDown("i", 1, "背包")
            self.delayKeyDown("i", 1, "背包")
            printSleep(2)
            pos = findImg("../img/15.PNG")
            if pos is not None:
                self.delayKeyDown("i", 1, "背包")
                printSleep(2)

            delayTap(self.ahk, 500, 421, 1)

            pos = findImg("../img/4.PNG", "智能系統")
            if pos is not None:
                delayTap(self.ahk, 870, 190, 1, "關閉")
                printSleep(3)
                pos = findImg("../img/3.PNG", "1000等")
                if pos is not None:
                    delayTap(self.ahk, 500, 400, 1)
            printSleep(2)

            pos = findImg("../img/3.PNG", "1000等")
            if pos is not None:
                delayTap(self.ahk, 500, 400, 1)
                printSleep(3)
                pos = findImg("../img/4.PNG", "智能系統")
                if pos is not None:
                    delayTap(self.ahk, 870, 190, 1, "關閉")
            printSleep(2)

            #pos = findImg("../img/68.PNG", "現在領取")
           # if pos is not None:
           #     delayTap(self.ahk, 395, 538, 1)
           #     printSleep(2)

          #  pos = findImg("../img/70.PNG", "現在領取")
           # if pos is not None:
           #     delayTap(self.ahk, 395, 538, 1)
           #     printSleep(2)


            while True:
                self.ahk.mouse_move(x=10, y=10, blocking=True)
                printSleep(3)
                pos = findImg("../img/87.PNG", "Online 禮物")
                if pos is not None:
                    delayTap(self.ahk, pos[0], pos[1], 1, "現在領取")
                else:
                    break




    def getSupplementMoneyPos(self, num):
        if self.supplementMoneyPos is None:
            self.supplementMoneyPos = findImg("../img/14.PNG")
        if self.supplementMoneyPos is not None:
            if num == 2:
                return self.supplementMoneyPos[0] - 140, self.supplementMoneyPos[1]
            if num == 3:
                return self.supplementMoneyPos[0] - 90, self.supplementMoneyPos[1]
            if num == 4:
                return self.supplementMoneyPos[0] - 40, self.supplementMoneyPos[1]
            if num == 5:
                return self.supplementMoneyPos[0] - 140, self.supplementMoneyPos[1] + 50
            if num == 6:
                return self.supplementMoneyPos[0] - 90, self.supplementMoneyPos[1] + 50
            if num == 7:
                return self.supplementMoneyPos[0] - 40, self.supplementMoneyPos[1] + 50
            if num == 8:
                return self.supplementMoneyPos[0] + 5, self.supplementMoneyPos[1] + 50
            if num == 9:
                return self.supplementMoneyPos[0] - 140, self.supplementMoneyPos[1] + 100
            if num == 10:
                return self.supplementMoneyPos[0] - 90, self.supplementMoneyPos[1] + 100

        return 0, 0

    def supplementMoney(self):

        pos = findImg("../img/15.PNG", "有背包")
        if pos is not None:
            self.delayKeyDown("i", 1, "關背包")
        loopCntC = 0
        while True:
            pos = findImg("../img/45.PNG", "銀行NPC", samePercentage=0.01)
            if pos is not None:
                break
            else:
                if self.checkIsDisConnectGame() is True:
                    break
                pos = findImg("../img/88.PNG", "銀行NPC continue", samePercentage=0.01)
                pos2 = findImg("../img/51.PNG", "銀行NPC continue2", samePercentage=0.01)
                if pos is not None:
                    self.delayKeyDown("Space", 1, "Space")
                    delayTap(self.ahk, pos[0], pos[1], 2, "繼續")
                elif pos2 is not None:
                    self.delayKeyDown("Space", 1, "Space")
                    delayTap(self.ahk, pos2[0], pos2[1], 2, "繼續")
                else:
                    printSleep(5)
                    self.delayKeyDownClick(900, 400, "Control", 1, "銀行NPC")

                pos = findImg("../img/52.PNG", "探險看版", samePercentage=0.5)
                if pos is not None:
                    self.delayKeyDownClick(900, 200, "Control", 1, "銀行NPC")
                    printSleep(5)
                loopCntC += 1
                if loopCntC % 10 == 0:
                    self.delayKeyDown("Space", 1, "Space")

                if(loopCntC > 20):
                    self.extManager.addHestory(
                        [self.getCurrentTime(), self.pcName, "error: 卡銀行"])
                    notifyImg("卡銀行", getWindow_Img(self.hwnd))
                    input()
                    printSleep(3)
                    delayTap(self.ahk, 500, 10, 0, "點擊瑪奇")


        if self.checkIsDisConnectGame() is False:
            printSleep(2)
            self.delayKeyDown("Space", 1, "Space")
            self.logErrImg()
            printSleep(3)
            delayTap(self.ahk, 220, 700, 3, "打開銀行")
            printSleep(3)
            delayTap(self.ahk, 427, 559, 2, "存錢")
            self.delayKeyDown("Enter", 2, "存錢")
            # for i in range(5):
            #     # self.logErrImg()
            #     delayTap(self.ahk, 420, 570, 1, "支出")
            #     self.delayKeyDown("Enter", 1, "取錢(10W)")
            delayTap(self.ahk, 480, 556, 1, "支出")
            for i in range(10):
                self.ahk.key_press('backspace')
            self.ahk.send_input('250000')
            self.delayKeyDown("Enter", 2, "取錢25W")
            delayTap(self.ahk, 562, 715, 2, "確定")
            #delayTap(self.ahk, 590, 490, 1, "關閉")
            delayTap(self.ahk, 480, 613, 2, "交易結束")
            # self.delayKeyDown("k", 2, "自動視角(北)")

            printSleep(2)
            pos = findImg("../img/15.PNG", "開背包")
            if pos is None:
                self.delayKeyDown("i", 1, "開背包")
                delayTap(self.ahk, 912, 410, 2, "堆疊")

            if self.isNeedClearExpiredItem:
                delayTap(self.ahk, 891, 410, 3, "移除過期物品")
                printSleep(2)
                pos = findImg("../img/17.PNG", "銷毀")
                if pos is not None:
                    delayTap(self.ahk, pos[0], pos[1], 3, "銷毀")

            self.delayKeyDown("2", 2, "召寵")

            printSleep(2)
            self.logErrImg()

            # for c in range(2, 10, 1):
            #     x2, y2 = self.getSupplementMoneyPos(c)
            #
            #     delayTap(self.ahk, x2, y2, 1, "自己錢包")
            #     self.delayKeyDown(".", 1, "打開寵背包")
            #     printSleep(1)
            #     #x, y = self.getPatWalletPos(c)
            #
            #     #delayTap(self.ahk, x, y, 1, "寵物背包錢包 {}".format(c))
            #
            #
            #
            #     self.delayKeyDown(".", 1, "關閉寵背包")
            #     delayTap(self.ahk, x2, y2, 1, "自己錢包")
            self.delayKeyDown(".", 1, "打開寵背包")
            printSleep(2)
            pos = findImg("../img/13.PNG", "寵物包升段章") #1357 573
            if pos is not None:
                delayTap(self.ahk, pos[0] - 80, pos[1] + 220, 1, "寵物金幣給予")  # 1277, 793
                printSleep(2)
                self.ahk.key_press('backspace')
                self.ahk.send_input('240000')
                delayTap(self.ahk, pos[0] - 14, pos[1] + 282, 1, "確定")  # 1343, 855

                delayTap(self.ahk, pos[0] - 75, pos[1] - 10, 2, "在寵物欄拿章")
                delayTap(self.ahk, 900, 475, 3, "放下章")
                printSleep(1)
                delayTap(self.ahk, pos[0] - 75, pos[1] - 10, 2, "暫放寵物背包")
                printSleep(2)
                self.delayKeyDownClick(900, 475, "Shift", 1, "分割章")
                delayTap(self.ahk, 850, 550, 2, "分割1個章")
                delayTap(self.ahk, pos[0] - 75, pos[1] - 10, 2, "放章(1)")
                delayTap(self.ahk, 900, 475, 3, "放下物品")
                delayTap(self.ahk, pos[0] - 100, pos[1] - 10, 2, "放章(多)")




            # self.extManager.addHestory([self.getCurrentTime(), self.pcName, "章"])

            printSleep(2)
            self.delayKeyDown("i", 2, "關背包")
            self.delayKeyDown(".", 1, "關寵物背包")
            self.delayKeyDownClick(200, 600, "Control", 1, "信箱")

            self.logErrImg()

            delayTap(self.ahk, 500, 330, 5)
            delayTap(self.ahk, 500, 330, 0.1, "收錢")
            self.ahk.click()
            delayTap(self.ahk, 400, 640, 1, "接收")
            delayTap(self.ahk, 550, 480, 2, "確認")

            delayTap(self.ahk, 260, 620, 2, "發送信件")
            printSleep(1)
            self.ahk.send_input('oreo0')
            delayTap(self.ahk, 600, 280, 1, "角色確認")
            self.delayKeyDown(".", 2, "開寵物背包")
            printSleep(2)
            pos = findImg("../img/13.PNG", "寵物包升段章")
            if pos is not None:
                delayTap(self.ahk, pos[0] - 100, pos[1] - 10, 2, "拿章(多)")
                self.delayKeyDown(".", 2, "關寵物背包")
            delayTap(self.ahk, 400, 550, 1, "放到信箱")
            delayTap(self.ahk, 470, 570, 1, "價錢要求")
            delayTap(self.ahk, 550, 570, 1)
            self.ahk.send_input('300000')
            pos = findImg("../img/35.PNG", "check is send screen")
            if pos is None:
                # if len(self.errImgList) > 0:
                #     for eimg in self.errImgList:
                #         notifyImg("checking", eimg)
                self.errorCnt += 1
                # notify("send screen error {}".format(self.account))
                self.extManager.addHestory([self.getCurrentTime(), self.pcName, "send screen error"])
                # notifyImg("送出", getWindow_Img(self.hwnd))
            tempSendStr = "送出"
            if self.errorCnt > 0:
                tempSendStr += " error: " + self.account
            self.extManager.addHestory([self.getCurrentTime(), self.pcName, tempSendStr])

            delayTap(self.ahk, 400, 640, 1, "發送")
            delayTap(self.ahk, 550, 480, 2, "確認")
            delayTap(self.ahk, 140, 700, 1, "結束")
            delayTap(self.ahk, 140, 700, 1, "關閉對話視窗")
        # self.delayKeyDown("`", 2, "收起寵物")

    def closeGame(self):
        self.errorCnt = 0
        pos = findImg("../img/45.PNG", "銀行NPC", samePercentage=0.01)
        if pos is not None:
            self.extManager.addHestory([self.getCurrentTime(), self.pcName, "error: 關game"])

            delayTap(self.ahk, 140, 700, 2, "關閉")
            delayTap(self.ahk, 140, 700, 2, "關閉對話視窗")
        for h in find_hwnd_by_name("新瑪奇 mabinogi", False):
            set_window_to_top(h, if_foreground=True)
            printSleep(1)
            win32gui.SendMessage(h, win32con.WM_CLOSE, 0, 0)
            delayTap(self.ahk, 550, 480, 2, "確認")
            delayTap(self.ahk, 507, 494, 2, "確認")

    def signal_handler(self, sig):
        print("pressed ctrl + c")
        if self.isNeedRecording:
            self.videoCap.stop()
        notifyImg("error-stop", getWindow_Img(self.hwnd))
        self.extManager.addHestory([self.getCurrentTime(), self.pcName, "error-stop"])
        sys.exit(0)

    def logErrImg(self, logName=""):
        self.errImgList.append(getWindow_Img(self.hwnd))

    def getHwnd(self):
        return self.hwnd

    def taskStart(self):
        signal.signal(signal.SIGINT, self.signal_handler)

        try:
            win32api.SetConsoleCtrlHandler(self.signal_handler, True)

            if self.isNeedRecording:
                self.videoCap.hwnd = 0
                self.videoCap.left = 0
                self.videoCap.top = 0
                self.videoCap.width = 1920
                self.videoCap.height = 1080
                self.videoCap.scale = 0.5

                t1 = threading.Thread(target=self.videoCap.start)
                t1.setDaemon(True)
                t1.start()

            if datetime.now().day % 3 == 0:
                self.isNeedClearExpiredItem = True
            while self.hwnd is None:
                self.initMainSubHwnd()
                printSleep(5)
            win32gui.SetForegroundWindow(self.hwnd)
            printSleep(2)
            win32gui.MoveWindow(self.hwnd, 0, 0, 1000, 800, True)
            delayTap(self.ahk, 500, 10, 0, "點擊瑪奇")
            printSleep(2)

            if self.charterCount is not 1:
                for i in range(self.charterCount, 10, 1):
                    self.charterCount = i
                    self.supplementMoneyPos = None
                    self.patWalletPos = None
                    if self.checkIsDisConnectGame() is True:
                        return False

                    self.select_charter()
                    if self.checkIsDisConnectGame() is True:
                        return False
                    self.promotion_action()
                    if self.checkIsDisConnectGame() is True:
                        return False
                    printSleep(10)

            self.charterCount = 1
            win32gui.SetForegroundWindow(self.hwnd)
            printSleep(2)
            if self.checkIsDisConnectGame() is True:
                return False
            self.select_charter()
            self.supplementMoney()

            self.closeGame()
            if self.isNeedRecording:
                self.videoCap.stop()

            return True

        except Exception as error:
            print("error: {}".format(error))
            notifyImg("error: {}".format(error), getWindow_Img(self.hwnd))
            self.extManager.addHestory([self.getCurrentTime(), self.pcName, "error: {}".format(error)])
            printSleep(3)
            input()

            return False
