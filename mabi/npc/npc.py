# NPC 半自動離場腳本
import sys
sys.path.append("../..")
from tool.wintools import *

def loopToExit():
    mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", True)
    x, y, weight, height = getWindow_W_H(0)
    newW = weight - 990
    newH = height - 835
    for h in mabi_game_hwnd_list:
        #set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
        set_window_to_top(h, if_foreground=True)
        delayTap(ahk, 1426, 722, 1, "確認OK")
        delayTap(ahk, 1884, 337, 1, "EXIT")
        delayTap(ahk, 1461, 719, 1, "確認")
        delayTap(ahk, 1461, 719, 0.5, "確認")

if __name__ == '__main__':
    ahk = AHK()
    delayTap(ahk, 1597, 960, 3, "選瑪奇")
    while True:
        pos = findImg("../img/8.PNG", "NPC 打完")
        if pos is not None:
            loopToExit()

        printSleep(5)
