import sys
sys.path.append("../..")
from pynput import keyboard
from threading import Thread
from time import sleep
from tool.wintools import *


def on_press(key, abortKey='f4'):
    global isUseMed
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    if k == "f2":
        isUseMed = True

    print('pressed %s' % (k))
    if k == abortKey:
        print('end loop ...')
        return False  # stop listener


def delayKeyDownClick(x, y, key, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    sleep(0.1)
    ahk.key_down(key)
    sleep(0.5)
    ahk.click()
    sleep(0.2)
    ahk.key_up(key)
    if description is not None:
        print("key tap x:{}, y:{}, key: {} {}".format(x, y, key, description))
    else:
        print("key tap x:{}, y:{}, key: {}".format(x, y, key))


def main_task():
    global isUseMed
    j = 0
    canAttack = False
    while True:
        pos = findImg("../img/89.PNG", samePercentage=0.1)
        if pos is not None:
            delayTap(ahk, pos[0], pos[1], 0.5)
            delayTap(ahk, 1443, 728, 0.3, "4")
            delayTap(ahk, 1443, 695, 0.3, "3")
            delayTap(ahk, 1443, 656, 0.3, "2")
            delayTap(ahk, 1443, 620, 0.3, "1")
            printSleep(7)
            delayKeyDown(ahk, "esc")
            delayTap(ahk, 1462, 725, 2)
            delayTap(ahk, 1652, 546, 1)
            printSleep(4)
            canAttack = True

           # while canAttack:
           #     for i in range(4):
           #         delayKeyDown(ahk, "2", 0.3)
           #         delayKeyDownClick(1500, 620, "Control")
           #     pos = findImg("../img/8.PNG", samePercentage=0.1)
            #    if pos is not None:
           #         canAttack = False
           #         delayTap(ahk, 1439, 723, 1)
           #         delayKeyDown(ahk, "esc")


        pos = findImg("../img/8.PNG", samePercentage=0.1)
        if pos is not None:
            delayTap(ahk, 1439, 723, 1)
            delayKeyDown(ahk, "esc")


if __name__ == '__main__':
    ahk = AHK()
    isUseMed = True
    delayTap(ahk, 1443, 728, 0.3, 'facus')
    abortKey = 't'
    listener = keyboard.Listener(on_press=on_press, abortKey=abortKey)
    listener.start()  # start to listen on a separate thread
    Thread(target=main_task, args=(), name='loop_fun', daemon=True).start()
    listener.join()  # wait for abortKey