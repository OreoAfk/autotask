import sys
sys.path.append("../..")
from pynput import keyboard
from threading import Thread
from time import sleep
from tool.wintools import *


def on_press(key, abortKey='esc'):
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    print('pressed %s' % (k))
    if k == abortKey:
        print('end loop ...')
        return False  # stop listener


def main_task():
    pass


if __name__ == '__main__':
    abortKey = 't'
    listener = keyboard.Listener(on_press=on_press, abortKey=abortKey)
    listener.start()  # start to listen on a separate thread
    Thread(target=main_task, args=(), name='loop_fun', daemon=True).start()
    listener.join()  # wait for abortKey