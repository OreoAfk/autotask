import sys

sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from pynput import keyboard
import subprocess
from tool.VideoCap import VideoCap
import threading

if __name__ == '__main__':
    ahk = AHK()
    mabi_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", True)
    delayTap(ahk, 1426, 662, 1, "點擊瑪奇")
    printSleep(3)

    for hwnd in mabi_hwnd_list:
        set_window_to_top(hwnd, if_foreground=True)
        delayTap(ahk, 1055, 1015, 1, "menu")
        delayTap(ahk, 1072, 936, 1, "登出")
        delayTap(ahk, 1458, 724, 1, "確定")
        printSleep(1)
    printSleep(3)
    for hwnd in mabi_hwnd_list:
        set_window_to_top(hwnd, if_foreground=True)
        delayTap(ahk, 1055, 1015, 1, "登入")
        printSleep(2)
    printSleep(10)
    for hwnd in mabi_hwnd_list:
        # delayTap(ahk, 982, 992, 1, "")
        set_window_to_top(hwnd, if_foreground=True)
        delayTap(ahk, 1465, 738, 1, "確定")
        printSleep(2)

    input("waiting")
    delayTap(ahk, 1426, 662, 1, "點擊瑪奇")
    for hwnd in mabi_hwnd_list:
        printSleep(1)
        set_window_to_top(hwnd, if_foreground=True)
        delayTap(ahk, 1265, 994, 1, "情報")
        delayTap(ahk, 1264, 952, 1, "點擊瑪奇")
        delayTap(ahk, 1280, 918, 1, "下")
        delayTap(ahk, 1280, 918, 1, "下")
        delayTap(ahk, 1352, 847, 1, "倫困")
        delayTap(ahk, 1749, 530, 1, "往地下城移動")
        delayTap(ahk, 1464, 724, 1, "確定")