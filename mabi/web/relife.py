import sys

sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from pynput import keyboard
import subprocess
from tool.VideoCap import VideoCap
import threading

def delayKeyDownClick(x, y, key, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    ahk.key_down(key)
    ahk.click()
    ahk.key_up(key)
    if description is not None:
        print("key tap x:{}, y:{}, key: {} {}".format(x, y, key, description))
    else:
        print("key tap x:{}, y:{}, key: {}".format(x, y, key))

def closeGame(hwnd):
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    delayTap(ahk, 1471, 723, 2, "確認")
    delayTap(ahk, 1471, 723, 1, "確認")
    delayTap(ahk, 1456, 740, 2, "確認(斷線頁面)")
    printSleep(3)

def cpuSaveing(isOpen):
    # "Desktop\BES_1.7.8"
    # p = subprocess.Popen(args=["BES.exe"], cwd ="D:\Script", shell = True)
    # C:/Users/oreo/Desktop/BES_1.7.8/BES.exe C:\Nexon\Mabinogi\Client.exe, 10;400
    DETACHED_PROCESS = 0x00000008

    if isOpen:
        p = subprocess.Popen(['C:/Users/oreo/Desktop/BES_1.7.8/BES.exe', 'C:\\Nexon\\Mabinogi\\Client.exe', '90;400'],
                             shell=False, stdin=None, stdout=None, stderr=None,
                             close_fds=True, creationflags=DETACHED_PROCESS)
    else:
        p = subprocess.Popen(['C:/Users/oreo/Desktop/BES_1.7.8/BES.exe', 'C:\\Nexon\\Mabinogi\\Client.exe', '10;400'],
                             shell=False, stdin=None, stdout=None, stderr=None,
                             close_fds=True, creationflags=DETACHED_PROCESS)
    # p.wait
    # stdout, stderr = p.communicate()
    # error = p.returncode
    # return error


if __name__ == '__main__':
    ahk = AHK()
    x, y, weight, height = getWindow_W_H(0)
    newW = weight - 990
    newH = height - 835
    cpuSaveing(False)
    local_mabi_hwnds = find_hwnd_by_name("新瑪奇 mabinogi", True)

    for h in local_mabi_hwnds:

        set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)

        delayTap(ahk, 1293, 275, 2, "點瑪奇")
        delayTap(ahk, 1055, 1015, 1, "menu")
        pos = findImg("../img/4.PNG", description="智能系統")
        if pos is not None:
            delayKeyDown(ahk, "[", 1, description="關閉智能系統")
        printSleep(2)
        pos = findImg("../img/15.PNG", "開背包")
        if pos is not None:
            delayKeyDown(ahk, "i", 1, "關背包")

        if len(sys.argv) > 1:
            delayKeyDown(ahk, "c", 1)
            delayTap(ahk, 1081, 372, 2, "增加資訊")
            delayTap(ahk, 1272, 529, 1, "執行重生")
            delayTap(ahk, 1495, 744, 1, "確認")
            delayTap(ahk, 1423, 447, 23, "娜歐")
            delayTap(ahk, 1068, 941, 5, "繼續")
            delayTap(ahk, 1068, 941, 0.5, "繼續")
            delayTap(ahk, 1068, 941, 0.5, "重生")
            delayTap(ahk, 1068, 941, 0.5, "繼續")
            delayTap(ahk, 1068, 941, 0.5, "繼續")
            delayTap(ahk, 1068, 941, 0.5, "重生")
            delayTap(ahk, 963, 877, 2, "下一步")
            delayTap(ahk, 1045, 958, 3, "下一步")
            delayTap(ahk, 1272, 382, 2, "生活才能")
            delayTap(ahk, 1054, 569, 1, "旅行")
            delayTap(ahk, 1617, 855, 2, "選擇才能")
            delayTap(ahk, 1429, 781, 2, "買")
            delayTap(ahk, 1068, 941, 2, "重生")
            delayTap(ahk, 1068, 941, 0.5, "繼續")
            delayTap(ahk, 1068, 941, 0.5, "繼續")
            delayTap(ahk, 1068, 941, 0.5, "重生")

            delayTap(ahk, 1435, 746, 10, "升50")
            delayKeyDown(ahk, "c", 1)
        pos = findImg("../img/15.PNG", "開背包")
        if pos is None:
            delayKeyDown(ahk, "i", 1, "開背包")

        delayTap(ahk, 1864, 656, 2, "堆疊")
        for i in range(22):
            delayKeyDownClick(1850, 724, "Control", 0, "VIP 卷")

        delayTap(ahk, 1864, 656, 2, "堆疊")
        ahk.right_click()
        delayKeyDown(ahk, "i", 2, "關背包")

        closeGame(h)