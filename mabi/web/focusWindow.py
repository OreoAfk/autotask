import sys
sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform

if __name__ == '__main__':

    mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", True)
    x, y, weight, height = getWindow_W_H(0)
    newW = weight - 990
    newH = height - 835
    for h in mabi_game_hwnd_list:
        set_window_to_top(h, newW, newH, 1000, 800)

    mabi_game_hwnd_list = find_hwnd_by_name("Chrome", False)
    for h in mabi_game_hwnd_list:
        set_window_to_top(h, if_foreground=True)
