import sys

sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from pynput import keyboard
import subprocess
from tool.VideoCap import VideoCap
import threading

# member...
ie_hwnd = None
acc = []
pwd = []
hwnds = []


# 1920 1080
# method...

def moveChromeWindow():
    print("moveChromeWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("Chrome" in t):
            set_window_to_top(h, 0, 0, 1000, 600)
            print("move the Chrome")
            ie_hwnd = h
            break
        print(" {} {}".format(h, t))


def openChrome(daily=5):
    os.startfile("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe")
    printSleep(daily)
    moveChromeWindow()


def closeChrome():
    for i in find_hwnd_by_name("訊息", False):
        win32gui.SendMessage(i, win32con.WM_CLOSE, 0, 0)
    for i in find_hwnd_by_name("錯誤", False):
        win32gui.SendMessage(i, win32con.WM_CLOSE, 0, 0)

    ie_hwnd_list = find_hwnd_by_name("Internet Explorer", False)
    for h in ie_hwnd_list:
        win32gui.SendMessage(h, win32con.WM_CLOSE, 0, 0)
    chrome_hwnd_list = find_hwnd_by_name("Chrome", False)
    for h in chrome_hwnd_list:
        win32gui.SendMessage(h, win32con.WM_CLOSE, 0, 0)


def openMabi():
    delayTap(ahk, 112, 562, 10, "快速啟動")
    delayTap(ahk, 109, 464, 5, "瑪奇")
    printSleep(40)
    pos = findImg("../img/28.PNG", "開始遊戲", samePercentage=0.15)
    if pos is not None:
        delayTap(ahk, pos[0], pos[1], 1, "開始遊戲")
    printSleep(8)
    pos = findImg("../img/29.PNG", "同意")
    if pos is not None:
        delayTap(ahk, pos[0], pos[1], 1, "同意")


def init_account():
    global acc, pwd
    sacc = []
    spwd = []
    with open("./normal_ac.txt", 'r') as fp:
        line = fp.readline()
        while line:
            arr = line.rstrip('\n').split(",")
            if len(arr) == 2:
                sacc.append(arr[0])
                spwd.append(arr[1])
                print(arr[0], arr[1])
            elif line == "\n":
                acc.append(sacc)
                sacc = []
                pwd.append(spwd)
                spwd = []
            line = fp.readline()
    acc.append(sacc)
    pwd.append(spwd)


def moveCMDWindow():
    print("moveCMDWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("cmd" in t or "taskeng" in t):  #
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            win32gui.MoveWindow(h, 0, 600, 500, 400, True)
            print("move the cmd")


def on_press(key):
    if key == keyboard.Key.esc:
        return False  # stop listener
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys
    if k == "x":
        print('Key pressed: ' + k)
        ahk.click()
    if k in ['1', '2', 'left', 'right']:  # keys of interest
        # self.keys.append(k)  # store it in global-like variable
        print('Key pressed: ' + k)
        return False  # stop listener; remove this if want more keys


def login(ac, pw):
    closeChrome()
    printSleep(3)
    openChrome()
    printSleep(5)
    while True:
        pos = findImg("../img/92.PNG", description="Login")
        if pos is not None:
            delayTap(ahk, 927, 560, 1, " 右下登入")
            printSleep(10)
            pos = findImg("../img/93.PNG", description="Login")
            if pos is not None:
                delayTap(ahk, pos[0] - 40, pos[1] - 100, 8, " 帳號或電郵")
                ahk.send_input(ac)
                delayTap(ahk, pos[0] - 40, pos[1] - 60, 2, "密碼")
                ahk.send_input(pw)
                delayTap(ahk, pos[0], pos[1], 2, "Login")
            # delayTap(ahk, 746, 446, 2, "Login")
            openMabi()
            printSleep(60)
            break
        else:
            closeChrome()
            printSleep(3)
            openChrome()
            printSleep(5)


def cpuSaveing(isOpen):
    DETACHED_PROCESS = 0x00000008

    if isOpen:
        p = subprocess.Popen(['C:/Users/oreo/Desktop/BES_1.7.8/BES.exe', 'C:\\Nexon\\Mabinogi\\Client.exe', '90;400'],
                             shell=False, stdin=None, stdout=None, stderr=None,
                             close_fds=True, creationflags=DETACHED_PROCESS)
    else:
        p = subprocess.Popen(['C:/Users/oreo/Desktop/BES_1.7.8/BES.exe', 'C:\\Nexon\\Mabinogi\\Client.exe', '10;400'],
                             shell=False, stdin=None, stdout=None, stderr=None,
                             close_fds=True, creationflags=DETACHED_PROCESS)


def run(ac_arr, pw_arr):
    global hwnds
    local_mabi_hwnds = find_hwnd_by_name("新瑪奇 mabinogi", True)

    for i in range(len(ac_arr)):
        login(ac_arr[i], pw_arr[i])
        currentMabiHwnd = find_hwnd_by_name("新瑪奇 mabinogi", False)
        newHwnds = list(set(currentMabiHwnd) - set(hwnds))
        print("c: {}, n: {}, h: {}".format(currentMabiHwnd, newHwnds, hwnds))

        if len(newHwnds) > 0:
            hwnds = hwnds + newHwnds

    printSleep(30)
    mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", True)
    x, y, weight, height = getWindow_W_H(0)
    newW = weight - 990
    newH = height - 835
    if second != 0:
        for h in mabi_game_hwnd_list:
            set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
    else:
        for h in mabi_game_hwnd_list:
            set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)

    new_open_mabi_list = list(set(hwnds) ^ set(local_mabi_hwnds))
    if second != 0:
        new_open_mabi_list = list(set(mabi_game_hwnd_list) ^ set(local_mabi_hwnds))

    for h in reversed(new_open_mabi_list):
        set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
        delayTap(ahk, 1365, 962, 2, "channel")
        delayTap(ahk, 1365, 932, 1, "channel 13")
        delayTap(ahk, 994, 999, 1, "開始")
        delayTap(ahk, 994, 999, 1, "開始")
        printSleep(1)

    chrome_hwnd_list = find_hwnd_by_name("Chrome", False)
    for h in chrome_hwnd_list:
        set_window_to_top(h, if_foreground=True)


def closeGame(hwnd):
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    delayTap(ahk, 1471, 723, 2, "確認")
    delayTap(ahk, 1471, 723, 1, "確認")
    delayTap(ahk, 1456, 740, 2, "確認(斷線頁面)")
    printSleep(3)
    closeChrome()


if __name__ == '__main__':
    ahk = AHK()
    try:
        startTime = getCurrentDateTime("%Y/%m/%d %H:%M:%S")
        extManager = ExtManager(worksheet="holding")
        mailAccList = extManager.getRowList(1)
        moveCMDWindow()
        second = 0
        isRecodingVideo = False
        deleteFiles(path=".", rFilter='\d.*\.mp4$', oldDate=1)
        videoCap = VideoCap()
        startPos = 0
        autoMoveToBorder = True
        if len(sys.argv) > 1:
            second = int(sys.argv[1])
            if len(sys.argv) > 2:
                startPos = (int(sys.argv[2]) - 1)
        else:
            isRecodingVideo = False
        init_account()

        for ac_num in range(startPos, len(acc), 1):
            if isRecodingVideo:
                videoCap.hwnd = 0
                videoCap.left = 0
                videoCap.top = 0
                videoCap.width = 1920
                videoCap.height = 1080
                videoCap.scale = 1
                t1 = threading.Thread(target=videoCap.start)
                t1.setDaemon(True)
                t1.start()

            if second != 0:
                printSleep(5)

            if ac_num == startPos and second > 0:
                printSleep(40)
                openChrome(10)
                printSleep(10)

            run(acc[ac_num], pwd[ac_num])
            if second != 0:
                cpuSaveing(False)
                closeChrome()
                printSleep(30)

            # if second > 0:
            # p = mailAccList.index(acc[ac_num][0])
            # if second > 0:
            #     extManager.updateFormList([startTime, getCurrentDateTime("%Y/%m/%d %H:%M:%S"), ""], p + 1, 3)

            # mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)
            # for h in mabi_game_hwnd_list:
            #     set_window_to_top(h, if_foreground=True)
            #     printSleep(2)
            #     delayTap(ahk, 1055, 1015, 1, "menu")
            #     pos = findImg("../img/4.PNG", description="智能系統")
            #     if pos is not None:
            #         delayKeyDown(ahk, "[", 1, description="關閉智能系統")
            #     delayKeyDown(ahk, "i", 2)
            #     printSleep(2)
            #     pos = findImg("../img/15.PNG", description="背包已開")
            #     if pos is None:
            #         delayKeyDown(ahk, "i", 1, "開背包")
            #         printSleep(2)
            #     delayTap(ahk, 1871, 650, 1, "堆疊")
            #     delayTap(ahk, 1845, 650, 2, "消除過期")
            #     printSleep(2)
            #     pos = findImg("../img/64.PNG", "斷線", samePercentage=0.08)
            #     if pos is not None:
            #         delayTap(ahk, 1387, 820, 2, "確定")
            #     printSleep(1)
            #     if pos is not None:
            #         delayKeyDown(ahk, "i", 1)
            #     printSleep(2)

            if second != 0:
                cpuSaveing(True)
            print(hwnds)
            print(acc[ac_num])
            x, y, weight, height = getWindow_W_H(0)
            newW = weight - 990
            newH = height - 835
            if isRecodingVideo:
                printSleep(10)
                videoCap.stop()

            mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)

            if second > 0:
                printSleep(second)
                if second != 0:
                    cpuSaveing(False)
                    printSleep(5)
                for h in mabi_game_hwnd_list:
                    set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
                    printSleep(5)
                    closeGame(h)
                # p = mailAccList.index(acc[ac_num][0])
                # extManager.update(getCurrentDateTime("%Y/%m/%d %H:%M:%S"), p + 1, 5)  # left top
                hwnds = []
                closeChrome()
            elif second < 0:
                if autoMoveToBorder:
                    print("auto Move To Border")
                    for i, h in enumerate(mabi_game_hwnd_list):
                        if i == 0:
                            set_window_to_top(h, 0, 0, 1000, 800, if_foreground=True)
                        elif i == 1:
                            set_window_to_top(h, newW, 0, 1000, 800, if_foreground=True)
                        elif i == 2:
                            set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
                        elif i == 3:
                            set_window_to_top(h, 0, newH, 1000, 800, if_foreground=True)
                        else:
                            set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
                    printSleep(3)

                input("{} - waiting... until enter {}/{}".format(getCurrentDateTime("%Y/%m/%d %H:%M:%S"), ac_num + 1,
                                                                 len(acc)))
                cpuSaveing(False)
                printSleep(2)
                mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)
                if second != 0:
                    for h in mabi_game_hwnd_list:
                        set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
                        printSleep(10)
                        closeGame(h)
                else:
                    for h in mabi_game_hwnd_list:
                        set_window_to_top(h, if_foreground=True)
                        printSleep(3)
                        closeGame(h)
                hwnds = []
                closeChrome()
            print("pack {} is finish".format(ac_num))

            if second > 0:
                mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)
                while len(mabi_game_hwnd_list) > 0:
                    for h in mabi_game_hwnd_list:
                        closeGame(h)
                    printSleep(30)
                    mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)

            if is_time_between("05:00", "07:00"):
                break

        if second > 0:
            os.system('shutdown -s')
    except Exception as error:
        errLine = 'Error on line {}'.format(sys.exc_info()[-1].tb_lineno)
        errMsg = "daily open - error: line {} - {}".format(errLine, error)
        # notify(errMsg)
        notifyImg(errMsg, getWindow_Img(0))
