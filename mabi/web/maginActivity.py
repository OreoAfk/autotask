import sys
sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform

# member...
ie_hwnd = None
acc = []
pwd = []

def moveIEWindow():
    print("moveIEWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("Explorer" in t): #
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            win32gui.SetForegroundWindow(h)
            win32gui.MoveWindow(h, 0, 0, 1500, 1000, True)
            print("move the cmd")
            ie_hwnd = h
            break
        print(" {} {}".format(h, t))

def init_account():
    with open("./temp_ac.txt", 'r') as fp:
        line = fp.readline()
        while line:
            arr = line.rstrip('\n').split(",")
            if len(arr) == 2:
                acc.append(arr[0])
                pwd.append(arr[1])
                print(arr[0], arr[1])
            line = fp.readline()

if __name__ == '__main__':
    # zoom 50
    ahk = AHK()
    init_account()
    moveIEWindow()
    for i in range(len(acc)):
        delayTap(ahk, 924, 290, 1, "登入")
        delayTap(ahk, 903, 244, 7, "HK")
        delayTap(ahk, 888, 247, 7, "帳")
        ahk.send_input(acc[i])
        delayTap(ahk, 888, 271, 1, "密")
        ahk.send_input(pwd[i])
        delayTap(ahk, 883, 310, 1, "登入")
        delayTap(ahk, 885, 293, 2, "請選擇")
        delayTap(ahk, 881, 301, 1, "1")
        delayTap(ahk, 886, 315, 1, "")
        #6
        delayTap(ahk, 1014, 572, 5, "我的故事")
        delayTap(ahk, 680, 701, 1, "冒險")
        delayTap(ahk, 630, 718, 1, "魔力藥草")
        delayTap(ahk, 805, 698, 1, "放上GOOGLE翻譯")
        delayTap(ahk, 690, 700, 1, "去謎之地下城冒險")
        delayTap(ahk, 741, 702, 1, "A")
        delayTap(ahk, 741, 718, 1, "A")
        delayTap(ahk, 944, 564, 1, "ref")
        delayTap(ahk, 725, 635, 1, "確認")
        #7
        delayTap(ahk, 1014, 572, 5, "我的故事")
        delayTap(ahk, 680, 701, 1, "冒險")
        delayTap(ahk, 630, 718, 1, "魔力藥草")
        delayTap(ahk, 805, 698, 1, "放上GOOGLE翻譯")
        delayTap(ahk, 816, 699, 1, "去搭船")
        delayTap(ahk, 741, 702, 1, "A")
        delayTap(ahk, 741, 718, 1, "A")
        delayTap(ahk, 944, 564, 1, "ref")
        delayTap(ahk, 725, 635, 1, "確認")
        #1
        delayTap(ahk, 1014, 572, 5, "我的故事")
        delayTap(ahk, 839, 702, 1, "幻聽")
        delayTap(ahk, 741, 702, 1, "A")
        delayTap(ahk, 741, 718, 1, "A")
        delayTap(ahk, 944, 564, 1, "ref")
        delayTap(ahk, 725, 635, 1, "確認")
        #2
        delayTap(ahk, 1014, 572, 5, "我的故事")
        delayTap(ahk, 680, 701, 1, "冒險")
        delayTap(ahk, 739, 716, 1, "蜂蜜")
        delayTap(ahk, 741, 702, 1, "A")
        delayTap(ahk, 741, 718, 1, "A")
        delayTap(ahk, 944, 564, 1, "ref")
        delayTap(ahk, 725, 635, 1, "確認")
        #3
        delayTap(ahk, 1014, 572, 5, "我的故事")
        delayTap(ahk, 680, 701, 1, "冒險")
        delayTap(ahk, 842, 718, 1, "劇毒藥草")
        delayTap(ahk, 741, 702, 1, "A")
        delayTap(ahk, 741, 718, 1, "A")
        delayTap(ahk, 944, 564, 1, "ref")
        delayTap(ahk, 725, 635, 1, "確認")
        #4
        delayTap(ahk, 1014, 572, 5, "我的故事")
        delayTap(ahk, 680, 701, 1, "冒險")
        delayTap(ahk, 630, 718, 1, "魔力藥草")
        delayTap(ahk, 660, 701, 1, "拿給克莉絲翻譯")
        delayTap(ahk, 648, 700, 1, "可惡的茉麗安")
        delayTap(ahk, 741, 702, 1, "A")
        delayTap(ahk, 741, 718, 1, "A")
        delayTap(ahk, 944, 564, 1, "ref")
        delayTap(ahk, 725, 635, 1, "確認")
        #5
        delayTap(ahk, 1014, 572, 5, "我的故事")
        delayTap(ahk, 680, 701, 1, "冒險")
        delayTap(ahk, 630, 718, 1, "魔力藥草")
        delayTap(ahk, 660, 701, 1, "拿給克莉絲翻譯")
        delayTap(ahk, 709, 704, 1, "不....我不相信你是這樣的人")
        delayTap(ahk, 741, 702, 1, "A")
        delayTap(ahk, 741, 718, 1, "A")
        delayTap(ahk, 944, 564, 1, "ref")
        delayTap(ahk, 725, 635, 1, "確認")

        delayTap(ahk, 1481, 190, 0.5)
        delayTap(ahk, 1481, 190, 0.5)


