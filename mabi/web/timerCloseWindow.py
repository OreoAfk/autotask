import sys

sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from pynput import keyboard
import subprocess
from tool.VideoCap import VideoCap
import threading

def cpuSaveing(isOpen):
    # "Desktop\BES_1.7.8"
    # p = subprocess.Popen(args=["BES.exe"], cwd ="D:\Script", shell = True)
    # C:/Users/oreo/Desktop/BES_1.7.8/BES.exe C:\Nexon\Mabinogi\Client.exe, 10;400
    DETACHED_PROCESS = 0x00000008

    if isOpen:
        p = subprocess.Popen(['C:/Users/oreo/Desktop/BES_1.7.8/BES.exe', 'C:\\Nexon\\Mabinogi\\Client.exe', '90;400'],
                             shell=False, stdin=None, stdout=None, stderr=None,
                             close_fds=True, creationflags=DETACHED_PROCESS)
    else:
        p = subprocess.Popen(['C:/Users/oreo/Desktop/BES_1.7.8/BES.exe', 'C:\\Nexon\\Mabinogi\\Client.exe', '10;400'],
                             shell=False, stdin=None, stdout=None, stderr=None,
                             close_fds=True, creationflags=DETACHED_PROCESS)
    # p.wait
    # stdout, stderr = p.communicate()
    # error = p.returncode
    # return error

def closeIE():
    for i in find_hwnd_by_name("訊息", False):
        win32gui.SendMessage(i, win32con.WM_CLOSE, 0, 0)
    for i in find_hwnd_by_name("錯誤", False):
        win32gui.SendMessage(i, win32con.WM_CLOSE, 0, 0)

    ie_hwnd_list = find_hwnd_by_name("Internet Explorer", False)
    for h in ie_hwnd_list:
        win32gui.SendMessage(h, win32con.WM_CLOSE, 0, 0)



def closeGame(hwnd):
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    delayTap(ahk, 1471, 723, 2, "確認")
    delayTap(ahk, 1471, 723, 1, "確認")
    delayTap(ahk, 1456, 740, 2, "確認(斷線頁面)")
    printSleep(3)
    closeIE()

if __name__ == '__main__':
    ahk = AHK()
    second = 0

    if len(sys.argv) > 1:
        second = int(sys.argv[1])
    printSleep(second)
    mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)
    x, y, weight, height = getWindow_W_H(0)
    newW = weight - 990
    newH = height - 835
    if second != 0:
        cpuSaveing(False)
        printSleep(5)
    for h in mabi_game_hwnd_list:
        set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
        printSleep(3)
        closeGame(h)

    cpuSaveing(False)

    if second > 0:
        os.system('shutdown -s')