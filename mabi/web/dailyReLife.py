import sys

sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
import os
import platform
from pynput import keyboard
import subprocess
from tool.VideoCap import VideoCap
import threading
import argparse

# member...
ie_hwnd = None
acc = []
pwd = []
hwnds = []


# 1920 1080
# method...
def moveIEWindow():
    print("moveIEWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("Explorer" in t):
            set_window_to_top(h, 0, 0, 1000, 600)
            # win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            # win32gui.SetForegroundWindow(h)
            # win32gui.MoveWindow(h, 0, 0, 1000, 600, True)
            print("move the ie")
            # delayTap(ahk, 100, 50, 2,"tap ie")
            ie_hwnd = h
            break
        print(" {} {}".format(h, t))


def init_account():
    global acc, pwd
    sacc = []
    spwd = []
    with open("./normal_ac.txt", 'r') as fp:
        line = fp.readline()
        while line:
            arr = line.rstrip('\n').split(",")
            if len(arr) == 2:
                sacc.append(arr[0])
                spwd.append(arr[1])
                print(arr[0], arr[1])
            elif line == "\n":
                acc.append(sacc)
                sacc = []
                pwd.append(spwd)
                spwd = []
            line = fp.readline()
    acc.append(sacc)
    pwd.append(spwd)


def closeIE():
    for i in find_hwnd_by_name("訊息", False):
        win32gui.SendMessage(i, win32con.WM_CLOSE, 0, 0)
    for i in find_hwnd_by_name("錯誤", False):
        win32gui.SendMessage(i, win32con.WM_CLOSE, 0, 0)

    ie_hwnd_list = find_hwnd_by_name("Internet Explorer", False)
    for h in ie_hwnd_list:
        win32gui.SendMessage(h, win32con.WM_CLOSE, 0, 0)


def moveCMDWindow():
    print("moveCMDWindow")
    for h, t in get_hwnd_and_title_list().items():
        if t is not None and ("cmd" in t or "taskeng" in t):  #
            win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
            win32gui.MoveWindow(h, 0, 600, 500, 400, True)
            print("move the cmd")


def openIE(daily=5):
    closeIE()
    os.startfile("C:\Program Files (x86)\Internet Explorer\iexplore.exe")
    printSleep(daily)
    isNotOpen = True
    while isNotOpen:
        pos = findImg("../img/42.PNG", "BF 已載入", samePercentage=0.1)
        if pos is not None:
            isNotOpen = False
        else:
            closeIE()
            os.startfile("C:\Program Files (x86)\Internet Explorer\iexplore.exe")
            printSleep(10, "登入waiting")
    moveIEWindow()


def on_press(key):
    if key == keyboard.Key.esc:
        return False  # stop listener
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys
    if k == "x":
        print('Key pressed: ' + k)
        ahk.click()
    if k in ['1', '2', 'left', 'right']:  # keys of interest
        # self.keys.append(k)  # store it in global-like variable
        print('Key pressed: ' + k)
        return False  # stop listener; remove this if want more keys


def login(ac, pw):
    tryLogin = True
    stepCnt = 0
    loopCnt = 0
    while tryLogin:
        if loopCnt > 8:
            raise Exception("loop on login: step {}".format(stepCnt))
        print("step: {}".format(stepCnt))

        openIE()
        loopCnt += 1
        printSleep(2)
        if stepCnt is 0:
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 1, " 右下登入")
                delayTap(ahk, pos[0], pos[1], 1, " 右下登入")
                printSleep(3)
                stepCnt = 1
                loopCnt = 0
            else:
                pos = findImg("../img/26.PNG", "登出", samePercentage=0.1)
                if pos is not None:
                    delayTap(ahk, pos[0], pos[1], 1, "登出")
                    printSleep(3)
                    pos = findImg("../img/27.PNG", "確定", samePercentage=0.12)
                    if pos is not None:
                        delayTap(ahk, pos[0], pos[1], 1, "確定")
                        printSleep(3)
                continue

        if stepCnt is 1:
            pos = findImg("../img/23.PNG", "登入(綠)", samePercentage=0.02)
            if pos is not None:
                delayTap(ahk, pos[0] - 150, pos[1] - 20, 1, " 帳號或電郵")
                ahk.send_input(ac)
                delayTap(ahk, pos[0] - 150, pos[1] + 10, 1, "密碼")
                ahk.send_input(pw)
                delayTap(ahk, 628, 228, 1, "Login")
                delayTap(ahk, 477, 368, 3, "確定")
                delayTap(ahk, 713, 562, 1, "cancel")
                printSleep(3)
                stepCnt = 2
                loopCnt = 0
            else:
                stepCnt = 0
                continue

        if stepCnt is 2:
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                stepCnt = 0
                continue
            pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.08)
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 1, "快速啟動")
                delayTap(ahk, pos[0], pos[1] - 100, 3, "帳1")
                printSleep(3)
                moveCMDWindow()
                closeIE()
                mabi_hwnd_list = find_hwnd_by_name("mabinogi", True)
                printSleep(3)
                for mh in mabi_hwnd_list:
                    set_window_to_top(mh, if_foreground=True)
                printSleep(3)
                stepCnt = 3
                loopCnt = 0
            else:
                continue

        if stepCnt is 3:
            if loopCnt > 2:
                pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.08)
                if pos is not None:
                    delayTap(ahk, pos[0], pos[1], 1, "快速啟動")
                    delayTap(ahk, pos[0], pos[1] - 100, 3, "帳1")
                    printSleep(3)
                    moveCMDWindow()
                    closeIE()
                    mabi_hwnd_list = find_hwnd_by_name("mabinogi", True)
                    printSleep(3)
                    for mh in mabi_hwnd_list:
                        set_window_to_top(mh, if_foreground=True)
                    printSleep(3)
                    stepCnt = 3
                    loopCnt = 0
                else:
                    continue

            closeIE()
            pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
            if pos is not None:
                stepCnt = 0
                continue
            mabi_hwnd_list = find_hwnd_by_name("mabinogi", True)
            printSleep(3)
            for mh in mabi_hwnd_list:
                set_window_to_top(mh, if_foreground=True)
            printSleep(2)
            pos = findImg("../img/28.PNG", "開始遊戲", samePercentage=0.15)
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 1, "開始遊戲")
                printSleep(3)
                stepCnt = 4
                loopCnt = 0
            else:
                continue

        if stepCnt is 4:
            pos = findImg("../img/29.PNG", "同意")
            if pos is not None:
                delayTap(ahk, pos[0], pos[1], 1, "同意")
                printSleep(5)
                stepCnt = 5
                loopCnt = 0
            else:
                for mh in find_hwnd_by_name("mabinogi", True):
                    set_window_to_top(mh, if_foreground=True)
                printSleep(2)
                pos = findImg("../img/28.PNG", "開始遊戲", samePercentage=0.15)
                if pos is not None:
                    stepCnt = 3
                continue

        tryLogin = False
        stepCnt = 0


def cpuSaveing(isOpen):
    # "Desktop\BES_1.7.8"
    # p = subprocess.Popen(args=["BES.exe"], cwd ="D:\Script", shell = True)
    # C:/Users/oreo/Desktop/BES_1.7.8/BES.exe C:\Nexon\Mabinogi\Client.exe, 10;400
    DETACHED_PROCESS = 0x00000008

    if isOpen:
        p = subprocess.Popen(['C:/Users/oreo/Desktop/BES_1.7.8/BES.exe', 'C:\\Nexon\\Mabinogi\\Client.exe', '90;400'],
                             shell=False, stdin=None, stdout=None, stderr=None,
                             close_fds=True, creationflags=DETACHED_PROCESS)
    else:
        p = subprocess.Popen(['C:/Users/oreo/Desktop/BES_1.7.8/BES.exe', 'C:\\Nexon\\Mabinogi\\Client.exe', '10;400'],
                             shell=False, stdin=None, stdout=None, stderr=None,
                             close_fds=True, creationflags=DETACHED_PROCESS)
    # p.wait
    # stdout, stderr = p.communicate()
    # error = p.returncode
    # return error


def run(ac_arr, pw_arr):
    global hwnds
    local_mabi_hwnds = find_hwnd_by_name("新瑪奇 mabinogi", True)

    for i in range(len(ac_arr)):
        # closeIE()
        # openIE()
        # pos = findImg("../img/41.PNG", "登入(橙)", samePercentage=0.045)
        # if pos is not None:
        #     delayTap(ahk, 921, 553, 1, "登入")
        # else:
        #     pos = findImg("../img/26.PNG", "登出", samePercentage=0.1)
        #     if pos is not None:
        #         delayTap(ahk, pos[0], pos[1], 1, "登出")
        #         delayTap(ahk, 454, 404, 3, "確定")
        #         printSleep(3)
        #         closeIE()
        #         openIE()
        #         delayTap(ahk, 921, 553, 5, "登入")
        #
        # printSleep(3)
        # if i == 0:
        #     delayTap(ahk, 417, 180, 2, "登入模式")
        #     delayTap(ahk, 409, 132, 1, "帳號密碼")
        # delayTap(ahk, 446, 227, 1, "帳: {}".format(ac_arr[i]))
        # ahk.send_input(ac_arr[i])
        # delayTap(ahk, 446, 256, 1, "pw_arr :{}".format(pw_arr[i]))
        # ahk.send_input(pw_arr[i])
        # delayTap(ahk, 578, 242, 1, "Login")
        # delayTap(ahk, 477, 368, 3, "確定")
        # delayTap(ahk, 713, 562, 1, "cancel")
        #
        # printSleep(3)
        # pos = findImg("../img/25.PNG", "快速啟動", samePercentage=0.08)
        # if pos is None:
        #     closeIE()
        #     openIE()
        #     printSleep(5)
        # delayTap(ahk, 153, 551, 3, "快速啟動")
        # delayTap(ahk, 186, 453, 1, "帳1")
        # printSleep(5)
        # moveCMDWindow()
        # mabi_hwnd_list = find_hwnd_by_name("mabinogi", True)
        # printSleep(3)
        # for mh in mabi_hwnd_list:
        #     # win32gui.ShowWindow(mh, win32con.SW_SHOWNORMAL)
        #     # win32gui.SetForegroundWindow(mh)
        #     set_window_to_top(hwnd=mh)
        # printSleep(3)
        # pos = findImg("../img/28.PNG", "開始遊戲", samePercentage=0.15)
        # if pos is None:
        #     closeIE()
        #     openIE()
        #     delayTap(ahk, 153, 551, 3, "快速啟動")
        #     delayTap(ahk, 186, 453, 1, "帳1")
        #     printSleep(5)
        #     moveCMDWindow()
        #     mabi_hwnd_list = find_hwnd_by_name("mabinogi", True)
        #     printSleep(3)
        #     for mh in mabi_hwnd_list:
        #         win32gui.ShowWindow(mh, win32con.SW_SHOWNORMAL)
        #         win32gui.SetForegroundWindow(mh)
        #     printSleep(3)
        #     delayTap(ahk, 1281, 728, 1, "開始遊戲")
        #     delayTap(ahk, 995, 661, 2, "確定")
        # else:
        #     delayTap(ahk, 1281, 728, 1, "開始遊戲")
        #     delayTap(ahk, 995, 661, 1, "確定")
        # printSleep(15)
        # moveIEWindow()
        # delayTap(ahk, 929, 555, 4, "登出")
        # delayTap(ahk, 454, 404, 3, "確定")
        # printSleep(3)
        login(ac_arr[i], pw_arr[i])
        currentMabiHwnd = find_hwnd_by_name("新瑪奇 mabinogi", False)
        newHwnds = list(set(currentMabiHwnd) - set(hwnds))
        print("c: {}, n: {}, h: {}".format(currentMabiHwnd, newHwnds, hwnds))

        if len(newHwnds) > 0:
            hwnds = hwnds + newHwnds

    printSleep(30)
    mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", True)
    x, y, weight, height = getWindow_W_H(0)
    newW = weight - 990
    newH = height - 835
    if second != 0:
        for h in mabi_game_hwnd_list:
            set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
    else:
        for h in mabi_game_hwnd_list:
            set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)

    # printSleep(30)
    # mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)
    new_open_mabi_list = list(set(hwnds) ^ set(local_mabi_hwnds))
    if second != 0:
        new_open_mabi_list = list(set(mabi_game_hwnd_list) ^ set(local_mabi_hwnds))

    for h in reversed(new_open_mabi_list):
        set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
        delayTap(ahk, 1365, 962, 2, "channel")
        delayTap(ahk, 1365, 932, 1, "channel 13")
        delayTap(ahk, 994, 999, 1, "開始")
        delayTap(ahk, 994, 999, 1, "開始")
        printSleep(1)

    chrome_hwnd_list = find_hwnd_by_name("Chrome", False)
    for h in chrome_hwnd_list:
        set_window_to_top(h, if_foreground=True)


def closeGame(hwnd):
    win32gui.SendMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    delayTap(ahk, 1471, 723, 2, "確認")
    delayTap(ahk, 1471, 723, 1, "確認")
    delayTap(ahk, 1456, 740, 2, "確認(斷線頁面)")
    printSleep(3)
    closeIE()

def delayKeyDownClick(x, y, key, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    ahk.key_down(key)
    ahk.click()
    ahk.key_up(key)
    if description is not None:
        print("key tap x:{}, y:{}, key: {} {}".format(x, y, key, description))
    else:
        print("key tap x:{}, y:{}, key: {}".format(x, y, key))



if __name__ == '__main__':
    ahk = AHK()
    try:
        startTime = getCurrentDateTime("%Y/%m/%d %H:%M:%S")
        extManager = ExtManager(worksheet="holding")
        mailAccList = extManager.getRowList(1)
        moveCMDWindow()
        second = 0
        isRecodingVideo = False
        # isRelife = datetime.now().day % 2 > 0
        deleteFiles(path=".", rFilter='\d.*\.mp4$', oldDate=1)
        videoCap = VideoCap()
        startPos = 0
        if len(sys.argv) > 1:
            second = int(sys.argv[1])
            if len(sys.argv) > 2:
                startPos = (int(sys.argv[2]) - 1)
        else:
            isRecodingVideo = False
        init_account()
        loopNum = 0
        # if second > 0:
        #     cpuSaveing(False)

        for ac_num in range(startPos, len(acc), 1):
            if isRecodingVideo:
                videoCap.hwnd = 0
                videoCap.left = 0
                videoCap.top = 0
                videoCap.width = 1920
                videoCap.height = 1080
                videoCap.scale = 1
                t1 = threading.Thread(target=videoCap.start)
                t1.setDaemon(True)
                t1.start()

            if second != 0:
                printSleep(5)

            if ac_num == startPos and second > 0:
                printSleep(40)
                openIE(10)
                printSleep(10)


            run(acc[ac_num], pwd[ac_num])
            if second != 0:
                cpuSaveing(False)
            closeIE()
            printSleep(30)

            if second > 0:
                p = mailAccList.index(acc[ac_num][0])
                # cpuSaveing(False)
                # if second > 0:
                #     extManager.updateFormList([startTime, getCurrentDateTime("%Y/%m/%d %H:%M:%S"), ""], p + 1, 3)

                mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)
                for h in mabi_game_hwnd_list:
                    set_window_to_top(h, if_foreground=True)
                    printSleep(2)
                    delayTap(ahk, 1055, 1015, 1, "menu")
                    pos = findImg("../img/4.PNG", description="智能系統")
                    if pos is not None:
                        delayKeyDown(ahk, "[", 1, description="關閉智能系統")
                    delayKeyDown(ahk, "i", 2)
                    printSleep(2)
                    pos = findImg("../img/15.PNG", description="背包已開")
                    if pos is None:
                        delayKeyDown(ahk, "i", 1, "開背包")
                        printSleep(2)
                    delayTap(ahk, 1871, 650, 1, "堆疊")
                    delayTap(ahk, 1845, 650, 2, "消除過期")
                    printSleep(2)
                    pos = findImg("../img/64.PNG", "斷線", samePercentage=0.08)
                    if pos is not None:
                        delayTap(ahk, 1387, 820, 2, "確定")
                    printSleep(1)
                    if pos is not None:
                        delayKeyDown(ahk, "i", 1)
                    printSleep(2)


            print(hwnds)
            print(acc[ac_num])
            x, y, weight, height = getWindow_W_H(0)
            newW = weight - 990
            newH = height - 835
            if isRecodingVideo:
                printSleep(10)
                videoCap.stop()

            mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", False)

            for h in mabi_game_hwnd_list:

                set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)

                delayTap(ahk, 1293, 275, 2, "點瑪奇")
                delayTap(ahk, 1055, 1015, 1, "menu")
                pos = findImg("../img/4.PNG", description="智能系統")
                if pos is not None:
                    delayKeyDown(ahk, "[", 1, description="關閉智能系統")
                printSleep(2)
                pos = findImg("../img/15.PNG", "開背包")
                if pos is not None:
                    delayKeyDown(ahk, "i", 1, "關背包")


                delayKeyDown(ahk, "c", 1)
                delayTap(ahk, 1081, 372, 2, "增加資訊")
                delayTap(ahk, 1272, 529, 1, "執行重生")
                delayTap(ahk, 1495, 744, 1, "確認")
                printSleep(0.5)
                pos = findImg("../img/67.PNG", description="不可重生", samePercentage=0.06)

                delayKeyDown(ahk, "c", 1)

                if pos is None:
                    delayTap(ahk, 1423, 447, 23, "娜歐")
                    delayTap(ahk, 1068, 941, 5, "繼續")
                    delayTap(ahk, 1068, 941, 0.5, "繼續")
                    delayTap(ahk, 1068, 941, 0.5, "重生")
                    delayTap(ahk, 1068, 941, 0.5, "繼續")
                    delayTap(ahk, 1068, 941, 0.5, "繼續")
                    delayTap(ahk, 1068, 941, 0.5, "重生")
                    delayTap(ahk, 963, 877, 2, "下一步")
                    delayTap(ahk, 1045, 958, 3, "下一步")
                    delayTap(ahk, 1272, 382, 2, "生活才能")
                    delayTap(ahk, 1054, 569, 1, "旅行")
                    delayTap(ahk, 1617, 855, 2, "選擇才能")
                    delayTap(ahk, 1429, 781, 2, "買")
                    delayTap(ahk, 1068, 941, 2, "重生")
                    delayTap(ahk, 1068, 941, 0.5, "繼續")
                    delayTap(ahk, 1068, 941, 0.5, "繼續")
                    delayTap(ahk, 1068, 941, 0.5, "重生")

                    delayTap(ahk, 1435, 746, 13, "升50")
                    delayKeyDown(ahk, "c", 1)

                printSleep(3)
                pos = findImg("../img/15.PNG", "開背包")
                if pos is None:
                    delayKeyDown(ahk, "i", 1, "開背包")

                loopNum = loopNum + 1
                fileName = "{}.png".format(loopNum)
                screenSave(path="screen", fileName=fileName)

                delayTap(ahk, 1864, 656, 5, "堆疊")
                ahk.right_click()
                printSleep(2)
                for i in range(22):
                    delayKeyDownClick(1850, 724, "Control", 0, "VIP 卷")

                delayTap(ahk, 1864, 656, 2, "堆疊")
                ahk.right_click()
                delayKeyDown(ahk, "i", 2, "關背包")

                pos = findImg("../img/65.PNG", description="活動ICON")
                delayTap(ahk, pos[0], pos[1], 2, "活動ICON")
                for i in range(20):
                    delayTap(ahk, 1447, 937, 0, "抽獎")

                closeGame(h)
                printSleep(3)
                closeIE()

            #if len(mabi_game_hwnd_list) > 1:
            #    windowChangeIp()
            #else:
            #    if ac_num % 5 == 0:
            #        windowChangeIp()

        for h in find_hwnd_by_name("新瑪奇 mabinogi", False):
            set_window_to_top(h, newW, newH, 1000, 800, if_foreground=True)
            printSleep(5)
            closeGame(h)



        os.system('shutdown -s')
    except Exception as error:
        print(error)
        errLine = 'Error on line {}'.format(sys.exc_info()[-1].tb_lineno)
        errMsg = "daily open - error: line {} - {}".format(errLine, error)
        # notify(errMsg)
        notifyImg(errMsg, getWindow_Img(0))
