import sys
sys.path.append("..")
from tool.wintools import *

hwnd = None
hwnds = []
hwnd_main = None
hwnd_sub = None
hwnd_title = dict()
gameName = "新瑪奇 mabinogi"

def get_all_hwnd(h, mouse):
    if win32gui.IsWindow(h) and win32gui.IsWindowEnabled(h) and win32gui.IsWindowVisible(h):
        hwnd_title.update({h: win32gui.GetWindowText(h)})

def getWindow_Img(hwnd):
    # 將 hwnd 換成 WindowLong
    s = win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE)
    win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, s | win32con.WS_EX_LAYERED)
    # 判斷視窗是否最小化
    show = win32gui.IsIconic(hwnd)
    # 將視窗圖層屬性改變成透明
    # 還原視窗並拉到最前方
    # 取消最大小化動畫
    # 取得視窗寬高
    if show == 1:
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 0)
        win32gui.SetLayeredWindowAttributes(hwnd, 0, 0, win32con.LWA_ALPHA)
        win32gui.ShowWindow(hwnd, win32con.SW_RESTORE)
        x, y, width, height = getWindow_W_H(hwnd)
        # 創造輸出圖層
    hwindc = win32gui.GetWindowDC(hwnd)
    srcdc = win32ui.CreateDCFromHandle(hwindc)
    memdc = srcdc.CreateCompatibleDC()
    bmp = win32ui.CreateBitmap()
    # 取得視窗寬高
    x, y, width, height = getWindow_W_H(hwnd)
    # 如果視窗最小化，則移到Z軸最下方
    if show == 1: win32gui.SetWindowPos(hwnd, win32con.HWND_BOTTOM, x, y, width, height, win32con.SWP_NOACTIVATE)
    # 複製目標圖層，貼上到 bmp
    bmp.CreateCompatibleBitmap(srcdc, width, height)
    memdc.SelectObject(bmp)
    memdc.BitBlt((0, 0), (width, height), srcdc, (8, 3), win32con.SRCCOPY)
    # 將 bitmap 轉換成 np
    signedIntsArray = bmp.GetBitmapBits(True)
    img = np.fromstring(signedIntsArray, dtype='uint8')
    img.shape = (height, width, 4)  # png，具有透明度的
    # 釋放device content
    srcdc.DeleteDC()
    memdc.DeleteDC()
    win32gui.ReleaseDC(hwnd, hwindc)
    win32gui.DeleteObject(bmp.GetHandle())
    # 還原目標屬性
    if show == 1:
        win32gui.SetLayeredWindowAttributes(hwnd, 0, 255, win32con.LWA_ALPHA)
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 1)
    # 回傳圖片
    return img


# def findImg(file_name, description=None, hw=None):
#     if hw is None:
#         hw = hwnd
#     frame = getWindow_Img(hw)
#     frame2 = frame[:, :, :3]
#     print(file_name)
#     template = cv2.imread(file_name)
#     if template is None:
#         print("openv template is null")
#         return None
#     result = cv2.matchTemplate(frame2, template, cv2.TM_SQDIFF_NORMED)
#     c, w, h = template.shape[::-1]
#     min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
#     if min_val > 0.01:
#         if description is not None:
#             print("-{} not find".format(description))
#         return None
#     top_left = min_loc
#     center_point = (top_left[0] + int(w / 2), top_left[1] + int(h / 2))
#     print("-{} x:{}, y:{}, h:{}, w:{}".format(description, center_point[0], center_point[1], h, w))
#
#     return center_point

def initMainSubHwnd():
    win32gui.EnumWindows(get_all_hwnd, 0)
    global hwnd_main, hwnd_sub, hwnd
    for h, t in hwnd_title.items():
        if t == gameName:
            # if hwnd_main is None and findImg("./img/10.PNG", "主帳", h):
            #     hwnd_main = h
            # elif hwnd_sub is None and findImg("./img/9.PNG", "副帳", h):
            #     hwnd_sub = h
            # else:
            hwnds.append(h)

if __name__ == '__main__':
    initMainSubHwnd()
    get_hwnd_and_title_list()
    x, y, weight, height = getWindow_W_H(0)
    newW = weight - 990
    newH = height - 835
    for h in hwnds:
        # win32gui.SetForegroundWindow(h)
        # win32gui.ShowWindow(h, win32con.SW_SHOWNORMAL)
        # win32gui.MoveWindow(h, newW, newH, 1000, 800, True)
        set_window_to_top(h, newW, newH, 1000, 800)