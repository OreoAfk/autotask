import sys
sys.path.append("../..")
from pynput import keyboard
from threading import Thread
from time import sleep
from tool.wintools import *


def on_press(key, abortKey='esc'):
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    print('pressed %s' % (k))
    if k == abortKey:
        print('end loop ...')
        return False  # stop listener


def loop_fun():
    while True:
        delayTap(ahk, 1869, 658, 1)
        delayTap(ahk, 1446, 789, 1)


if __name__ == '__main__':
    ahk = AHK()
    abortKey = 't'
    listener = keyboard.Listener(on_press=on_press, abortKey=abortKey)
    listener.start()  # start to listen on a separate thread
    Thread(target=loop_fun, args=(), name='loop_fun', daemon=True).start()

    listener.join()  # wait for abortKey