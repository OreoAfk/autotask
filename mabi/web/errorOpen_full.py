import sys
sys.path.append("../..")

from mabi.promotion.sheetEdit import ExtManager
import re
import sys
from tool.wintools import *
from mabi.promotion import loopPromotion
from mabi.promotion.orignal_promotion import MabiPromotion


fullAcAcc = []
fullAcPwd = []

def initFullAcc():
    global fullAcAcc, fullAcPwd
    with open("./account_full.txt", 'r') as fp:
        line = fp.readline()
        while line:
            arr = line.rstrip('\n').split(",")
            if len(arr) == 2:
                fullAcAcc.append(arr[0])
                fullAcPwd.append(arr[1])
                print(arr[0], arr[1])
            line = fp.readline()

def resizeMabiWindow():
    mabi_game_hwnd_list = find_hwnd_by_name("新瑪奇 mabinogi", True)
    for h in mabi_game_hwnd_list:
        set_window_to_top(h, 0, 0, 1000, 800, if_foreground=True)
        delayTap(ahk, 500, 10, 0, "點擊瑪奇")

if __name__ == '__main__':
    initFullAcc()
    # extManager = ExtManager(updateForm="history")
    # # extManager.addHestory(["1", "2"])
    # # extManager.updateFormState("oreo", 1, 1) # left top
    # original_list = extManager.getRowDice([1, 2])
    # print(original_list)

    # tempDict = dict.fromkeys(original_list[1])
    #
    # for key, value in tempDict.items():
    #     tempDict[key] = []
    #
    # for i in range(len(original_list[1])):
    #     text = original_list[2][i]
    #     if "error" in text:
    #         tempDict[original_list[1][i]].append(text)
    # print(tempDict)

    # finalaDict = dict()
    # for key, value in tempDict.items():
    #     pcArray = []
    #     acArray = []
    #     for text in value:
    #         if "gmail" in text:
    #             acArray = re.findall(r'\S+@\S+', text)
    #             pcArray.append(acArray)
    #         else:
    #             if len(pcArray) > 0:
    #                 pcArray[-1].append(text)
    #     if len(pcArray) > 0:
    #         finalaDict[key] = pcArray

    # print(finalaDict)

    ahk = AHK()
    loopPromotion.ahk = ahk
    MabiPromotion.ahk = ahk
    # for key, value in finalaDict.items():
    for index, ac in enumerate(fullAcAcc):
        print("acc: {}".format(ac))
        loopPromotion.login(ac, fullAcPwd[index])
        loopPromotion.closeIE()
        printSleep(20)
        loopPromotion.moveCMDWindow()
        printSleep(10)
        resizeMabiWindow()
        ma = MabiPromotion()
        for charter in range(9, 0, -1):
            ma.charterCount = charter
            print(ma.charterCount)
            ma.select_charter(needCheck=False)
            print("acc: {} - {}".format(ac, charter))
            input("waiting")
            printSleep(2)
            delayTap(ahk, 500, 10, 0, "點擊瑪奇")
            delayTap(ahk, 120, 770, 1, "menu")
            delayTap(ahk, 120, 680, 1, "登出")
            delayTap(ahk, 550, 480, 1, "確定")
            printSleep(3)

        printSleep(3)
        ma.closeGame()