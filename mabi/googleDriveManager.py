from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from pydrive.files import GoogleDriveFileList
import googleapiclient.errors

import ast
import json
import requests
import io

import sys
sys.path.append("../..")
from mabi.promotion.sheetEdit import ExtManager
import win32api
from tool.wintools import *
from tool import wintools
from PIL import Image



class GoogleDriveManager:

    def __init__(self):
        GoogleAuth.DEFAULT_SETTINGS['client_config_file'] = "./json/client_secrets.json"
        gauth = GoogleAuth()
        if os.path.isfile("mycreds.txt") :
            gauth.LoadCredentialsFile("mycreds.txt")
        if gauth.credentials is None:
            gauth.LocalWebserverAuth()
        elif gauth.access_token_expired:
            gauth.Refresh()
        else:
            gauth.Authorize()
        gauth.SaveCredentialsFile("mycreds.txt")
        # gauth.LocalWebserverAuth()
        drive = GoogleDrive(gauth)

        self.gauth = gauth
        self.drive = drive

    def upload(self, file):
        pass

    def createFolder(self, name, parent_folder_id=None):
        folder_name = name
        params = {"title": folder_name, "mimeType": "application/vnd.google-apps.folder"}
        folder = self.drive.CreateFile(params)
        folder.Upload()

    def createSubFoldre(self, folder, sfldname):
        new_folder = self.drive.CreateFile({'title': '{}'.format(sfldname),
                                       'mimeType': 'application/vnd.google-apps.folder'})
        if folder is not None:
            new_folder['parents'] = [{u'id': folder['id']}]
        new_folder.Upload()
        return new_folder

    def getFolderID(self, folder_name, parent_folder_id="root"):
        file_list = self.getFolderFullList(parent_folder_id, False)
        for file in file_list:
            if file["title"] == folder_name:
                print('title: %s, id: %s' % (file['title'], file['id']))
                return file["id"]

    def findFolders(self, fldname):
        file_list = self.drive.ListFile({
            'q': "title='{}' and mimeType contains 'application/vnd.google-apps.folder' and trashed=false".format(
                fldname)
        }).GetList()
        return file_list

    def getFolderFullList(self, parent_folder_id="root", is_only_file_title_and_id=True):
        file_list = GoogleDriveFileList()
        try:
            file_list = self.drive.ListFile(
                {'q': "'{0}' in parents and trashed=false".format(parent_folder_id)}
            ).GetList()
        except googleapiclient.errors.HttpError as err:
            message = ast.literal_eval(err.content)["error"]["message"]
            if message == "File not found: ":
                print(message)
                exit(1)
            else:
                raise
        return file_list

    def uploadFile(self, folder_id, src_folder_name):
        try:
            os.chdir(src_folder_name)
        except OSError:
            print(src_folder_name)

        for file in os.listdir("."):
            statinfo = os.stat(file)
            if statinfo.st_size > 0:
                print("uploading " + file)
                f = self.drive.CreateFile(
                    {"parents": [{"kind": "drive#fileLink", "id": folder_id}]}
                )
                f.SetContentFile(file)
                f.Upload()
            else:
                print("file {} is empty".format(file))

    def uploadSingleFile(self, folder_id, file, file_name="tempfile"):
        # f = self.drive.CreateFile(
        #     {"parents": [{"kind": "drive#fileLink", "id": folder_id}]}
        # )
        # f["title"] = file_name
        # f.SetContentFile(file)
        # f.Upload()

        metadata = {
            "name": file_name,
            "parents": [folder_id]
        }
        files = {
            "data": ("metadata", json.dumps(metadata), "application/json; charset=UTF-8"),
            "file": file
        }
        r = requests.post("https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart",
                          headers={"Authorization": "Bearer " + self.gauth.credentials.access_token},
                          files=files)
        print(r.text)


if __name__ == '__main__':
    gdm = GoogleDriveManager()
    # gdm.createFolder("02")
    folderID = gdm.getFolderID("screenCap", "root")
    # folder01Id = gdm.getFolderID("01", "root")
    # folder03id = gdm.getFolderID("03", folder01Id)
    # print(folder03id)

    screen = getWindow_Img(0)
    # img = cv2.imread("./img/2.PNG")
    ret, img_encode = cv2.imencode(".jpg", screen)
    img = io.BufferedReader(io.BytesIO(img_encode.tobytes()))
    # img3 = io.BufferedReader(io.BytesIO(screen.tobytes()))
    # img = bytes(Image.fromarray(screen).tobytes())

    # img.name = "oreo.png"
    # img2 = open("./img/1.PNG", "rb")
    gdm.uploadSingleFile(folderID, img, "test.png")