import win32api, win32con, win32gui, win32ui, win32service, os, time
from ahk import AHK
import math

def printSleep(delay):
    current_time = math.modf(delay)
    loopCount = current_time[1]
    decimalDelay = current_time[0]

    for i in range(0, int(loopCount)):
        print("sleep: {}".format(delay - i))
        time.sleep(1)

    if decimalDelay > 0:
        print("sleep: {}".format(decimalDelay))
        time.sleep(decimalDelay)


def delayTap(x, y, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_move(x=x, y=y, blocking=True)
    ahk.click()
    if description is not None:
        print("tap x:{}, y:{}, {}".format(x, y, description))
    else:
        print("tap x:{}, y:{}".format(x, y))

def delayKeyDown(str, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.key_press(str)
    if description is not None:
        print("key down: {}".format(description))

if __name__ == '__main__':
    ahk = AHK()
    delayTap(1078, 1019, 1)
    delayKeyDown("Space")
    delayKeyDown("Space")
    delayKeyDown("Space")
    delayKeyDown("Space")
    delayKeyDown("Space")
    delayKeyDown("Space")
    delayTap(1082, 952, 1, "交出")
    delayKeyDown("Space")
    delayTap(1446, 913, 1, "9")
    delayTap(1446, 913, 1, "9")
    for i in range(8):
        delayKeyDown("Space")
    delayTap(1082, 952, 1, "交出")
    delayKeyDown("Space")
    delayTap(1098, 913, 1, "F")
    for i in range(8):
        delayKeyDown("Space")
    delayTap(1082, 952, 1, "交出")
    delayKeyDown("Space")
    delayTap(1240, 913, 1, "C")
    for i in range(8):
        delayKeyDown("Space")
    delayTap(1082, 952, 1, "交出")
    delayKeyDown("Space")

    delayTap(1584, 913, 1, "5")
    for i in range(8):
        delayKeyDown("Space")
    delayTap(1082, 952, 1, "交出")
    delayKeyDown("Space")
    delayTap(1082, 952, 2, "1")
    for i in range(8):
        delayKeyDown("Space")
    delayTap(1385, 952, 1, "結束")
    delayKeyDown("Space")
    delayTap(1082, 952, 2, "關")
    # for i in range(5):
    #     delayTap(500, 421, 1)
