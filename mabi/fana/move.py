import sys
sys.path.append("..")
from tool.wintools import *

import pyautogui
import signal
from pynput import keyboard
from threading import Thread

def on_press(key, abortKey='esc'):
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    #print('pressed %s' % (k))
    if k == abortKey:
        print('end loop ...')
        return False  # stop listener


def loop_fun():
    tx = 0
    ty = 0
    loopCnt = 0
    loopSecond = 0
    try:
        while True:
            printSleep(1)
            n = loopCnt % 4
            if n == 0:
                ahk.mouse_position = (1583, 683)
            elif n == 1:
                ahk.mouse_position = (1446, 833)
            elif n == 2:
                ahk.mouse_position = (1215, 423)
            elif n == 3:
                ahk.mouse_position = (1158, 755)

            loopSecond += 1
            print(f"ls: {loopSecond}")
            if loopSecond % 5 == 0:
                loopCnt += 1

    except KeyboardInterrupt:
        print('\nDone')


if __name__ == '__main__':
    ahk = AHK()
    printSleep(10)
    abortKey = 't'
    listener = keyboard.Listener(on_press=on_press, abortKey=abortKey)
    listener.start()  # start to listen on a separate thread
    Thread(target=loop_fun, args=(), name='loop_fun', daemon=True).start()
    listener.join()  # wait for abortKey