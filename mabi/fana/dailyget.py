import sys
sys.path.append("..")
from tool.wintools import *

"""
分身弄哨子用
"""
class DailyFynny:
    def __init__(self):
        self.ahk = AHK()
        self.charterPos = 0
        """self.charterType = [1, 1, 1, 1, 1, 1, 2, 2,
                            2, 1, 1, 2, 3, 0, 2, 2,
                            1]"""
        self.charterType = [1, 1, 1, 1, 1, 1, 2, 2,
                            2, 1]
        self.size = len(self.charterType)


    def focusMabi(self):
        delayTap(self.ahk, 1597, 960, 3, "選瑪奇")

    def delayKeyDownClick(self, x, y, key, delay=0, description=None):
        if delay is not None and delay != 0:
            printSleep(delay)
        self.ahk.mouse_position = (x, y)
        sleep(0.1)
        self.ahk.key_down(key)
        sleep(0.5)
        self.ahk.click()
        sleep(0.2)
        self.ahk.key_up(key)
        if description is not None:
            print("key tap x:{}, y:{}, key: {} {}".format(x, y, key, description))
        else:
            print("key tap x:{}, y:{}, key: {}".format(x, y, key))

    def createFynnyStep(self, numStr, x, y):
        delayTap(self.ahk, x, y, 1, f"第{numStr}個籠子")
        printSleep(1)
        pos = findImg("../img/80.PNG", "未完成")
        if pos is None:
            pos = findImg("../img/79.PNG", "空")
            if pos is None:
                delayTap(self.ahk, 1176, 776, 1, "拿出")
                printSleep(4)
                delayTap(self.ahk, x, y, 1, f"第{numStr}個籠子")

            delayTap(self.ahk, 1829, 725, 1, "拿起菲尼寶石")
            delayTap(self.ahk, 1268, 611, 1, "放入")
            delayTap(self.ahk, 1169, 844, 1, "綻放開始")
            printSleep(6)
        else:
            delayTap(self.ahk, 1390, 780, 1, "關閉")
            printSleep(1)

    def createFynny(self, pos):
        printSleep(2)
        pos = findImg("../img/15.PNG", "開背包")
        if pos is None:
            delayKeyDown(self.ahk, "i", 1, "開背包")
            printSleep(2)
        pos = findImg("../img/77.PNG", "沒有菲尼寶石")
        if pos is None:
            delayKeyDown(self.ahk, "i", 1, "關背包")
        delayTap(self.ahk, 1502, 997, 1, "農場")
        delayTap(self.ahk, 1390, 956, 2, "進入農場")
        if pos == 0:
            printSleep(20)
        else:
            printSleep(10)
        self.createFynnyStep("一", 1482, 703)
        self.createFynnyStep("二", 1512, 567)
        self.createFynnyStep("三", 1367, 561)
        self.createFynnyStep("四", 1326, 665)
        pos = findImg("../img/15.PNG", "開背包")
        if pos is not None:
            delayKeyDown(self.ahk, "i", 1, "關背包")
        delayTap(self.ahk, 1877, 904, 1, "EXIT")
        delayTap(self.ahk, 1401, 690, 1, "離開")
        printSleep(12)

    def openBankGetGem(self, index):
        pos = findImg("../img/15.PNG", "開背包")
        if pos is None:
            delayKeyDown(self.ahk, "i", 1, "開背包")

        printSleep(2)
        pos1 = findImg("../img/77.PNG", "沒有菲尼寶石")
        pos2 = findImg("../img/84.PNG", "沒魔")
        if pos1 is None and pos2 is None:
            delayKeyDown(self.ahk, "i", 1, "關背包")
        else:
            self.delayKeyDownClick(1319, 543, "Control", 1, "銀行NPC")
            delayTap(self.ahk, 1121, 948, 1)
            delayTap(self.ahk, 1121, 948, 1)
            delayTap(self.ahk, 1121, 948, 1)
            delayTap(self.ahk, 1121, 948, 1, "開啟銀行")
            printSleep(3)

            #1291 522
            dPos = findImg("../img/86.PNG", "杜巴銀行")

            if self.charterType[index] == 1:  # 人類
                delayTap(self.ahk, dPos[0]-120, dPos[1]+41, 1)
                delayTap(self.ahk, dPos[0]-5, dPos[1]+41, 1)
            elif self.charterType[index] == 2:  # 精靈
                delayTap(self.ahk, dPos[0]-72, dPos[1]+41, 1)
                delayTap(self.ahk, dPos[0]-5, dPos[1]+41, 1)
            elif self.charterType[index] == 3:  # 巨人
                delayTap(self.ahk, dPos[0]-112, dPos[1]+72, 1) #角色1
            printSleep(2)
            if pos1 is not None:
                pos = findImg("../img/81.PNG", "菲尼寶石")
                if pos is not None:
                    delayTap(self.ahk, pos[0], pos[1], 1, "拿起")
                    delayTap(self.ahk, 1829, 725, 1, "放菲尼寶石到身上")

            if pos2 is not None:
                pos = findImg("../img/85.PNG", "魔水")
                if pos is not None:
                    delayTap(self.ahk, pos[0], pos[1], 1, "拿起")
                    printSleep(1)
                    self.ahk.right_click()
                    printSleep(1)
            delayTap(self.ahk, 1415, 843, 1, "交易結束")
            printSleep(2)

            if findImg("../img/84.PNG", "沒魔") is not None:
                self.ahk.key_press('d')

    def openBankSaveWhistle(self, index):
        self.delayKeyDownClick(1319, 543, "Control", 1, "銀行NPC")
        delayTap(self.ahk, 1121, 948, 1)
        delayTap(self.ahk, 1121, 948, 1)
        delayTap(self.ahk, 1121, 948, 1)
        printSleep(3)

        # 1291 522
        dPos = findImg("../img/86.PNG", "杜巴銀行")
    #1447 588
        if self.charterType[index] == 1:  # 人類
            delayTap(self.ahk, dPos[0] - 120, dPos[1] + 41, 1)
            delayTap(self.ahk, dPos[0] - 5, dPos[1] + 41, 1)
        elif self.charterType[index] == 2:  # 精靈
            delayTap(self.ahk, dPos[0] - 72, dPos[1] + 41, 1)
            delayTap(self.ahk, dPos[0] - 5, dPos[1] + 41, 1)
        elif self.charterType[index] == 3:  # 巨人
            delayTap(self.ahk, dPos[0] - 112, dPos[1] + 72, 1)  # 角色1

        for i in range(6 + int((index * 4) / 24)):
            delayTap(self.ahk, dPos[0]+156, dPos[1] + 66, 0.3, "next")

        delayTap(self.ahk, 1179, 594, 1)
        for i in range(4):
            self.delayKeyDownClick(1431, 882, "Alt", 0.5, "存銀行")

        delayTap(self.ahk, 1415, 843, 1, "交易結束")

        #getstone

        # add click 6 (24)

    def start(self, startPos = 0):
        for i in range(startPos, self.size, 1):
            charterType = self.charterType[i]
            if charterType != 0:
                delayTap(self.ahk, 1715, 481, 1, "查看全部")
                lineTap = int(i/8)
                for j in range(lineTap):
                    delayTap(self.ahk, 1907, 717, 1, "line")
                y = 532
                y = y + 50 * (i%8)
                delayTap(self.ahk, 1741, y, 1, f"charter {i+1}")
                delayTap(self.ahk, 986, 996, 1, "開始")
                printSleep(15)
                self.checkBrthday()
                printSleep(2)
                if findImg("../img/84.PNG", "沒魔") is not None:
                    self.ahk.key_press('d')
                self.openBankGetGem(i)
                self.createFynny(i)
                if findImg("../img/83.PNG", "物品暫存視窗") is not None:
                    self.openBankSaveWhistle(i)
                delayTap(self.ahk, 1050, 1025, 1, "menu")
                delayTap(self.ahk, 1092, 939, 1, "登出")
                delayTap(self.ahk, 1466, 727, 1, "確定")
                printSleep(15)

            self.charterPos += 1

    def testTask(self):
        self.openBankGetGem(0)
        self.createFynny()

    def checkBrthday(self):
        delayTap(self.ahk, 1581, 726, 1, "生日取消")
        delayTap(self.ahk, 1749, 991, 1, "開拍賣")
        delayTap(self.ahk, 1749, 991, 1, "關拍賣")

if __name__ == '__main__':
    dailyFynny = DailyFynny()
    dailyFynny.focusMabi()
    dailyFynny.start(0)
    #print(findImg("../img/86.PNG"))

