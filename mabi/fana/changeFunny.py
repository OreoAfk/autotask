import sys
sys.path.append("..")
from tool.wintools import *
from threading import Thread

"""
菲尼哨子換松果腳本
"""

def delayKeyDownClick(x, y, key, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.mouse_position = (x, y)
    sleep(0.1)
    ahk.key_down(key)
    sleep(0.5)
    ahk.click()
    sleep(0.2)
    ahk.key_up(key)
    if description is not None:
        print("key tap x:{}, y:{}, key: {} {}".format(x, y, key, description))
    else:
        print("key tap x:{}, y:{}, key: {}".format(x, y, key))


def on_press(key, abortKey='esc'):
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys
    if k == abortKey:
        print('end loop ...')
        return False  # stop listener


def loop_fun():
    tx = 0
    ty = 0
    try:
        while True:
            delayKeyDownClick(1251, 732, 'Control',1, "菲歐納特")
            for i in range(5):
                delayTap(ahk, 1147, 955, 0.5)
            delayTap(ahk, 1147, 955, 1, "交易")

            delayTap(ahk, 1586, 703, 1)
            delayTap(ahk, 1687, 798, 1)
            delayTap(ahk, 1069, 955, 3)

    except KeyboardInterrupt:
        print('\nDone')

if __name__ == '__main__':
    ahk = AHK()
    delayTap(ahk, 1597, 960, 3, "選瑪奇")
    abortKey = 't'
    listener = keyboard.Listener(on_press=on_press, abortKey=abortKey)
    listener.start()  # start to listen on a separate thread
    Thread(target=loop_fun, args=(), name='loop_fun', daemon=True).start()
    listener.join()  # wait for abortKey
