import sys
sys.path.append("..")
from tool.wintools import *
from threading import Thread
"""
弄銀行禮物
"""

class BankGift:

    def __init__(self):
        self.backPos = findImg("../img/86.PNG", "杜巴銀行") #1179 428

    def start(self):
        if self.backPos is not None:
            self.tapBank(-59, 43, 1, "精靈")
            self.tapBank(-111, 43, 1, "人類")
            self.tapBank(-59, 43, 1, "精靈")
            self.tapBank(-16, 43, 1, "巨人")
            printSleep(5)
        for i in range(51):
            self.selectCharter()


    def tapBank(self, x, y, delay, desc):
        delayTap(ahk, self.backPos[0] + x, self.backPos[1] + y, delay, desc)

    def getGift(self, isRandom=True):
        self.tapBank(149, 42, 0.5, "禮物")
        if isRandom:
            self.tapBank(234, 190, 0.5, "抽籤")
            self.tapBank(271, 157, 0.5, "禮物")
            self.tapBank(57, 224, 0.5, "確認")
        else:
            self.tapBank(83, 42, 1, "指定")
            self.tapBank(-61, 342, 1, "現在領取")
        printSleep(2)

    def selectCharter(self):
        self.tapBank(-116, 63, 1, "Greedy+凱琳")
        self.getGift()
        self.tapBank(155, 60, 0.7, ">")

if __name__ == '__main__':
    ahk = AHK()
    printSleep(5)
    bankgift = BankGift()
    bankgift.start()

