import win32api, win32con, win32gui, win32ui, win32service, os, time

from ahk import AHK
import time
from pynput import keyboard
from pynput.mouse import Button, Controller
import numpy as np
import cv2
from PIL import Image
import math

gameName = "新瑪奇 mabinogi"
hwnd = 657542
break_program = False

charterCount = 1

def on_press(key):
    global break_program
    print (key)
    if key == keyboard.Key.esc:
        print ('end pressed')
        break_program = True
        return False

def findAttackMonster():
    loopCnt = 0
    while True:
        pos = ahk.mouse_position
        print(loopCnt)
        if pos[0] < 100 and pos[1] < 100:
            print("end")
            break
        ahk.key_press("2")
        time.sleep(0.5)
        loopCnt += 1

def getWindow_Img(hwnd):
    # 將 hwnd 換成 WindowLong
    s = win32gui.GetWindowLong(hwnd, win32con.GWL_EXSTYLE)
    win32gui.SetWindowLong(hwnd, win32con.GWL_EXSTYLE, s | win32con.WS_EX_LAYERED)
    # 判斷視窗是否最小化
    show = win32gui.IsIconic(hwnd)
    # 將視窗圖層屬性改變成透明
    # 還原視窗並拉到最前方
    # 取消最大小化動畫
    # 取得視窗寬高
    if show == 1:
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 0)
        win32gui.SetLayeredWindowAttributes(hwnd, 0, 0, win32con.LWA_ALPHA)
        win32gui.ShowWindow(hwnd, win32con.SW_RESTORE)
        x, y, width, height = getWindow_W_H(hwnd)
        # 創造輸出圖層
    hwindc = win32gui.GetWindowDC(hwnd)
    srcdc = win32ui.CreateDCFromHandle(hwindc)
    memdc = srcdc.CreateCompatibleDC()
    bmp = win32ui.CreateBitmap()
    # 取得視窗寬高
    x, y, width, height = getWindow_W_H(hwnd)
    # 如果視窗最小化，則移到Z軸最下方
    if show == 1: win32gui.SetWindowPos(hwnd, win32con.HWND_BOTTOM, x, y, width, height, win32con.SWP_NOACTIVATE)
    # 複製目標圖層，貼上到 bmp
    bmp.CreateCompatibleBitmap(srcdc, width, height)
    memdc.SelectObject(bmp)
    memdc.BitBlt((0, 0), (width, height), srcdc, (8, 3), win32con.SRCCOPY)
    # 將 bitmap 轉換成 np
    signedIntsArray = bmp.GetBitmapBits(True)
    img = np.fromstring(signedIntsArray, dtype='uint8')
    img.shape = (height, width, 4)  # png，具有透明度的
    # 釋放device content
    srcdc.DeleteDC()
    memdc.DeleteDC()
    win32gui.ReleaseDC(hwnd, hwindc)
    win32gui.DeleteObject(bmp.GetHandle())
    # 還原目標屬性
    if show == 1:
        win32gui.SetLayeredWindowAttributes(hwnd, 0, 255, win32con.LWA_ALPHA)
        win32gui.SystemParametersInfo(win32con.SPI_SETANIMATION, 1)
    # 回傳圖片
    return img

def getWindow_W_H(hwnd):
    # 取得目標視窗的大小
    left, top, right, bot = win32gui.GetWindowRect(hwnd)
    width = right - left - 15
    height = bot - top - 11
    return (left, top, width, height)

def findImg(fileName, description=None):
    frame = getWindow_Img(hwnd)
    frame2 = frame[:, :, :3]
    template = cv2.imread(fileName)
    result = cv2.matchTemplate(frame2, template, cv2.TM_SQDIFF_NORMED)
    c, w, h = template.shape[::-1]
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    if min_val > 0.01:
        if description is not None:
            print("-{} not find".format(description))
        return None
    top_left = min_loc  # 左上角的位置
    # bottom_right = (top_left[0] + w, top_left[1] + h)  # 右下角的位置
    # cv2.imshow("img_template", template)
    center_point = (top_left[0] + int(w / 2), top_left[1] + int(h / 2))
    # 在原图上画矩形
    # cv2.rectangle(frame, top_left, bottom_right, (0, 255, 255), 2)
    # mouse.position =
    print("-{} x:{}, y:{}, h:{}, w:{}".format(description, center_point[0], center_point[1], h, w))

    return center_point


def printSleep(delay):
    currentTime = math.modf(delay)
    loopCount = currentTime[1]
    decimalDelay = currentTime[0]

    for i in range(0, int(loopCount)):
        print("sleep: {}".format(delay - i))
        time.sleep(1)

    if decimalDelay > 0:
        print("sleep: {}".format(decimalDelay))
        time.sleep(decimalDelay)

def delayTap(x, y, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    # mouse.position = (x, y)
    # mouse.click(Button.left, 1)
    ahk.mouse_move(x=x, y=y, blocking=True)
    ahk.click()
    if description is not None:
        print("tap x:{}, y:{}, {}".format(x, y, description))
    else:
        print("tap x:{}, y:{}".format(x, y))

def delayKeyDown(str, delay=0, description=None):
    if delay is not None and delay != 0:
        printSleep(delay)
    ahk.key_press(str)
    if description is not None:
        print("key down: {}".format(description))

def getPatWalletPos():
    # 寵物錢包
    if charterCount == 2:
        return 560, 460
    elif charterCount == 3:
        return 560, 510
    elif charterCount == 4:
        return 560, 560
    elif charterCount == 5:
        return 560, 610
    elif charterCount == 6:
        return 510, 460
    elif charterCount == 7:
        return 510, 510
    elif charterCount == 8:
        return 510, 560
    elif charterCount == 9:
        return 510, 610

if __name__ == '__main__':
    ahk = AHK()
    win32gui.SetForegroundWindow(hwnd)
    win32gui.MoveWindow(hwnd, 0, 0, 1000, 800, True)
    # for i in range(5):
    #     pos = findImg("./res/img/1.PNG", "選角")
    #     if pos is not None:
    #         charterCount += 1
    #         if charterCount > 9:
    #             delayTap(970, 700, 1)
    #             delayTap(700, 700, 2, "角色{}".format(charterCount))
    #         else:
    #             delayTap(700, 250 + charterCount * 50, 1, "角色{}".format(charterCount))
    #         delayTap(50, 750, 3, "開始")
    #         printSleep(20)
    #         pos = findImg("./res/img/3.PNG", "1000等")
    #         if pos is not None:
    #             delayTap(500, 400, 1)
    #             printSleep(3)
    #             pos = findImg("./res/img/4.PNG", "智能系統")
    #             if pos is not None:
    #                 delayTap(870, 190, 1, "關閉")
    #         printSleep(3)
    #         pos = findImg("./res/img/4.PNG", "智能系統")
    #         if pos is not None:
    #             delayTap(870, 190, 1, "關閉")
    #             printSleep(3)
    #             pos = findImg("./res/img/3.PNG", "1000等")
    #             if pos is not None:
    #                 delayTap(500, 400, 1)
    #
    #     delayKeyDown("2", 1)
    #     delayKeyDown(".", 2, "打開寵物背包")
    #     x, y = getPatWalletPos()
    #     delayTap(x, y, 2, "寵物背包")
    #     delayTap(860, 490, 2, "自己錢包")
    #     delayTap(x, y, 2, "寵物背包")

    ahk.key_down('Control')
    delayTap(100, 190, 0, "升段嚮導")
    ahk.key_up('Control')
    delayKeyDown("Space", 1)
    delayTap(220, 700, 0, "升段考試")
    delayTap(380, 290, 1, "戰鬥")
    delayTap(380, 350, 1, "重擊3段")
    delayTap(590, 530, 1, "申請")
    delayKeyDown("Tab", 5, "換武器")
    delayTap(220, 700, 0, "開始")
    for i in range(500):
        delayKeyDown("2")
        if i % 4 == 0:
            ahk.key_down('Control')
            delayTap(500, 500, 0, "攻擊")
            ahk.key_up('Control')
        printSleep(0.5)
    printSleep(15)
    delayTap(950, 100, 0, "EXIT")
    delayTap(550, 480, 1, "確定")
    printSleep(5)
    pos = findImg("promotion/img/6.PNG", "物品欄")
    if pos is None:
        delayKeyDown("i", 1, "物品欄")
        printSleep(2)
    pos = findImg("promotion/img/7.PNG", "升段章")
    if pos is not None:
        delayKeyDown("2", 1, "寵物")
        delayKeyDown(".", 2, "寵物欄")
        delayTap(pos[0], pos[1], 1, "升段章")
        delayTap(475, 445, 2, "放章")








# 657542 新瑪奇 mabinogi
# 133686 新瑪奇 mabinogi
# 264594 新瑪奇 mabinogi