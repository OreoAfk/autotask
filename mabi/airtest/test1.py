from airtest.core.api import *
dev = connect_device("Windows:///133120")
# 通用的接口调用方式，与其他平台相同：
print(dev.get_title())
# 把窗口移动到某个坐标位置
dev.move((50, 50))
dev.mouse.scroll(coords=(80, 100), wheel_dist=1)
dev.keyboard.parse_keys()