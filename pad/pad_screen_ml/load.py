from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg
import os

import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()

# import some common libraries
import numpy as np
import cv2
import random
# from google.colab.patches import cv2_imshow

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.structures.boxes import Boxes
from detectron2.structures.rotated_boxes import RotatedBoxes

from detectron2.data.datasets import register_coco_instances

path = "."

class BoxItem:
    def __init__(self, label, box):
        self.labtl = label
        if isinstance(box, Boxes) or isinstance(box, RotatedBoxes):
            self.box = box.tensor.numpy()
        else:
            self.box = np.asarray(box)

    def __str__(self):
        return str(self.labtl) + " " + str(self.getCenterPoint())

    def getCenterPoint(self):
        return [self.box[0][0] + (self.box[0][2] - self.box[0][0]) / 2, self.box[0][1] + (self.box[0][3] - self.box[0][1]) / 2]

class PadBoard:
    def __init__(self):
        pass

    def setBoardInfo(self, predictions, metadata):
        self.boxes = predictions.pred_boxes if predictions.has("pred_boxes") else None
        # scores = predictions.scores if predictions.has("scores") else None
        self.classes = predictions.pred_classes if predictions.has("pred_classes") else None
        self.labels = [metadata.get("thing_classes", None)[i] for i in self.classes]
        self.keypoints = predictions.pred_keypoints if predictions.has("pred_keypoints") else None

        self.screenHeight, self.screenWidth = outputs["instances"].image_size
        self.boxItems = self.toBoxItem()

        self.boxTextConvertToNumBoard(self.boxItems)

        for b in self.boxItems:
            print(b)

    def boxTextConvertToNumBoard(self, boxItems):
        numBoard = np.copy(boxItems)
        print(type(boxItems))
        print(type(numBoard))



    def toBoxItem(self):
        boxitems = []
        boxSize = len(self.boxes)

        self.labenBoard = None
        self.numBoard = None
        self.plusBoard = None
        self.lockBoard = None
        boardRow = 0
        boardCol = 0
        if "6x5board" in self.labels:
            p = self.labels.index("6x5board")
            if isinstance(self.boxes[p], Boxes) or isinstance(self.boxes[p], RotatedBoxes):
                boardBox = self.boxes[p].tensor.numpy()
            else:
                boardBox = np.asarray(self.boxes[p])

            self.boardTop = boardBox[0][1]
            self.boardHeight = boardBox[0][3] - boardBox[0][1]
            self.labenBoard = [None] * 5
            self.numBoard = [None] * 5
            self.plusBoard= [None] * 5
            self.lockBoard = [None] * 5
            for i in range(5):
                self.labenBoard[i] = [None] * 6
                self.numBoard[i] = [None] * 6
                self.plusBoard[i] = [0] * 6
                self.lockBoard[i] = [0] * 6
            boardRow = 5
            boardCol = 6

        if "7x6board" in self.labels:
            p = self.labels.index("7x6board")
            if isinstance(self.boxes[p], Boxes) or isinstance(self.boxes[p], RotatedBoxes):
                boardBox = self.boxes[p].tensor.numpy()
            else:
                boardBox = np.asarray(self.boxes[p])

            self.boardTop = boardBox[0][1]
            self.boardHeight = boardBox[0][3] - boardBox[0][1]
            self.labenBoard = [None] * 6
            self.numBoard = [None] * 6
            self.plusBoard = [None] * 6
            self.lockBoard = [None] * 6
            for i in range(6):
                self.labenBoard[i] = [None] * 7
                self.numBoard[i] = [None] * 7
                self.plusBoard[i] = [0] * 7
                self.lockBoard[i] = [0] * 7
            boardRow = 6
            boardCol = 7

        orbCount = 0
        orbNonUseCount = 0
        lockCount = 0
        plusCount = 0

        rorbCount = 0
        borbCount = 0
        gorbCount = 0
        lorbCount = 0
        dorbCount = 0
        horbCount = 0
        for j in range(boxSize):
            boxitem = BoxItem(self.labels[j], self.boxes[j])
            boxitems.append(boxitem)
            posX, posY = self.toBoardPos(boardRow, boardCol, boxitem.getCenterPoint())
            if len(boxitem.labtl) > 4 and boxitem.labtl[1: 4] == "orb":
                orbCount = orbCount + 1
                # print("orb {}: {} {} {}".format(orbCount, boxitem.labtl, boxitem.getCenterPoint(), boxitem.box))
                if boxitem.labtl.find("orb_n"):
                    orbNonUseCount = orbNonUseCount + 1
                # print("{}, {}".format(posX, posY))
                if posX is not None and posY is not None:
                    self.labenBoard[posY][posX] = self.labels[j]
                    orbNum = self.boardType2Num(self.labels[j])
                    self.numBoard[posY][posX] = orbNum
                    if orbNum == 0:
                        rorbCount = rorbCount + 1
                    elif orbNum == 1:
                        borbCount = borbCount + 1
                    elif orbNum == 2:
                        gorbCount = gorbCount + 1
                    elif orbNum == 3:
                        lorbCount = lorbCount + 1
                    elif dorbCount == 4:
                        dorbCount = dorbCount + 1
                    elif horbCount == 5:
                        horbCount = horbCount + 1
            if boxitem.labtl == "plus":
                self.plusBoard[posY][posX] = 1
                plusCount = plusCount + 1
            if boxitem.labtl == "lock":
                self.lockBoard[posY][posX] = 1
                lockCount = lockCount + 1
        print(self.labenBoard)
        print(self.numBoard)
        print(self.plusBoard)
        print(self.lockBoard)
        print("rorb: {}, borb: {}, gorb: {}, lorb: {}, dorb: {}, horb: {}"
              .format(rorbCount, borbCount, gorbCount, lorbCount, dorbCount, horbCount))
        return boxitems

    def toBoardPos(self, boardRow, boardCol, box):
        posX = -1
        posY = -1
        preWidth = self.screenWidth / boardCol
        preHeight = self.boardHeight / boardRow
        print("{}, {}, {}".format(preWidth, self.screenWidth, boardCol))
        if preWidth > box[0]:
            posX = 0
        elif preWidth * 2 > box[0]:
            posX = 1
        elif preWidth * 3 > box[0]:
            posX = 2
        elif preWidth * 4 > box[0]:
            posX = 3
        elif preWidth * 5 > box[0]:
            posX = 4
        elif preWidth * 6 > box[0]:
            posX = 5
        elif preWidth * 7 > box[0]:
            posX = 6

        if self.boardTop + preHeight > box[1]:
            posY = 0
        elif self.boardTop + preHeight * 2 > box[1]:
            posY = 1
        elif self.boardTop + preHeight * 3 > box[1]:
            posY = 2
        elif self.boardTop + preHeight * 4 > box[1]:
            posY = 3
        elif self.boardTop + preHeight * 5 > box[1]:
            posY = 4
        elif self.boardTop + preHeight * 6 > box[1]:
            posY = 5

        if posX == -1 or posY == -1:
            return None

        return posX, posY

    def boardType2Num(self, boardType):
        if boardType == "rorb":
            return 0
        elif boardType == "rorb_n":
            return 0
        elif boardType == "borb":
            return 1
        elif boardType == "borb_n":
            return 1
        elif boardType == "gorb":
            return 2
        elif boardType == "gorb_n":
            return 2
        elif boardType == "lorb":
            return 3
        elif boardType == "lorb_n":
            return 3
        elif boardType == "dorb":
            return 4
        elif boardType == "dorb_n":
            return 4
        elif boardType == "horb":
            return 5
        elif boardType == "horb_n":
            return 5
        elif boardType == "jorb":
            return 6
        elif boardType == "jorb_n":
            return 6
        elif boardType == "porb":
            return 7
        elif boardType == "porb_n":
            return 7
        elif boardType == "oorb":
            return 8
        elif boardType == "oorb_n":
            return 8
        elif boardType == "morb":
            return 9
        elif boardType == "morb_n":
            return 9
        elif boardType == "norb":
            return 10
        else:
            return None


register_coco_instances("pad_orb_train", {}, path + "/trainval.json", path + "/train")
register_coco_instances("pad_orb_test", {}, path + "/trainval.json", path + "/test")
pad_orb_train_metadata = MetadataCatalog.get("pad_orb_train")
dataset_dicts = DatasetCatalog.get("pad_orb_train")

cfg = get_cfg()
cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
cfg.DATASETS.TRAIN = ("pad_orb_train",)
cfg.DATASETS.TEST = ("pad_orb_test",)   # no metrics implemented for this dataset
cfg.DATALOADER.NUM_WORKERS = 2
cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")  # initialize from model zoo
cfg.SOLVER.IMS_PER_BATCH = 2
cfg.SOLVER.BASE_LR = 0.02
cfg.SOLVER.MAX_ITER = 5000    # 300 iterations seems good enough, but you can certainly train longer
cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128   # faster, and good enough for this toy dataset
cfg.MODEL.ROI_HEADS.NUM_CLASSES = 53  # 3 classes (data, fig, hazelnut)
cfg.MODEL.WEIGHTS = os.path.join(path + "/output", "model_final.pth")
cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7   # set the testing threshold for this model
predictor = DefaultPredictor(cfg)

from detectron2.utils.visualizer import ColorMode
# dataset_dicts = get_balloon_dicts("test")
# for d in random.sample(dataset_dicts, 3):
from os import listdir
from os.path import isfile, join

onlyfiles = [f for f in listdir(path + "/test") if isfile(join(path + "/test", f))]
fileName = random.choice(onlyfiles)
im = cv2.imread(path + "/test/" + fileName)

import urllib.request

padImgUrl = "https://1.bp.blogspot.com/-7-xYz9_Te4Y/XpiCEYQ7dpI/AAAAAAADh2o/RNApfsNpfs8qS_hhVtbyThTiErXIy5s4ACLcBGAsYHQ/s1600/IMG_8566.PNG"

resp = urllib.request.urlopen(padImgUrl)
im = np.asarray(bytearray(resp.read()), dtype="uint8")
im = cv2.imdecode(im, cv2.IMREAD_COLOR)



outputs = predictor(im)
v = Visualizer(im[:, :, ::-1],
                metadata=pad_orb_train_metadata,
                scale=0.8,
                instance_mode=ColorMode.IMAGE_BW   # remove the colors of unsegmented pixels
)

# print(outputs["instances"])
v = v.draw_instance_predictions(outputs["instances"].to("cpu"))
# cv2_imshow(v.get_image()[:, :, ::-1])
padBoard = PadBoard()
padBoard.setBoardInfo(outputs["instances"].to("cpu"), pad_orb_train_metadata)

cv2.imshow('My Image', v.get_image())
cv2.waitKey(0)
cv2.destroyAllWindows()

