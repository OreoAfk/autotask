from copy import copy, deepcopy
import operator
import collections


class PadSolver:
    def __init__(self,maxPathLength=20):

        self.TYPES = 20
        self.MULTI_ORB_BONUS = 0.25
        self.COMBO_BONUS = 0.25
        self.so_id = 0
        self.maxPathLength = maxPathLength

    def updateSolver(self, row, col):
        self.ROWS = row
        self.COLS = col
        self.MAX_SOLUTIONS_COUNT = self.ROWS * self.COLS * 20

    def is_8_dir_movement_supported(self):
        return False

    def make_rc(self, row, col):
        return {'row': row, 'col': col}

    def make_match(self, mtype, count, match_type):
        return {'type': mtype, 'count': count, 'match_type': match_type}

    def copy_rc(self, rc):
        new_rc = deepcopy(rc)
        return {'row': rc['row'], 'col': rc['col']}

    def equals_xy(self, a, b):
        return a['x'] == b['x'] and a['y'] == b['y']

    def equals_rc(self, a, b):
        return a['row'] == b['row'] and a['col'] == b['col']

    def create_empty_board(self):
        result = [None] * self.ROWS
        for i in range(self.ROWS):
            result[i] = [None] * self.COLS
        return result

    def copy_board(self, board):
        return deepcopy(board)

    def get_weights(self):
        weights = [None] * (self.TYPES + 1)
        weights[0] = {
            'normal': 1,
            'mass': 1,
        }
        weights[1] = {
            'normal': 1,
            'mass': 1,
        }
        weights[2] = {
            'normal': 1,
            'mass': 1,
        }
        weights[3] = {
            'normal': 1,
            'mass': 1,
        }
        weights[4] = {
            'normal': 1,
            'mass': 1,
        }
        weights[5] = {
            'normal': 0.3,
            'mass': 0.3,
        }
        for i in range(6, (self.TYPES + 1)):
            weights[i] = {
                'normal': 0.1,
                'mass': 0.1,
            }
        return weights

    def find_matches(self, board):
        match_board = self.create_empty_board()
        for i in range(self.ROWS):
            prev_1_orb = 'X'
            prev_2_orb = 'X'
            for j in range(self.COLS):
                cur_orb = board[i][j]
                if prev_1_orb == prev_2_orb and prev_2_orb == cur_orb and cur_orb != 'X':
                    match_board[i][j] = cur_orb
                    match_board[i][j-1] = cur_orb
                    match_board[i][j-2] = cur_orb
                prev_1_orb = prev_2_orb
                prev_2_orb = cur_orb

        for j in range(self.COLS):
            prev_1_orb = 'X'
            prev_2_orb = 'X'
            for i in range(self.ROWS):
                cur_orb = board[i][j]
                if prev_1_orb == prev_2_orb and prev_2_orb == cur_orb and cur_orb != 'X':
                    match_board[i][j] = cur_orb
                    match_board[i-1][j] = cur_orb
                    match_board[i-2][j] = cur_orb
                prev_1_orb = prev_2_orb
                prev_2_orb = cur_orb

        scratch_board = self.copy_board(match_board)

        temp_check_match = []
        for r in range(self.ROWS):
            temp_check_match.append([0] * self.COLS)

        matches = []
        total_count = 0
        for i in range(self.ROWS):
            for j in range(self.COLS):
                cur_orb = scratch_board[i][j]
                if cur_orb is None:
                    continue
                stack = [self.make_rc(i, j)]
                count = 0
                temp_check_match = []
                for r in range(self.ROWS):
                    temp_check_match.append([0] * self.COLS)

                while len(stack):
                    n = stack.pop()
                    if scratch_board[n['row']][n['col']] != cur_orb:
                        continue
                    count = count + 1
                    temp_check_match[n['row']][n['col']] = 1
                    scratch_board[n['row']][n['col']] = None
                    if n['row'] > 0:
                        stack.append(self.make_rc(n['row']-1, n['col']))
                    if n['row'] < self.ROWS-1:
                        stack.append(self.make_rc(n['row']+1, n['col']))
                    if n['col'] > 0 :
                        stack.append(self.make_rc(n['row'], n['col']-1))
                    if n['col'] < self.COLS-1:
                        stack.append(self.make_rc(n['row'], n['col']+1))
                match_type = 0
                # match 0 = 3 match
                # 1 = 4
                # 2 = col 5
                # 3 = row 6

                if count == 4:
                    match_type = 1
                if match_type == 0 and count == self.ROWS:
                    for c in range(self.COLS):
                        col_same = 0
                        for r in range(self.ROWS):
                            if temp_check_match[r][c] == 1:
                                col_same = col_same + 1
                        if col_same == self.ROWS:
                            match_type = 2
                if match_type == 0 and count == self.COLS:
                    for r in range(self.ROWS):
                        row_same = 0
                        for c in range(self.COLS):
                            if temp_check_match[r][c] == 1:
                                row_same = row_same + 1
                        if row_same == self.COLS:
                            match_type = 3

                matches.append(self.make_match(cur_orb, count, match_type))
                total_count = total_count + count

        return {'matches': matches, 'board': match_board, 'total_count': total_count}

    def compute_weight(self, matches, weights):
        total_weight = 0
        for m in matches:
            try:
                base_weight = weights[m['type']]['mass' if m['count'] >= 5 else 'normal']
            except:
                print("error")
            multi_orb_bonus = (m['count'] - 3) * self.MULTI_ORB_BONUS + 1
            total_weight = total_weight + multi_orb_bonus * base_weight
        combo_bonus = (len(matches) - 1) * self.COMBO_BONUS + 1
        return total_weight * combo_bonus

    def in_place_remove_matches(self, board, match_board):
        for i in range(self.ROWS):
            for j in range(self.COLS):
                if match_board[i][j] is not None:
                    board[i][j] = 'X'
        return board

    def in_place_drop_empty_spaces(self, board):
        for j in range(self.COLS):
            dest_i = self.ROWS-1
            for src_i in range(self.ROWS-1, -1, -1):
                if board[src_i][j] != 'X':
                    board[dest_i][j] = board[src_i][j]
                    dest_i = dest_i - 1
            for d in range(dest_i, -1, -1):
                board[d][j] = 'X'
        return board

    def can_move_orb(self, rc, dir):
        if dir == 0:
            return rc['col'] < self.COLS-1
        elif dir == 1:
            return rc['row'] < self.ROWS-1 and rc['col'] < self.COLS-1
        elif dir == 2:
            return rc['row'] < self.ROWS-1
        elif dir == 3:
            return rc['row'] < self.ROWS-1 and rc['col'] > 0
        elif dir == 4:
            return rc['col'] > 0
        elif dir == 5:
            return rc['row'] > 0 and rc['col'] > 0
        elif dir == 6:
            return rc['row'] > 0
        elif dir == 7:
            return rc['row'] > 0 and rc['col'] < self.COLS-1
        return False

    def in_place_move_rc(self, rc, dir):
        if dir == 0:
            rc['col'] = rc['col'] + 1
        elif dir == 1:
            rc['row'] = rc['row'] + 1
            rc['col'] = rc['col'] + 1
        elif dir == 2:
            rc['row'] = rc['row'] + 1
        elif dir == 3:
            rc['row'] = rc['row'] + 1
            rc['col'] = rc['col'] - 1
        elif dir == 4:
            rc['col'] = rc['col'] - 1
        elif dir == 5:
            rc['row'] = rc['row'] - 1
            rc['col'] = rc['col'] - 1
        elif dir == 6:
            rc['row'] = rc['row'] - 1
        elif dir == 7:
            rc['row'] = rc['row'] - 1
            rc['col'] = rc['col'] + 1

    def in_place_swap_orb(self, board, rc, dir):
        old_rc = self.copy_rc(rc)
        new_rc = self.copy_rc(rc)
        new_board = self.copy_board(board)
        self.in_place_move_rc(new_rc, dir)
        orig_type = board[old_rc['row']][old_rc['col']]
        board[old_rc['row']][old_rc['col']] = board[new_rc['row']][new_rc['col']]
        board[new_rc['row']][new_rc['col']] = orig_type
        return {'board': board, 'rc': new_rc}

    def copy_solution_with_cursor(self, solution, i, j, init_cursor = None):
        new_solution = deepcopy(solution)
        new_cursor = init_cursor
        if new_cursor is None:
            new_cursor = self.make_rc(i, j)
        return {
            'board': self.copy_board(new_solution['board']),
            'cursor': self.make_rc(i, j),
            'init_cursor': new_cursor,
            'path': new_solution['path'],
            'is_done': new_solution['is_done'],
            'so_id': new_solution['so_id'],
            'eliminate_count': new_solution['eliminate_count'],
            'weight': 0,
            'matches': []}

    def copy_solution(self, solution):
        # new_solution = deepcopy(solution)
        # return new_solution
        return self.copy_solution_with_cursor(solution,
                                         solution['cursor']['row'], solution['cursor']['col'],
                                         solution['init_cursor'])

    def make_solution(self, board):
        return {
            'board': self.copy_board(board),
            'cursor': self.make_rc(0, 0),
            'init_cursor': self.make_rc(0, 0),
            'path': [],
            'is_done': False,
            'eliminate_count': 0,
            'weight': 0,
            'so_id' : -1,
            'matches': []}

    def in_place_evaluate_solution(self, solution, weights):
        current_board = self.copy_board(solution['board'])
        all_matches = []
        eliminate_count = 0
        # global loopCount
        # loopCount = loopCount + 1
        while True:
            matches = self.find_matches(current_board)
            if len(matches['matches']) == 0:
                break
            self.in_place_remove_matches(current_board, matches['board'])
            self.in_place_drop_empty_spaces(current_board)
            all_matches = all_matches + matches['matches']
            eliminate_count = eliminate_count + matches['total_count']
        # print("finish " + str(loopCount))
        solution['weight'] = self.compute_weight(all_matches, weights)
        solution['matches'] = all_matches
        solution['eliminate_count'] = eliminate_count
        return current_board

    def can_move_orb_in_solution(self, solution, dir):
        if len(solution['path']) > 0 and solution['path'][len(solution['path']) - 1] == (dir + 4) % 8:
            return False
        return self.can_move_orb(solution['cursor'], dir)

    def in_place_swap_orb_in_solution(self, solution, dir):
        res = self.in_place_swap_orb(solution['board'], solution['cursor'], dir)
        solution['cursor'] = res['rc']
        solution['path'].append(dir)

    def evolve_solutions(self, solutions, weights, dir_step):
        new_solutions = []
        num = 0
        for s in solutions:
            num = num + 1
            try:
                if "is_done" in s and s['is_done'] is False:
                    for dir in range(0, 8, dir_step):
                        if self.can_move_orb_in_solution(s, dir) is False:
                            continue
                        solution = self.copy_solution(s)
                        self.in_place_swap_orb_in_solution(solution, dir)
                        self.in_place_evaluate_solution(solution, weights)
                        global so_id
                        solution['so_id'] = so_id
                        so_id = so_id + 1
                        new_solutions.append(solution)
                    s['is_done'] = True
            except:
                print("error")

        solutions = solutions + new_solutions

        # solutions.sort(key=lambda e: (str(e['weight']), e['so_id']),  reverse=True)
        # collections.OrderedDict(sorted(solutions, key=lambda k: k['weight']))
        # ordered_solutions = collections.OrderedDict()
        # for sol in solutions:
        #     ordered_solutions = sol
        solutions.sort(key=lambda e: e['so_id'])
        solutions.sort(key=lambda e: e['weight'],  reverse=True)
        return solutions[0: self.MAX_SOLUTIONS_COUNT]

    def solve_board(self, board, step_callback, finish_callback, firstPoint=None):
        for i in range(self.ROWS):
            for j in range(self.COLS):
                if board[i][j] is None:
                    return False

        solutions = [None] * (self.ROWS * self.COLS)
        weights = self.get_weights()
        global so_id
        so_id = 0

        seed_solution = self.make_solution(board)
        self.in_place_evaluate_solution(seed_solution, weights)
        s = 0
        if firstPoint is not None:
            solutions = self.copy_solution_with_cursor(seed_solution, firstPoint[0], firstPoint[1])
        else:
            for i in range(self.ROWS):
                for j in range(self.COLS):
                    solutions[s] = self.copy_solution_with_cursor(seed_solution, i, j)
                    solutions[s]['so_id'] = so_id
                    s = s + 1
                    so_id = so_id + 1

        solve_state = {
            'step_callback': step_callback,
            'finish_callback': finish_callback,
            'max_length': self.maxPathLength,
            'dir_step': 1 if self.is_8_dir_movement_supported() else 2,
            'p': 0,
            'solutions': solutions,
            'weights': weights,
        }

        self.solve_board_step(solve_state)
        return True

    def solve_board_step(self, solve_state):
        if solve_state['p'] >= solve_state['max_length']:
            solve_state['finish_callback'](solve_state['solutions'])
            return
        solve_state['p'] = solve_state['p'] + 1

        solve_state['solutions'] = self.evolve_solutions(solve_state['solutions'],
                                                 solve_state.get('weights'),
                                                 solve_state.get('dir_step'))
        solve_state['step_callback'](solve_state['p'], solve_state['max_length'])

        self.solve_board_step(solve_state)

    def simplify_path(self, xys):
        simplified_xys = [xys[0]]
        xys_length_1 = len(xys)- 1
        for i in range(1, xys_length_1):
            dx0 = xys[i]['x'] - xys[i-1]['x']
            dx1 = xys[i+1]['x'] - xys[i]['x']
            if dx0 == dx1:
                dy0 = xys[i]['y'] - xys[i-1]['y']
                dy1 = xys[i+1]['y'] - xys[i]['y']
                if dy0 == dy1:
                    continue
            simplified_xys.append(xys[i])
        simplified_xys.append(xys[xys_length_1])

        return simplified_xys
