from screenDetect import ScreenDetect, ScreenDetectForColor
import urllib.request

import numpy as np
import cv2




if __name__ == '__main__':
    # screenDetect = ScreenDetect(".", False)
    screenDetect = ScreenDetectForColor(".", False)

    # padImgUrl = "https://1.bp.blogspot.com/-yI8vq3Ludcs/XqhRYr7YDtI/AAAAAAADjDQ/ey3t3W-BebQnb9OZUeByllUXodmzZde3wCLcBGAsYHQ/s1600/0.PNG"
    # resp = urllib.request.urlopen(padImgUrl)
    # im = np.asarray(bytearray(resp.read()), dtype="uint8")
    # im = cv2.imdecode(im, cv2.IMREAD_COLOR)

    # im = cv2.imread('../simulator/screenCap/n20200501215805.jpg')
    im = cv2.imread('../simulator/screenCap/n20200425160722.jpg')
    boxItems = screenDetect.getBoxItems(im)

    for b in boxItems:
        print("label: {}, box: {}".format(b.getLabel(), b.getCenterPoint()))

    width = int(1080 / 2)
    height = int(1920 / 2)
    labelImg = screenDetect.getLabelImg()
    cv2.namedWindow('org', 0)
    # cv2.imshow('org', im)
    img_concate_Hori = np.concatenate((im, labelImg), axis=1)
    cv2.imshow('org', img_concate_Hori)
    if type(screenDetect) == ScreenDetectForColor:
        cv2.imshow("rorb", cv2.resize(screenDetect.getImgForName("rorb"), (width, height), interpolation=cv2.INTER_CUBIC))
        cv2.imshow("borb", cv2.resize(screenDetect.getImgForName("borb"), (width, height), interpolation=cv2.INTER_CUBIC))
        cv2.imshow("gorb", cv2.resize(screenDetect.getImgForName("gorb"), (width, height), interpolation=cv2.INTER_CUBIC))
        cv2.imshow("lorb", cv2.resize(screenDetect.getImgForName("lorb"), (width, height), interpolation=cv2.INTER_CUBIC))
        cv2.imshow("dorb", cv2.resize(screenDetect.getImgForName("dorb"), (width, height), interpolation=cv2.INTER_CUBIC))
        cv2.imshow("horb", cv2.resize(screenDetect.getImgForName("horb"), (width, height), interpolation=cv2.INTER_CUBIC))
        # cv2.imshow("jorb", cv2.resize(screenDetect.getImgForName("jorb"), (width, height), interpolation=cv2.INTER_CUBIC))
        # cv2.imshow("porb", cv2.resize(screenDetect.getImgForName("porb"), (width, height), interpolation=cv2.INTER_CUBIC))
        # cv2.imshow("oorb", cv2.resize(screenDetect.getImgForName("oorb"), (width, height), interpolation=cv2.INTER_CUBIC))
    cv2.waitKey(0)


    cv2.destroyAllWindows()


