from screenDetect import ScreenDetect, BoxItem
from solver8 import PadSolver
import urllib.request

import numpy as np
import cv2
from copy import copy, deepcopy

class PadBoard:
    def __init__(self, boardWidth=1080, boardHeight=920):
        self.boardWidth = boardWidth
        self.boardHeight = boardHeight

    def updateBoard(self, boxes, labels, defaultNoneBoard=None):
        self.boxes = boxes
        self.labels = labels
        boxitems = []

        self.labenBoard = None
        self.numBoard = None
        self.plusBoard = None
        self.lockBoard = None
        self.boardRow = 0
        self.boardCol = 0

        self.boardState = []

        self.orbCount = 0
        self.orbNonUseCount = 0
        self.lockCount = 0
        self.plusCount = 0

        rorbCount = 0
        borbCount = 0
        gorbCount = 0
        lorbCount = 0
        dorbCount = 0
        horbCount = 0

        self.latestNonOrb = None

        if "6x5board" in self.labels:
            p = self.labels.index("6x5board")
            boardBox = boxes[p].getBox()

            self.boardTop = boardBox[0][1]
            # self.boardWidth = boardBox[0][2] - boardBox[0][0]
            # self.boardHeight = boardBox[0][3] - boardBox[0][1]
            self.labenBoard = [None] * 5
            self.numBoard = [None] * 5
            self.plusBoard = [None] * 5
            self.lockBoard = [None] * 5
            for i in range(5):
                self.labenBoard[i] = [None] * 6
                self.numBoard[i] = [None] * 6
                self.plusBoard[i] = [0] * 6
                self.lockBoard[i] = [0] * 6
            self.boardRow = 5
            self.boardCol = 6

        elif "7x6board" in self.labels:
            p = self.labels.index("7x6board")
            boardBox = boxes[p].getBox()

            self.boardTop = boardBox[0][1]
            # self.boardWidth = boardBox[0][2] - boardBox[0][0]
            # self.boardHeight = boardBox[0][3] - boardBox[0][1]

            self.labenBoard = [defaultNoneBoard] * 6
            self.numBoard = [defaultNoneBoard] * 6
            self.plusBoard = [defaultNoneBoard] * 6
            self.lockBoard = [defaultNoneBoard] * 6
            for i in range(6):
                self.labenBoard[i] = [defaultNoneBoard] * 7
                self.numBoard[i] = [defaultNoneBoard] * 7
                self.plusBoard[i] = [0] * 7
                self.lockBoard[i] = [0] * 7
            self.boardRow = 6
            self.boardCol = 7

        else:
            return None


        for b in self.boxes:

            if len(b.labtl) >= 4 and b.labtl[1: 4] == "orb":
                posX, posY = self.toBoardPos(self.boardRow, self.boardCol, b.getCenterPoint())
                # print("orb {}: {} {} {}".format(orbCount, b.labtl, b.getCenterPoint(), b.box))
                if b.labtl[1: 6] == "orb_n":
                    self.orbNonUseCount = self.orbNonUseCount + 1
                else:
                    self.orbCount = self.orbCount + 1
                    self.latestNonOrb = [posX, posY]

                # print("{}, {}".format(posX, posY))
                if posX is not None or posY is not None:
                    self.labenBoard[posY][posX] = b.getLabel()
                    orbNum = self.boardType2Num(b.getLabel())
                    if defaultNoneBoard is not None and orbNum is None:
                        orbNum = defaultNoneBoard
                    self.numBoard[posY][posX] = orbNum
                    if orbNum == 0:
                        rorbCount = rorbCount + 1
                    elif orbNum == 1:
                        borbCount = borbCount + 1
                    elif orbNum == 2:
                        gorbCount = gorbCount + 1
                    elif orbNum == 3:
                        lorbCount = lorbCount + 1
                    elif orbNum == 4:
                        dorbCount = dorbCount + 1
                    elif orbNum == 5:
                        horbCount = horbCount + 1
                else:
                    print("error")
            if b.labtl == "plus":
                posX, posY = self.toBoardPos(self.boardRow, self.boardCol, b.getCenterPoint())
                if posX is not None or posY is not None:
                    self.plusBoard[posY][posX] = 1
                    self.plusCount = self.plusCount + 1
            if b.labtl == "lock":
                posX, posY = self.toBoardPos(self.boardRow, self.boardCol, b.getCenterPoint())
                if posX is not None or posY is not None:
                    self.lockBoard[posY][posX] = 1
                    self.lockCount = self.lockCount + 1
        print("".format(self.labenBoard))
        print("board: {}".format(self.numBoard))
        print("plus: {}".format(self.plusBoard))
        print("lock: {}".format(self.lockBoard))
        print("rorb: {}, borb: {}, gorb: {}, lorb: {}, dorb: {}, horb: {}"
              .format(rorbCount, borbCount, gorbCount, lorbCount, dorbCount, horbCount))
        print("orb: {}, non orb: {}".format(self.orbCount, self.orbNonUseCount))
        return boxitems

    def getNumBoard(self):
        # return np.copy(self.numBoard)
        return deepcopy(self.numBoard)

    def getBoardSize(self):
        return self.boardRow, self.boardCol

    def getBoardState(self):
        return self.boardState

    def getNonOrbCount(self):
        return self.orbNonUseCount

    def getLatstNonOrbPos(self):
        return self.latestNonOrb

    def getOrbCount(self):
        return self.orbCount

    def getLockCount(self):
        return self.lockCount

    def toBoardPos(self, boardRow, boardCol, box):
        posX = -1
        posY = -1
        preWidth = self.boardWidth / boardCol
        preHeight = self.boardHeight / boardRow
        # print("{}, {}, {}".format(preWidth, self.boardWidth, boardCol))
        if preWidth > box[0]:
            posX = 0
        elif preWidth * 2 > box[0]:
            posX = 1
        elif preWidth * 3 > box[0]:
            posX = 2
        elif preWidth * 4 > box[0]:
            posX = 3
        elif preWidth * 5 > box[0]:
            posX = 4
        elif preWidth * 6 > box[0]:
            posX = 5
        elif preWidth * 7 > box[0]:
            posX = 6
        else:
            return posX, None

        if self.boardTop + preHeight > box[1]:
            posY = 0
        elif self.boardTop + preHeight * 2 > box[1]:
            posY = 1
        elif self.boardTop + preHeight * 3 > box[1]:
            posY = 2
        elif self.boardTop + preHeight * 4 > box[1]:
            posY = 3
        elif self.boardTop + preHeight * 5 > box[1]:
            posY = 4
        elif self.boardTop + preHeight * 6 > box[1]:
            posY = 5
        else:
            return None, posY

        return posX, posY

    def boardType2Num(self, boardType):
        if boardType == "rorb":
            return 0
        elif boardType == "rorb_n":
            return 0
        elif boardType == "borb":
            return 1
        elif boardType == "borb_n":
            return 1
        elif boardType == "gorb":
            return 2
        elif boardType == "gorb_n":
            return 2
        elif boardType == "lorb":
            return 3
        elif boardType == "lorb_n":
            return 3
        elif boardType == "dorb":
            return 4
        elif boardType == "dorb_n":
            return 4
        elif boardType == "horb":
            return 5
        elif boardType == "horb_n":
            return 5
        elif boardType == "jorb":
            return 6
        elif boardType == "jorb_n":
            return 6
        elif boardType == "porb":
            return 7
        elif boardType == "porb_n":
            return 7
        elif boardType == "oorb":
            return 8
        elif boardType == "oorb_n":
            return 8
        elif boardType == "morb":
            return 9
        elif boardType == "morb_n":
            return 9
        elif boardType == "norb":
            return 10
        else:
            return None

def step_callback(p, max_p):
    print('Solving ({} / {})...'.format(p, max_p))


def finish_callback(solutions):
    print("solution :{}".format(solutions))

if __name__ == '__main__':
    screenDetect = ScreenDetect(".", False)

    # padImgUrl = "https://1.bp.blogspot.com/-7-xYz9_Te4Y/XpiCEYQ7dpI/AAAAAAADh2o/RNApfsNpfs8qS_hhVtbyThTiErXIy5s4ACLcBGAsYHQ/s1600/IMG_8566.PNG"
    padImgUrl = "https://4.bp.blogspot.com/-EocazgLOVYA/XK60j2TgNKI/AAAAAAADDhU/sCwI0j5bo3UJBmruI5vUas4zYvSkwx1ngCLcBGAs/s1600/1.jpg"

    resp = urllib.request.urlopen(padImgUrl)
    im = np.asarray(bytearray(resp.read()), dtype="uint8")
    im = cv2.imdecode(im, cv2.IMREAD_COLOR)

    boxItems = screenDetect.getBoxItems(im)
    labels = screenDetect.getLabels()
    padBoard = PadBoard()
    padBoard.updateBoard(boxItems, labels)

    for b in boxItems:
        print("label: {}, box: {}".format(b.getLabel(), b.getCenterPoint()))

    row, col = padBoard.getBoardSize()
    print("{}x{}\n{}".format(col, row, padBoard.getNumBoard()))

    padSolver = PadSolver()
    padSolver.solve_board(padBoard.getNumBoard(), step_callback, finish_callback)

    cv2.imshow('org', im)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
