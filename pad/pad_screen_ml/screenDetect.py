from detectron2.engine import DefaultTrainer
from detectron2.config import get_cfg
from detectron2.utils.visualizer import ColorMode

import os

import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()

import numpy as np
import cv2
import random

from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import DatasetCatalog, MetadataCatalog
from detectron2.structures.boxes import Boxes
from detectron2.structures.rotated_boxes import RotatedBoxes

from detectron2.data.datasets import register_coco_instances

class BoxItem:
    def __init__(self, label, box):
        self.labtl = label
        if isinstance(box, Boxes) or isinstance(box, RotatedBoxes):
            self.box = box.tensor.numpy()
        else:
            self.box = np.asarray(box)

    def __str__(self):
        return str(self.labtl) + " " + str(self.getCenterPoint())

    def getCenterPoint(self):
        return [self.box[0][0] + (self.box[0][2] - self.box[0][0]) / 2, self.box[0][1] + (self.box[0][3] - self.box[0][1]) / 2]

    def getLabel(self):
        return self.labtl

    def getBox(self):
        return self.box

class ScreenDetect:
    def __init__(self):
        pass

    def getBoxItems(self, screen):
        return None

    def getScreenWidthHeight(self):
        return None

    def getLabels(self):
        return None

    def getLabelImg(self):
        return None

class ScreenDetectForDetectron2(ScreenDetect):
    def __init__(self, path, isGpu=True):
        self.gpu = True
        if isGpu is False:
            self.gpu = False
        register_coco_instances("pad_orb_train", {}, path + "/trainval.json", path + "/train")
        register_coco_instances("pad_orb_test", {}, path + "/trainval.json", path + "/test")
        self.pad_orb_train_metadata = MetadataCatalog.get("pad_orb_train")
        dataset_dicts = DatasetCatalog.get("pad_orb_train")

        self.cfg = get_cfg()
        self.cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
        self.cfg.DATASETS.TRAIN = ("pad_orb_train",)
        self.cfg.DATASETS.TEST = ("pad_orb_test",)  # no metrics implemented for this dataset
        self.cfg.DATALOADER.NUM_WORKERS = 2
        self.cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(
            "COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")  # initialize from model zoo
        self.cfg.SOLVER.IMS_PER_BATCH = 2
        self.cfg.SOLVER.BASE_LR = 0.02
        self.cfg.SOLVER.MAX_ITER = 12000  # 30 0 iterations seems good enough, but you can certainly train longer
        self.cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128  # faster, and good enough for this toy dataset
        self.cfg.MODEL.ROI_HEADS.NUM_CLASSES = 57  # 3 classes (data, fig, hazelnut)
        self.cfg.MODEL.WEIGHTS = os.path.join(path + "/output3", "model_final.pth")
        # self.cfg.MODEL.WEIGHTS = os.path.join(path + "/output2", "model_0004999.pth")

        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7  # set the testing threshold for this model
        # https://github.com/facebookresearch/detectron2/issues/374
        if self.gpu is False:
            self.cfg.MODEL.DEVICE='cpu'
        self.predictor = DefaultPredictor(self.cfg)

    def getBoxItems(self, screen):
        self.screen = screen
        self.predictorScreen = self.predictor(screen)
        self.outputs = self.predictorScreen["instances"].to("cpu")

        self.boxes = self.outputs.pred_boxes if self.outputs.has("pred_boxes") else None
        # scores = predictions.scores if predictions.has("scores") else None
        self.classes = self.outputs.pred_classes if self.outputs.has("pred_classes") else None
        self.labels = [self.pad_orb_train_metadata.get("thing_classes", None)[i] for i in self.classes]
        self.keypoints = self.outputs.pred_keypoints if self.outputs.has("pred_keypoints") else None

        self.screenHeight, self.screenWidth = self.outputs.image_size

        boxSize = len(self.boxes)
        boxItems = []
        for j in range(boxSize):
            boxItem = BoxItem(self.labels[j], self.boxes[j])
            boxItems.append(boxItem)
        return boxItems

    def getScreenWidthHeight(self):
        return self.screenWidth, self.screenHeight

    def getLabels(self):
        return self.labels

    def getLabelImg(self):
        if self.screen is not None and self.outputs is not None:
            v = Visualizer(self.screen[:, :, ::-1],
                           metadata=self.pad_orb_train_metadata,
                           scale=1,
                           instance_mode=ColorMode.IMAGE_BW  # remove the colors of unsegmented pixels
                           )
            v = v.draw_instance_predictions(self.outputs)
            return v.get_image()
        return None


class ScreenDetectForColor(ScreenDetect):
    def __init__(self, path, isGpu=True):
        self.screenHeight = 1920
        self.screenWidth = 1080
        # self.COLS = 6
        # self.ROWS = 5
        # self.is6x5 = True
        self.COLS = 7
        self.ROWS = 6
        self.is6x5 = False
        self.labels = []
        self.orbInfo = None
        self.setupDateSet()

    def contrast_img(self, img1, c, b):  # 亮度就是每个像素所有通道都加上b
        rows, cols, channels = img1.shape

        # 新建全零(黑色)图片数组:np.zeros(img1.shape, dtype=uint8)
        blank = np.zeros([rows, cols, channels], img1.dtype)
        dst = cv2.addWeighted(img1, c, blank, 1-c, b)
        return dst

    def setupDateSet(self):
        self.orbInfo = []
        self.rorbImg = None
        self.borbImg = None
        self.gorbImg = None
        self.lorbImg = None
        self.dorbImg = None
        self.horbImg = None
        self.jorbImg = None
        self.porbImg = None
        self.oorbImg = None

        if self.is6x5:
            self.boardCellSize = 178
            self.boardSizeWidth = 150
            self.boardSizeHeight = 120
        else:
            self.boardCellSize = 147

            self.boardSizeWidth = 100
            self.boardSizeHeight = 90

        self.orbInfo.append(
            self.orbInfoFormat("rorb", [0, 90, 150], [22, 255, 255], self.boardSizeWidth, self.boardSizeHeight))
        self.orbInfo.append(
            self.orbInfoFormat("borb", [82, 70, 165], [119, 255, 209], self.boardSizeWidth, self.boardSizeHeight))
        self.orbInfo.append(
            self.orbInfoFormat("gorb", [36, 0, 0], [85, 255, 255], self.boardSizeWidth, self.boardSizeHeight))
        self.orbInfo.append(
            self.orbInfoFormat("lorb", [20, 50, 100], [30, 255, 255], self.boardSizeWidth, self.boardSizeHeight))
        self.orbInfo.append(
            self.orbInfoFormat("dorb", [150, 40, 174], [161, 156, 233], self.boardSizeWidth, self.boardSizeHeight))
        self.orbInfo.append(
            self.orbInfoFormat("horb", [158, 87, 54], [171, 255, 255], self.boardSizeWidth, self.boardSizeHeight))
        self.orbInfo.append(
            self.orbInfoFormat("jorb", [100, 120, 0], [120, 200, 150], self.boardSizeWidth - 10, self.boardSizeHeight))
        self.orbInfo.append(
            self.orbInfoFormat("porb", [130, 0, 0], [150, 255, 80], self.boardSizeWidth, self.boardSizeHeight))
        self.orbInfo.append(
            self.orbInfoFormat("oorb", [25, 0, 17], [161, 165, 118], self.boardSizeWidth - 10, self.boardSizeHeight - 10))


    def getImgForName(self, name):
        if self.orbInfo is not None:
            for info in self.orbInfo:
                if info["name"] == name:
                    return info["img"]
            return None
        else:
            return None

    def orbInfoFormat(self, name, hsvMin, hsvMax, width, height):
        return {"name": name,
                "img": None,
                "hsvMin": hsvMin,
                "hsvMax": hsvMax,
                "width": width,
                "height": height
                }

    def getBoxItems(self, screen):
        self.screen = screen
        boxItems = []
        self.labels = []
        if self.is6x5:
            boxItem = BoxItem("6x5board", [[0, self.screenHeight - (self.boardCellSize * 5), self.screenWidth, self.screenHeight]])
            boxItems.append(boxItem)
            self.labels.append("6x5board")
        else:
            boxItem = BoxItem("7x6board",
                              [[0, self.screenHeight - (self.boardCellSize * 6), self.screenWidth, self.screenHeight]])
            boxItems.append(boxItem)
            self.labels.append("7x6board")

        leftSpace = int((self.screenWidth - self.boardCellSize * self.COLS) / 2)

        for i in range(self.COLS):
            cv2.line(screen,
                     (leftSpace + int(self.boardCellSize * (i + 1.0)), int(self.screenHeight / 2)),
                     (leftSpace + int(self.boardCellSize * (i + 1.0)), self.screenHeight),
                     (255, 255, 255),
                     3)
        imgBlur = self.contrast_img(screen, 1.3, 3)
        self.im = cv2.fastNlMeansDenoisingColored(screen, None, 10, 10, 7, 21)
        hsv_img = cv2.cvtColor(self.im, cv2.COLOR_BGR2HSV)  # HSV image

        # All
        for info in self.orbInfo:
            print(info)
            imgGrey = cv2.inRange(hsv_img, np.array(info["hsvMin"], np.uint8), np.array(info["hsvMax"], np.uint8))
            ret, thresh = cv2.threshold(imgGrey, 127, 255, 0)
            info["img"], contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            for cnt in contours:
                x, y, w, h = cv2.boundingRect(cnt)
                if w > info["width"] and h > info["height"] and y > self.screenHeight / 2:
                    print("{} x: {}, y: {}, w: {}, h: {}".format(info["name"], x, y, w, h))
                    cv2.rectangle(self.im, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    cv2.putText(self.im, info["name"], (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255), 5, cv2.LINE_AA)
                    cv2.putText(self.im, info["name"], (x, y), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2, cv2.LINE_AA)
                    pos = [[x, y, x + w, y + h]]
                    boxItem = BoxItem(info["name"], pos)
                    boxItems.append(boxItem)
                    self.labels.append(info["name"])

        return boxItems

    def getScreenWidthHeight(self):
        return self.screenWidth, self.screenHeight

    def getLabels(self):
        return self.labels

    def getLabelImg(self):
        return self.im


