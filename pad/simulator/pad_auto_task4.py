# -*- encoding=utf8 -*-
__author__ = "hugho"

import sys
import cv2
import numpy as np
import urllib.request

sys.path.append("../..")

from pad.base.BasePadAutoSolver import BasePadAutoSolver
from tool.tools import *
# from tool.programWindowCap import *

class SimulatorPadSolver(BasePadAutoSolver):


    def finishAndAfterAction(self):
        pass

    def screenLabelAction(self, labels):
        if self.labels is not None:
            if "selfstate_nonawakening" in self.labels:
                self.useSkill(6, 1, 4, "解覺醒")

            if "monsterstate_throughshield" in self.labels and "monsterstate_shield" in self.labels:
                self.useSkill(4, 1, 4, "貫通")
                self.useSkill(1, 1, 8, "up攻擊")
                self.reAsyncScreenLabel()
            norbCount = 0
            for label in self.labels:
                if label[1: 6] == "orb_n":
                    norbCount = norbCount + 1

            # if norbCount > 30:
            #     self.abstainGame()
            #     return True

        return False
            # if "monsterstate_1hp" in self.labels and self.roundCount > 5:
            #     self.useSkill(4, 1, 4, "傷吸")



    def roundLoopAction(self, roundCount, loopCount):
        # if self.roundCount > 1 and (self.roundCount - 1) % 6 == 0:
        #     self.useSkill(3, 1, 2, "光drop珠")
        #     self.screen = G.DEVICE.snapshot(filename=None)
        # elif self.roundCount > 1 and (self.roundCount - 1) % 3 == 0:
        #     self.useSkill(2, 1, 2, "光drop珠")
        #     self.screen = G.DEVICE.snapshot(filename=None)
        pass
        # if self.roundCount > 5:
        #     self.screenDetect = self.screenDetectForDetectron2
        # else:
        #     self.screenDetectForColor.is6x5 = False
        #     self.screenDetect = self.screenDetectForColor

    def startEnterGame(self):
        super().startEnterGame()
        self.useSkill(2, 1, 2, "+2SB")
        self.useSkill(6, 1, 2, "勇者變身")

    def boardSolvedDaily(self):
        return 22

    def gameClear(self):
        self.delayTap(556, 1582, 2, "OK")
        self.delayTap(556, 1667, 1, "OK")
        self.delayTap(54, 655, 5)
        self.printSleep(3)


        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(622, 649, 2, "選關")
        self.enterState()
        self.printSleep(3)
        screen = G.DEVICE.snapshot(filename=None)
        if self.screenMatch(screen,
                       Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)),
                       "沒體"):
            self.delayTap(76, 1805, 1, "HOME")
            self.delayTap(76, 1805, 2, "HOME")
            self.delayTap(286, 1486, 2, "左下")
            self.delayTap(597, 620, 2, "0體百花")
            self.enterState()

        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def gameOver(self):
        saveScreen(self.previousScreen, "screenCap/{}.jpg".format(getCurrentDateTime()))

        self.delayTap(672, 1571, 5, "否")
        self.delayTap(348, 1599, 2, "是")
        self.delayTap(524, 1605, 1, "OK")
        self.delayTap(54, 655, 6)
        self.delayTap(622, 649, 2, "選關")
        self.enterState()
        self.printSleep(3)
        screen = G.DEVICE.snapshot(filename=None)
        if self.screenMatch(screen,
                       Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)),
                       "沒體"):
            input("")

        self.delayTap(520, 544, 3, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()
        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def abstainGame(self):
        self.delayTap(982, 101, 2, "MENU")
        self.delayTap(507, 1439, 2, "放棄")
        self.delayTap(366, 1154, 2, "是")
        self.printSleep(5)
        self.delayTap(622, 649, 2, "選關")
        self.enterState()
        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def enterState(self):
        self.delayTap(622, 649, 3, "第1關")
        # self.delayTap(622, 900, 3, "第2關")
        # self.delayTap(622, 1200, 3, "第3關")
        # self.delayTap(622, 1500, 3, "第4關")
        # self.delayTap(622, 1624, 3, "第5關")

if __name__ == '__main__':
    # 米奇隊
    pasSolver = SimulatorPadSolver(1080, 1920)
    G.DEVICE.snapshot(filename=None)
    pasSolver.startEnterGame()
    while True:
        pasSolver.solver()










