# -*- encoding=utf8 -*-
__author__ = "hugho"

import sys
import cv2
import numpy as np
import urllib.request

sys.path.append("../..")

from pad.base.BasePadAutoSolver import BasePadAutoSolver
from tool.tools import *


# from tool.programWindowCap import *

class SimulatorPadSolver(BasePadAutoSolver):

    def needCheckScreen(self, round):
        # if self.screenMatch(self.screen,Template(r"tpl1591516353331.png", record_pos=(-0.024, -0.573), resolution=(1080, 1920)),
        #                     "聖誕龍"):
        #     return True
        # else:
        if round == 5:
            self.useSkill(round + 1, 1, 5)
        else:
            self.useSkill(round, 1, 8)
        return False

    def useSkill(self, num, beforeDelay, afterDelay, msg=None):
        if msg is not None:
            logMsg("使用技能: {}".format(msg))
        skillPos = ((self.deviceWidth / 6) * num) - (self.deviceWidth / 12)
        self.delayTap(skillPos, 851, beforeDelay, "第{}只角色".format(num))  # skill
        self.delayTap(380, 1655, 1, "確定")
        # self.delayTap(380, 1580, 0.5, "確定")
        self.printSleep(afterDelay)

    def boardSolvedDaily(self):
        return 8

    def gameClear(self):
        self.delayTap(556, 1582, 2, "OK")
        self.delayTap(556, 1667, 1, "OK")
        self.delayTap(54, 655, 5)
        self.printSleep(3)

        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)

        screen = self.getCapScreen()
        if self.tapScreenIsMatch(self.screenMatch(screen, Template(r"tpl1592224607734.png", record_pos=(-0.002, 0.074), resolution=(1080, 1920)), "OK")):
            self.printSleep(7)
            self.delayTap(532, 1250, 5, "OK")
            self.delayTap(525, 1319, 3, "OK")
            self.delayTap(510, 1094, 3, "OK")
            self.delayTap(76, 1805, 5, "HOME")
            self.printSleep(5)
            
        self.delayTap(622, 649, 2, "選關")
        screen = self.getCapScreen()
        self.printSleep(2)

        # self.tapScreenIsMatch(self.screenMatch(screen, Template(r"tpl1592223467217.png", record_pos=(-0.213, -0.28), resolution=(1080, 1920)), "出擊"))

        self.enterState()
        self.printSleep(3)
        screen = self.getCapScreen()
        if self.screenMatch(screen,
                            Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)),
                            "沒體"):
            self.delayTap(76, 1805, 1, "HOME")
            self.delayTap(76, 1805, 2, "HOME")
            self.delayTap(286, 1486, 2, "左下")
            self.delayTap(597, 620, 2, "0體百花")
            self.enterState()

        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def gameOver(self):
        saveScreen(self.previousScreen, "screenCap/{}.jpg".format(getCurrentDateTime()))

        self.delayTap(672, 1571, 5, "否")
        self.delayTap(348, 1599, 2, "是")
        self.delayTap(524, 1605, 1, "OK")
        self.delayTap(54, 655, 6)
        self.delayTap(622, 649, 2, "選關")
        self.enterState()
        self.printSleep(3)
        screen = G.DEVICE.snapshot(filename=None)
        if self.screenMatch(screen,
                            Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)),
                            "沒體"):
            self.delayTap(76, 1805, 1, "HOME")
            self.delayTap(76, 1805, 2, "HOME")
            # self.delayTap(286, 1486, 2, "左下")
            # self.delayTap(597, 620, 2, "0體百花")
            # self.enterState()
            input("")

        self.delayTap(520, 544, 3, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()
        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def abstainGame(self):
        self.delayTap(982, 101, 2, "MENU")
        self.delayTap(507, 1439, 2, "放棄")
        self.delayTap(366, 1154, 2, "是")
        self.printSleep(5)
        self.delayTap(622, 649, 2, "選關")
        self.enterState()
        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def enterState(self):
        self.delayTap(622, 649, 3, "第1關")
        # self.delayTap(622, 900, 3, "第2關")
        # self.delayTap(622, 1200, 3, "第3關")
        # self.delayTap(622, 1500, 3, "第4關")
        # self.delayTap(622, 1624, 3, "第5關")


if __name__ == '__main__':
    # 射擊隊
    pasSolver = SimulatorPadSolver(1080, 1920)
    G.DEVICE.snapshot(filename=None)
    pasSolver.startEnterGame()
    while True:
        pasSolver.solver()















