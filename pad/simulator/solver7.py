from copy import copy, deepcopy
import operator
import collections

ROWS = 5
COLS = 6
TYPES = 8
MULTI_ORB_BONUS = 0.25
COMBO_BONUS = 0.25
MAX_SOLUTIONS_COUNT = ROWS * COLS * 40
so_id = 0


def get_max_path_length() :
    return 40


def is_8_dir_movement_supported():
    return False

def make_rc(row, col):
    return {'row': row, 'col': col}


def make_match(mtype, count, match_type):
    return {'type': mtype, 'count': count, 'match_type': match_type}


def copy_rc(rc):
    new_rc = deepcopy(rc)
    return {'row': rc['row'], 'col': rc['col']}


def equals_xy(a, b):
    return a['x'] == b['x'] and a['y'] == b['y']


def equals_rc(a, b):
    return a['row'] == b['row'] and a['col'] == b['col']


def create_empty_board():
    result = [None] * ROWS
    for i in range(ROWS):
        result[i] = [None] * COLS
    return result


def copy_board(board):
    return deepcopy(board)


def get_weights():
    weights = [None] * TYPES
    weights[0] = {
        'normal':1,
        'mass':1,
    }
    weights[1] = {
        'normal':1,
        'mass':1,
    }
    weights[2] = {
        'normal':1,
        'mass':1,
    }
    weights[3] = {
        'normal':1,
        'mass':1,
    }
    weights[4] = {
        'normal':1,
        'mass':1,
    }
    weights[5] = {
        'normal':0.3,
        'mass':0.3,
    }
    weights[6] = {
        'normal':0.1,
        'mass':0.1,
    }
    weights[7] = {
        'normal':0.1,
        'mass':0.1,
    }
    return weights


def find_matches(board):
    match_board = create_empty_board()
    for i in range(ROWS):
        prev_1_orb = 'X'
        prev_2_orb = 'X'
        for j in range(COLS):
            cur_orb = board[i][j]
            if prev_1_orb == prev_2_orb and prev_2_orb == cur_orb and cur_orb != 'X':
                match_board[i][j] = cur_orb
                match_board[i][j-1] = cur_orb
                match_board[i][j-2] = cur_orb
            prev_1_orb = prev_2_orb
            prev_2_orb = cur_orb

    for j in range(COLS):
        prev_1_orb = 'X'
        prev_2_orb = 'X'
        for i in range(ROWS):
            cur_orb = board[i][j]
            if prev_1_orb == prev_2_orb and prev_2_orb == cur_orb and cur_orb != 'X':
                match_board[i][j] = cur_orb
                match_board[i-1][j] = cur_orb
                match_board[i-2][j] = cur_orb
            prev_1_orb = prev_2_orb
            prev_2_orb = cur_orb

    scratch_board = copy_board(match_board)

    temp_check_match = []
    for r in range(ROWS):
        temp_check_match.append([0] * COLS)

    matches = []
    total_count = 0
    for i in range(ROWS):
        for j in range(COLS):
            cur_orb = scratch_board[i][j]
            if cur_orb is None:
                continue
            stack = [make_rc(i, j)]
            count = 0
            temp_check_match = []
            for r in range(ROWS):
                temp_check_match.append([0] * COLS)

            while len(stack):
                n = stack.pop()
                if scratch_board[n['row']][n['col']] != cur_orb:
                    continue
                count = count + 1
                temp_check_match[n['row']][n['col']] = 1
                scratch_board[n['row']][n['col']] = None
                if n['row'] > 0:
                    stack.append(make_rc(n['row']-1, n['col']))
                if n['row'] < ROWS-1:
                    stack.append(make_rc(n['row']+1, n['col']))
                if n['col'] > 0 :
                    stack.append(make_rc(n['row'], n['col']-1))
                if n['col'] < COLS-1:
                    stack.append(make_rc(n['row'], n['col']+1))
            match_type = 0
            # match 0 = 3 match
            # 1 = 4
            # 2 = col 5
            # 3 = row 6

            if count == 4:
                match_type = 1
            if match_type == 0 and count == ROWS:
                for c in range(COLS):
                    col_same = 0
                    for r in range(ROWS):
                        if temp_check_match[r][c] == 1:
                            col_same = col_same + 1
                    if col_same == ROWS:
                        match_type = 2
            if match_type == 0 and count == COLS:
                for r in range(ROWS):
                    row_same = 0
                    for c in range(COLS):
                        if temp_check_match[r][c] == 1:
                            row_same = row_same + 1
                    if row_same == COLS:
                        match_type = 3

            matches.append(make_match(cur_orb, count, match_type))
            total_count = total_count + count

    return {'matches': matches, 'board': match_board, 'total_count': total_count}


def compute_weight(matches, weights):
    total_weight = 0
    for m in matches:
        base_weight = weights[m['type']]['mass' if m['count'] >= 5 else 'normal']
        multi_orb_bonus = (m['count'] - 3) * MULTI_ORB_BONUS + 1
        total_weight = total_weight + multi_orb_bonus * base_weight
    combo_bonus = (len(matches) - 1) * COMBO_BONUS + 1
    return total_weight * combo_bonus


def in_place_remove_matches(board, match_board):
    for i in range(ROWS):
        for j in range(COLS):
            if match_board[i][j] is not None:
                board[i][j] = 'X'
    return board


def in_place_drop_empty_spaces(board):
    for j in range(COLS):
        dest_i = ROWS-1
        for src_i in range(ROWS-1, -1, -1):
            if board[src_i][j] != 'X':
                board[dest_i][j] = board[src_i][j]
                dest_i = dest_i - 1
        for d in range(dest_i , -1, -1):
            board[d][j] = 'X'
    return board


def can_move_orb(rc, dir):
    if dir == 0:
        return rc['col'] < COLS-1
    elif dir == 1:
        return rc['row'] < ROWS-1 and rc['col'] < COLS-1
    elif dir == 2:
        return rc['row'] < ROWS-1
    elif dir == 3:
        return rc['row'] < ROWS-1 and rc['col'] > 0
    elif dir == 4:
        return rc['col'] > 0
    elif dir == 5:
        return rc['row'] > 0 and rc['col'] > 0
    elif dir == 6:
        return rc['row'] > 0
    elif dir == 7:
        return rc['row'] > 0 and rc['col'] < COLS-1
    return False


def in_place_move_rc(rc, dir):
    if dir == 0:
        rc['col'] = rc['col'] + 1
    elif dir == 1:
        rc['row'] = rc['row'] + 1
        rc['col'] = rc['col'] + 1
    elif dir == 2:
        rc['row'] = rc['row'] + 1
    elif dir ==3:
        rc['row'] = rc['row'] + 1
        rc['col'] = rc['col'] - 1
    elif dir == 4:
        rc['col'] = rc['col'] - 1
    elif dir == 5:
        rc['row'] = rc['row'] - 1
        rc['col'] = rc['col'] - 1
    elif dir == 6:
        rc['row'] = rc['row'] - 1
    elif dir == 7:
        rc['row'] = rc['row'] - 1
        rc['col'] = rc['col'] + 1


def in_place_swap_orb(board, rc, dir):
    old_rc = copy_rc(rc)
    new_rc = copy_rc(rc)
    new_board = copy_board(board)
    in_place_move_rc(new_rc, dir)
    orig_type = board[old_rc['row']][old_rc['col']]
    board[old_rc['row']][old_rc['col']] = board[new_rc['row']][new_rc['col']]
    board[new_rc['row']][new_rc['col']] = orig_type
    return {'board': board, 'rc': new_rc}


def copy_solution_with_cursor(solution, i, j, init_cursor = None):
    new_solution = deepcopy(solution)
    new_cursor = init_cursor
    if new_cursor is None:
        new_cursor = make_rc(i, j)
    return {
        'board': copy_board(new_solution['board']),
        'cursor': make_rc(i, j),
        'init_cursor': new_cursor,
        'path': new_solution['path'],
        'is_done': new_solution['is_done'],
        'so_id': new_solution['so_id'],
        'eliminate_count': new_solution['eliminate_count'],
        'weight': 0,
        'matches': []}


def copy_solution(solution):
    # new_solution = deepcopy(solution)
    # return new_solution
    return copy_solution_with_cursor(solution,
                                     solution['cursor']['row'], solution['cursor']['col'],
                                     solution['init_cursor'])


def make_solution(board):
    return {
        'board': copy_board(board),
        'cursor': make_rc(0, 0),
        'init_cursor': make_rc(0, 0),
        'path': [],
        'is_done': False,
        'eliminate_count': 0,
        'weight': 0,
        'so_id' : -1,
        'matches': []}


def in_place_evaluate_solution(solution, weights):
    current_board = copy_board(solution['board'])
    all_matches = []
    eliminate_count = 0
    # global loopCount
    # loopCount = loopCount + 1
    while True:
        matches = find_matches(current_board)
        if len(matches['matches']) == 0:
            break
        in_place_remove_matches(current_board, matches['board'])
        in_place_drop_empty_spaces(current_board)
        all_matches = all_matches + matches['matches']
        eliminate_count = eliminate_count + matches['total_count']
    # print("finish " + str(loopCount))
    solution['weight'] = compute_weight(all_matches, weights)
    solution['matches'] = all_matches
    solution['eliminate_count'] = eliminate_count
    return current_board


def can_move_orb_in_solution(solution, dir):
    if len(solution['path']) > 0 and solution['path'][len(solution['path']) - 1] == (dir + 4) % 8:
        return False
    return can_move_orb(solution['cursor'], dir)


def in_place_swap_orb_in_solution(solution, dir):
    res = in_place_swap_orb(solution['board'], solution['cursor'], dir)
    solution['cursor'] = res['rc']
    solution['path'].append(dir)


def evolve_solutions(solutions, weights, dir_step):
    new_solutions = []
    num = 0
    for s in solutions:
        num = num + 1
        if s['is_done'] is False:
            for dir in range(0, 8, dir_step):
                if can_move_orb_in_solution(s, dir) is False:
                    continue
                solution = copy_solution(s)
                in_place_swap_orb_in_solution(solution, dir)
                in_place_evaluate_solution(solution, weights)
                global so_id
                solution['so_id'] = so_id
                so_id = so_id + 1
                new_solutions.append(solution)
            s['is_done'] = True

    solutions = solutions + new_solutions

    # solutions.sort(key=lambda e: (str(e['weight']), e['so_id']),  reverse=True)
    # collections.OrderedDict(sorted(solutions, key=lambda k: k['weight']))
    # ordered_solutions = collections.OrderedDict()
    # for sol in solutions:
    #     ordered_solutions = sol
    solutions.sort(key=lambda e: e['so_id'])
    solutions.sort(key=lambda e: e['weight'],  reverse=True)
    return solutions[0: MAX_SOLUTIONS_COUNT]


def solve_board(board, step_callback, finish_callback):
    for i in range(ROWS):
        for j in range(COLS):
            if board[i][j] is None:
                return False

    solutions = [None] * (ROWS * COLS)
    weights = get_weights()
    global so_id
    so_id = 0

    seed_solution = make_solution(board)
    in_place_evaluate_solution(seed_solution, weights)
    s = 0
    for i in range(ROWS):
        for j in range(COLS):
            solutions[s] = copy_solution_with_cursor(seed_solution, i, j)
            solutions[s]['so_id'] = so_id
            s = s + 1
            so_id = so_id + 1

    solve_state = {
        'step_callback': step_callback,
        'finish_callback': finish_callback,
        'max_length': get_max_path_length(),
        'dir_step': 1 if is_8_dir_movement_supported() else 2,
        'p': 0,
        'solutions': solutions,
        'weights': weights,
    }

    solve_board_step(solve_state)
    return True


def solve_board_step(solve_state):
    if solve_state['p'] >= solve_state['max_length']:
        solve_state['finish_callback'](solve_state['solutions'])
        return
    solve_state['p'] = solve_state['p'] + 1

    solve_state['solutions'] = evolve_solutions(solve_state['solutions'],
                                             solve_state.get('weights'),
                                             solve_state.get('dir_step'))
    solve_state['step_callback'](solve_state['p'], solve_state['max_length'])

    solve_board_step(solve_state)


def simplify_path(xys):
    simplified_xys = [xys[0]]
    xys_length_1 = len(xys)- 1
    for i in range(1, xys_length_1):
        dx0 = xys[i]['x'] - xys[i-1]['x']
        dx1 = xys[i+1]['x'] - xys[i]['x']
        if dx0 == dx1:
            dy0 = xys[i]['y'] - xys[i-1]['y']
            dy1 = xys[i+1]['y'] - xys[i]['y']
            if dy0 == dy1:
                continue
        simplified_xys.append(xys[i])
    simplified_xys.append(xys[xys_length_1])

    return simplified_xys
