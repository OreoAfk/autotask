# -*- encoding=utf8 -*-
__author__ = "hugho"

import sys
import cv2
import numpy as np
import urllib.request

sys.path.append("../..")

from pad.base.BasePadAutoSolver import BasePadAutoSolver
from tool.tools import *
# from tool.programWindowCap import *

class SimulatorPadSolver(BasePadAutoSolver):


    def finishAndAfterAction(self):
        pass

    def screenLabelAction(self, labels):
        if self.labels is not None:
            if "selfstate_nonawakening" in self.labels:
                self.useSkill(6, 1, 4, "解覺醒")

            if "monsterstate_throughshield" in self.labels and "monsterstate_shield" in self.labels:
                self.useSkill(4, 1, 4, "貫通")
                self.useSkill(1, 1, 8, "up攻擊")
                self.reAsyncScreenLabel()
            norbCount = 0
            for label in self.labels:
                if label[1: 6] == "orb_n":
                    norbCount = norbCount + 1

            # if norbCount > 30:
            #     self.abstainGame()
            #     return True

        return False
            # if "monsterstate_1hp" in self.labels and self.roundCount > 5:
            #     self.useSkill(4, 1, 4, "傷吸")



    def roundLoopAction(self, roundCount, loopCount):
        if self.roundCount > 1 and (self.roundCount - 1) % 6 == 0:
            self.useSkill(3, 1, 2, "光drop珠")
            self.screen = G.DEVICE.snapshot(filename=None)
        elif self.roundCount > 1 and (self.roundCount - 1) % 3 == 0:
            self.useSkill(2, 1, 2, "光drop珠")
            self.screen = G.DEVICE.snapshot(filename=None)

        # if self.roundCount > 5:
        #     self.screenDetect = self.screenDetectForDetectron2
        # else:
        #     self.screenDetectForColor.is6x5 = False
        #     self.screenDetect = self.screenDetectForColor

    def startEnterGame(self):
        super().startEnterGame()
        self.useSkill(3, 1, 2, "光drop珠")
        self.useSkill(5, 1, 2, "+3SB")
        self.useSkill(6, 1, 2, "光魔女變身")

    def boardSolvedDaily(self):
        return 22

    def gameClear(self):
        self.delayTap(556, 1582, 2, "OK")
        self.delayTap(556, 1667, 1, "OK")
        self.delayTap(54, 655, 5)
        self.printSleep(3)
        
        # screen = G.DEVICE.snapshot(filename=None)
#         if screenMatch(screen, Template(r"tpl1588901892167.png", record_pos=(-0.018, -0.038), resolution=(1080, 1920)), "3點"):
#         if screenMatch(screen, Template(r"tpl1588902022934.png", record_pos=(-0.001, 0.069), resolution=(1080, 1920)), "OK"):

#             self.delayTap(553, 1016, 1)
#             printSleep(10)
        # if screenMatch(screen, Template(r"tpl1587820480077.png", record_pos=(-0.013, 0.096), resolution=(1080, 1920)),
        #                "Level up"):
        #     self.delayTap(54, 655, 2)
        #     self.delayTap(54, 655, 2)
        #     self.delayTap(54, 655, 2)
        #     self.delayTap(54, 655, 2)
        #     self.delayTap(54, 655, 2)
        #     self.delayTap(76, 1805, 1, "HOME")
        #     self.delayTap(76, 1805, 2, "HOME")
        #     self.delayTap(228, 810, 2, "左上")
        #     self.delayTap(597, 1620, 2, "星寶")
        #     self.delayTap(535, 589, 3, "第一關")
        #     # self.delayTap(535, 883, 1, "第二關")
        #     self.delayTap(520, 544, 3, "選好友")
        #     self.delayTap(418, 1498, 2, "挑戰")
        #     printSleep(20)
        #     self.roundCount = 1
        #     self.loopCount = 0
        #     self.nonSolverCount = 0
        #     self.useSkill(3, 1, 4, "光drop珠")
        #     self.useSkill(5, 1, 4, "+3SB")
        #     self.useSkill(1, 1, 4, "光魔女變身")
        #     return True

        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(54, 655, 2)
        self.delayTap(622, 649, 2, "選關")
        self.enterState()
        self.printSleep(3)
        screen = G.DEVICE.snapshot(filename=None)
        if self.screenMatch(screen,
                       Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)),
                       "沒體"):
            self.delayTap(76, 1805, 1, "HOME")
            self.delayTap(76, 1805, 2, "HOME")
            self.delayTap(286, 1486, 2, "左下")
            self.delayTap(597, 620, 2, "0體百花")
            self.enterState()

        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def gameOver(self):
        saveScreen(self.previousScreen, "screenCap/{}.jpg".format(getCurrentDateTime()))

        self.delayTap(672, 1571, 5, "否")
        self.delayTap(348, 1599, 2, "是")
        self.delayTap(524, 1605, 1, "OK")
        self.delayTap(54, 655, 6)
        self.delayTap(622, 649, 2, "選關")
        self.enterState()
        self.printSleep(3)
        screen = G.DEVICE.snapshot(filename=None)
        if self.screenMatch(screen,
                       Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)),
                       "沒體"):
            # self.delayTap(76, 1805, 1, "HOME")
            # self.delayTap(76, 1805, 2, "HOME")
            # self.delayTap(286, 1486, 2, "左下")
            # self.delayTap(597, 620, 2, "0體百花")
            # self.enterState()
            input("")

        self.delayTap(520, 544, 3, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()
        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def abstainGame(self):
        self.delayTap(982, 101, 2, "MENU")
        self.delayTap(507, 1439, 2, "放棄")
        self.delayTap(366, 1154, 2, "是")
        self.printSleep(5)
        self.delayTap(622, 649, 2, "選關")
        self.enterState()
        self.delayTap(520, 544, 2, "選好友")
        self.delayTap(418, 1498, 2, "挑戰")
        self.printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def enterState(self):
        self.delayTap(622, 649, 3, "第1關")
        # self.delayTap(622, 900, 3, "第2關")
        # self.delayTap(622, 1200, 3, "第3關")
        # self.delayTap(622, 1500, 3, "第4關")
        # self.delayTap(622, 1624, 3, "第5關")

if __name__ == '__main__':
    # 光魔女隊(對根性)
    pasSolver = SimulatorPadSolver(1080, 1920)
    # t = threading.Thread(target=screenCapToVideo, args=("padLoop,AGA", ))
    # t.start()
    #
    # # t.do
    G.DEVICE.snapshot(filename=None)
    pasSolver.startEnterGame()
    while True:
        pasSolver.solver()

    # hwnd = FindWindow_bySearch("padLoop,AGA")
    #
    # fourcc = cv2.VideoWriter_fourcc('M','J','P','G')
    # # vout = cv2.VideoWriter()
    # x, y, width, height = getWindow_W_H(hwnd)
    # vout = cv2.VideoWriter('output.mp4', fourcc, 30, (int(width), int(height)))
    # # vout.open('output.mp4', fourcc, 20, (int(width), int(height)), True)
    #
    # cnt = 1
    # while cnt < 200:
    #     sleep(0.03)
    #     frame = getWindow_Img(hwnd)
    #     vout.write(frame)
    #     cv2.imshow("screen box", frame)
    #     cnt += 1
    #     k = cv2.waitKey(30) & 0xFF  # 64bits! need a mask
    #     if k == 27:
    #         # cv2.destroyAllWindows()
    #         break
    # vout.release()
    #
    # # t.do_run = False
    # # t.join()
    # # cv2.imshow('org', im)
    # # cv2.waitKey(0)
    # # cv2.destroyAllWindows()











