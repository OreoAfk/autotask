# -*- encoding=utf8 -*-
__author__ = "hugho"

import sys
import cv2
import numpy as np
import urllib.request

sys.path.append("../..")

from pad.base.BasePadAutoSolver import BasePadAutoSolver
from tool.tools import *

class SimulatorPadSolver(BasePadAutoSolver):


    def finishAndAfterAction(self):
        pass

    def screenLabelAction(self, labels):
        if self.labels is not None:
            if "selfstate_nonawakening" in self.labels:
                self.useSkill(1, 3, 4, "解覺醒")
            if "monsterstate_throughshield" in self.labels:
                self.useSkill(5, 3, 4, "破貫通, 升攻")

    def roundLoopAction(self, roundCount, loopCount):
        pass

    def startEnterGame(self):
        super().startEnterGame()
        # self.useSkill(3, 1, 2, "+3SB")

    def boardSolvedDaily(self):
        return 17

    def gameClear(self):
        self.padDelayTap(556, 1582, 2, "OK")
        self.padDelayTap(556, 1667, 1, "OK")
        self.padDelayTap(54, 655, 5)
        printSleep(3)
        self.padDelayTap(54, 655, 2)
        self.padDelayTap(54, 655, 2)
        self.padDelayTap(54, 655, 2)
        self.padDelayTap(54, 655, 2)
        self.padDelayTap(54, 655, 2)
        self.padDelayTap(622, 649, 2, "選關")
        self.padDelayTap(535, 589, 3, "第一關")
        # self.padDelayTap(535, 883, 1, "第二關")
        printSleep(3)
        screen = G.DEVICE.snapshot(filename=None)
        if screenMatch(screen,
                       Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)),
                       "沒體"):
            # self.padDelayTap(76, 1805, 1, "HOME")
            # self.padDelayTap(76, 1805, 2, "HOME")
            # self.padDelayTap(286, 1486, 2, "左下")
            # self.padDelayTap(597, 620, 2, "0體百花")
            # self.padDelayTap(535, 589, 3, "第一關")
            input("stop")

        self.padDelayTap(520, 544, 2, "選好友")
        self.padDelayTap(418, 1498, 2, "挑戰")
        printSleep(self.boardSolvedDaily())
        self.startEnterGame()

    def gameOver(self):
        saveScreen(self.previousScreen, "screenCap/{}.jpg".format(getCurrentDateTime()))

        self.padDelayTap(672, 1571, 5, "否")
        self.padDelayTap(348, 1599, 2, "是")
        self.padDelayTap(524, 1605, 1, "OK")
        self.padDelayTap(54, 655, 6)
        self.padDelayTap(622, 649, 2, "選關")
        self.padDelayTap(535, 589, 3, "第一關")
        printSleep(3)
        screen = G.DEVICE.snapshot(filename=None)
        if screenMatch(screen,
                       Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)),
                       "沒體"):
            self.padDelayTap(76, 1805, 1, "HOME")
            self.padDelayTap(76, 1805, 2, "HOME")
            self.padDelayTap(286, 1486, 2, "左下")
            self.padDelayTap(597, 620, 2, "0體百花")
            self.padDelayTap(622, 649, 3, "第一關")

        self.padDelayTap(520, 544, 3, "選好友")
        self.padDelayTap(418, 1498, 2, "挑戰")
        printSleep(self.boardSolvedDaily())
        self.startEnterGame()

if __name__ == '__main__':
    pasSolver = SimulatorPadSolver(1080, 1920, 40)
    pasSolver.startEnterGame()
    while True:
        pasSolver.solver()