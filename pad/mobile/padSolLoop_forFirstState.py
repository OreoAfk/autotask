# -*- encoding=utf8 -*-
__author__ = "hugho"

from airtest.core.api import *
from airtest.cli.parser import cli_setup
from airtest.core.android.minitouch import *
from airtest.core.android.rotation import XYTransformer
from airtest import aircv

import sys
sys.path.append("../..")
from tool.tools import *
import cv2
from solver7 import *
import numpy as np
import math

# "Android://127.0.0.1:5037/32d319f8?cap_method=JAVACAP",
# "Android://127.0.0.1:5037/10.29.201.133:5555?cap_method=JAVACAP",

# "Android://127.0.0.1:5037/127.0.0.1:62001?cap_method=JAVACAP&&ori_method=ADBORI",
if not cli_setup():
    # auto_setup(__file__, logdir=True, devices=[
    #     "Android://127.0.0.1:5037/32d319f8?cap_method=JAVACAP",
    # ])
    auto_setup(__file__, logdir=True, devices=[
        "Android://127.0.0.1:5037/32d319f8?cap_method=JAVACAP&&ori_method=ADBORI",
    ])
width = 1080
height = 1920

textSize = 0.5

preWidth = width / 6
preHeight = height / 10

dev = device()
boardScreen = None


def printSleep(delay):
    currentTime = math.modf(delay)
    loopCount = currentTime[1]
    decimalDelay = currentTime[0]

    for i in range(0, int(loopCount)):
        logMsg("sleep: " + str(delay - i))
        sleep(1)

    if decimalDelay > 0:
        logMsg("sleep: " + str(decimalDelay))
        sleep(decimalDelay)

def screenMatch(screen, query, description=""):
    if screen is None:
        logMsg("screen none")
        return False
    else:
        match_pos = query.match_in(screen)
        #         print(screen)
        if match_pos:
            print(query)
            if description is not "":
                logMsg(description)
            logMsg("screen match : " + str(match_pos))
            return match_pos
        return False

def in_place_move_rc(rc, dir):
    if dir == 0:
        rc['col'] = rc['col'] + 1
    elif dir == 1:
        rc['row'] = rc['row'] + 1
        rc['col'] = rc['col'] + 1
    elif dir == 2:
        rc['row'] = rc['row'] + 1
    elif dir == 3:
        rc['row'] = rc['row'] + 1
        rc['col'] = rc['col'] - 1
    elif dir == 4:
        rc['col'] = rc['col'] - 1
    elif dir == 5:
        rc['row'] = rc['row'] - 1
        rc['col'] = rc['col'] - 1
    elif dir == 6:
        rc['row'] = rc['row'] - 1
    elif dir == 7:
        rc['row'] = rc['row'] - 1
        rc['col'] = rc['col'] + 1


def to_xy(rc):
    x = rc['col'] * preWidth + preWidth / 2
    y = height / 2 + (rc['row'] * preHeight) + preHeight / 2
    return {'x': x, 'y': y}


def move_path(init_rc, path):
    rc = copy_rc(init_rc)
    # xys = [to_xy(rc)]
    xys = []
    firstTap = to_xy(rc)
    swipe_event = [DownEvent((firstTap['x'], firstTap['y'])), SleepEvent(0.1)]
    # xys.append(to_xy(rc))
    for p in path:
        in_place_move_rc(rc, p)
        xys.append(to_xy(rc))
    # print(xys)
    for xy in xys:
        swipe_event.append(MoveEvent((xy['x'], xy['y'])))
        swipe_event.append(SleepEvent(0.1))

    swipe_event.append(UpEvent())
    dev.minitouch.perform(swipe_event)
    sleep(1)

def delayTap(x, y, second=0, actionName=None, isTap=True, longTapTime=1):
    if isTap:
        printSleep(second)
    if actionName is not None:
        logMsg("x: {}, y: {}, t: {}".format(x, y, actionName))
    else:
        logMsg("x: {}, y: {}".format(x, y))
    for _ in range(longTapTime):
        if isTap:
            G.DEVICE.touch((x, y))
            sleep(0.05)
    if isTap:
        delay_after_operation()


def duplicateDelayTap(x, y, delayTime):
    delayTap(x, y, delayTime)
    delayTap(x, y, 0.2)
    delayTap(x, y, 0.2)
    delayTap(x, y, 0.2)


def useSkill(num, beforeDelay, afterDelay):
    skillPos = ((width / 6) * num) - (width / 12)
    padDelayTap(skillPos, 851, beforeDelay)  # skill
    # padDelayTap(380, 1529, 2)
    printSleep(afterDelay)


# def saveScreen(screen, filename):
#     if screen is not None:
#         aircv.imwrite(filename, screen)
#         print("screen saved: {}".format(filename))

def padDelayTap(x, y, delay, msg=""):
    printSleep(delay)
    swipe_event = [DownEvent((x, y)), SleepEvent(0.2)]
    swipe_event.append(UpEvent())
    device().minitouch.perform(swipe_event)
    if msg is "":
        print("tap: " + str(x) + ", " + str(y))
    else:
        print("tap: " + str(x) + ", " + str(y) + " " + msg)


def checkUseShkill(screen):
    print("check 哪只怪")
    #     if screenMatch(screen, , "火屬吸"):
    #         useSkill(4, 1, 3)
    if screenMatch(screen, Template(r"tpl1586719496785.png", record_pos=(0.021, -0.66), resolution=(1080, 1920)),
                   "貫通盾"):
        useSkill(5, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586797875680.png", record_pos=(-0.061, -0.608), resolution=(1080, 1920)),
                   "暗屬吸"):
        useSkill(4, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586718516069.png", record_pos=(-0.044, -0.471), resolution=(1080, 1920)),
                   "傷吸"):
        useSkill(2, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586715087684.png", record_pos=(0.106, -0.595), resolution=(1080, 1920)),
                   "傷吸"):
        useSkill(2, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586714828101.png", record_pos=(0.051, -0.531), resolution=(1080, 1920)),
                   "光屬吸"):
        useSkill(4, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586718592106.png", record_pos=(0.08, -0.644), resolution=(1080, 1920)),
                   "火屬吸"):
        useSkill(4, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586718736095.png", record_pos=(0.0, -0.594), resolution=(1080, 1920)), "暗屬吸"):
        useSkill(4, 1, 10)
        return True

    if screenMatch(screen, Template(r"tpl1586714644239.png", record_pos=(-0.034, -0.478), resolution=(1080, 1920)),
                   "根性"):
        useSkill(3, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586722050038.png", record_pos=(-0.001, -0.637), resolution=(1080, 1920)),
                   "解覺醒"):
        useSkill(3, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586722842238.png", record_pos=(-0.049, -0.643), resolution=(1080, 1920)),
                   "屬吸"):
        useSkill(4, 1, 3)
        return True

    if screenMatch(screen, Template(r"tpl1586802814140.png", record_pos=(-0.036, -0.605), resolution=(1080, 1920)),
                   "傷吸"):
        useSkill(2, 1, 3)
        return True
    
    # if screenMatch(screen,Template(r"tpl1587211241234.png", record_pos=(-0.035, -0.576), resolution=(1080, 1920)), "傷吸"):
    #     useSkill(2, 1, 3)
    #     return True
    #
    # if screenMatch(screen,Template(r"tpl1587212317616.png", record_pos=(0.028, -0.528), resolution=(1080, 1920)), "解覺醒"):
    #     useSkill(3, 1, 3)
    #     return True


def checkEatStone(screen):
    if screenMatch(screen, Template(r"tpl1586964653321.png", record_pos=(-0.153, 0.153), resolution=(1080, 1920)),
                   "吃石"):
        padDelayTap(354, 1106, 1, "回復")
        padDelayTap(535, 1096, 2, "OK")
        padDelayTap(520, 544, 1, "選好友")
        padDelayTap(418, 1498, 2, "挑戰")
        printSleep(20)
        return True


def main():
    global boardScreen
    loopCnt = 1
    print("loopCnt: {}".format(loopCnt))
    while True:
        print("capScreen")
        screen = G.DEVICE.snapshot(filename=None)

        # checkUseShkill(screen)


        screen = G.DEVICE.snapshot(filename=None)
        orgScreen = G.DEVICE.snapshot(filename=None)

        if screenMatch(screen, Template(r"tpl1586507422342.png", record_pos=(-0.006, -0.427), resolution=(1080, 1920))):
            padDelayTap(556, 1582, 2, "OK")
            padDelayTap(556, 1667, 1, "OK")
            padDelayTap(54, 655, 5)
            padDelayTap(54, 655, 2)
            padDelayTap(54, 655, 2)
            padDelayTap(54, 655, 2)
            padDelayTap(622, 649, 2, "選關")
            padDelayTap(535, 589, 2, "第一關")
            # padDelayTap(535, 883, 1, "第二關")
            padDelayTap(520, 544, 2, "選好友")
            padDelayTap(418, 1498, 2, "挑戰")
            printSleep(20)
            loopCnt = 1

        if screenMatch(screen, Template(r"tpl1586510770121.png", record_pos=(-0.019, 0.38), resolution=(1080, 1920))):
            saveScreen(boardScreen, "screenCap/" + getCurrentDateTime() + ".jpg")

            padDelayTap(672, 1571, 5, "否")
            padDelayTap(348, 1599, 2, "是")
            padDelayTap(524, 1605, 1, "OK")
            padDelayTap(54, 655, 6)
            padDelayTap(622, 649, 2, "選關")
            padDelayTap(535, 589, 2, "第一關")
            # padDelayTap(535, 883, 1, "第二關")
            padDelayTap(520, 544, 2, "選好友")
            padDelayTap(418, 1498, 2, "挑戰")
            printSleep(20)
            loopCnt = 1
            # show board screen

        if screenMatch(screen, Template(r"tpl1587206968839.png", record_pos=(0.002, 0.212), resolution=(1080, 1920))):
            saveScreen(boardScreen, "screenCap/{}.jpg".format(getCurrentDateTime()))

            padDelayTap(524, 1605, 1, "OK")
            padDelayTap(54, 655, 6)
            padDelayTap(622, 649, 2, "選關")
            #             padDelayTap(535, 589, 2, "第一關")
            padDelayTap(535, 883, 1, "第二關")
            padDelayTap(520, 544, 2, "選好友")
            padDelayTap(418, 1498, 2, "挑戰")
            printSleep(20)
            loopCnt = 1
            # show board screen

        if loopCnt == 1:
            useSkill(6, 1, 3)
            useSkill(1, 1, 3)
        elif loopCnt == 2:
            useSkill(2, 1, 3)
        else:
            if loopCnt % 2 == 0:
                useSkill(2, 1, 3)
            else:
                useSkill(1, 1, 3)

        if (capscreenCheck(screen)):
            boardScreen = orgScreen
            printSleep(20)
            loopCnt += 1
        else:
            printSleep(5)



def equals_rc(a, b):
    return a['row'] == b['row'] and a['col'] == b['col']


def equals_matches(a, b):
    if len(a) != len(b):
        return False
    for i in range(len(a)):
        if a[i]['type'] != b[i]['type'] or a[i]['count'] != b[i]['count']:
            return False
    return True


def contrast_img(img1, c, b):  # 亮度就是每个像素所有通道都加上b
    rows, cols, channels = img1.shape

    # 新建全零(黑色)图片数组:np.zeros(img1.shape, dtype=uint8)
    blank = np.zeros([rows, cols, channels], img1.dtype)
    dst = cv2.addWeighted(img1, c, blank, 1 - c, b)
    return dst


def simplify_solutions(solutions):
    simplify_solution_array = []
    for s in solutions:
        is_add = True
        for i in range(len(simplify_solution_array) - 1, -1, -1):
            if len(simplify_solution_array) > 0:
                simplified_solution = simplify_solution_array[i]
                if equals_rc(simplified_solution['init_cursor'], s['init_cursor']) is False:
                    continue
                if equals_matches(simplified_solution['matches'], s['matches']) is False:
                    continue
                is_add = False
        if is_add:
            simplify_solution_array.append(s)
    return simplify_solution_array


def step_callback(p, max_p):
    print('Solving (' + str(p) + '/' + str(max_p) + ')...')


def finish_callback(solutions):
    new_solutions = simplify_solutions(solutions)
    size = len(new_solutions)
    print("size: " + str(size))
    new_solutions.reverse()
    filter_solutions = []
    for solution in new_solutions:
        # print('finish_callback: ' + str(solution))
        sorted_matches = solution['matches']
        # print('W=' + str(solution['weight']) + ' ,L=' + str(len(solution['path'])) + ', id:' + str(solution['so_id']) + ", C: " + str(len(sorted_matches)) +" ,s: " + str(sorted_matches))
        print('W=' + str(solution['weight']) + ' ,L=' + str(len(solution['path'])) + ', id:' + str(
            solution['so_id']) + ', c: ' + str(len(sorted_matches)) + ", e: " + str(solution['eliminate_count']))
        # for m in sorted_matches:
        #     if m['match_type'] == 2 and m['type'] == 5:
        #         filter_solutions.append(solution)
        #         break
    move_path(new_solutions[size - 1]['init_cursor'], new_solutions[size - 1]['path'])
    # print(filter_solutions)


def updateBoard(arr, x, y, type):
    pos_x = 0
    pos_y = 0
    if x < preWidth:
        pos_x = 0
    elif x < preWidth * 2:
        pos_x = 1
    elif x < preWidth * 3:
        pos_x = 2
    elif x < preWidth * 4:
        pos_x = 3
    elif x < preWidth * 5:
        pos_x = 4
    else:
        pos_x = 5

    if y < height / 2 + preHeight:
        pos_y = 0
    elif y < height / 2 + preHeight * 2:
        pos_y = 1
    elif y < height / 2 + preHeight * 3:
        pos_y = 2
    elif y < height / 2 + preHeight * 4:
        pos_y = 3
    else:
        pos_y = 4

    arr[pos_y][pos_x] = type
    return arr


def capscreenCheck(im):
    # boardSizeW = width / 9
    # boardSizeH = height / 17
    boardSizeW = width / 10
    boardSizeH = height / 18
    boardTable = create_empty_board()
    print('bw: ' + str(boardSizeW) + ', bh: ' + str(boardSizeH))
    for i in range(COLS):
        cv2.line(im, (int(preWidth * (i + 1)), height // 2), (int(preWidth * (i + 1.0)), height), (255, 255, 255), 3)
    # for j in ROWS:

    # imgBlur = cv2.GaussianBlur(im, ksize=(21, 21), sigmaX=0, sigmaY=0)
    # im = cv2.bilateralFilter(im, 9, 75, 75)
    imgBlur = contrast_img(im, 1.3, 3)
    im = cv2.fastNlMeansDenoisingColored(im, None, 10, 10, 7, 21)
    hsv_img = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)  # HSV image

    # RED
    imgRedGrey = cv2.inRange(hsv_img, np.array([0, 90, 150], np.uint8), np.array([22, 255, 255], np.uint8))
    ret, thresh = cv2.threshold(imgRedGrey, 127, 255, 0)
    tempImg, redContours, redHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # BLUE
    imgBlueGrey = cv2.inRange(hsv_img, np.array([90, 0, 100], np.uint8), np.array([135, 140, 255], np.uint8))
    ret, thresh = cv2.threshold(imgBlueGrey, 127, 255, 0)
    tempImg, blueContours, blueHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # GREEN
    imgGreenGrey = cv2.inRange(hsv_img, np.array([36, 0, 0], np.uint8), np.array([85, 255, 255], np.uint8))
    ret, thresh = cv2.threshold(imgGreenGrey, 127, 255, 0)
    tempImg, greenContours, greenHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # LIGHT
    imgYelloGray = cv2.inRange(hsv_img, np.array([20, 50, 100], np.uint8), np.array([30, 255, 255], np.uint8))  # yellow
    ret, thresh = cv2.threshold(imgYelloGray, 127, 255, 0)
    tempImg, yelloContours, yellowHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # print(np.size(yelloContours))

    # DARK
    imgDarkGray = cv2.inRange(hsv_img, np.array([140, 50, 150], np.uint8),
                              np.array([160, 255, 255], np.uint8))  # yellow
    ret, thresh = cv2.threshold(imgDarkGray, 127, 255, 0)
    tempImg, darkContours, darkHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # HEART
    imgHeartGray = cv2.inRange(hsv_img, np.array([160, 50, 100], np.uint8),
                               np.array([170, 200, 255], np.uint8))  # yellow
    ret, thresh = cv2.threshold(imgHeartGray, 127, 255, 0)
    tempImg, heartContours, heartHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # USELESS
    imgUselessGray = cv2.inRange(hsv_img, np.array([100, 120, 0], np.uint8), np.array([120, 200, 150], np.uint8))
    ret, thresh = cv2.threshold(imgUselessGray, 127, 255, 0)
    tempImg, uselessContours, uselessHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # POISON
    imgPoisonGray = cv2.inRange(hsv_img, np.array([130, 0, 0], np.uint8), np.array([150, 255, 80], np.uint8))
    ret, thresh = cv2.threshold(imgPoisonGray, 127, 255, 0)
    tempImg, poisonContours, oisonHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # HIGHLYTOXIC

    # BOMB
    imgBombGray = cv2.inRange(hsv_img, np.array([30, 0, 0], np.uint8), np.array([90, 100, 100], np.uint8))
    ret, thresh = cv2.threshold(imgBombGray, 127, 255, 0)
    tempImg, bombContours, bombHierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for cnt in redContours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > boardSizeW and h > boardSizeH and y > height / 2:
            print("red x: " + str(x) + ", y: " + str(y) + ", w:" + str(w) + ", h: " + str(h))
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, "red", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 255, 255), 5, cv2.LINE_AA)
            cv2.putText(im, "red", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 0), 2, cv2.LINE_AA)
            updateBoard(boardTable, x, y, 0)

    for cnt in blueContours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > boardSizeW and h > boardSizeH and y > height / 2:
            print("blue x: " + str(x) + ", y: " + str(y) + ", w:" + str(w) + ", h: " + str(h))
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, "blue", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 255, 255), 5, cv2.LINE_AA)
            cv2.putText(im, "blue", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 0), 2, cv2.LINE_AA)
            updateBoard(boardTable, x, y, 1)

    for cnt in greenContours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > boardSizeW and h > boardSizeH and y > height / 2:
            print("green x: " + str(x) + ", y: " + str(y) + ", w:" + str(w) + ", h: " + str(h))
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, "green", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 255, 255), 5, cv2.LINE_AA)
            cv2.putText(im, "green", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 0), 2, cv2.LINE_AA)
            updateBoard(boardTable, x, y, 2)

    for cnt in yelloContours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > boardSizeW and h > boardSizeH and y > height / 2:
            print("light x: " + str(x) + ", y: " + str(y) + ", w:" + str(w) + ", h: " + str(h))
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, "yellow", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 255, 255), 5, cv2.LINE_AA)
            cv2.putText(im, "yellow", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 0), 2, cv2.LINE_AA)
            updateBoard(boardTable, x, y, 3)

    for cnt in darkContours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > boardSizeW and h > boardSizeH and y > height / 2:
            print("dark x: " + str(x) + ", y: " + str(y) + ", w:" + str(w) + ", h: " + str(h))
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, "dark", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 255, 255), 5, cv2.LINE_AA)
            cv2.putText(im, "dark", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 0), 2, cv2.LINE_AA)
            updateBoard(boardTable, x, y, 4)

    for cnt in heartContours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > boardSizeW and h > boardSizeH and y > height / 2:
            print("heart x: " + str(x) + ", y: " + str(y) + ", w:" + str(w) + ", h: " + str(h))
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, "heart", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 255, 255), 5, cv2.LINE_AA)
            cv2.putText(im, "heart", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 0), 2, cv2.LINE_AA)
            updateBoard(boardTable, x, y, 5)

    for cnt in uselessContours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > boardSizeW and h > boardSizeH - 10 and y > height / 2:
            print("useless x: " + str(x) + ", y: " + str(y) + ", w:" + str(w) + ", h: " + str(h))
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, "useless", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 255, 255), 5, cv2.LINE_AA)
            cv2.putText(im, "useless", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 0), 2, cv2.LINE_AA)
            updateBoard(boardTable, x, y, 6)

    for cnt in poisonContours:
        x, y, w, h = cv2.boundingRect(cnt)
        if w > boardSizeW and h > boardSizeH and y > height / 2:
            print("poison x: " + str(x) + ", y: " + str(y) + ", w:" + str(w) + ", h: " + str(h))
            cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(im, "poison", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (255, 255, 255), 5, cv2.LINE_AA)
            cv2.putText(im, "poison", (x, y), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 0), 2, cv2.LINE_AA)
            updateBoard(boardTable, x, y, 7)
    print(boardTable)
    # boardTable = [[4, 2, 6, 0, 3, 3],[0, 4, 1, 4, 1, 0],[1, 3, 6, 4, 6, 1],[6, 3, 5, 6, 0, 1],[6, 4, 5, 5, 2, 4]]
    # solve_board(boardTable, step_callback, finish_callback)
    return solve_board(boardTable, step_callback, finish_callback)


if __name__ == "__main__":
    try:
        main()
    except:
        main()
