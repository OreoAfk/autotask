# -*- encoding=utf8 -*-
__author__ = "hugho"

import sys
from airtest.core.api import *
from airtest.cli.parser import cli_setup
from airtest.core.android.minitouch import *
from airtest.core.android.rotation import XYTransformer

import cv2
import numpy as np
import urllib.request

sys.path.append("../pad_screen_ml")
from screenDetect import ScreenDetect, BoxItem
from pad_autoTask import PadBoard
# from solver7 import *
from solver8 import PadSolver
sys.path.append("../..")
from tool.tools import *

class SimulatorPadSolver:
    def __init__(self, width, height):
        self.deviceWidth = width
        self.deviceHeight = height
        self.connectDevice()
        self.dev = device()
        self.screenDetect = ScreenDetect("../pad_screen_ml", False)
        self.padBoard = PadBoard(width, int(height * 0.4791))
        self.padSolver = PadSolver()
        self.previousScreen = None
        self.roundCount = 1
        self.screen = None
        self.loopCount = 0
        self.nonSolverCount = 0

    def connectDevice(self):
        if not cli_setup():
            auto_setup(__file__, logdir=True, devices=[
                "Android://127.0.0.1:5037/127.0.0.1:62033?cap_method=JAVACAP&&ori_method=ADBORI",
            ])

    def solver(self):
        self.loopCount = self.loopCount + 1
        self.screen = G.DEVICE.snapshot(filename=None)
        print("screen type: {}".format(type(self.screen)))


        if self.checkScreenForAction(self.screen):
            return False
        if self.roundCount > 1 and (self.roundCount - 1) % 6 == 0:
            self.useSkill(3, 1, 4, "光drop珠")
            self.screen = G.DEVICE.snapshot(filename=None)
        elif self.roundCount > 1 and (self.roundCount - 1) % 3 == 0:
            self.useSkill(2, 1, 4, "光drop珠")
            self.screen = G.DEVICE.snapshot(filename=None)
        print("搜尋畫面項目 回合:{}/{}/{}".format(self.roundCount, self.nonSolverCount, self.loopCount))
        # width = int(1080 / 3)
        # height = int(1920 / 3)
        # self.boxItems = self.screenDetect.getBoxItems(cv2.resize(self.screen, dsize=(width, height), interpolation=cv2.INTER_CUBIC))
        self.boxItems = self.screenDetect.getBoxItems(self.screen)
        if self.boxItems is not None:
            self.labels = self.screenDetect.getLabels()
            # if self.labels.count("lock") > 30:
            #     self.useSkill(5, 3, 4, "解鎖")
            if self.labels is not None:
                if "selfstate_nonawakening" in self.labels:
                    self.useSkill(1, 3, 4, "解覺醒")
                    # return False

                # if ""

            self.padBoard.updateBoard(self.boxItems, self.labels)

            for b in self.boxItems:
                print("label: {}, box: {}".format(b.getLabel(), b.getCenterPoint()))

            row, col = self.padBoard.getBoardSize()
            if self.padBoard.getNumBoard() is None:
                saveScreen(self.screen, "screenCap/b{}.jpg".format(getCurrentDateTime()))
                return False
            for a in self.padBoard.getNumBoard():
                if None in a:
                    saveScreen(self.screen, "screenCap/n{}.jpg".format(getCurrentDateTime()))
                    # self.screenDetect.showImg()
                    return False

            print("{}x{}\n{}".format(col, row, self.padBoard.getNumBoard()))
            if self.padBoard.getOrbCount() is not 0 and col > 0 and row > 0:
                self.orbWidth = self.deviceWidth / col
                self.boardTop = self.deviceHeight - self.orbWidth * row
                self.orbHeight = self.deviceWidth / col
                self.padSolver.updateSolver(row, col)
                print("start solver")
                if self.padBoard.getNonOrbCount() == 1 and self.padBoard.getLatstNonOrbPos() is not None:
                    self.padSolver.solve_board(self.padBoard.getNumBoard(), self.step_callback, self.finish_callback, self.padBoard.getLatstNonOrbPos())
                else:
                    self.padSolver.solve_board(self.padBoard.getNumBoard(), self.step_callback, self.finish_callback)
                # self.move_path({'row': 1, 'col': 6},
                #                [2, 2, 4, 4, 2, 4, 6, 4, 4, 4, 6, 0, 0, 6, 4, 6, 0, 2, 2, 4, 2, 2, 2, 0, 6, 0, 0, 6, 6, 6, 6, 0,
                #                 2, 4, 2, 0, 6, 0,
                #                 2, 4])
                printSleep(20)
                self.roundCount = self.roundCount + 1
            else:
                self.nonSolverCount = self.nonSolverCount + 1
                if self.nonSolverCount > 3:
                    saveScreen(self.previousScreen, "screenCap/d{}.jpg".format(getCurrentDateTime()))
                    self.nonSolverCount = 0

        else:
            print("not board")
        self.previousScreen = self.screen

    def useSkill(self, num, beforeDelay, afterDelay, msg=None):
        if msg is not None:
            print("使用技能: {}".format(msg))
        skillPos = ((self.deviceWidth / 6) * num) - (self.deviceWidth / 12)
        self.padDelayTap(skillPos, 851, beforeDelay)  # skill
        self.padDelayTap(380, 1529, 2)
        printSleep(afterDelay)

    def checkScreenForAction(self, screen):
        if screenMatch(screen, Template(r"tpl1586718516069.png", record_pos=(-0.044, -0.471), resolution=(1080, 1920)), "傷吸") and self.roundCount > 5:
            self.useSkill(4, 3, 4, "破吸")
            return False

        if screenMatch(screen, Template(r"tpl1586715087684.png", record_pos=(0.106, -0.595), resolution=(1080, 1920)), "傷吸") and self.roundCount > 5:
            self.useSkill(4, 3, 4, "破吸")
            return False

        if screenMatch(screen, Template(r"tpl1586719496785.png", record_pos=(0.021, -0.66), resolution=(1080, 1920)),"貫通盾"):
            self.padDelayTap(982, 101, 2, "MENU")
            self.padDelayTap(507, 1439, 2, "放棄")
            self.padDelayTap(366, 1154, 2, "是")
            self.padDelayTap(535, 589, 6, "第一關")
            # self.padDelayTap(535, 883, 1, "第二關")
            self.padDelayTap(520, 544, 2, "選好友")
            self.padDelayTap(418, 1498, 4, "挑戰")
            printSleep(20)
            return True

        if screenMatch(screen, Template(r"tpl1586507422342.png", record_pos=(-0.006, -0.427), resolution=(1080, 1920)), "CLEAR"):
            self.padDelayTap(556, 1582, 2, "OK")
            self.padDelayTap(556, 1667, 1, "OK")
            self.padDelayTap(54, 655, 5)
            printSleep(3)
            if screenMatch(screen, Template(r"tpl1587627546732.png", record_pos=(-0.006, 0.1), resolution=(1080, 1920)), "升等"):
                self.padDelayTap(54, 655, 2)
                self.padDelayTap(54, 655, 2)
                self.padDelayTap(54, 655, 2)
                self.padDelayTap(54, 655, 2)
                self.padDelayTap(54, 655, 2)
                self.padDelayTap(76, 1805, 1, "HOME")
                self.padDelayTap(76, 1805, 2, "HOME")
                self.padDelayTap(228, 810, 2, "左上")
                self.padDelayTap(597, 1620, 2, "星寶")
                self.padDelayTap(535, 589, 2, "第一關")
                # self.padDelayTap(535, 883, 1, "第二關")
                self.padDelayTap(520, 544, 2, "選好友")
                self.padDelayTap(418, 1498, 4, "挑戰")
                printSleep(20)
                self.roundCount = 1
                self.loopCount = 0
                self.nonSolverCount = 0
                self.useSkill(3, 1, 4, "光drop珠")
                self.useSkill(5, 1, 4, "+3SB")
                self.useSkill(1, 1, 4, "光魔女變身")
                return True
            self.padDelayTap(54, 655, 2)
            self.padDelayTap(54, 655, 2)
            self.padDelayTap(54, 655, 2)
            self.padDelayTap(54, 655, 2)
            self.padDelayTap(54, 655, 2)
            self.padDelayTap(622, 649, 2, "選關")
            self.padDelayTap(535, 589, 2, "第一關")
            # self.padDelayTap(535, 883, 1, "第二關")
            printSleep(3)
            screen = G.DEVICE.snapshot(filename=None)
            if screenMatch(screen, Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)), "沒體"):
                self.padDelayTap(76, 1805, 1, "HOME")
                self.padDelayTap(76, 1805, 2, "HOME")
                self.padDelayTap(286, 1486, 2, "左下")
                self.padDelayTap(597, 620, 2, "0體百花")
                self.padDelayTap(535, 589, 2, "第一關")
            self.padDelayTap(520, 544, 2, "選好友")
            self.padDelayTap(418, 1498, 4, "挑戰")
            printSleep(20)
            self.roundCount = 1
            self.loopCount = 0
            self.nonSolverCount = 0
            self.useSkill(3, 1, 4, "光drop珠")
            self.useSkill(5, 1, 4, "+3SB")
            self.useSkill(1, 1, 4, "光魔女變身")

            return True

        if screenMatch(screen, Template(r"tpl1586510770121.png", record_pos=(-0.019, 0.38), resolution=(1080, 1920)), "掛掉"):
            saveScreen(self.previousScreen, "screenCap/{}.jpg".format(getCurrentDateTime()))

            self.padDelayTap(672, 1571, 5, "否")
            self.padDelayTap(348, 1599, 2, "是")
            self.padDelayTap(524, 1605, 1, "OK")
            self.padDelayTap(54, 655, 6)
            self.padDelayTap(622, 649, 2, "選關")
            self.padDelayTap(535, 589, 2, "第一關")
            # self.padDelayTap(535, 883, 1, "第二關")
            printSleep(3)
            screen = G.DEVICE.snapshot(filename=None)
            if screenMatch(screen, Template(r"tpl1587639463480.png", record_pos=(-0.154, 0.143), resolution=(1080, 1920)), "沒體"):
                self.padDelayTap(76, 1805, 1, "HOME")
                self.padDelayTap(76, 1805, 2, "HOME")
                self.padDelayTap(286, 1486, 2, "左下")
                # self.padDelayTap(597, 1620, 2, "0體百花")
                self.padDelayTap(597, 620, 2, "0體百花")
                self.padDelayTap(622, 649, 2, "第一關")
            self.padDelayTap(520, 544, 2, "選好友")
            self.padDelayTap(418, 1498, 4, "挑戰")
            printSleep(20)
            self.roundCount = 1
            self.loopCount = 0
            self.nonSolverCount = 0
            self.useSkill(3, 1, 4, "光drop珠")
            self.useSkill(5, 1, 4, "+3SB")
            self.useSkill(1, 1, 4, "光魔女變身")
            return True
        return False

    def getScreen(self):
        return self.screen

    def padDelayTap(self, x, y, delay, msg=""):
        printSleep(delay)
        swipe_event = [DownEvent((x, y)), SleepEvent(0.2)]
        swipe_event.append(UpEvent())
        self.dev.minitouch.perform(swipe_event)
        print("tap: {} {} {}".format(x, y, msg))

    def getDevice(self):
        return self.dev





    def equals_rc(self, a, b):
        return a['row'] == b['row'] and a['col'] == b['col']

    def equals_matches(self, a, b):
        if len(a) != len(b):
            return False
        for i in range(len(a)):
            if a[i]['type'] != b[i]['type'] or a[i]['count'] != b[i]['count']:
                return False
        return True

    def simplify_solutions(self, solutions):
        simplify_solution_array = []
        for s in solutions:
            is_add = True
            for i in range(len(simplify_solution_array) - 1, -1, -1):
                if len(simplify_solution_array) > 0:
                    simplified_solution = simplify_solution_array[i]
                    if self.equals_rc(simplified_solution['init_cursor'], s['init_cursor']) is False:
                        continue
                    if self.equals_matches(simplified_solution['matches'], s['matches']) is False:
                        continue
                    is_add = False
            if is_add:
                simplify_solution_array.append(s)
        return simplify_solution_array

    def step_callback(self, p, max_p):
        print('Solving ({} / {})...'.format(p, max_p))


    def finish_callback(self, solutions):
        new_solutions = self.simplify_solutions(solutions)
        size = len(new_solutions)
        print("size: " + str(size))
        new_solutions.reverse()
        for solution in new_solutions:
            sorted_matches = solution['matches']
            print('W={} ,L={}, id:{}, c: {}, e: {}'
                  .format(solution['weight'], len(solution['path']), solution['so_id'], len(sorted_matches), solution['eliminate_count']))
        print("point: {}, flow: {}".format(new_solutions[size - 1]['init_cursor'], new_solutions[size - 1]['path']))
        self.move_path(new_solutions[size - 1]['init_cursor'], new_solutions[size - 1]['path'])


    def to_xy(self, rc):
        x = rc['col'] * self.orbWidth + self.orbWidth / 2
        y = self.boardTop + (rc['row'] * self.orbHeight) + self.orbHeight / 2
        return {'x': x, 'y': y}

    def in_place_move_rc(self, rc, dir):
        if dir == 0:
            rc['col'] = rc['col'] + 1
        elif dir == 1:
            rc['row'] = rc['row'] + 1
            rc['col'] = rc['col'] + 1
        elif dir == 2:
            rc['row'] = rc['row'] + 1
        elif dir == 3:
            rc['row'] = rc['row'] + 1
            rc['col'] = rc['col'] - 1
        elif dir == 4:
            rc['col'] = rc['col'] - 1
        elif dir == 5:
            rc['row'] = rc['row'] - 1
            rc['col'] = rc['col'] - 1
        elif dir == 6:
            rc['row'] = rc['row'] - 1
        elif dir == 7:
            rc['row'] = rc['row'] - 1
            rc['col'] = rc['col'] + 1

    def copy_rc(self, rc):
        return {'row': rc['row'], 'col': rc['col']}

    def move_path(self, init_rc, path):
        rc = self.copy_rc(init_rc)
        # xys = [to_xy(rc)]
        xys = []
        firstTap = self.to_xy(rc)
        swipe_event = [DownEvent((firstTap['x'], firstTap['y'])), SleepEvent(0.1)]
        # xys.append(to_xy(rc))
        for p in path:
            self.in_place_move_rc(rc, p)
            xys.append(self.to_xy(rc))
        # print(xys)
        for xy in xys:
            swipe_event.append(MoveEvent((xy['x'], xy['y'])))
            swipe_event.append(SleepEvent(0.1))

        swipe_event.append(UpEvent())
        self.dev.minitouch.perform(swipe_event)
        sleep(1)

if __name__ == '__main__':

    # pasSolver = SimulatorPadSolver(int(1080/3), int(1920/3))
    pasSolver = SimulatorPadSolver(1080, 1920)
    screen = G.DEVICE.snapshot(filename=None)
    if screenMatch(screen, Template(r"tpl1586510770121.png", record_pos=(-0.019, 0.38), resolution=(1080, 1920))):
        pass
    # pasSolver.useSkill(3, 1, 4, "光drop珠")
    # pasSolver.useSkill(5, 1, 4, "+3SB")
    # pasSolver.useSkill(1, 1, 4, "光魔女變身")
    while True:
        pasSolver.solver()

    # cv2.imshow('org', im)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()







