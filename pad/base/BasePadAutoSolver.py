# -*- encoding=utf8 -*-
__author__ = "hugho"

import sys
from airtest.core.api import *
from airtest.cli.parser import cli_setup
from airtest.core.android.minitouch import *
from airtest.core.android.rotation import XYTransformer

import cv2
import numpy as np
import urllib.request
import pathlib
import csv

sys.path.append("../pad_screen_ml")
from screenDetect import ScreenDetect, ScreenDetectForColor, ScreenDetectForDetectron2, BoxItem
from pad_autoTask import PadBoard
# from solver7 import *
from solver8 import PadSolver
sys.path.append("../..")
from tool.tools import *
from base.dailyQuest import DailyQuest


class BasePadAutoSolver(DailyQuest):
    def __init__(self, width, height, maxPathLength=20):
        self.deviceWidth = width
        self.deviceHeight = height
        self.connectDevice()
        self.dev = device()
        self.screenDetectForColor = ScreenDetectForColor("", False)
        self.screenDetectForDetectron2 = ScreenDetectForDetectron2("../pad_screen_ml", False)
        self.screenDetect = self.screenDetectForDetectron2
        self.padBoard = PadBoard(width, int(height * 0.4791))
        self.padSolver = PadSolver(maxPathLength)
        self.previousScreen = None
        self.roundCount = 1

        self.screen = None
        self.loopCount = 0
        self.nonSolverCount = 0
        self.saveLogHeader = ['id', 'start_point', 'start_board', 'end_board', "path", "matches", "labels", 'time']
        self.preScreenCrop = None

        pathlib.Path('./log/board_start').mkdir(parents=True, exist_ok=True)
        pathlib.Path('./log/board_end').mkdir(parents=True, exist_ok=True)
        logfile = pathlib.Path("./log/board.csv")
        if not logfile.is_file():
            logfile.touch()
            with open("./log/board.csv", "w") as csvFile:
                # writer = csv.writer(csvFile)
                writer = csv.DictWriter(csvFile, fieldnames=self.saveLogHeader)
                # writer.writerow(fieldNames)
                writer.writeheader()


    def connectDevice(self):
        if not cli_setup():
            auto_setup(__file__, logdir=True, devices=[
                "Android://127.0.0.1:5037/127.0.0.1:62025?cap_method=JAVACAP&&ori_method=ADBORI",
            ])
            # auto_setup(__file__, logdir=True, devices=[
            #      "Android://127.0.0.1:5037/c572159a?cap_method=JAVACAP&&ori_method=ADBORI&&touch_method=ADBTOUCH",
            # ])


    def changeScreenMode(self, isOpenCv):
        if isOpenCv:
            self.screenDetect = self.screenDetectForColor
        else:
            self.screenDetect = self.screenDetectForDetectron2

    def solver(self):
        self.loopCount = self.loopCount + 1
        self.screen = G.DEVICE.snapshot(filename=None)
        # print("screen type: {}".format(type(self.screen)))

        if not self.checkScreenForContinueCheck(self.screen):
            return False
        self.roundLoopAction(self.roundCount, self.loopCount)
        logMsg("搜尋畫面項目 回合:{}/{}/{}".format(self.roundCount, self.nonSolverCount, self.loopCount))
        if self.loopCount > 15:
            notify("Pad 卡了")
            input("卡了")
            self.loopCount = 0
            print("continute")

        # width = int(1080 / 3)
        # height = int(1920 / 3)
        # self.boxItems = self.screenDetect.getBoxItems(cv2.resize(self.screen, dsize=(width, height), interpolation=cv2.INTER_CUBIC))
        if self.needCheckScreen(self.roundCount):
            self.boxItems = self.screenDetect.getBoxItems(self.screen)
            if self.boxItems is not None:
                self.labels = self.screenDetect.getLabels()
                if self.screenLabelAction(self.labels):
                    return False

                self.padBoard.updateBoard(self.boxItems, self.labels, 20)

                for b in self.boxItems:
                    logMsg("label: {}, box: {}".format(b.getLabel(), b.getCenterPoint()))

                row, col = self.padBoard.getBoardSize()
                if self.padBoard.getNumBoard() is None:
                    saveScreen(self.screen, "screenCap/b{}.jpg".format(getCurrentDateTime()))
                    return False
                for a in self.padBoard.getNumBoard():
                    if None in a:
                        saveScreen(self.screen, "screenCap/n{}.jpg".format(getCurrentDateTime()))
                        # self.screenDetect.showImg()
                        return False

                logMsg("{}x{}\n{}".format(col, row, self.padBoard.getNumBoard()))
                if self.padBoard.getOrbCount() is not 0 and col > 0 and row > 0:
                    self.orbWidth = self.deviceWidth / col
                    self.boardTop = self.deviceHeight - self.orbWidth * row
                    self.orbHeight = self.deviceWidth / col
                    self.padSolver.updateSolver(row, col)
                    logMsg("start solver")
                    if self.padBoard.getNonOrbCount() == 1 and self.padBoard.getLatstNonOrbPos() is not None:
                        # print("statr point: {}".format(self.padBoard.getLatstNonOrbPos()))
                        self.padSolver.solve_board(self.padBoard.getNumBoard(), self.step_callback, self.finish_callback, self.padBoard.getLatstNonOrbPos())
                    else:
                        self.padSolver.solve_board(self.padBoard.getNumBoard(), self.step_callback, self.finish_callback)
                    # self.move_path({'row': 1, 'col': 6},
                    #                [2, 2, 4, 4, 2, 4, 6, 4, 4, 4, 6, 0, 0, 6, 4, 6, 0, 2, 2, 4, 2, 2, 2, 0, 6, 0, 0, 6, 6, 6, 6, 0,
                    #                 2, 4, 2, 0, 6, 0,
                    #                 2, 4])
                    self.printSleep(self.boardSolvedDaily())
                else:
                    self.nonSolverCount = self.nonSolverCount + 1
                    if self.nonSolverCount > 3:
                        saveScreen(self.previousScreen, "screenCap/d{}.jpg".format(getCurrentDateTime()))
                        self.nonSolverCount = 0

            else:
                logMsg("not board")
        self.previousScreen = self.screen

    def useSkill(self, num, beforeDelay, afterDelay, msg=None):
        if msg is not None:
            logMsg("使用技能: {}".format(msg))
        skillPos = ((self.deviceWidth / 6) * num) - (self.deviceWidth / 12)
        self.delayTap(skillPos, 851, beforeDelay, "第{}只角色".format(num))  # skill
        self.delayTap(380, 1529, 1, "確定")
        # self.delayTap(380, 1580, 0.5, "確定")
        self.printSleep(afterDelay)

    def delayTap(self, x, y, second=0, actionName="", isTap=True, longTapTime=1):
        self.printSleep(second)
        swipe_event = [DownEvent((x, y)), SleepEvent(0.2)]
        swipe_event.append(UpEvent())
        device().minitouch.perform(swipe_event)

        logMsg("tap: {} {} {}".format(x, y, actionName))

    def swipeTo(self, x1, y1, x2, y2, time=3):
        print("from ({},{}) to ({}, {}), time: {}".format(x1, x2, y1, y2, time))
        swipe_event = [DownEvent((x1, y1)), SleepEvent(0.3)]
        pTime = time * 10
        pX = (x2 - x1) / pTime
        pY = (y2 - y1) / pTime
        for i in range(pTime):
            swipe_event.append(MoveEvent((x1 + pX * i, y1 + pY * i)))
            swipe_event.append(SleepEvent(0.1))

        swipe_event.append(UpEvent())
        device().minitouch.perform(swipe_event)

    def getDevice(self):
        return self.dev

    def equals_rc(self, a, b):
        return a['row'] == b['row'] and a['col'] == b['col']

    def equals_matches(self, a, b):
        if len(a) != len(b):
            return False
        for i in range(len(a)):
            if a[i]['type'] != b[i]['type'] or a[i]['count'] != b[i]['count']:
                return False
        return True

    def simplify_solutions(self, solutions):
        simplify_solution_array = []
        for s in solutions:
            is_add = True
            for i in range(len(simplify_solution_array) - 1, -1, -1):
                if len(simplify_solution_array) > 0:
                    simplified_solution = simplify_solution_array[i]
                    if self.equals_rc(simplified_solution['init_cursor'], s['init_cursor']) is False:
                        continue
                    if self.equals_matches(simplified_solution['matches'], s['matches']) is False:
                        continue
                    is_add = False
            if is_add:
                simplify_solution_array.append(s)
        return simplify_solution_array

    def step_callback(self, p, max_p):
        logMsg('Solving ({} / {})...'.format(p, max_p))

    def finish_callback(self, solutions):
        new_solutions = self.simplify_solutions(solutions)
        size = len(new_solutions)
        logMsg("size: " + str(size))
        new_solutions.reverse()
        for solution in new_solutions:
            sorted_matches = solution['matches']
            logMsg('W={} ,L={}, id:{}, c: {}, e: {}'
                  .format(solution['weight'], len(solution['path']), solution['so_id'], len(sorted_matches), solution['eliminate_count']))
        logMsg("point: {}, flow: {}".format(new_solutions[size - 1]['init_cursor'], new_solutions[size - 1]['path']))
        # fieldNames = ['id', 'start_point', 'start_board', 'end_board', "path", "matches", 'time']

        saveDate = {
            "id": "{}".format(getCurrentDateTime()),
            "labels": "{}".format(self.labels),
            "start_point": "{}".format(new_solutions[size-1]["init_cursor"]),
            "start_board": "{}".format(self.padBoard.getNumBoard()),
            "end_board": "{}".format(new_solutions[size-1]['board']),
            "path":  "{}".format(new_solutions[size - 1]['path']),
            "matches": "{}".format(new_solutions[size - 1]["matches"])
        }
        self.solvedBoard(new_solutions)
        self.move_path(new_solutions[size - 1]['init_cursor'], new_solutions[size - 1]['path'])
        startBoardScreen = self.screen
        endBoardScreen = G.DEVICE.snapshot(filename=None)
        self.saveBoardData(saveDate, startBoardScreen, endBoardScreen)

    def to_xy(self, rc):
        x = rc['col'] * self.orbWidth + self.orbWidth / 2
        y = self.boardTop + (rc['row'] * self.orbHeight) + self.orbHeight / 2
        return {'x': x, 'y': y}

    def needCheckScreen(self, round):
        return True

    def in_place_move_rc(self, rc, dir):
        if dir == 0:
            rc['col'] = rc['col'] + 1
        elif dir == 1:
            rc['row'] = rc['row'] + 1
            rc['col'] = rc['col'] + 1
        elif dir == 2:
            rc['row'] = rc['row'] + 1
        elif dir == 3:
            rc['row'] = rc['row'] + 1
            rc['col'] = rc['col'] - 1
        elif dir == 4:
            rc['col'] = rc['col'] - 1
        elif dir == 5:
            rc['row'] = rc['row'] - 1
            rc['col'] = rc['col'] - 1
        elif dir == 6:
            rc['row'] = rc['row'] - 1
        elif dir == 7:
            rc['row'] = rc['row'] - 1
            rc['col'] = rc['col'] + 1

    def copy_rc(self, rc):
        return {'row': rc['row'], 'col': rc['col']}

    def move_path(self, init_rc, path):
        rc = self.copy_rc(init_rc)
        # xys = [to_xy(rc)]
        xys = []
        firstTap = self.to_xy(rc)
        swipe_event = [DownEvent((firstTap['x'], firstTap['y'])), SleepEvent(0.1)]
        # xys.append(to_xy(rc))
        for p in path:
            self.in_place_move_rc(rc, p)
            xys.append(self.to_xy(rc))
        # print(xys)
        for xy in xys:
            swipe_event.append(MoveEvent((xy['x'], xy['y'])))
            swipe_event.append(SleepEvent(0.1))

        swipe_event.append(UpEvent())
        self.dev.minitouch.perform(swipe_event)
        sleep(1)

    def checkScreenForContinueCheck(self, screen):
        # if self.loopCount - self.roundCount > 5 and (self.loopCount - self.roundCount > 5) % 5 == 0:
        #     notify("pad 卡了")
        #     return False

        if self.preScreenCrop is None:
            # self.preScreenCrop = screen[100:70, 334:120]
            self.preScreenCrop = screen[70:120, 100:334] # y1:y2, x1:x2

        currentPriceCrop = screen[70:120, 100:334]
        difference = cv2.subtract(self.preScreenCrop, currentPriceCrop)
        result = not np.any(difference)
        if result is True:
            logMsg("Pictures are the same")
        else:
            logMsg("Pictures are different")
            self.roundCount = self.roundCount + 1
            self.preScreenCrop = currentPriceCrop

        if self.screenMatch(screen, Template(r"tpl1586507422342.png", record_pos=(-0.006, -0.427), resolution=(1080, 1920)),
                       "CLEAR"):
            self.gameClear()
            return False

        if self.screenMatch(screen, Template(r"tpl1587814019817.png", record_pos=(-0.251, -0.701), resolution=(1080, 1920)),
                       "掛掉"):
            self.gameOver()
            return False

        return True

    def reAsyncScreenLabel(self):
        self.screen = G.DEVICE.snapshot(filename=None)
        logMsg("搜尋畫面項目 回合:{}/{}/{}".format(self.roundCount, self.nonSolverCount, self.loopCount))
        self.boxItems = self.screenDetect.getBoxItems(self.screen)
        if self.boxItems is not None:
            self.labels = self.screenDetect.getLabels()

    def screenLabelAction(self, labels):
        return False

    def roundLoopAction(self, roundCount, loopCount):
        pass

    def startEnterGame(self):
        self.roundCount = 1
        self.loopCount = 0
        self.nonSolverCount = 0
        self.preScreenCrop = None

    def gameClear(self):
        pass

    def gameOver(self):
        pass

    def solvedBoard(self, solustions):
        pass

    def boardSolvedDaily(self):
        return 20

    def saveBoardData(self, saveData, startBoardScreen, endBoardScreen):
        pass
        # with open("./log/board.csv", 'a', newline ='') as csvFile:
        #     writer = csv.DictWriter(csvFile, fieldnames=self.saveLogHeader)
        #     writer.writerow(saveData)
        # saveScreen(startBoardScreen, "./log/board_start/{}.jpg".format(saveData['id']))
        # saveScreen(endBoardScreen, "./log/board_end/{}.jpg".format(saveData['id']))


if __name__ == '__main__':

    pasSolver = BasePadAutoSolver(1080, 1920)
    while True:
        pasSolver.solver()









