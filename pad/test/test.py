import subprocess

if __name__ == '__main__':
    logcat = subprocess.Popen(["adb", "logcat"], stdout=subprocess.PIPE)
    while not logcat.poll():
        line = logcat.stdout.readline()
        if line:
            print(line)
        else:
            break
    print(f"Return coue:{logcat.returncode}")
    logcat.kill()