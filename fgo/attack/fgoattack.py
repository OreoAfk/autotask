# -*- encoding=utf8 -*-
__author__ = "hugho"
import sys
from airtest.core.api import *
from airtest.cli.parser import cli_setup
sys.path.append("../..")
from tool.tools import *
from datetime import date
from base.dailyQuest import DailyQuest

class Fgo(DailyQuest):
    def __init__(self):
        self.eatStone = False
        self.mode = 0
        self.weekDay = date.today().weekday()

    def connectDevice(self):
        if not cli_setup():
            auto_setup(__file__, logdir=True, devices=[
                "Android://127.0.0.1:5037/127.0.0.1:62001?cap_method=JAVACAP&&ori_method=ADBORI",
            ])

    def runTask(self):
        screen = G.DEVICE.snapshot(filename=None)
        if self.tapScreenIsMatch(self.screenMatch(screen, Template(r"tpl1566666466976.png", record_pos=(0.385, 0.186),
                                                         resolution=(1280, 720))), 0, 1142, 609):
            self.delayTap(387, 201, 2, "寶具1")
            self.delayTap(647, 201, 0.2, "寶具2")
            self.delayTap(858, 201, 0.2, "寶具3")
            self.delayTap(140, 533, 0.2)
            self.delayTap(427, 533, 0.2)
            self.delayTap(650, 533, 0.2)
            self.delayTap(800, 533, 0.2)
            self.delayTap(1000, 533, 0.2)
            self.printSleep(5)
            return True

        if self.tapScreenIsMatch(self.screenMatch(screen, Template(r"tpl1577372046447.png", record_pos=(0.095, -0.123),
                                                         resolution=(1280, 720)))):
            self.delayTap(595, 402, 0, "獲得EXP")
            self.delayTap(1145, 661, 1)
            return True

        if self.tapScreenIsMatch(self.screenMatch(screen, Template(r"tpl1566666988069.png", record_pos=(-0.299, -0.135),
                                                         resolution=(1280, 720))), 0, 595, 402):
            self.delayTap(595, 402, 0, "絆")
            self.delayTap(595, 402, 1, "次回")
            self.printSleep(3)
            if self.exists_ex(Template(r"tpl1589423448368.png", record_pos=(-0.282, -0.175), resolution=(1280, 720))):
                self.delayTap(602, 120, 1)
                self.delayTap(898, 224, 5, "關卡")
                self.delayTap(511, 271, 3, "好友")
                self.delayTap(1182, 665, 2, "開始")
                return True

            self.delayTap(1145, 661, 1)
            self.delayTap(1145, 661, 1, "開始")
            return True
        
        if self.exists_ex(Template(r"tpl1589423448368.png", record_pos=(-0.282, -0.175), resolution=(1280, 720))):
            self.delayTap(602, 120, 1)
            self.delayTap(898, 224, 5, "關卡")
            self.printSleep(3)
            if self.exists_ex(Template(r"tpl1588646187725.png", record_pos=(0.226, 0.091), resolution=(1280, 720)), description="倉庫滿了"):
                notifyImg("倉庫滿了")
                return False

            self.delayTap(511, 271, 3, "好友")
            self.delayTap(1182, 665, 2, "開始")
            return True


        if self.screenMatch(screen, Template(r"tpl1566668560305.png", record_pos=(0.159, 0.158), resolution=(1280, 720))):
            self.delayTap(800, 548, 1, "連續出擊")
            self.printSleep(10)
            screen = G.DEVICE.snapshot(filename=None)
            if self.screenMatch(screen, Template(r"tpl1588646187725.png", record_pos=(0.226, 0.091), resolution=(1280, 720)), description="倉庫滿了"):
                notifyImg("倉庫滿了")
                return False
            
            if self.screenMatch(screen, Template(r"tpl1588646616384.png", record_pos=(0.009, -0.241), resolution=(1280, 720)), "AP不足"):
                if self.eatStone:
                    self.delayTap(665, 520, 1, "吃蘋果")
                    self.delayTap(837, 545, 1, "使用")
                    self.delayTap(620, 320, 8, "好友")
                    return True
                else:
                    self.delayTap(645, 600, 3, "關閉")
                    notifyImg("打完每日")
                    return False

            self.delayTap(626, 286, 5, "好友")
            return True

        return True

    def startLoopTask(self):
        self.connectDevice()
        while self.runTask():
            self.printSleep(7)
            print("Loop")
            
    def isHomePage(self):
        if self.tapScreenIsMatch(self.exists_ex(Template(r"tpl1579753127484.png", record_pos=(0.468, -0.252), resolution=(1280, 720)))):
            self.printSleep(3)
        if self.tapScreenIsMatch(self.exists_ex(Template(r"tpl1579752966159.png", record_pos=(-0.001, 0.158), resolution=(1280, 720)))):
            self.printSleep(3)
        
        isHome = False
        if self.exists_ex(Template(r"tpl1579753059481.png", record_pos=(0.424, 0.252), resolution=(1280, 720))):
            isHome = True
        return isHome
        
    def dailyTask(self):
        while not self.isHomePage():
            self.delayTap(660, 400, 1)


if __name__ == '__main__':
    fgo = Fgo()
    try:
        inputMode = input("不回體刷=0, 回體刷=1: ")
        if inputMode == "":
            fgo.eatStone = False
        else:
            fgo.eatStone = True
    except ValueError:
        sys.exit("輸入錯誤")

    fgo.startLoopTask()



