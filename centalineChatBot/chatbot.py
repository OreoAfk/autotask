import sys
sys.path.append("../..")

import numpy as np
from tool.tools import *
from base.dailyQuest import DailyQuest
from tool.screenFindItemForHSV import BoxItem, ScreenFindItemForHSV


class ChatBot(DailyQuest):

    def connectDevice(self):
        if not cli_setup():
            auto_setup(__file__, logdir=True, devices=[
                "Android://127.0.0.1:5037/127.0.0.1:62034?cap_method=JAVACAP&&ori_method=ADBORI",
            ])

    def __init__(self):
        self.screenFind = ScreenFindItemForHSV();


    def inputText(self, msg):
        delayTap(49, 525, 1)
        text("{}".format(msg))
        delayTap(502, 904, 1, "send")

    def scrollLeft(self, y):
        swipeTo(463, 466, 210, 466, 1)

    def findItem(self, screen, type):
        infoList = []
        infoList.append(self.screenFind.orbInfoFormat("查看詳情", [100, 0, 226], [255, 255, 255], 140, 20))
        # infoList.append(self.screenFind.orbInfoFormat("rorb", [0, 90, 150], [22, 255, 255], 40, 40))
        boxs, labels = self.screenFind.find(screen, infoList)
        print(boxs)
        return boxs


    def startQuest(self):
        self.connectDevice()
        # self.inputText(1)
        # delayTap(45, 880, 4)
        # delayTap(45, 880, 4)
        # delayTap(45, 880, 4)
        # delayTap(45, 880, 4)
        # self.scrollLeft(400)
        screen = device().snapshot(filename=None)
        checkBoxs = self.findItem(screen, 12)
        if len(checkBoxs):
            point = checkBoxs[0].getCenterPoint()
            x = point[0]
            y = point[1]
            delayTap(x, y, 1, "查看詳情")
            printSleep(5)
            if exists_ex(Template(r"tpl1590118886532.png", record_pos=(-0.194, -0.091), resolution=(540, 960))):
            for i in range(4):
               keyevent("BACK")
#         Template(r"tpl1590033402034.png", record_pos=(-0.28, 0.387), resolution=(540, 960))


if __name__ == '__main__':
    chatBot = ChatBot()
    chatBot.startQuest()