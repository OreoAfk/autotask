import re
import os

# with open("somefile.txt") as file:
#    data = file.read()
#    result = re.findall(r'\b[A-Z0-9]{7}\b', data)
#    print(result)
#

text = """
package com.centanet.centaline.api.response.postdetail


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Addr(
    @SerializedName("Line1Big")
    val line1Big: String?,
    @SerializedName("Line2Est")
    val line2Est: String?,
    @SerializedName("Line3Floor")
    val line3Floor: String?,
    @SerializedName("Line4Addr")
    val line4Addr: String?
): Parcelable
"""
def strLotter(str):
    value = str.group(0)
    # print(value)
    return value[:2] + value[2].lower() + value[3:]

findList = re.findall(r'[(](.*)[)]', text)
# print(findList)
newText = re.sub(r'[(](.*)[)]', strLotter, text)

entries = os.listdir(r"D:\py\autotask\centalineChatBot\postdetail")

for name in entries:
    with open(os.path.join("D:/py/autotask/centalineChatBot/postdetail", name), "r") as f:
        code = f.read()
    code = re.sub(r'[(](.*)[)]', strLotter, code)
    with open(os.path.join("D:/py/autotask/centalineChatBot/postdetail", name), "w") as f:
        f.write(code)
    print(name)

# print(entries)