# -*- encoding=utf8 -*-
__author__ = "hugho"

import sys
from airtest.core.api import *
from airtest.cli.parser import cli_setup
from airtest.core.android.minitouch import *
from airtest.core.android.rotation import XYTransformer
import random
sys.path.append("../..")
from tool.tools import *

# class AzureFindTarget:
#

#
#     def isShipOfIcon(self, img,des=""):
#         pos = screenMatch(screen, img, des)
#         if pos:
#             delayTap(pos[0] + 50, pos[1] + 50, 1)
#             global isNotSelectAny
#             isNotSelectAny = False
#             if screenMatch(screen, Template(r"tpl1586403042646.png", record_pos=(-0.076, 0.126), resolution=(1280, 720)), "船塢已滿"):
#                 delayTap(792, 515, 2, "強化")
#                 notify("船塢已滿")
#                 input()
#
#     def randomSwipe(self):
#         x1 = random.randint(0, 1280)
#         x2 = random.randint(0, 1280)
#         y1 = random.randint(0, 600)
#         y2 = random.randint(0, 600)
#         swipeTo(x1, y1, x2, y2)
    

class AzureLane:

    def __init__(self, checkPoint):
        self.initCheckpointMap()
        self.currentCheckPoint = None
        if checkPoint in self.checkPointMap:
            self.currentCheckPoint = self.checkPointMap[checkPoint]
        self.isNotSelectAny = False
        self.isLoop = True
        self.pos = None
        self.isChangeTeam = False
        self.isFighting = False
        # isOnBattlefield
        self.isOnGrid = False
        self.isActivity = False
        self.isFindUntilBoss = False
        self.resetStatus()
        # start_time = time.time()

    def resetStatus(self):
        self.isFindBoss = False
        self.roundCount = 0
        self.isNotFindShipRound = 0
        self.isNotSelectAny = True

    def connectDevice(self):
        if not cli_setup():
            auto_setup(__file__, logdir=True, devices=[
                "Android://127.0.0.1:5037/127.0.0.1:62001?cap_method=JAVACAP&&ori_method=ADBORI",
            ])

    def runTask(self):
        screen = G.DEVICE.snapshot(filename=None)

        if self.isActivity:
            if screenMatch(screen, Template(r"tpl1572835593498.png", record_pos=(-0.434, 0.241), resolution=(1280, 720)), "普通"):
                delayTap(783, 280, 1, "D3")
                self.isOnGrid = False
                delayTap(927, 510, 1)
                delayTap(1072, 606, 1)
                printSleep(5)
                swipeTo(600, 300, 600, 600)
                self.isFighting = False
                self.roundCount = 0
                self.isFindBoss = False
                delayTap(927, 510, 1)
                delayTap(1072, 606, 1)
                return True
        else:
            if screenMatch(screen, Template(r"tpl1570444838341.png", record_pos=(-0.43, 0.244), resolution=(1280, 720)), "困難"):
                delayTap(self.currentCheckPoint["x"], self.currentCheckPoint["y"], 1)

        if (screenMatch(screen, Template(r"tpl1586405088306.png", record_pos=(0.001, -0.013), resolution=(1280, 720)),
                        "應援")):
            delayTap(465, 531, 1, "知道了")
            return True

        if screenMatch(screen, Template(r"tpl1576828673653.png", record_pos=(-0.455, -0.243), resolution=(1280, 720)),
                       "戰鬥中"):
            self.isFighting = True
            return True
        else:
            self.isFighting = False

        if tapScreenIsMatch(screenMatch(screen, Template(r"tpl1570170750240.png", record_pos=(0.121, 0.12), resolution=(1280, 720)),
                            "打完確定")):  # 打完確定
            delayTap(1109, 613, 6)
            self.isFindBoss = False
            self.isNotSelectAny = True
            self.isFighting = False
            return True

        if screenMatch(screen, Template(r"tpl1583165954821.png", record_pos=(0.432, 0.24), resolution=(1280, 720)),
                       "海戰場"):
            self.isOnGrid = True
            pos = screenMatch(screen, Template(r"tpl1571633990132.png", record_pos=(0.002, -0.016), resolution=(1280, 720)),
                              "出BOSS")
            if pos:
                if self.isChangeTeam is False:
                    self.isFindBoss = True
                    delayTap(1022, 673, 2, "切換")  # click change team
                    self.isChangeTeam = True
                    printSleep(3)
                    if self.isFindUntilBoss:
                        print("出BOSS")
                        return False
                    else:
                        #                     swipeTo(500, 400, 700, 100) # go to fing boss 7-2
                        #                     swipeTo(1000, 400, 100, 550)
                        swipeTo(700, 100, 500, 400)  # d3
                else:
                    tapScreenIsMatch(pos)
                    delayTap(1109, 613, 5)
                    delayTap(754, 482, 1)
                    self.isChangeTeam = False

            if self.isFindBoss is False:
                isNotSelectAny = True
                if isNotSelectAny and self.isShip(Template(r"tpl1576727636525.png", record_pos=(0.001, 0.052), resolution=(1280, 720)), "道具"):
                    isNotSelectAny = False

                #             isShipOfIcon(Template(r"tpl1571020399470.png", record_pos=(-0.096, -0.146), resolution=(1280, 720)))
                #             isShipOfIcon(Template(r"tpl1571021052646.png", record_pos=(-0.286, -0.146), resolution=(1280, 720)))
                #             isShipOfIcon(Template(r"tpl1570984282413.png", record_pos=(0.144, -0.132), resolution=(1280, 720)))
                if isNotSelectAny and self.isShip(Template(r"tpl1571021079071.png", record_pos=(-0.252, -0.109), resolution=(1280, 720)), "船1"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1571038311877.png", record_pos=(-0.285, 0.11), resolution=(1280, 720)), "船2"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1571041444505.png", record_pos=(-0.192, 0.188), resolution=(1280, 720)), "船3"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1571120404824.png", record_pos=(-0.066, 0.034), resolution=(1280, 720)), "船4"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1576841922044.png", record_pos=(0.034, 0.13), resolution=(1280, 720)), "船5"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1583204852305.png", record_pos=(-0.076, 0.067), resolution=(1280, 720)), "船6"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1583206499557.png", record_pos=(0.004, -0.014), resolution=(1280, 720)), "船7"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1586406145945.png", record_pos=(0.248, 0.119), resolution=(1280, 720)), "船8"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1585570710597.png", record_pos=(-0.263, 0.053), resolution=(1280, 720)), "船9"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1585817999161.png", record_pos=(-0.276, 0.054), resolution=(1280, 720)), "船10"):
                    isNotSelectAny = False
                if isNotSelectAny and self.isShip(Template(r"tpl1586406113047.png", record_pos=(0.219, -0.084), resolution=(1280, 720)), "船11"):
                    isNotSelectAny = False

            #         isShip(Template(r"tpl1571624190060.png", record_pos=(0.035, -0.018), resolution=(1280, 720)))

            printSleep(5)

            if screenMatch(screen, Template(r"tpl1586022815531.png", record_pos=(0.147, 0.248),
                                            resolution=(1280, 720))) and isNotSelectAny is False:
                self.isNotFindShipRound = self.isNotFindShipRound + 1
                print("沒找到船: {}".format(self.isNotFindShipRound))

            elif isNotSelectAny is False:
                self.isNotFindShipRound = 0
                print("isNotSelectAny: {}" .format(isNotSelectAny))

            return True

        # if isFindBoss == True:
        #     print("找到BOSS並swipe")
        #     randomSwipe()
        # elif screenMatch(screen, Template(r"tpl1586022815531.png", record_pos=(0.147, 0.248),
        #                                   resolution=(1280, 720))) and self.isNotFindShipRound > 3:
        #     self.isNotFindShipRound = 0
        #     randomSwipe()

        if screenMatch(screen, Template(r"tpl1575353609704.png", record_pos=(-0.451, 0.029), resolution=(1280, 720)),
                       "出船"):
            delayTap(616, 201, 1)
            delayTap(616, 201, 2)
            delayTap(616, 201, 2)
            return True

        if tapScreenIsMatch(screenMatch(screen, Template(r"tpl1570160604066.png", record_pos=(0.393, 0.217),
                                                         resolution=(1280, 720)), "出擊")):
            delayTap(754, 482, 1)
            self.roundCount = self.roundCount + 1
            return True

        if tapScreenIsMatch(screenMatch(screen, Template(r"tpl1570160219210.png", record_pos=(0.329, 0.087),
                                                         resolution=(1280, 720)), "規避")):
            printSleep(5)
            return True

        if tapScreenIsMatch(screenMatch(screen, Template(r"tpl1576834557166.png", record_pos=(-0.003, 0.093), resolution=(1280, 720)), "點撃繼續")):
            return True

        if tapScreenIsMatch(screenMatch(screen, Template(r"tpl1570159835011.png", record_pos=(-0.351, 0.208),
                                                         resolution=(1280, 720)), "打完")):
            delayTap(1000, 618, 1)
            printSleep(5)

            screen = G.DEVICE.snapshot(filename=None)
            if screenMatch(screen,
                           Template(r"tpl1575353609704.png", record_pos=(-0.451, 0.029), resolution=(1280, 720)), "出船"):
                delayTap(616, 201, 1)
                delayTap(616, 201, 2)
                delayTap(616, 201, 2)
            else:
                delayTap(1064, 660, 1)

            return True

        # if isFindBoss == False:
        #     if isNotSelectAny == False and isOnGrid == True:
        #         print("沒找到船")
        #     else:
        #         printSleep(3)
        #
        #     if isFighting and isNorFindShipRound is not 0:
        #         printSleep(10)
        #     else:
        #         printSleep(3)


    def isShip(self, screen, img, des=""):
        pos = screenMatch(screen, img, des, "找船")
        if pos:
            if pos[1] > 60:
                delayTap(pos[0], pos[1], 1)
                return True

            if screenMatch(screen, Template(r"tpl1586403042646.png", record_pos=(-0.076, 0.126), resolution=(1280, 720)), "船塢已滿"):
                delayTap(792, 515, 2, "強化")
                notify("船塢已滿")
                input()
        else:
            return False

    def initCheckpointMap(self):
        checkPointMap = {
            "1-4": {"x": 785, "y": 221},
            
            "2-3": {"x": 290, "y": 307},
            
            "3-3": {"x": 765, "y": 169},
            "3-4": {"x": 614, "y": 387},
            "4-3": {"x": 803, "y": 572},
            "4-4": {"x": 755, "y": 309},
            "5-1": {"x": 226, "y": 399},
            "5-2": {"x": 829, "y": 562},
            "5-4": {"x": 572, "y": 272},
            "6-1": {"x": 888, "y": 540},
            "6-4": {"x": 268, "y": 466},
            "7-2": {"x": 448, "y": 215, "bossScroll": [500, 400, 700, 100]},

        }
        self.checkPointMap = checkPointMap
        return checkPointMap


    def startLoopTask(self):
        self.connectDevice()
        while self.runTask():
            printSleep(5)

if __name__ == '__main__':
    azure = AzureLane()
    try:
        inputMode = input("Boss自動停=0, 無限刷=1: ")
        if inputMode == "":
            azure.isFindUntilBoss = True
        else:
            azure.isFindUntilBoss = False
    except ValueError:
        sys.exit("輸入錯誤")

