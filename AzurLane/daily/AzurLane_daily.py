# -*- encoding=utf8 -*-
__author__ = "hugho"

from airtest.core.api import *
from airtest.cli.parser import cli_setup
import sys
import datetime
sys.path.append("D:\\AirtestIDE_2019-04-16_py3_win64\\AirtestIDE_2019-05-09_py3_win64\\processPy\\oreotools")
# from excel import StoreHelper
from tools import *

class AzureLaneDaily:
    
    def __init__(self):
        pass
    
    def jobStart(self):
        self.connectDevice()
        self.leftPanelQuest()
        self.dailyBatter()
    
    def leftPanelQuest(self):
        if exists_ex(Template(r"tpl1587132496694.png", record_pos=(0.17, 0.029), resolution=(1280, 720))):
            delayTap(19, 163, 1, "開啟左側")
            delayTap(172, 108, 3, "拿石油")
            delayTap(407, 88, 1, "拿物資")
            delayTap(420, 280, 1, "軍事委托")
            for i in range(12):
                delayTap(626, 64, 1)
            if exists_ex(Template(r"tpl1587132496694.png", record_pos=(0.17, 0.029), resolution=(1280, 720))):
                delayTap(19, 163, 1, "開啟左側")
                delayTap(420, 280, 1, "軍事委托")
                
            delayTap(434,279, 2, "前往")
            delayTap(653, 272, 3, "任務1")
            delayTap(896, 360, 1, "推薦")
            delayTap(1060, 360, 1, "開始")
            delayTap(626, 64, 1)
            
            delayTap(653, 311, 3, "任務2")
            delayTap(927, 338, 1, "推薦")
            delayTap(1060, 340, 1, "開始")
            delayTap(626, 64, 1)
            
            delayTap(653, 466, 3, "任務3")
            delayTap(896, 351, 1, "推薦")
            delayTap(1060, 351, 1, "開始")
            delayTap(626, 64, 1)
            
            delayTap(653, 617, 3, "任務4")
            delayTap(896, 351, 1, "推薦")
            delayTap(1060, 351, 1, "開始")
            delayTap(626, 64, 1)
            
            delayTap(59, 44, 3, "返回")
            
            delayTap(469, 418, 3, "戰術學院")
            
            delayTap(797, 519, 1, "確定")
            delayTap(434, 351, 1, "藍書")
            delayTap(1074, 600, 1, "開始課程")
            delayTap(786, 508, 1, "確定")
            
            delayTap(797, 519, 1, "確定")
            delayTap(267, 325, 1, "紅書")
            delayTap(1074, 600, 1, "開始課程")
            delayTap(786, 508, 1, "確定")
            
            delayTap(797, 519, 1, "確定")
            delayTap(573, 325, 1, "黃書")
            delayTap(1074, 600, 1, "開始課程")
            delayTap(786, 508, 1, "確定")
            
            delayTap(59, 44, 3, "返回")
            
            delayTap(447, 564, 3, "軍事研究部")
            delayTap(613, 541, 3)
            delayTap(447, 564, 3, "軍事研究部")
            delayTap(620, 320, 2, "中間")
            delayTap(491, 573, 3, "開始研發")
            delayTap(775, 513, 1, "確定")
            
            delayTap(1229, 35, 2, "Home")
            printSleep(2)
            
    def dailyBatter(self):
         if exists_ex(Template(r"tpl1587132496694.png", record_pos=(0.17, 0.029), resolution=(1280, 720))):
                delayTap(1089, 356, 1, "出擊")

                for i in range(3):
                    delayTap(279, 462, 3, "6-4")
                    delayTap(947, 504, 2, "立即前往")
                    delayTap(1076, 601, 2, "立即前往")
                    printSleep(10)
                    pos = exists_ex(Template(r"tpl1571633990132.png", record_pos=(0.002, -0.016), resolution=(1280, 720)))
                    if pos:
                        tapScreenIsMatch(pos)
                        delayTap(800, 449, 30, "不再提示")
                        delayTap(642, 532, 2, "知道了")

                    if  findUntil(Template(r"tpl1570159835011.png", record_pos=(-0.351, 0.208), resolution=(1280, 720)), 5):
                        delayTap(1000, 618, 1)
                        delayTap(565, 580, 2)
                        delayTap(1062, 654, 5, "確定")

                        delayTap(640, 529, 10, "訊息 確定")
                
                delayTap(779, 663, 5, "每日任務")
                delayTap(654, 369, 5, "中間")
                for i in range(3):
                    delayTap(750, 196, 3, "Lv 70")
                    delayTap(1134, 630, 3, "出擊")
                    if  findUntil(Template(r"tpl1570159835011.png", record_pos=(-0.351, 0.208), resolution=(1280, 720)), 5):
                        delayTap(1000, 618, 1)
                        delayTap(565, 580, 2)
                        delayTap(1062, 654, 5, "確定")
                        
                delayTap(50, 62, 3, "返回")
                
#                 if 星期日



            
    def connectDevice(self):
        if not cli_setup():
            auto_setup(__file__, logdir=True, devices=[
                "Android://127.0.0.1:5037/127.0.0.1:62001?cap_method=JAVACAP&&ori_method=ADBORI",
            ])


if __name__ == '__main__':
    daily = AzureLaneDaily()
    daily.jobStart()

# def record_to_excel(sheet,list,**kwargs):
#     rng = sheet.range('A1').expand()
#     nrow = rng.last_cell.row
#     currentcell="A"+str(nrow+1)
#     #记录截图的链接和实际地址
#     currentcellpng="J"+str(nrow+1)
#     currentcellpngvalue="K"+str(nrow+1)
#     currentcellrange=currentcell+":"+"H"+str(nrow+1)
#     sheet.range(currentcell).value =list
#     #分行换色
#     if nrow % 2 == 0:
#         sheet.range(currentcellrange).color = 173, 216, 230
#     else:
#         sheet.range(currentcellrange).color = 221, 245, 250
#     #单独处理颜色参数以及截图参数
#     for  key, value  in kwargs.items():
#         if key=="color":
#             sheet.range(currentcellrange).color=value
#         if key == "png":
#             sheet.range(currentcellpng).add_hyperlink(value,"截图","提示：点击打开截图")
#             sheet.range(currentcellpngvalue).value=value
#     sheet.autofit()
