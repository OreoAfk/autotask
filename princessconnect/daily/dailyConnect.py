# -*- encoding=utf8 -*-
__author__ = "hugho"

from airtest.core.api import *
from airtest.cli.parser import cli_setup
import sys
import datetime
sys.path.append("../..")
from tool.tools import *
from base.dailyQuest import DailyQuest

class PrincessDaily(DailyQuest):
    def __init__(self):
        pass

    def isSpecialStoreOpen(self):
        print("open")

    def openApp(self):
        stop_app("tw.sonet.princessconnect")
        sleep(1)
        start_app("tw.sonet.princessconnect")

    def play(self):
        if self.exists_ex(Template(r"tpl1575347619216.png", record_pos=(0.119, 0.105), resolution=(1280, 720)), 100, 2):
            pass

    def explore(self):
        self.delayTap(634, 678, 2, "冒險")
        self.delayTap(970, 205, 3, "探索")
        self.delayTap(783, 329, 3, "經驗值冒險")
        self.delayTap(949, 207, 2, "第1關")
        self.qurchBatter(2)
        self.delayTap(1100, 329, 3, "瑪那冒險")
        self.delayTap(949, 207, 2, "第1關")
        self.qurchBatter(2)
        self.delayTap(880, 594, 2, "取消")

    def dungeons(self):
        self.delayTap(634, 678, 2, "冒險")
        self.delayTap(1166, 205, 3, "地下城")
        self.delayTap(1097, 317, 3, "毒瘴闍稜")
        self.delayTap(773, 492, 2, "OK")
        self.delayTap(638, 360, 10, "箱1")
        self.delayTap(1100, 616, 5, "進行挑戰")
        self.delayTap(1141, 120, 5, "我的隊伍")
        self.delayTap(1049, 244, 1, "呼叫這個隊伍")
        self.delayTap(1087, 613, 1, "戰鬥開始")
        if self.findUntil(Template(r"tpl1578498278711.png", record_pos=(0.363, 0.242), resolution=(1280, 720)), 5):
            self.delayTap(1091, 669)
            self.delayTap(631, 618, 10) # OK
            
        self.delayTap(877, 405, 5, "箱2")
        self.delayTap(1100, 616, 1, "進行挑戰")
        self.delayTap(1087, 613, 5, "戰鬥開始")
        if self.findUntil(Template(r"tpl1578498278711.png", record_pos=(0.363, 0.242), resolution=(1280, 720)), 5):
            self.delayTap(1091, 669)
            self.delayTap(631, 618, 10, "OK")
            
        self.delayTap(640, 410, 5, "箱3")
        self.delayTap(1100, 616, 1, "進行挑戰")
        self.delayTap(1087, 613, 5, "戰鬥開始")
        if self.findUntil(Template(r"tpl1578498278711.png", record_pos=(0.363, 0.242), resolution=(1280, 720)), 5):
            self.delayTap(1091, 669)
            self.delayTap(631, 618, 10, "OK")

        self.delayTap(548, 346, 5, "箱4")
        self.delayTap(1100, 616, 1, "進行挑戰")
        self.delayTap(1087, 613, 5, "戰鬥開始")
        if self.findUntil(Template(r"tpl1578498278711.png", record_pos=(0.363, 0.242), resolution=(1280, 720)), 5):
            self.delayTap(1091, 669)
            self.delayTap(631, 618, 10, "OK")
            
        self.delayTap(716, 380, 5, "BOSS")
        self.delayTap(1100, 616, 1, "進行挑戰")
        self.delayTap(1141, 120, 5, "我的隊伍")
        self.delayTap(1049, 390, 1, "呼叫這個隊伍")
        self.delayTap(1087, 613, 1, "戰鬥開始")
        
#         if findUntil(Template(r"tpl1580349465262.png", record_pos=(0.339, 0.23), resolution=(1280, 720)), 5):
#             self.delayTap(1091, 669, "前往地下城")
#             printSleep(8)
#             self.delayTap(716, 380, 5, "BOSS")
#             self.delayTap(1100, 616, 1, "進行挑戰")
#             self.delayTap(1087, 613, 1, "戰鬥開始")
        while (True):
            self.printSleep(5)
            screen = G.DEVICE.snapshot(filename=None)
            if self.screenMatch(screen,Template(r"tpl1578498278711.png", record_pos=(0.363, 0.242), resolution=(1280, 720))):
                self.delayTap(1091, 669, 1, "下一步")
                self.delayTap(631, 618, 10, "OK")
                break
            if self.screenMatch(screen, Template(r"tpl1580349465262.png", record_pos=(0.339, 0.23), resolution=(1280, 720))):
                self.delayTap(1091, 669, 1, "前往地下城")
                self.delayTap(716, 380, 5, "BOSS")
                self.delayTap(1100, 616, 5, "進行挑戰")
                self.delayTap(1087, 613, 2, "戰鬥開始")

            
        # self.delayTap(1146, 561, 5, "返回")
        # self.delayTap(773, 497, 2,"OK")
        self.printSleep(6)
        
    def qurchBatter(self, useCount):
        self.delayTap(1158, 445, 2, "+")
        for i in range(0, useCount-1):
            self.delayTap(1158, 445, 0.2, "+")
        self.delayTap(999, 429, 1, "使用 X 張")
        self.delayTap(787, 499, 1, "OK")
        self.delayTap(640, 638, 1.5)
        self.delayTap(640, 638, 1.3, "返回 XX Top")
#         self.delayTap(640, 638, 1)
        self.printSleep(3)
        if self.exists_ex(Template(r"tpl1578538758676.png", record_pos=(0.113, 0.105), resolution=(1280, 720))):
            self.delayTap(756, 492, 1)
            self.printSleep(5)
            self.buyTempShop()

#         else:
#             self.delayTap(999, 429, 3, "OK")

        #戰隊取消
        if self.exists_ex(Template(r"tpl1579945995074.png", record_pos=(0.116, 0.17), resolution=(1280, 720))):
            self.delayTap(490, 581, 1, "取消")
            self.delayTap(912, 607, 3, "取消")
    

    def survey(self):
        self.delayTap(634, 678, 2, "冒險")
        self.delayTap(972, 375, 3, "調查")
        self.delayTap(789, 317, 3, "聖跡調查")
        self.delayTap(900, 192, 2, "聖跡調查冒險2")
        self.qurchBatter(5)
        self.delayTap(880, 594, 2, "取消")
        self.delayTap(928, 331, 2, "聖跡調查冒險1")
        self.qurchBatter(5)
        self.delayTap(880, 594, 2, "取消")
        self.delayTap(34, 50, 3, "返回")
        self.delayTap(1066, 317, 3, "神殿調查")
        self.delayTap(900, 192, 2, "神殿調查冒險2")
        self.qurchBatter(5)
        self.delayTap(880, 594, 2, "取消")
        self.delayTap(928, 331, 2, "神殿調查冒險1")
        self.qurchBatter(5)
        self.delayTap(880, 594, 2, "取消")
        
        
    def pvp(self):
        self.delayTap(634, 678, 2, "冒險")
        self.delayTap(790, 552, 3, "戰鬥競技場")
        self.printSleep(5)
        if self.exists_ex(Template(r"tpl1578497909751.png", record_pos=(0.109, 0.104), resolution=(1280, 720))):
            self.delayTap(469, 486, 1, "取消")
        
        self.delayTap(377, 469, 2, "領取")
        self.delayTap(634, 481, 2, "OK")
        
        self.delayTap(830, 233, 2, "第一隊對手")
        self.delayTap(1107, 587, 3, "戰鬥開始")
        if self.findUntil(Template(r"tpl1578498278711.png", record_pos=(0.363, 0.242), resolution=(1280, 720)), 5):
            self.delayTap(1091, 669)
            self.printSleep(10)
        
        self.delayTap(634, 678, 2, "冒險")
        self.delayTap(1089, 552, 3,"公主競技場")
        self.printSleep(5)
        if self.exists_ex(Template(r"tpl1578497909751.png", record_pos=(0.109, 0.104), resolution=(1280, 720))):
            self.delayTap(108, 282, 3, "取消")
        
        self.delayTap(377, 469, 2, "領取")
        self.delayTap(634, 481, 2, "OK")
        
        self.delayTap(830, 233, 2, "第一隊對手")
        self.delayTap(1107, 587, 3, "隊伍2")
        self.delayTap(1107, 587, 3, "隊伍3")
        self.delayTap(1107, 587, 3, "戰鬥開始")
        if self.findUntil(Template(r"tpl1578498278711.png", record_pos=(0.363, 0.242), resolution=(1280, 720)), 5):
            self.delayTap(1091, 669)
            self.printSleep(10)

    def freeLottery(self):
        self.delayTap(979, 678, 2, "轉蛋")
        self.delayTap(1186, 93, 5, "普通")
        self.delayTap(1000, 472, 3, "10連免費")
        self.delayTap(788, 504, 2, "OK")
        self.delayTap(629, 579, 6, "OK")
        
    def buyTempShop(self):
        self.delayTap(506, 188, 0.2)
        self.delayTap(736, 188, 0.2)
        self.delayTap(960, 188, 0.2)
        self.delayTap(1196, 188, 0.2)
        self.swipeTo(760, 477, 760, 292, 1)
        self.delayTap(506, 364, 0.2)
        self.delayTap(736, 364, 0.2)
        self.delayTap(960, 364, 0.2)
        self.delayTap(1196, 364, 0.2)
        self.delayTap(1037, 586, 0.5, "一次購買")
        self.delayTap(785, 624, 1, "OK")
        
        self.delayTap(641, 636, 2, "OK")
        self.delayTap(686, 584, 1, "立即關閉")
        self.delayTap(790, 484, 2, "OK")
        self.delayTap(484, 484, 2, "取消")
        self.delayTap(40, 38, 2, "返回")
        self.printSleep(5)
        
    def buyStone(self):
        if self.exists_ex(Template(r"tpl1578491878142.png", record_pos=(-0.284, 0.257), resolution=(1280, 720))):
            self.delayTap(453, 372, 2, "中心")
            self.delayTap(812, 565, 1, "商店")

            self.printSleep(5)
            self.delayTap(522, 199, 0.2)
            self.delayTap(736, 199, 0.2)
            self.delayTap(960, 199, 0.2)
            self.delayTap(1196, 199, 0.2)
            self.swipeTo(760, 477, 760, 292, 1)
            self.delayTap(506, 364, 0.2)
            self.delayTap(736, 364, 0.2)
            self.delayTap(960, 364, 0.2)
            self.delayTap(1196, 364, 0.2)
            self.delayTap(1037, 586, 0.5, "一次購買")
            self.delayTap(785, 624, 1, "OK")

            self.delayTap(641, 636, 2, "OK")
            self.delayTap(40, 38, 2, "返回")
            self.printSleep(3)

    
    def getHp(self):
        if self.exists_ex(Template(r"tpl1578491878142.png", record_pos=(-0.284, 0.257), resolution=(1280, 720))):
            self.delayTap(453, 372)
            self.delayTap(793, 688, 1, "公會小屋")
            self.delayTap(793, 688, 1, "公會小屋")
            self.delayTap(1193, 554, 8, "全部領取")
            self.delayTap(638, 636, 3, "關閉")
    
    def login(self):
        if self.findUntil(Template(r"tpl1578477097798.png", record_pos=(-0.194, -0.242), resolution=(1280, 720)), 10):
            self.delayTap(611, 375,2)
        
        if self.findUntil(Template(r"tpl1578477685054.png", record_pos=(0.449, -0.248), resolution=(1280, 720)), 10, 5):
            self.delayTap(1173, 45)
            
        if self.findUntil(Template(r"tpl1578622053582.png", record_pos=(0.003, 0.217), resolution=(1280, 720)), 10, 20):
            self.delayTap(634, 634)
            self.delayTap(634, 400, 3)
            self.printSleep(3)
            
    def veryHard(self):
        self.delayTap(634, 678, 2, "冒險")
    
    def connectDevice(self):
        if not cli_setup():
            auto_setup(__file__, logdir=True, devices=[
                "Android://127.0.0.1:5037/127.0.0.1:62001?cap_method=JAVACAP&&ori_method=ADBORI",
            ])
    
    def activityHard(self):
        
        # self.delayTap(414, 572, 5, "活動")
        self.delayTap(542, 572, 5, "活動")
        
        # printSleep(5)
        # if exists_ex(Template(r"tpl1587006870364.png", record_pos=(0.455, -0.241), resolution=(1280, 720))):
        #     if findUntil(Template(r"tpl1578622053582.png", record_pos=(0.003, 0.217), resolution=(1280, 720)), 10, 20):
        #         self.delayTap(580, 307, 1)
        #         printSleep(5)
        #
        #
        # self.delayTap(630, 611, 3, "關閉")
        #
        # self.delayTap(607, 252, 1, "活動冒險")
        
        self.delayTap(903, 354, 7, "1-5")
        self.qurchBatter(3)
        self.delayTap(419, 585, 2, "取消")
        self.delayTap(844, 606, 3, "取消")
        
        self.delayTap(704, 455, 2, "1-4")
        self.qurchBatter(3)
        self.delayTap(419, 585, 2, "取消")
        self.delayTap(844, 606, 3, "取消")
        
        self.delayTap(475, 535, 2, "1-3")
        self.qurchBatter(3)
        self.delayTap(419, 585, 2, "取消")
        self.delayTap(844, 606, 3, "取消")
        
        self.delayTap(344, 332, 2, "1-2")
        self.qurchBatter(3)
        self.delayTap(419, 585, 2, "取消")
        self.delayTap(844, 606, 3, "取消")
        
        self.delayTap(171, 469, 2, "1-1")
        self.qurchBatter(3)
        self.delayTap(419, 585, 2, "取消")
        self.delayTap(844, 606, 3, "取消")
        
        self.delayTap(1190, 409, 3, "Very Hard")
        self.delayTap(1115, 624, 2, "進行挑戰")
        self.delayTap(1074, 609, 2, "戰鬥開始")

        if self.findUntil(Template(r"tpl1578498278711.png", record_pos=(0.363, 0.242), resolution=(1280, 720)), 5):
            self.delayTap(1091, 669, 5, "下一步")
            self.delayTap(1139, 635, 10, "下一步")
            
        self.printSleep(10)

        self.delayTap(635, 302, 5, "Hard")
        for i in range(0, 10):
            self.delayTap(1164, 504, 0.2, "+")
        self.delayTap(999, 488, 1, "使用 X 張")
        self.delayTap(787, 499, 1, "OK")
        self.delayTap(640, 638, 1.5)
        self.delayTap(640, 638, 1.3, "返回 XX Top")
#         self.delayTap(640, 638, 1)
        self.printSleep(3)
        
        self.delayTap(419, 585, 2, "取消")
        self.delayTap(844, 606, 3, "取消")
        
        self.delayTap(1230, 575, 3, "活動任務")
        self.delayTap(1002, 567, 7, "全部領取")
        self.delayTap(641, 651, 2, "關閉")
        
    
    def loginChecking(self):
        if self.exists_ex(Template(r"tpl1585187139450.png", record_pos=(-0.291, -0.055), resolution=(1280, 720))):
            self.delayTap(1208, 37, 2, ">>")
            
        self.printSleep(15)
        self.delayTap(650, 631, 2, "關閉")
        self.printSleep(5)
            
    def nightJob(self):
        self.getHp()
        self.freeLottery()
        self.pvp()
        self.delayTap(151, 669, 1, "主頁")
        self.delayTap(1107, 580, 6, "任務")
        self.delayTap(1071, 575, 10, "全部領取")
        self.delayTap(630, 611, 6, "關閉")
        self.delayTap(634, 678, 2, "冒險")

        # self.veryHard()


        self.delayTap(634, 678, 2, "冒險")
    
    def morningJob(self):
        # self.buyStone()
        self.pvp()
        self.getHp()
        self.explore()
        self.survey()
        self.freeLottery()
        # self.dungeons()  # 地下城
        self.pvp()


    def startQuest(self):
        print("start")
        now = datetime.datetime.now()
        print(now.hour)
        self.connectDevice()
        hour = datetime.datetime.now().hour
        print('活動中? 活動中 = 1')
        self.isActivity = input()
        hour = 10

        if 8 < hour < 18:
            self.morningJob()
            self.delayTap(634, 678, 2, "冒險")
            if self.isActivity is not None and self.isActivity is "1":
                # self.delayTap(414, 572, 5, "活動")
                self.delayTap(542, 572, 5, "活動")
        else:
            self.nightJob()
            self.printSleep(5)
            if self.isActivity is not None and self.isActivity is "1":
                self.activityHard()


if __name__ == '__main__':
    dailyQuest = PrincessDaily()
    dailyQuest.startQuest()

