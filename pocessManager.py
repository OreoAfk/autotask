import sys

from bm3.daily.dailyBm3 import Bm3Daily
from princessconnect.daily.dailyConnect import PrincessDaily
from fgo.attack.fgoattack import Fgo as FgoAttack
from AzurLane.findTarget.findTarget7_2 import AzureLane
from tool.tools import *
# from tool.programWindowCap import *

# def screenShow():
#     hwnd = FindWindow_bySearch("弓箭,崩3,pad,艦B")
#     while True:
#         sleep(0.03)
#         frame = getWindow_Img(hwnd)
#         cv2.imshow("screen box", frame)
#         k = cv2.waitKey(30) & 0xFF  # 64bits! need a mask
#         if k == 27:
#             cv2.destroyAllWindows()
#             break

if __name__ == '__main__':
    print("遊戲: 1.{}, 2.{}, 3.{}, 4.{}".format("公連", "崩壞3", "fgo", "碧藍"))
    selectGame = input()
    if selectGame == "1":
        dailyQuest = PrincessDaily()
        dailyQuest.startQuest()
    elif selectGame == "2":
        dailyQuest = Bm3Daily()
        dailyQuest.startQuest()
    elif selectGame == "3":
        fgoAttack = FgoAttack()
        try:
            inputMode = input("不回體刷=0, 回體刷=1: ")
            if inputMode == "":
                fgoAttack.eatStone = False
            else:
                fgoAttack.eatStone = True
        except ValueError:
            sys.exit("輸入錯誤")

        fgoAttack.startLoopTask()
    elif selectGame == "4":
        azure = AzureLane()
        azure.isFindUntilBoss = False
        azure.startLoopTask("7-2")
    elif selectGame == "5":
        for i in range(5):
            print(randomText())

