import pyautogui
from time import sleep
from threading import Thread

def click_function():
    sleep(5)
    while True:
        x, y = pyautogui.position()
        pyautogui.click(x, y, button='left')
        sleep(0.5)  # an acceptable waiting time between one click and another

t = Thread(target=click_function)
t.daemon = True
t.start()
sleep(60)