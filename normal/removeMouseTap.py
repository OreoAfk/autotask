import sys
sys.path.append("../..")

import win32api
from tool.wintools import *
from tool import wintools
from mabi.promotion.sheetEdit import ExtManager
import numpy as np
import math
import os
import pyautogui
from pynput import keyboard


def on_press(key):
    if key == keyboard.Key.esc:
        return False  # stop listener
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys
    if k == "x":
        print('Key pressed: ' + k)
        ahk.click()
    if k in ['1', '2', 'left', 'right']:  # keys of interest
        # self.keys.append(k)  # store it in global-like variable
        print('Key pressed: ' + k)
        return False  # stop listener; remove this if want more keys


if __name__ == '__main__':
    ahk = AHK()

    listener = keyboard.Listener(on_press=on_press)
    listener.start()  # start to listen on a separate thread
    listener.join()  # remove if main thread is polling self.keys