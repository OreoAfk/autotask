import sys
sys.path.append("../..")
sys.path.append("..")
import win32api
from tool.wintools import *
from tool import wintools
from mabi.promotion.sheetEdit import ExtManager
import numpy as np
import math
import os
import pyautogui
import signal
from pynput import keyboard
from threading import Thread
from time import sleep

acc = []
pwd = []
bf_hwnd = None
mabi_hwnd = []
screenWidth = 1920
screenHeight = 1080

def on_press(key, abortKey='esc'):
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    print('pressed %s' % (k))
    if k == abortKey:
        print('end loop ...')
        return False  # stop listener


def loop_fun():
    tx = 0
    ty = 0
    try:
        while True:
            x, y = pyautogui.position()
            if x != tx and y != ty:
                tx = x
                ty = y
                positionStr = 'X: ' + str(x).rjust(4) + ' Y: ' + str(y).rjust(4)
                print(positionStr)

    except KeyboardInterrupt:
        print('\nDone')


if __name__ == '__main__':

    abortKey = 't'
    listener = keyboard.Listener(on_press=on_press, abortKey=abortKey)
    listener.start()  # start to listen on a separate thread
    Thread(target=loop_fun, args=(), name='loop_fun', daemon=True).start()
    listener.join()  # wait for abortKey